Installation
=====================

Conda:
---------------
- conda install --yes --file requirements.txt
- python setup.py install

Pip:
---------------
- pip install -r requirements.txt
- python setup.py install