.. ASTErIsM documentation master file, created by

.. image:: _static/logo.png

=============================================

ASTErIsM is a ptyhon-based framework for detection of astronomical sources, extraction of morphometric features, and machine learning.

:Author: `Andrea Tramacere <www.www.www>`_




User Documentation
******************
.. toctree::
   :maxdepth: 1

   overview <overview/overview.rst>
   installation <installation.rst>
   documentation <user_documentation/user_documentation.rst>
   tutorial <tutorial/tutorial.rst>


Developer Documentation
***********************



Indices and Tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`