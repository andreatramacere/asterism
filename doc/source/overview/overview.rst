
Overview of the Asetrims Project
================================
.. contents:: :local:



What is ASTErIsM
----------------

ASTErIsM is a ptyhon-based framework for detection of astronomical sources, and extraction of morphometric features.
The detection and features extraction are based mainly on density clustering algorithms (DBSCAN and DENCLUE).
ASTErIsM also offers tools for machine learning supervised classification, based on the scikit-learn framework.
The kernel computation in the DENCLUE algorithm has been written in Cython to speed up the computational time.
Both the DBSCAN and DENCLUE algorithms have been modified in order to work with digital images. Some image processing
tasks are performed using the ndimage package from the scipy, and the scikit-image framework.
ASTErIsM implements also the possibility to design flexible user-oriented pipelines, by the use
of its pipeline manager sub-package, that allows to combine together different task, with the possibility to configure
parameters through configuration files

overview of the subpackages
---------------------------




.. image:: asterism_overview_fig0.png
    :scale: 80%

Asterism provides five main subpackages:

* **core**: where the algorithms are implemented. This subpackage relies of well knows and robust library such as numpy scipy,
  open-CV, scikit-learn, scikit-image, astropy, and provides alghorithms regarding

    * clustering: clustring algorithms (dbscan/denclue/nn), and clusters characterization as detected sources
    * image processing (filters,geometric manipulation)
    * geometry: metrics and geometric manipulation
    * morphometry : shape measurements (invariants moments, statistical distributions of attractors)
    * photometry : basic photometric support
    * statistics: basis support for histograms and spatial 2D distribution (radial distributions etc...)


* **pipeline_manager**: this subpackage offers all the facilities to embed algorithms into tasks, to facilitate the developing
  of pipelines. The basic idea is that **task** provides a bridge between the **core** algorithms and the **pipeline manager**
  package. Tasks can be combined into processes and processes into pipelines (as shown in the blue-dashed box) and the
  **pipeline manager** will orchestrate all the data/work
  flow, together with the I/O and the configuration

* **analysis_tasks** subpackage contains implemented tasks

* **analysis_processes** subpackage contains implemented procesess

* **analysis_piepelines** subpackage contains implemented pipelines



