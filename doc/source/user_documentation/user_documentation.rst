
ASTErIsM User Documentation
===========================

.. toctree::
   :maxdepth: 1

   core modules <core_modules/index.rst>
   source detection pipeline <analysis_pipelines/src_detection/src_detection.rst>
   implemented processes <>
   implemented tasks <>
   pipeline manager <pipeline_manager/index.rst>



