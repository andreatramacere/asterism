.. automodule:: asterism.analysis_tasks.source_detection.image_segmentation.image_segmentation
    :members:
    :undoc-members:
    :show-inheritance:
