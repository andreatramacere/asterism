asterism.analysis\_tasks.source\_detection.image\_segmentation package
======================================================================

Submodules
----------

.. toctree::

   asterism.analysis_tasks.source_detection.image_segmentation.catalog
   asterism.analysis_tasks.source_detection.image_segmentation.image_segmentation

Module contents
---------------

.. automodule:: asterism.analysis_tasks.source_detection.image_segmentation
    :members:
    :undoc-members:
    :show-inheritance:
