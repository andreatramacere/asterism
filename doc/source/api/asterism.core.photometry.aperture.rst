


Module: aperture
---------------

.. automodule:: asterism.core.photometry.aperture
    :members:
    :undoc-members:
    :show-inheritance:
    
