asterism.analysis\_pipelines package
====================================

Submodules
----------

.. toctree::

   asterism.analysis_pipelines.morphometric_features_extraction_euclid
   asterism.analysis_pipelines.source_detection

Module contents
---------------

.. automodule:: asterism.analysis_pipelines
    :members:
    :undoc-members:
    :show-inheritance:
