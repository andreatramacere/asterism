.. automodule:: asterism.analysis_tasks.source_detection.deblending.deblending_scale
    :members:
    :undoc-members:
    :show-inheritance:
