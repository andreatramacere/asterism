.. automodule:: asterism.analysis_tasks.morphometry_features.morphometric_input_products
    :members:
    :undoc-members:
    :show-inheritance:
