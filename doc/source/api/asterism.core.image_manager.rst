asterism.core.image\_manager package
====================================

Submodules
----------

.. toctree::

   asterism.core.image_manager.image
   asterism.core.image_manager.utils

Module contents
---------------

.. automodule:: asterism.core.image_manager
    :members:
    :undoc-members:
    :show-inheritance:
