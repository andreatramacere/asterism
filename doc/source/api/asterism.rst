asterism package
================

Subpackages
-----------

.. toctree::

    asterism.analysis_pipelines
    asterism.analysis_processes
    asterism.analysis_tasks
    asterism.command_line
    asterism.core
    asterism.pipeline_manager
    asterism.plotting

Module contents
---------------

.. automodule:: asterism
    :members:
    :undoc-members:
    :show-inheritance:
