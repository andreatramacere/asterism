
package: asterism.core.machine_learning.old
=========================

Subpackages
-----------

.. toctree::
   :maxdepth: 1


   asterism.core.machine_learning.old.model_evaluation 

Modules
---------------
.. toctree::
   :maxdepth: 1


  asterism.core.machine_learning.old.classification 
  asterism.core.machine_learning.old.regression 
