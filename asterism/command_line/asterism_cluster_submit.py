#!/usr/bin/env python

'''
Created on Sep 22, 2014

@author: andrea tramacere
'''


from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import numpy as np
import sys
import argparse
from  astropy.io import fits as pf
from   asterism.pipeline_manager.processing_tools import *
import time,stat
import subprocess

script_dir=os.path.dirname(__file__)

def main (argv=sys.argv):
    
    parser = argparse.ArgumentParser()
    
   
    
    #parser.add_argument('-cluster_submit',    help='flag to write cluster job exit file',action='store_true')
    

    parser.add_argument('script_name', type=str, help='script to submit',default=None )

    
    parser.add_argument('infile', type=str, help='fits file with galaxies images',default=None )
    
    parser.add_argument('conf_file', type=str, help='configuration input file',default=None )
    
    parser.add_argument('-image_id_file', type=str, help='fits/txt file with image ids in column wiht name original_id',default=None)
    
    
    parser.add_argument('-image_id',  type=int,nargs='*' ,help='id of the image',default=None)

    parser.add_argument('-max_image_id',  type=int ,help='max id of the image data cube',default=None)
    

    parser.add_argument('-working_dir', type=str, help='working_directory',default='./' )

    parser.add_argument('-processing_root_dir', type=str, help='cluster processing dir',default=None )

    parser.add_argument('-images_per_job', type=np.int, help='number of images per job',default=None )
    
    parser.add_argument('-queue', type=np.str, help='queue',default='short' )

    parser.add_argument('-outfile', type=str, help='output fits file',default=None )
    
    parser.add_argument('-no_job_sub', help='jobs will be not submitted',action='store_true' )

    parser.add_argument('-resub', help='only jobs failed will be resubmitted',action='store_true' )

    parser.add_argument('-flag', type=str, help='',default=None )

    parser.add_argument('-save_products', action='store_true' )
    
    parser.add_argument('-j_wait_time', type=np.float, help='time (in s) to wait in the loop for all the jobs to be done',default=30 ) 
    
    parser.add_argument('-input_seg_map_file', type=str, help='',default=None)

    parser.add_argument('-jobs_array',  help='uses qsub jobs array',action='store_true')
    
    args = parser.parse_args()

    infile= os.path.abspath(args.infile)
    conf_file= os.path.abspath(args.conf_file)
   
    
    data=pf.getdata(infile)
    
    image_id=None
    if args.image_id_file is not None:
        try:
            image_id=list(pf.getdata(args.image_id_file)['original_id'])
        except:
            try:
                image_id=list(np.genfromtxt(args.image_id_file, np.int64))
            except:
                raise RuntimeError("image_id file %s, is neither txt nor fits %s, or the column original_id is not present", args.image_id_file)
        
    if args.image_id is not None and args.image_id is None:
        image_id=args.image_id
    
    
    if args.image_id is not None and args.max_image_id is not None:
        image_id=list(np.arange(args.image_id,args.max_image_id+1))
    
    if args.image_id is None and args.max_image_id is not None:
        image_id=list(np.arange(0,args.max_image_id+1))
    
    
    if image_id is not None:
        image_id.sort()
    
    
    if data.ndim==3:
        if image_id is None:
            (image_num,x,y)=data.shape
            print ("images num=",image_num)
        else:
            image_num=len(image_id)
    else:
        print( "the input file has just one one image!!!")
        image_num=1
        
    
    #if args.n_jobs>=image_num:
    #    n_jobs=image_num
    #else:
    #    n_jobs=args.n_jobs
        
        
    images_per_job=args.images_per_job
    n_jobs=np.floor_divide(image_num,images_per_job)
    #iamges_per_job=np.floor_divide(image_num,n_jobs)
    
    last_size=np.remainder(image_num,images_per_job)
    #print (image_num,n_jobs,last_size)
    if last_size!=0:
        n_jobs=n_jobs+1
   
    wd=os.path.abspath(args.working_dir)
    mkdir(wd)

    if args.processing_root_dir is not None:

        processing_dir=args.processing_root_dir+'/'+args.working_dir
        wd_local=wd
        wd=processing_dir
        print ("processing dir", processing_dir)
        mkdir(wd)
    jobs_dir_list=[]

    for chunck in range(n_jobs):
        jobs_dir_list.append('%s/JOB_%6.6d'%(wd,chunck))
        mkdir(jobs_dir_list[chunck])

    if args.jobs_array==True:
        do_sleep=False
    else:
        do_sleep=True

    f=open(wd+'/jobs_dir_list.txt','w')
    for chunck in range(n_jobs):
       
        gal_id=chunck*images_per_job
        if chunck==n_jobs-1 and last_size!=0:
            delta_id=last_size
        else:
            delta_id=images_per_job
            
        max_gal_id=gal_id+delta_id-1
        print ("chunck",n_jobs,chunck, gal_id,max_gal_id)
        
        

        job_script,job_out_file,job_exit_file,job_flag=write_job_sh_file(args,script_dir,gal_id,max_gal_id,image_id,infile,conf_file,jobs_dir_list,chunck,do_sleep=do_sleep)
        print(jobs_dir_list[chunck],file=f)



        if args.jobs_array==False:

            if args.no_job_sub==False and args.resub==False:
                qsub(jobs_dir_list[chunck],job_script,queue=args.queue)

            if args.no_job_sub==False and args.resub==True:
                check_file=jobs_dir_list[chunck]+'/'+job_flag
                if os.path.exists(check_file)==False:

                    qsub(jobs_dir_list[chunck],job_script,queue=args.queue)


    f.close()

    if args.jobs_array==True and args.no_job_sub==False:

        qsub_array(args.queue,wd,len(jobs_dir_list))


    if args.no_job_sub==False:
        monitor_jobs(args,jobs_dir_list,job_exit_file,n_jobs)  
        time.sleep(5)
        wirte_job_report(wd,jobs_dir_list,job_exit_file,n_jobs)
        
        
    
        if args.outfile is not None:
            merge_out_files(wd,args,data,n_jobs,jobs_dir_list,job_out_file,image_id)
        
        #if args.flag!=None:
        #    merge_out_analysis_files(wd,args,data,jobs_dir_list,job_out_analysis_file,n_jobs,images_per_job,image_id)

    if args.processing_root_dir is not None:
        cmd='mv %s/* %s'%(processing_dir,wd_local)
        os.system(cmd)
        
        
def write_job_sh_file(args,script_dir,gal_id,max_gal_id,image_id,infile,conf_file,jobs_dir_list,chunck,do_sleep=False):
    job_out_file='out.fits'
    job_flag='job_prod'
    job_exit_file='job_exit_status.txt'
       
    if image_id is not None:
       image_list=image_id[gal_id:max_gal_id+1]
       image_list_str=''
       for n in image_list:
           image_list_str+='%s '%n
       
       script_args=['%s'%infile,
                    '%s'%conf_file,
                    '-image_id %s'%image_list_str,
                    '-cluster_submit',
                    '-job_exit_file=%s'%job_exit_file,
                    '-working_dir=%s'%jobs_dir_list[chunck]]
    else:
       script_args=['%s'%infile,
                    '%s'%conf_file,
                    '-image_id=%d'%gal_id,
                    '-max_image_id=%d'%max_gal_id,
                    '-cluster_submit',
                    '-job_exit_file=%s'%job_exit_file,
                    '-working_dir=%s'%jobs_dir_list[chunck]]    
   
    if args.outfile is not None:
       script_args.append('-outfile=%s'%job_out_file,)
    if args.flag is not None:
       script_args.append('-flag=%s'%job_flag)
    if args.save_products==True:
       script_args.append('-save_products ')

    if args.input_seg_map_file is not None:
       input_seg_map_file=os.path.abspath(args.input_seg_map_file)
       script_args.append('-input_seg_map_file=%s'%input_seg_map_file)
  
    script_string = " ".join(str(x) for x in script_args)
    #bin_path='/home/isdc/tramacer/.local/bin/'
    #script_file=bin_path+'/'args.script_name
    script_file=script_dir+'/'+args.script_name
    command='%s  %s'%(script_file,script_string)
    job_script='%s/job.sh'%jobs_dir_list[chunck]
    #print script_file
    f=open(job_script,'w')
    #print>>f,"source /home/isdc/tramacer/Enthought/Canopy_64bit/User/bin/activate"
    print('export DISPLAY=',file=f)
    print('export LC_ALL=en_US.UTF-8',file=f)
    print('export LANG=en_US.UTF-8',file=f)
    print(command,file=f)
    f.close()
    os.chmod(job_script, stat.S_IRWXU)
    if do_sleep==True:
        time.sleep(1)
    return job_script,job_out_file,job_exit_file,job_flag


def monitor_jobs(args,jobs_dir_list,job_exit_file,n_jobs):
    ALL_DONE=False
    while ALL_DONE==False:
        
        ALL_DONE=True
        time.sleep(args.j_wait_time)
        ALL_DONE=check_all_done_status(jobs_dir_list,job_exit_file,n_jobs)
       
    
    print ("check")
    
    return ALL_DONE




def check_all_done_status(jobs_dir_list,job_exit_file,n_jobs):
    ALL_DONE=True
    
    for chunck in range(n_jobs):
        try:
            file_name=jobs_dir_list[chunck]+'/'+job_exit_file
            f=open(file_name,'r')
            DONE=True
        except:
            DONE=False
        
        ALL_DONE=ALL_DONE and DONE
    
    
    return ALL_DONE 
    
   

def wirte_job_report(wd,jobs_dir_list,job_exit_file,n_jobs):
    ALL_OK=True
    f_rep=open('%s/jobs_report.txt'%wd,'w')
    for chunck in range(n_jobs):
        file_name=jobs_dir_list[chunck]+'/'+job_exit_file
        
        f=open(file_name,'r')
        lines=f.readlines()
        f.close()
        print (file_name)
        print (lines[0])
        print( chunck,lines[0],file=f_rep)
        if lines[0].strip()!='SUCCESS':
            
            ALL_OK=False
    print('\n',file=f_rep)
    if ALL_OK==True:
        print("ALL_JOBS_OK",file=f_rep)
    else:
        print("SOME_JOBS_FAILED",file=f_rep)

    f_rep.close()
    
    
def merge_out_files(wd,args,data,n_jobs,jobs_dir_list,job_out_file,images_per_job,image_id):
    outfile=wd+args.outfile
    if image_id is None:
        out_shape=data.shape
        #print "images num=",image_num
    else:
        (image_num,x,y)=data.shape
        image_num=len(image_id)
        print ("images num=",image_num)
        
        out_shape=(image_num,x,y)
        
    data_out=np.zeros(out_shape)
    for chunck in range(n_jobs):
        gal_id=chunck*images_per_job
        
        
        file_name=jobs_dir_list[chunck]+'/'+job_out_file
        chunck_data=pf.getdata(file_name)
        (image_num,x,y)=chunck_data.shape
        print (file_name,chunck_data.shape,gal_id,image_num)
        data_out[gal_id:gal_id+image_num]=chunck_data
    
    
    print (args.outfile)
    pf.writeto(args.outfile, data_out,clobber='yes')


def merge_out_analysis_files(wd,args,data,jobs_dir_list,job_out_analysis_file,n_jobs,images_per_job,image_id):
    out_analysis_file=wd+'/'+args.out_analysis_file
    
    
    if image_id is None:
        (rows,foo,foo)=data.shape
        print ("images num=",rows)
    else:
         
        rows=len(image_id)
        print ("images num=",rows)
    
    analysis_data=pf.getdata(jobs_dir_list[0]+'/'+job_out_analysis_file)
    dt=analysis_data.dtype
    data_analysis_out=np.zeros(rows,dtype=dt)
    for chunck in range(n_jobs):
        gal_id=chunck*images_per_job
        
        
        file_name=jobs_dir_list[chunck]+'/'+job_out_analysis_file
        chunck_data=pf.getdata(file_name)
        print (chunck_data.shape,file_name)
        (rows_num,)=chunck_data.shape
        print (file_name,chunck_data.shape,gal_id,rows)
        data_analysis_out[gal_id:gal_id+rows_num]=np.array(chunck_data)

    print (args.outfile)
    pf.writeto(out_analysis_file, data_analysis_out,clobber='yes')




def qsub_array(queue,wd,size):
    script=wd+'/job_sub.sh'

    f=open(script,'w')
    print ("""
    #!/bin/bash

    #$ -j no
    #$ -b no
    #$ -o /dev/null
    #$ -e /dev/nell

    #$ -t 1-%d"""%(size),file=f)

    print("""
    let "ID= $SGE_TASK_ID - 1"
    while [ ${#ID} -ne 6 ];
    do
    ID="0"$ID
    done
    """,file=f)

    print("""
    cd %s/JOB_$ID
    ./job.sh 1>>%s/JOB_$ID/job.out 2>>%s/JOB_$ID/job.err
    """%(wd,wd,wd),file=f)
    f.close()

    print ("script",script)

    os.chmod(script, stat.S_IRWXU)
    p=subprocess.Popen(['which',script],stdout=subprocess.PIPE)
    script=p.communicate()[0].strip()

    cmd=""" qsub -q %s  -t 1-%d   %s """%(queue,size,script)



    print (cmd)


    os.system(cmd)


def qsub(grid_output_dir,script,queue='short',script_args=''):


    p=subprocess.Popen(['which',script],stdout=subprocess.PIPE)


    script=p.communicate()[0].strip()

    #print script

    cmd=""" qsub -q %s -o %s  -j no -b no -e %s %s """%(queue,grid_output_dir,grid_output_dir,script)



    print (cmd)


    os.system(cmd)



if __name__=="__main__":
    main(sys.argv)
        
        
