#!/usr/bin/env python

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


from astropy.io import fits as pf
import numpy as np
import sys
import argparse


def main(argv=None):
    parser = argparse.ArgumentParser()
    
    

    parser.add_argument('out_file_name', type=str, help='out cube file',default='out.fits' )
    parser.add_argument('input_files', type=str,nargs='+', help='input files list, you can use wildcards',default=None )
    parser.add_argument('-extension', type=int, help='extension with image ',default=0)

    args = parser.parse_args()
    
    merge_to_cube_file(args.input_files,args.out_file_name,args.extension)
    
def merge_to_cube_file(files_list,out_file,ext):
    
    
    
    
    data=pf.getdata(files_list[0],ext=ext)
    dt=data.dtype
    nrows=len(files_list)

    #for infile in files_list:
    #    data=pf.getdata(infile)
    #    nrows+=data.shape[0]
    
    out_shape=tuple([nrows])+data.shape
    data_analysis_out=np.zeros(out_shape,dtype=dt)
    
    
    for ID,infile in enumerate(files_list):
        data=pf.getdata(infile,ext)
        data_analysis_out[ID]=data
        
           
    pf.writeto(out_file, data_analysis_out,clobber='yes')

            
                        
if __name__=="__main__":
    main(sys.argv)
        
