#!/usr/bin/env python

# -*- coding: utf-8 -*-
"""
Created on Tue Apr  1 17:21:02 2014

@author: andrea tramacere
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import argparse
import sys
from asterism.dismissed.morphometric_features_extraction import MorphometricFeatureExtractionPipeline


def main (argv=sys.argv):
    parser = argparse.ArgumentParser()
    pipeline=MorphometricFeatureExtractionPipeline(parser=parser,argv=argv)
    pipeline.set_pars_from_parser()
    pipeline.list_parameters()
    pipeline.run()

if __name__=="__main__":
    main(sys.argv)