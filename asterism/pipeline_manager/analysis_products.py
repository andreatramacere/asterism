"""
Module: analysis_products
========================



Overview
--------

This modules provides an

The :class:`.class_name` is used to...



Classes relations
---------------------------------------
.. figure::  classes_rel_pkg.sub_pkg.modname.png
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::   classes_pkg.sub_pkg.modname.png




Summary
---------
.. autosummary::
   some func
   some classes


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np
from  astropy.io import fits as pf
import os
from .processing_tools import write_html_page

__author__ = 'andrea tramacere'


#class AnalysisProducts(object):
#    def __init__(self,out_image=None,prod_list=None,analysis_array=None):
#        self.out_image=out_image
#        self.prod_list=prod_list
#        self.analysis_array=analysis_array



class OutFile(object):
    def __init__(self,name='out_file',working_dir=None,flag=None,suffix=''):
        self.working_dir=working_dir
        self.flag=flag
        self.name=name
        self.suffix=suffix
        #print('OUTFILE', self.flag,self.name,)

    def write_to_file(self):
        pass

    def set_file_name(self,suffix=None):

        if self.flag is not None:
            self.file_name=self.flag+'_'+self.name
        else:
            self.file_name=self.name


        if self.working_dir is not None:
            self.file_name= os.path.abspath(self.working_dir+'/'+self.file_name)

        if suffix is not None:
            self.file_name+=suffix

        elif self.suffix is not None:
            self.file_name+=self.suffix
        else:
            self.file_name+='.out_prod'

        #self.file_name=self.file_name.decode('utf-8')
        #print ('self.file_name',self.file_name)

class OutFileGeneric(OutFile):
    def __init__(self,name='generic_out_file',working_dir=None,flag=None):
        super(OutFileGeneric,self).__init__(name=name,working_dir=working_dir,flag=flag)
        #self.data=data

    def fill(self,analysis_prod,out_image_id=None,image_id=None):
        pass

    def write_to_file(self):
        pass


class OutFileRegionCatalog(OutFile):
    def __init__(self,catalog_text,name='generic_out_file',working_dir=None,flag=None,suffix='.reg'):
        super(OutFileRegionCatalog,self).__init__(name=name,working_dir=working_dir,flag=flag,suffix=suffix)
        self.catalog_text=catalog_text

    def fill(self,analysis_prod,out_image_id=None,image_id=None):
        pass

    def write_to_file(self):
        self.set_file_name()
        with   open(self.file_name,'w') as f:
           f.write(u"%s"%self.catalog_text)

        f.close()



class OutFileFitsImage(OutFile):
    def __init__(self,out_shape,working_dir=None,flag=None,name='image_out_file',suffix='.fits'):
        super(OutFileFitsImage,self).__init__(name=name,working_dir=working_dir,flag=flag,suffix=suffix)
        self.image_array=np.zeros(out_shape)

    def write_to_file(self):
        self.set_file_name()
        pf.writeto(self.file_name, self.image_array,clobber='yes')

    def fill(self,analysis_prod,out_image_id=None,image_id=None):
        if out_image_id is None:
            self.image_array=analysis_prod.image_array
        else:
            self.image_array[out_image_id]=analysis_prod.image_array


class OutFileFigure(OutFile):
    """
    OutFile class for :class:`.AnalysisProductFigure` objects

    """
    def __init__(self,n_entries,file_type='png',working_dir=None,flag=None,name='figure_out_file'):
        """

        Parameters
        ----------
        n_entries
        file_type

        Returns
        -------

        """
        super(OutFileFigure,self).__init__(name=name,working_dir=working_dir,flag=flag)
        self.out_images_list=[]
        self.save_plot_type=file_type
        self.name=name



        if self.save_plot_type is not None:
            self.suffix='.'+self.save_plot_type


    def set_file_name(self,fig_name='',image_id=None,out_image_id=None):


        if self.flag is not None:
            self.file_name=self.flag+'_'+self.name+'_'+fig_name
        else:
            self.file_name=self.name+'_'+fig_name

        if out_image_id is not None:
            self.file_name+='_%4.4d'%out_image_id

        if image_id is not None:
            self.file_name+='_%4.4d'%image_id

        self.file_name+=self.suffix

        if self.working_dir is not None:
            self.file_name= os.path.abspath(self.working_dir+'/'+self.file_name)



    def write_to_file(self):
        if self.save_plot_type=='pdf' :
           self.pdf_out.close()
        #if self.hmtl_index_file!=None:
        #    write_html_page(self.hmtl_index_file,self.out_images_list)



    def fill(self,analysis_prod,image_id=None,out_image_id=None):
        self.save_plot_func(analysis_prod,image_id=image_id,out_image_id=out_image_id)



    def save_plot_func(self,analysis_prod,image_id=None,out_image_id=None):

        if  self.save_plot_type=='pdf':
            self.suffix='.pdf'
            from matplotlib.backends.backend_pdf import PdfPages
            self.set_file_name(self.name)
            self.pdf_out=PdfPages(self.file_name)

        for ID,fig in enumerate(analysis_prod.figures_list):
            if fig is not None:
                if self.save_plot_type=='pdf' :

                    self.pdf_out.savefig(fig)

                else:
                    self.set_file_name(fig_name=analysis_prod.figures_name_list[ID],image_id=image_id,out_image_id=out_image_id)
                    import  pylab as plt
                    fig.savefig(self.file_name )
                    self.out_images_list.append(os.path.relpath(self.file_name))
                    plt.close(fig)
        #this has moved to writ_to_file() method
        #if self.save_plot_type=='pdf' :
        #    self.pdf_out.close()



class OutFileCatalog(OutFile):
    """

    """
    def __init__(self,working_dir=None,flag=None,name='catalog',fmt=None,fits_format=True):
        """

        Parameters
        ----------
        data
        working_dir
        flag
        name

        Returns
        -------

        """
        self.fits_format=fits_format
        if fits_format==True:
            suffix='.fits'
        else:
            suffix = '.dat'
        super(OutFileCatalog,self).__init__(name=name,working_dir=working_dir,flag=flag,suffix=suffix)
        self.data_list = []

    def write_to_file(self):
        self.set_file_name()
        if len(self.data_list)>0:

            for ID,d in enumerate(self.data_list):
                if ID==0:
                    data =self.data_list[0]
                    #print(type(data))
                else:
                    data=np.concatenate((data,d))

            from astropy.io import ascii
            if self.fits_format ==False:
                ascii.write(data,output=self.file_name)
            else:
                pf.writeto(self.file_name,data,clobber=True)


    def fill(self,prod,out_image_id=None,image_id=None):
        if isinstance(prod, AnalysisProductCatalog):
            self.data_list.append(prod.np_rec_array)
        else:
            raise RuntimeError("data has to be an instance of ",type(AnalysisProductCatalog),'found',type(prod))


class OutFileRecArray(OutFile):
    """
    OutFile class for :class:`.AnalysisProductRecArray` objects
    The numpy record array is stored in the `.data` class member
    """
    def __init__(self,dtype,working_dir=None,flag=None,name='recarray_out_file',suffix='.fits'):
        """


        Parameters
        ----------
        n_entries : int
            number of images processed by the pipeline, (single files, or cube entries)

        dtype : numpy.dtype
            dtype for the numpy record array

        Returns
        -------

        """

        super(OutFileRecArray,self).__init__(name=name,working_dir=working_dir,flag=flag,suffix=suffix)

        self.data_list=[]
        self.dtype=dtype

    def fill(self,data,out_image_id=None,image_id=None):
        if type(data)==list:
            for d in data:
                if type(d)==list:
                    pass
                else:
                    raise  RuntimeError("data has to be a list of tuples")
            self.data_list.extend(data)
        elif isinstance(data,AnalysisProductRecArray):
            self.data_list.extend(data.array_values)
        else:
            raise RuntimeError("data has to either a list of tuples or an instance of "%AnalysisProductRecArray)

    def write_to_file(self):
        self.set_file_name()
        #print('->falg', self.flag)
        #print('->file_name', self.file_name)
        #print([len(l) for l in self.data_list],len(self.dtype))
        #print (self.data_list)
        #print(self.dtype)
        data=np.array(self.data_list,dtype=self.dtype)
        pf.writeto(self.file_name, data,clobber='yes')





class OutFilesCollection(object):
    """
    Class storing a collection of :class:`.OutFile` objects
    """
    def __init__(self,out_file=None):
        self.out_files_list=[]
        if out_file is not None:
            if type(out_file)==list:
                for o in out_file:
                    self.add_out_file(o)
            else:
                self.add_out_file(out_file)


    def add_out_file(self,out_file):

        if isinstance(out_file,OutFile):

            self.out_files_list.append(out_file)
        else:
            raise TypeError("pord is not a %s derived object"%OutFile)

    def fill_file(self):
        pass



#One for each image
class AnalysisProduct(object):
    """
    Base class for analysis products
    """
    def __init__(self,name='AnalysisProd',prods=None):
        self.name=name
        self.prods=prods


class AnalysisProductsCollection(object):
    """
    Class storing a collection of :class:`.AnalysisProduct` objects
    """
    def __init__(self,prod=None):
        self.prod_list=[]
        if prod is not None:
            if type(prod)==list:
                for p in prod:
                    self.add_product(p)
            else:
                self.add_product(prod)

    def add_product(self,prod):
        if isinstance(prod,AnalysisProduct):

            pass
        else:
            raise TypeError("prod is not a %s derived object"%AnalysisProduct)

        for p in self.prod_list:
            if prod.name==p.name:
                raise  RuntimeError("name %s, already used"%prod.name)

        self.prod_list.append(prod)


    def get_product_by_name(self,name):
        for p in self.prod_list:
            if name==p.name:
                return p
        raise RuntimeError("product with name %s not present in collection"%name)



    def extend(self,prod_collection):
        if isinstance(prod_collection,AnalysisProductsCollection):
            pass
        else:
            raise TypeError("prod_collection must be a list of %s "%AnalysisProductsCollection)

        for prod in prod_collection.prod_list:
            if isinstance(prod,AnalysisProduct):
                pass
            else:
                raise TypeError("prod is not a %s derived object"%AnalysisProduct)

            self.add_product(prod)



class AnalysisProductGeneric(AnalysisProduct):
    def __init__(self,prods,name='prod_generic'):
        super(AnalysisProductGeneric,self).__init__(name=name)
        self.prods=prods



class AnalysisProductRegionCatalog(AnalysisProduct):
    def __init__(self,catalog_text,name='prod_generic'):
        super(AnalysisProductRegionCatalog,self).__init__(name=name)
        self.catalog_text=catalog_text

class AnalysisProductCatalog(AnalysisProduct):
    """
    AnalysisProduct for ASCII catalog
    """
    def __init__(self,data,name='catalog'):
        """

        Parameters
        ----------
        np_rec_array


        Returns
        -------

        """
        super(AnalysisProductCatalog, self).__init__(name=name)
        self.np_rec_array=data

class AnalysisProductFitsImage(AnalysisProduct):
    """
    AnalysisProduct for Image
    """
    def __init__(self,image_array,name='prod_image'):
        """

        Parameters
        ----------
        image_array
        name

        Returns
        -------

        """
        super(AnalysisProductFitsImage, self).__init__(name=name)
        self.image_array=image_array


class AnalysisProductRecArray(AnalysisProduct):
    """
    AnalysisProduct for numpy record array
    """
    def __init__(self,array_values,analysis_array_dtype,name='prod_rec_array'):
        """

        Parameters
        ----------
        array_values
        analysis_array_dtype
        name

        Returns
        -------

        """
        super(AnalysisProductRecArray,self).__init__(name=name)
        #print(array_values,type(array_values))
        if   type(array_values)==tuple:
            pass
        else:
            raise  RuntimeError('array_values must be tuple')

        self.array_values=[array_values]

        self.analysis_array_dtype=analysis_array_dtype


    def append_row(self,list_of_array_values):

        self.array_values.extend(list_of_array_values)


class AnalysisProductFigure(AnalysisProduct):
    """
    AnalysisProduct for matplotlib figures
    """
    def __init__(self,figures_list,figures_name_list,name='prod_figure'):
        """

        Parameters
        ----------
        figure_list :  list of pylab figures
        figures_name_list : list of corresponding names
        name

        Returns
        -------

        """
        super(AnalysisProductFigure,self).__init__(name=name)
        if type(figures_list)!=list:
            figures_list=[figures_list]

        self.figures_list=figures_list

        if type(figures_name_list)!=list:
            figures_name_list=[figures_name_list]

        self.figures_name_list=figures_name_list


