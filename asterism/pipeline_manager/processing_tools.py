"""

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

__author__ = 'andrea tramacere'

import os

def mkdir(dir_name):
    if os.path.isdir(dir_name):
        print( "dir %s exists"%dir_name)
    elif os.path.isfile(dir_name):
        print ("dir %s exists as file"%dir_name)
        raise RuntimeError("dir %s exists as file"%dir_name)
    else:
        try:
            os.makedirs(dir_name)
        except OSError:
            pass





def read_config_file(file_name):
    f=open(file_name,'r')
    lines=f.readlines()
    f.close()
    
    return lines



def parse_process_section(lines,process_name,task_name):
    
    dict={}

    start_p=False
    stop_p=False
    start_t=False
    stop_t=False
    for line in lines:

        line=line.strip()
        if len(line)>0:

        

            if start_t==True and stop_t==False and start_p==True and stop_p==False:
                if line[0]!='#':
                    tkn=line.strip().split('=')
                
                
                if len(tkn)==2:

                    dict[tkn[0].strip()]=tkn[1].strip()
            if line[0]!='#' and line[0]=='[':
                line=line.strip(']\[')
                line=line.strip()
                line=line.replace(' ','')
                tkn=line.split(':')

                if tkn[1]==process_name and tkn[2]=='start':
                    start_p=True

                if tkn[1]==process_name and tkn[2]=='stop':
                    stop_p=True

                if tkn[1]==task_name and tkn[2]=='start' and start_p==True:
                    start_t=True

                if tkn[1]==task_name and tkn[2]=='stop' and start_p==True:
                    stop_t=True

    #print ('=> built dict',dict )
    return dict

def conv_str(val,verbose=False):
    try:
        return eval(val)
    except:
        return val




def write_html_page(html_file_name,images_list,images_per_page=20):
    html_index_file=open(html_file_name,'w')    
    
    N_pages= len(images_list)/images_per_page+1
    path_html_images_file=os.path.dirname(html_file_name)
    
    for i in range(int(N_pages)):
        images_page='images_%4.4d-%4.4d_'%( i*images_per_page,(i+1)*images_per_page)+'images.html'
       

        print(u"""<a href="%s"> Images %4.4d - %4.4d </a> <br>"""%(images_page,i*images_per_page,(i+1)*images_per_page),file=html_index_file)
                
        write_images_page(path_html_images_file+'/'+images_page,images_list[i*images_per_page:(i+1)*images_per_page])
    
  
    
   
    
    
    
def write_images_page(html_file_name,images_list):
    html_file=open(html_file_name,'w')
    header=u"""
  <!DOCTYPE html>
<html>
  <head>
    <title>Images</title>
  </head>
  <body>
    <h1>Images</h1>
"""

    end=u"""</body>
</html>"""
    
    print(header,file=html_file)
    for image in images_list:
        print(div_image(image),file=html_file)
    print(end,file=html_file)
    
    html_file.close()
    
    
def div_image(image_file):
    return """</div>
    <div class="image">
      <img src="%s">
    </div>"""%os.path.basename(image_file)


def file_base_name(file_name):
    if '.' in file_name:
        separator_index = file_name.index('.')
        base_name = file_name[:separator_index]
        return base_name
    else:
        return file_name


def path_base_name(path,out_name):
    if out_name is None:
        file_name = os.path.basename(path)
        name = file_base_name(file_name)
    else:
        name = out_name

    return out_name
