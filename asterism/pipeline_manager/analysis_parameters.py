"""
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str
    basestring = (str, bytes)

from collections import Iterable
from .  processing_tools import conv_str
import sys

__author__ = 'andrea tramacere'


class Parameter(object):
    
    def __init__(self,name,**kwargs):
        
        self.name=None
        self.default=None
        self.help=None
        self.type=None
        self.value=None
        self.to_conf_file=True

        if name[0]=='-':
            self.positional=False
        else:
            self.positional=True
        
        self.name=name.replace('-','')
        
        if 'value' in kwargs:
            self.value=kwargs['value']
        
        if 'type' in kwargs:
            self.type=kwargs['type']
        elif 'action' in kwargs:
            self.type=bool
            self.value=False
        else:
            self.type=None
        
        if 'default' in kwargs:
            self.default=kwargs['default']
            if self.value is None:
                self.value=self.default
        else:
            self.default=None
        
        if 'help' in kwargs:
            self.help=kwargs['help']
        else:
            self.help=None

        if 'to_conf_file' in kwargs:
            if type(kwargs['to_conf_file'])==bool:
                self.to_conf_file=kwargs['to_conf_file']
            else:
                raise RuntimeError('to_conf_file parameter must be bool')

        self.command_line=False
    
            
            
class ParametersList(object):
    def __init__(self,name,task,parser=None):
        self.name=name
        self._task=task
        
        self.parser=parser

        self.pars=[]
        self.mex_groups=[]
        self.parser_mex_groups=[]

        
    def list_paremters(self):
        print('|-------------------------------------------------------')


        print ("|task:",self._task.name)
        print ("|")
        for par in self.pars:
            print("| par_name=",par.name,"  value=",par.value,"  doc= " ,par.help)
        
        for mex_group in self.mex_groups:
            print( "+--mex group:", mex_group.name)
            for par in mex_group.pars:
                print("   x par_name=",par.name,"  value=",par.value,"  doc= " ,par.help)
        
        print ("|-------------------------------------------------------")
 
    
    def dump_to_conf_file(self,f):
        print(u'',file=f)
        print(u"  [ task: %s: start]"%self._task.name,file=f)
        for par in self.pars:
            if par.to_conf_file==True:
                print(u'   ', u'%s' % par.name,u'=',u'%s'%str(par.value),file=f)
                #print(u'   ', u'%s'%par.name,u'=',par.value,file=f)
        

        for mex_group in self.mex_groups:
            print(u"    # mex group:%s"%mex_group.name,file=f)
            for par in mex_group.pars:
                if par.positional==False and par.to_conf_file==True:
                    print(u'   ', u'%s' % par.name, u'=', u'%s' % str(par.value), file=f)
        
        print(u"  [ task: %s: stop]"%self._task.name,file=f)
        #print(u'\n', file=f)
        
        
    def add_par(self,par_name,add_to_parser=False,**kwargs):
    
        for par in self.pars:
            if par_name==par.name:
                raise ValueError('par name, already present')
        
        for mex_group in self.mex_groups:
            for par_name in mex_group:
                if par_name==par.name:
                    raise ValueError('par name, already present')
        
        par=Parameter(par_name,**kwargs)
        
        self.pars.append(par)
 
        if self.parser is not None and add_to_parser==True:
            par.command_line=True
            #print kwargs
            self.parser.add_argument(par_name,**kwargs)
       
        
    def add_mex_group(self,name):
        for mex_group in self.mex_groups:
            if name==mex_group.name:
                raise ValueError('mex_gropu name, already present')
 
        mex_group=MexParametersGroup(name,self._task,parser=self.parser)
        self.mex_groups.append(mex_group)
        
        
        return mex_group
    
    
    def get_par_value(self,par_name):
        return self.get_par(par_name).value
        #found=False
        #for par in self.pars:
        #    if par_name==par.name:
        #        #print "-->",par_name,par.name,par.value,getattr(par,'value')
        #        found=True
        #        return  getattr(par,'value')
    
    
        #for mex_group in self.mex_groups:
        #    mex_group.get_par_value(par_name)
        
        #if found==False:
        #    raise RuntimeError('par %s not found'%par_name)
        
    def get_par(self,par_name,verbose=False):
        #print"-->",par_name
        found=False
        par=None
        for par in self.pars:
            if par_name==par.name:
                #print "c-->",par_name,par.name,par.value,getattr(par,'value')
                found=True
                return  par
            else:
                par=None
    
        par=None
        found=False
        for mex_group in self.mex_groups:
            par=mex_group.get_par(par_name)
            if par is not None:
                found=True
                #print "c-->",par_name,par.name,par.value,getattr(par,'value')
                return par
            else:
                par=None
        if (found == False or par is None) and verbose==True:
            raise RuntimeError('par %s not found'%par_name)
        
        #print "->PAR",par
        return par
            
            
    def set_par(self,par_name,verbose=False,**kwargs):
        par=self.get_par(par_name)
        #print ('setting par',par.name)
        if par is not None:
            if 'value' in kwargs:
                self.check_type(par,kwargs['value'])
                            
            for name, value in kwargs.items():
                        
                #print par_name,name,value
                setattr(par,name,value)
  
        else:
            #if verbose==True:
            raise RuntimeError('par %s not found, check your conf_file and/or your set_part() assignement'%par_name)
    
        #print ('==>',type(par.value),par.type)
    
    
    def set_pars_from_parser(self,args,argv,args_dict,conf_file_dict=None):
        
        #print conf_file_dict
        #print args
        #print"--> args_dict", args_dict
        #print args_dict

        if args_dict is not None:
            args_dict_kw=args_dict.keys()
            argv=argv[1:]
            for i,val in enumerate(argv):
                argv[i]=argv[i].replace('-','')
                argv[i]=argv[i].split('=')[0]
            #no conf file
        if conf_file_dict is None:
            pass
            #for k in args_dict_kw:
            #    self.set_par(k,value=args_dict[k])
        
        #with conf file    
        else:
            conf_file_dict_kw=conf_file_dict.keys()
            #print "->",self.name
            #print "-> argv",argv
            #print "-> args",args_dict_kw

            for k in conf_file_dict_kw:
                #print("k->", k)
                if type(conf_file_dict[k])==str or  isinstance(conf_file_dict[k],basestring) :
                    val=conv_str(conf_file_dict[k],verbose=False)
                else:
                    val=conf_file_dict[k]

                self.set_par(k,value=val)

                      
        #command line
        if args_dict is not None:
            for k in args_dict_kw:
                #print "--> k",k
                par=self.get_par(k)
                #print par.name
                if par is not None:
                    #print "-->",args_dict[k],par.value,par.default
                    if args_dict[k] != par.default:
                        
                        self.set_par(k,value=args_dict[k])



    def check_type(self,par,values):
        #print('par name ->',par.name,values)
        if isinstance(values,Iterable)==False or isinstance(values,basestring)==True:
            values=[values]
        
        for val in values:
            #print('->',par.name,val,isinstance(val, basestring))
            if isinstance(val,basestring) and  par.type==str:
                pass
            #if isinstance(val,int) and  isinstance( par.type,int):
            #    pass

            elif par.type !=type(val) and type(val)  !=type(None):

                print( "par %s, is of type %s,  but is expected to be %s"%(par.name,type(val), par.type))
                sys.exit(0)
            
            
        
               
class MexParametersGroup(ParametersList):
    def __init__(self,name,task,parser=None,argv=None):
        self.name=name
        self._task=task
        
        self.parser=parser
        self.argv=argv

        self.pars=[]
        
        if self.parser is not None:
            self.parser_mex_group= self.parser.add_mutually_exclusive_group()
            
        
    
     
    def add_par(self,name,add_to_parser=False,**kwargs):
        for par in self.pars:
            if name==par.name:
                raise ValueError('par name, already present')
        
        par=Parameter(name,**kwargs)
        
        self.pars.append(par)  
        
        if self.parser is not None and add_to_parser==True:
            self.parser_mex_group.add_argument('-'+name,**kwargs)
        
    def get_par_value(self,name):
        found=False
        for par in self.pars:
            if name==par.name:
                found=True
                return getattr(par,'value')
        if found==False:
            raise RuntimeError('par %s not found'%name)
        
        return None
    
    def get_par(self,name):
        found=False
        for par in self.pars:
            if name==par.name:
                found=True
                return par
            
            #if found==False:
            #    raise RuntimeError('par %s not found'%name)
        
        return None
    
    def set_par(self,par_name,**kwargs):
        found=False
        N_False=0
        for par in self.pars:
            #print ('->',par.name,name)
            if par_name==par.name:
                #print"-> mex", par.name,par_name,kwargs['value']
                found=True
                #print ('par',par,kwargs['value'],type(kwargs['value']))
                if 'value' in kwargs:
                        self.check_type(par,kwargs['value'])

                for name, value in kwargs.items():
                    setattr(par,name,value)
            
            if par.value is not None:
                N_False+=1
        
        if  N_False>1:
            for par in self.pars:
                print (par.name, par.value)
            
            raise ValueError("Only one parameter in mex group:%s can be different from None"%(self.name))
        
        return found