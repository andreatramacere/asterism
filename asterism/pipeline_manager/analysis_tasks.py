"""

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from .analysis_parameters import ParametersList
import inspect
from .processing_tools import *
import traceback
import sys

__author__ = 'andrea tramacere'

class AnalysisTask(object):
    """
    Class for single Task
    """
    def __init__(self,name,func,parser=None,process=None):
       
       
        self.name=name
        
        self.func=func
        
        self.parameters_list=ParametersList(name,self,parser=parser)
        
        self._build_par_dic()
        
        self._parsing_done=False
        
        self.parser=parser

        self.conf_file_dict=None
        
        self.process=process
   
   
    def _build_par_dic(self):
        #print "----------> FUNC in _build_par_dic <------------------"
        self._par_dic={}
        varnames=inspect.getargs(self.func.__code__)[0]
        default=inspect.getargspec(self.func)[-1]
        if default is None:
            d=0
        else:
            d=len(varnames)-len(default)
        for ID,vname in enumerate(varnames):
            #print"var name",vname
            self._par_dic[vname]=None
        if d>0:
            for ID in range(d,len(varnames)):
                self._par_dic[vname]=default[ID-d]
                #print "keywordarg",varnames[ID],default[ID-d]

        #self._par_dic={key: None for (key) in varnames}
        #print 'v',varnames, self._par_dic
        #print "----------------------------------------"

    def _build_conf_pars_dict(self,conf_file_lines):
        self.conf_file_dict=parse_process_section(conf_file_lines,self.process.name,self.name)
        
        
    def _set_par_dic(self,**kwargs):
        
        for par in self.parameters_list.pars:
            self._par_dic[par.name]=par.value

        for mex_group in self.parameters_list.mex_groups:
            for par in mex_group.pars:
                self._par_dic[par.name]=par.value
                #print par.name
                
        for key in kwargs.keys():
            self._par_dic[key]=kwargs[key]



    
    def set_par(self,name,**kwargs):
        
        self.parameters_list.set_par(name,**kwargs)
           
      
    def get_par_value(self,name):
        try:
            #print "->", name
            return self.parameters_list.get_par_value(name)
           
        except Exception as e:
            print('exception for par',name,  e)
        
    def add_par(self,name,**kwargs):
        try:
            self.parameters_list.add_par(name,**kwargs)
            
        except Exception as e:
            print(e)
        
    def list_parameters(self):
        
        self.parameters_list.list_paremters()
         
    
  
    
    def set_pars_from_parser(self,args,argv,args_dict,conf_file_lines=None):
        if conf_file_lines is not None:
            self._build_conf_pars_dict(conf_file_lines)
        self.parameters_list.set_pars_from_parser(args,argv,args_dict,conf_file_dict=self.conf_file_dict)
        self._parsing_done=True

        
    def run(self,extra_message=None,**kwargs):


        self.start_message()
        if self.parser is not None and self._parsing_done==False:
            print( "|set_par_from_parser")
            self.set_pars_from_parser()

        
        
        self._set_par_dic(**kwargs)
        



        
        try:
            return self.func(**self._par_dic)
        
        except Exception as e:
            exception_mess=e
            exit_status='FAILED'
            print ("Failed, task:", self.name)
            print (traceback.format_exc())
            sys.exit(1)
            return None



    def start_message(self,extra_message=None):
        print ("|------------------------------")
        print ("| Task:",self.name, "Start")
        print ("|------------------------------")
        if extra_message is not None:
                print ("|",extra_message)


    def stop_message(self,):
        print ("|-------------------------------------------------------")
        print ("| Task:",self.name, "Done")
        print ("|-------------------------------------------------------")
        print('\n')
        print('\n')

    