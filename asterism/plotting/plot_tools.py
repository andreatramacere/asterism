"""
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


import numpy as np
from math import fabs
__author__ = 'andrea tramacere'

#from skimage import exposure
#from ..image_processing.filters import normalize



try:

    import matplotlib
    #matplotlib.use('Agg')


    import matplotlib.pylab as plt
    #from scipy import ndimag
    from matplotlib.patches import Ellipse, Circle

    #from  matplotlib.pyplot.arrow as arrow
    import matplotlib.gridspec as gridspec

    #from scipy import ndimage
    import matplotlib.cm as cm
    #from matplotlib.colors import Normalize
    from matplotlib.colors import LogNorm, PowerNorm

except:
    print("no pylab found")


class Formatter(object):
    def __init__(self, im):
        self.im = im
    def __call__(self, x, y):
        z = self.im.get_array()[int(y), int(x)]
        return 'x={:.01f}, y={:.01f}, flux={:.05f}'.format(x, y, z)


def show_image(image_array,plot=False,ax=None,vmax=None,vmin=None,cmap=None):

    y_size,x_size=image_array.shape

    if ax is None:
        fig, ax = plt.subplots()

    im=ax.imshow(image_array,interpolation='nearest',cmap=cmap)
    ax.format_coord = Formatter(im)

    ax.set_ylim([0,y_size-1])
    ax.set_xlim([0,x_size-1])
    if vmax is not None or vmin is not None:
        im.set_clim(vmin,vmax)
    if plot==True:
        plt.show()
    return im,ax


def plot_contour(ax,x_contour,y_contour,color,delta_max=5,s=1.0):
    #if len(x_contour)>3:
    #    dx=fabs(x_contour[0]-x_contour[-1])
    #    dy=fabs(y_contour[0]-y_contour[-1])
    #    if dx<=delta_max and dy<=delta_max:
    #        ax.plot([x_contour[0],x_contour[-1]],[y_contour[0],y_contour[-1]],'-',c=color,lw=1)#linestyle='None')

    #    for ID in range(x_contour.size-1):
    #        dx=fabs(x_contour[ID]-x_contour[ID+1])
    #        dy=fabs(y_contour[ID]-y_contour[ID+1])
    #        if dx<=delta_max and dy<=delta_max:
    #            ax.plot([x_contour[ID],x_contour[ID+1]],[y_contour[ID],y_contour[ID+1]],'-',c=color,lw=1)#linestyle='None')
    ax.scatter(x_contour,y_contour,marker='.',color=color,s=s)


def do_detection_plot(ax,image,cluster_list,sex_srcs_array=None,plot_core_points=True,plot_cnt=True,arrows=True,plot_ID=True,plot_cl_centroid=True,title='detection plot',font_size=10,plot_image_center=False):

    ax.set_title(title,fontsize=font_size)

    if image is not None:
        if hasattr(image,'array'):
            image_array=image.array
        else:
            image_array=image
        imgplot,ax=show_image(image_array,ax=ax)



        image_center_y=np.float(image.shape[0])/2.0
        image_center_x=np.float(image.shape[1])/2.0

        if plot_image_center==True:
            ax.scatter(image_center_x,image_center_y,marker='+', linewidths=1,c='black')

    clip_flux=np.zeros(len(cluster_list))
    clip_size=np.zeros(len(cluster_list))

    for ID,cluster in enumerate(cluster_list):
        c = cm.hsv(float(cluster.ID)/10,1)

        clip_flux[ID]=cluster.pts_weight.mean()+2.0*cluster.pts_weight.std()
        clip_size[ID]=cluster.pts_weight.size

        if plot_ID==True:
            ax.annotate('C.ID=%d'%(cluster.ID), xy=(cluster.x_c,cluster.y_c), color='w'   )
        if plot_core_points==True:
            ax.plot(cluster.cartesian.x,cluster.cartesian.y,'s',mew=0)

        if plot_cl_centroid==True:
            ax.scatter(cluster.x_c,cluster.y_c,marker='+',s=100, linewidths=1,c='black')
            ellipse=Ellipse(xy=(cluster.x_c,cluster.y_c), width=cluster.sig_x*2.0, height=cluster.sig_y*2.0, angle=cluster.semi_major_angle,edgecolor='y',fc='None')
            ax.add_patch(ellipse)


        if plot_cnt==True and cluster.contour_shape is not None:


            if cluster.contour_shape is not None:
                plot_contour(ax,cluster.contour_shape[:,0],cluster.contour_shape[:,1],color='w')


        if arrows==True:
            delta_x=cluster.sig_x*np.cos(np.deg2rad(cluster.semi_major_angle))
            delta_y=cluster.sig_x*np.sin(np.deg2rad(cluster.semi_major_angle))
            ax.arrow(cluster.x_c,cluster.y_c,delta_x ,delta_y ,head_width=0.05, head_length=0.1)

            delta_x=image_center_x-cluster.x_c
            delta_y=image_center_y-cluster.y_c
            ax.arrow(cluster.x_c,cluster.y_c,delta_x ,delta_y ,head_width=0.05, head_length=0.1)

    clip_flux_level=None
    if len(cluster_list)>0:
        clip_flux_level=clip_flux[np.argmax(clip_size)]*1.5
        if image is not None:
            imgplot.set_clim(image_array.min(),clip_flux_level)


    if sex_srcs_array is not None:
         for sex_src in sex_srcs_array:
            ellipse=Ellipse(xy=(sex_src['X_IMAGE']-1,sex_src['Y_IMAGE']-1), width=sex_src['A_IMAGE'], height=sex_src['B_IMAGE'], angle=sex_src['THETA_IMAGE'],edgecolor='white',fc='None')
            ax.add_patch(ellipse)

    return clip_flux_level



def analysis_plot(image,selected_coords,fluxes,cluster_list,title='fig',plot=False,gal_id=None,threshold=None,K=None,only_detection=False,fig_name='detection_plot',msk=None):

    if only_detection==True:
        fig,((ax)) = plt.subplots(1, 1)
        do_detection_plot(ax,image,cluster_list,sex_srcs_array=None)
        if plot==True:
            plt.show()
            return

    fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)

    fig.canvas.set_window_title(fig_name)



    if gal_id is not None:
        fig.suptitle('gal ID=%d'%gal_id,fontsize=10)




    clip_flux_level=do_detection_plot(ax3,image,cluster_list,sex_srcs_array=None,plot_core_points=False,arrows=False)

    ax=ax1
    ax.set_title('original image',fontsize=10)
    show_image(image.array,ax=ax1,vmax=clip_flux_level)


    ax=ax2
    ax.set_title('bool mask',fontsize=10)
    show_image(image.array,ax=ax2,vmax=clip_flux_level)
    if selected_coords is not None:
        ax.scatter(selected_coords[:,0],selected_coords[:,1],marker='o', edgecolor='white', linewidth='0',facecolor='white',s=2)




    if threshold is not None:

        ax4.hist(np.log10(fluxes[fluxes>0]),bins='auto')
        if np.shape(threshold)==():
            ax4.plot([np.log10(threshold),np.log10(threshold)],[0,ax4.axis()[3]])

    else:
        ax4.hist(  image.flatten(),bins=50)


    #ax=ax6
    #ax.hist(fluxes,bins=20,)
    #if K!=None:
    #    ax.plot( [K*threshold,K*threshold],[0,ax.axis()[3]],lw=3)




    #ax=ax4
    #ax.set_title('Final Image',fontsize=10)

    #if image is not None:
    #    show_image(image.array,ax=ax4)

    plt.tight_layout()


    if plot==True:
        plt.show()

    #plt.close()

    return fig



def ring_detection_plot(image,ring_image_array,new_coords_cartesian,cluster_list,x_c,y_c,x_hist, y_hist,x_c_fit=None,y_c_fit=None,R_c_fit=None,title='fig',plot=False,gal_id=None,fig_name='detection_plot',msk=None):


    fig,((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)

    fig.canvas.set_window_title(fig_name)



    if gal_id is not None:
        fig.suptitle('gal ID=%d'%gal_id,fontsize=10)




    clip_flux_level=do_detection_plot(ax3,image,cluster_list,sex_srcs_array=None,plot_core_points=False,arrows=False,plot_ID=False)

    ax=ax1
    ax.set_title('original image',fontsize=10)
    show_image(image.array,ax=ax1,clim=clip_flux_level)



    ax2.set_title('ring mask',fontsize=10)
    img=np.copy(image.array)
    img[~ring_image_array]=0
    show_image(img,ax=ax2,cmap='gray')
    if new_coords_cartesian is not None:
        ax2.plot(new_coords_cartesian.x,new_coords_cartesian.y,'.',ms=5,c='y')

    if x_hist is not None:
        #for r in x_hist:
        ellipse = Ellipse(xy=(x_c,y_c), width=2*x_hist.min(), height=2*x_hist.min(), edgecolor='w', fc='None',linewidth=1.0)
        ax2.add_patch(ellipse)
        ellipse = Ellipse(xy=(x_c, y_c), width=2 * x_hist.max() , height=2 * x_hist.max(), edgecolor='w',fc='None', linewidth=1.0)
        ax2.add_patch(ellipse)
        ax4.plot(x_hist, y_hist)
        ax4.set_xlim(0, x_hist.max())

    if x_c_fit is not None:
        ellipse = Ellipse(xy=(x_c_fit,y_c_fit), width=2 * R_c_fit, height=2 * R_c_fit, edgecolor='r', fc='None',
                          linewidth=1.0,linestyle='--')
        ax2.add_patch(ellipse)
        ellipse = Ellipse(xy=(x_c_fit, y_c_fit), width=2 * R_c_fit, height=2 * R_c_fit, edgecolor='r', fc='None',
                          linewidth=1.0, linestyle='--')
        ax1.add_patch(ellipse)
    #ax=ax6
    #ax.hist(fluxes,bins=20,)
    #if K!=None:
    #    ax.plot( [K*threshold,K*threshold],[0,ax.axis()[3]],lw=3)




    #ax=ax4
    #ax.set_title('Final Image',fontsize=10)

    #if image is not None:
    #    show_image(image.array,ax=ax4)

    plt.tight_layout()

    if plot==True:
        plt.show()
    #plt.close()
    return fig