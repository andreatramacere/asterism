"""

This module provides the implementation of the :class:`.DoDBSCANImageSegmentationTask` class
used to perform the image segmentation Task
The algorithm in of the task is implemented by the :func:`.do_image_segmentation`


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::


Summary
---------
.. autosummary::
    DetectionInputProducts
    DoImageSegmentationTask

Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


import  numpy as np
from ....core.clustering.density_based.dbscan_binned import  DBSCANBinned
from ....core.clustering.density_based.dbscan_unbinned import DBSCANUnbinned
from ....core.clustering.connectivity_based.connected_components import  ConnectedComponents
from math import sqrt
from ....core.tables.events_table import EventTable
from ....pipeline_manager.analysis_tasks import AnalysisTask

__author__ = 'andrea tramacere'






class DetectionInputProducts(object):

    def __init__(self,image_array,masked,offset_x=0,offset_y=0):
        self.image_array=image_array
        self.masked=masked
        self.offset_x=offset_x
        self.offset_y=offset_y


def build_seg_map(image,cl_list,off_set_id,out_seg_map_overlap=False):
    seg_map = np.zeros(image._shape, dtype=np.int32)
    if out_seg_map_overlap is True:
        seg_map_overlap = np.zeros(image._shape, dtype=np.int32)
    else:
        seg_map_overlap = None
    # print("|seg map shape",seg_map.shape)
    for cl in cl_list[:]:
        # fix rounding at border
        # print ('cl.ID',cl.ID + off_set_id)
        _x_px = cl.cartesian.x.astype(np.int32)
        _y_px = cl.cartesian.y.astype(np.int32)
        _x_px[_x_px >= seg_map.shape[1]] = seg_map.shape[1] - 1
        _y_px[_y_px >= seg_map.shape[0]] = seg_map.shape[0] - 1
        seg_map[tuple([_y_px, _x_px])] = cl.ID + off_set_id

        if out_seg_map_overlap is True:
            seg_map_overlap[tuple([_y_px, _x_px])] = cl.ID + off_set_id
            seg_map_overlap[tuple([_y_px[cl.overlap], _x_px[cl.overlap]])] = -1
            seg_map_overlap[tuple([_y_px[cl.overlap], _x_px[cl.overlap]])] = -1
            seg_map_overlap[tuple([_y_px[cl.overlap], _x_px[cl.overlap]])] = -1

    return  seg_map,seg_map_overlap


def build_source_clusters(ID,dbscan_clusters_list,offset_x=0,offset_y=0,original_flux=None,verbose=False):
    sources_cluster_list=[]


    for ID1,cl in enumerate(dbscan_clusters_list):
        if original_flux is not None:

            cl.weight_array=original_flux[cl.position_array[:,1].astype(np.int32), cl.position_array[:,0].astype(np.int32)]

        #print('cl.ID', cl.ID)
        sources_cluster_list.append(cl.SourceCluster_factory(ID=cl.ID+ID,x_off_set=offset_x,y_off_set=offset_y,parent_ID=-(cl.ID+ID)))
        #if verbose==True:
        #print('|cl.ID',cl.ID,ID)
            #print('|cl size',cl.size)
    return sources_cluster_list


def connected_components_segmentation(image_array,K,masked=None,verbose=False,min_pix_size=None):
    import time
    t = time.time()
    conn=ConnectedComponents(1)
    conn.run(image_array,K,masked=masked,min_pix_size=min_pix_size,verbose=verbose)
    y,x=np.where(~masked)
    #print('|done', time.time() - t)
    return conn.clusters_list,np.column_stack((x,y))

def dbscan_unbinned_segmentation(image_array,K,Eps,K_pix=False,masked=None,verbose=False,min_pix_size=None):

    table=EventTable.from_image_array(image_array,masked=masked)
    db=DBSCANUnbinned(Eps,K,buffer_size=None,K_pix=K_pix)
    db.run(table.cartesian.coords,weight_array=table.flux.values,verbose=verbose,min_pix_size=min_pix_size)

    return db.clusters_list,table.cartesian.coords



def dbscan_binned_segmentation(image_array,K,Eps,K_pix=False,masked=None,verbose=False,min_pix_size=None):

    db=DBSCANBinned(Eps, K,K_pix=K_pix)
    db.run(image_array,masked=masked,verbose=verbose,min_pix_size=min_pix_size)
    y,x=np.where(~masked)

    return db.clusters_list,np.column_stack((x,y))



def build_detection_input_prod_list(image,bkg_th=None,max_image_size=None,min_sub_stamp_size=25,use_only_central_sub_stamp=False):
    detection_input_prod_list=[]

    #No bkg Full stamp will be used
    if bkg_th is None:
        masked=image.masked
        detection_input_prod_list.append(DetectionInputProducts(image.array,masked))

    #bkg is used to get sub stamps if image.size>max_image_size
    elif max_image_size is not None and bkg_th is not None:

        if image.array.size>max_image_size:
            print("|Building image sub stamps")

            image._make_connected_sub_stamps(bkg_th,min_sub_stamp_size)
            for ID in range(image._N_connected_sub_stamps):
                dist=np.zeros( image._N_connected_sub_stamps)
                image_array,masked,x_offset,y_offset=image._get_connected_stamp_and_mask(ID,min_size=min_sub_stamp_size)
                (x_c,y_c)=image.shape
                dx= x_c*0.5 - image._center_x
                dy= y_c*0.5 - image._center_y

                dist[ID]=sqrt(dx*dx+dy*dy)
                if image_array is not None:
                    masked_bg=image_array<bkg_th
                    masked=np.logical_or(masked_bg,masked)
                    detection_input_prod_list.append(DetectionInputProducts(image_array,masked,x_offset,y_offset))
        else:
            masked_bg=image.array<bkg_th
            masked=np.logical_or(masked_bg,image.masked)
            detection_input_prod_list.append(DetectionInputProducts(image.array,masked))
    else:
        masked_bg=image.array<bkg_th
        masked=np.logical_or(masked_bg,image.masked)
        detection_input_prod_list.append(DetectionInputProducts(image.array,masked))

    if len(detection_input_prod_list)>1 and use_only_central_sub_stamp==True:
        id_c=np.argmin(dist)
        detection_input_prod_list=[detection_input_prod_list[id_c]]

    return  detection_input_prod_list


def from_image_detection(allowed_list,method,bkg_threshold,image,max_image_size,min_sub_stamp_size,dbscan_eps,K,K_pix,verbose,plot_dbscan,use_only_central_sub_stamp=False,min_pix_size=None):
    if verbose==True:
        print("|method", method)

        print("|K,",K)
        print("|K_pix,", K_pix)
        #print("|bkg_threshold.shape",bkg_threshold.shape)
        print("|bkg_th (mean value if rms map is provided)", np.mean(bkg_threshold))
        print("|min_pix_size",min_pix_size)

    original_flux=image._data
    image._data=image._data/bkg_threshold
    #print("CICCIO",image._data[image._data>1.0].sum())


    detection_input_prod_list=build_detection_input_prod_list(image,1.0,max_image_size=max_image_size,min_sub_stamp_size=min_sub_stamp_size,use_only_central_sub_stamp=use_only_central_sub_stamp)
    final_cl_list=[]
    final_src_cl_list=[]
    final_selected_coords=None
    #import time
    #t=time.time()
    for ID,prod in enumerate(detection_input_prod_list):
        if method=='dbscan':
            cl_list,selected_coords=dbscan_unbinned_segmentation(prod.image_array,K,dbscan_eps,K_pix=K_pix,masked=prod.masked,verbose=verbose,min_pix_size=min_pix_size)
        elif method=='dbscan_binned':
            cl_list,selected_coords=dbscan_binned_segmentation(prod.image_array,K,dbscan_eps,K_pix=K_pix,masked=prod.masked,verbose=verbose,min_pix_size=min_pix_size)
        elif method=='connected':
            cl_list,selected_coords=connected_components_segmentation(prod.image_array,K,masked=prod.masked,verbose=verbose,min_pix_size=min_pix_size)
        else:
            raise RuntimeError("""method='%s'  is not in the allowed list%s"""%(method,allowed_list))

        if ID==0:
            final_selected_coords=selected_coords+[prod.offset_x,prod.offset_y]
        else:
            selected_coords=selected_coords+[prod.offset_x,prod.offset_y]
            final_selected_coords=np.row_stack((final_selected_coords,selected_coords))

        final_cl_list.extend(cl_list)


        #print("stamp",ID,"size",prod.image_array.size,"time",time.time()-start,"N srcs",len(src_cl_list),len(cl_list))
        final_src_cl_list.extend(build_source_clusters(len(final_src_cl_list)+1,
                                                       cl_list,
                                                       offset_x=prod.offset_x,
                                                       offset_y=prod.offset_y,
                                                       original_flux=original_flux,
                                                       verbose=verbose))

    #seg_map = np.zeros(image._shape, dtype=np.int32)

    #print("|seg map shape", seg_map.shape)
    #for cl in cl_list[:]:
    #    seg_map[[cl.position_array[:,1].astype(np.int32), cl.position_array[:,0].astype(np.int32)]] = cl.ID + 1
    #print("|seg map done")

    #final_src_cl_list=do_from_seg_map(original_flux,seg_map)
    image._data=original_flux

    #print ('|done',time.time()-t)
    return final_src_cl_list,K,final_selected_coords

def do_from_seg_map(image_array,
                seg_map,
                seg_map_bkg_val=0):


    #print("|method: from seg map")



    conn=ConnectedComponents(1)
    #ID_list=np.arange(seg_map[seg_map!=seg_map_bkg_val].max()+1)
    #print('ID_list', ID_list,seg_map.max())

    #To preserve same id as in segmap
    #_src_cl_list=[]
    #_s=np.copy(seg_map)
    #   ID_list=ID_list[ID_list!=seg_map_bkg_val]
    #for ID in ID_list:
    #    msk=seg_map!=ID
    #   _s[msk]=seg_map_bkg_val
    conn.run(image_array,seg_map_bkg_val,seg_map=seg_map,seg_map_bkg=seg_map_bkg_val)
    #    _s[msk]=seg_map[msk]
    #    for c in conn.clusters_list:
    #        c.ID=np.int(ID)
    #    _src_cl_list.extend(conn.clusters_list)

    final_src_cl_list=build_source_clusters(0,conn.clusters_list)


    return final_src_cl_list

def do_image_segmentation(image,
                          bkg_threshold,
                          method='dbscan_binned',
                          dbscan_eps=1.0,
                          K=1.5,
                          K_pix=False,
                          max_image_size=1E6,
                          min_sub_stamp_size=25,
                          verbose=False,
                          plot=False,
                          input_seg_map=None,
                          seg_map_bkg_val=0,
                          image_id=None,
                          min_pix_size=None,
                          use_only_central_sub_stamp=False):

    """

    Task function to perform image segmentation


    user guide: :ref:`image_segmentation_task_user_guide`

    Parameters
    ----------
    image : :class:`~asterism.core.image_manager.image.Image` object
        the image used to estimate the bkg

    method : string
       segmentation method  among : 'dbscan','dbscan_binned','connected'

    bkg_threshold : float
        value of the background threshold

    dbscan_eps : float
        radius of the dbscan scanning brush

    K : float
        sets the segmentation threshold as K*bkg_threshold

    K_pix : bool
      if True,  total flux in the dbscan bursh is divided by the number of pixels in the brush, and the compared to K*bkg_threshold

    dbscan_buffer_size : float

    max_image_size : int
        if is not None, the image is segmented in substamps

    min_sub_stamp_size : int
        the minimum substamp size to perform segmentation

    verbose : bool

    plot_dbscan : bool

    input_seg_map : 2dim numpy array
        segmentation map

    seg_map_bkg_val : int (default 0)
        value for the bkg pixel in the seg map

    image_id : int

    use_only_central_sub_stamp : bool

    Returns
    -------
    src_cl_list : list
        list of cluster sources

    K : float
     th value used for the detection

    selected_coords : numpy 2dim array
        array storing in the first column the x, and in the second column the y
        of the coordinates of the input image that were not masked

    """
    if bkg_threshold is None:
        raise RuntimeError("bkg_threshold can not be None")

    allowed_list=['dbscan','dbscan_binned','connected']
    #print ('seg_map',input_seg_map)
    if verbose ==True:
        print("|method:",method)

    if input_seg_map is None and method!='from_seg_map':

        src_cl_list,K,selected_coords=from_image_detection(allowed_list,
                                                           method,
                                                           bkg_threshold,
                                                           image,
                                                           max_image_size,
                                                           min_sub_stamp_size,
                                                           dbscan_eps,
                                                           K,
                                                           K_pix,
                                                           verbose,
                                                           plot,
                                                           use_only_central_sub_stamp=use_only_central_sub_stamp,
                                                           min_pix_size=min_pix_size)
    elif input_seg_map is not None:

        src_cl_list=do_from_seg_map(image.array,input_seg_map,seg_map_bkg_val=seg_map_bkg_val)
        K=None
        selected_coords=None

    elif input_seg_map is None and method=='from_seg_map':
        raise RuntimeError(
            "if  you set method to 'from_seg_map', you must provide a seg_map")
    else:

        raise RuntimeError("either you provide a seg_map file and you set method to 'from_seg_map',  or yuo choose one of the following %s"%allowed_list)



    if plot is True:
        from asterism.plotting.plot_tools import show_image,plot_contour
        import pylab as plt
        im,ax=show_image(image.array)


        for cl in src_cl_list:
            if hasattr(cl,'contour_shape'):

                if cl.contour_shape is not None:
                    plot_contour(ax,cl.contour_shape[:,0],cl.contour_shape[:,1],'w')

            plt.text(cl.x_c,cl.y_c,'%s'%cl.ID)
            ax.scatter(cl.x_c,cl.y_c,marker='s', edgecolor='black', linewidth='0',facecolor='white',s=10)

        plt.show()




    return src_cl_list,K,selected_coords




class DoImageSegmentationTask(AnalysisTask):
    """
    Class that implements the image segmentation Task

    user guide: :ref:`image_segmentation_task_user_guide`
    """
    def __init__(self, name='image_segmentation', func=do_image_segmentation, parser=None, process=None):



        super(DoImageSegmentationTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-method',type=str, help='method: dbscan or dbscan_binned, from_seg_map',default='dbscan_binned')


        self.parameters_list.add_par('-bkg_threshold', type=float, help='', default=None,to_conf_file=False)
        self.parameters_list.add_par('-K', type=float, help=''' sets the segmentation threshold as K*bkg_threshold ''', default=1.5)
        self.parameters_list.add_par('-K_pix', help='if True,  total flux in the dbscan bursh is divided by the number of pixels in the brush, and the compared to K*bkg_threshold', action='store_true')
        self.parameters_list.add_par('-seg_map_bkg_val',  type=int ,help='value for the bkg pixels in the seg map (default 0)',default=0)
        self.parameters_list.add_par('-min_pix_size', type=int, help='''min size for accepted cluster ''',default=25)
        self.parameters_list.add_par('-dbscan_eps', type=float, help='the scanning brush radius for the dbscan. (In the case of binned, it is the size of the scanning box)', default=1.0)


        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')

        self.parameters_list.add_par('-plot',help='plot the segmentation results',action='store_true')

        self.parameters_list.add_par('-max_image_size',type=np.int ,help='if is not None, the image is segmented in substamps, according to bkg_threshold',default=1000000)
        self.parameters_list.add_par('-min_sub_stamp_size',type=np.int ,help='the minimum substamp size, if smaller the sub stamp will be rejected',default=25)

        self.parameters_list.add_par('-use_only_central_sub_stamp', help=' if True, only the central source is returned',action='store_true')







        self.func=func

