"""

This module provides the implementation of the :class:`.DoSourceCatalogTask` class
used to create the catalog of the detected components
The algorithm in of the task is implemented by the :func:`.do_source_catalog`




Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.analysis_pipelines.morphometric_features_extraction.DoSourceCatalogTask



Summary
---------
.. autosummary::
   do_source_catalog
   DoSourceCatalogTask


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import numpy as np
from ....core.clustering.cluster_tools import get_cluster_closest_to_position
from ....pipeline_manager.analysis_tasks import AnalysisTask
from .image_segmentation import build_seg_map
__author__ = 'andrea tramacere'


def do_source_catalog(image,
                      cl_list,catalog_name='out_catalog.reg',
                      out_catalog=False,
                      out_seg_map=False,
                      out_seg_map_overlap=False,
                      significance_th=None,
                      min_pix_size=None,
                      max_cat_size=None,
                      image_id=0,
                      get_only_central_source=False,
                      get_only_central_debl_source=False,
                      from_seg_map=False,
                      override_id=False,
                      file_name=''):

    """

    Parameters
    ----------
    image
    cl_list
    catalog_name
    out_catalog
    out_seg_map
    significance_th
    min_pix_size
    image_id
    get_only_central_source

    Returns
    -------

    """
    #print("|initial number of detected sources",len(cl_list))
    _cl_list_full=cl_list[::]




    if significance_th is not None:
        #print("|filtering for significance_th",significance_th)
        for cl in cl_list[:]:
            if cl.significance<significance_th:
                cl_list.remove(cl)

    if min_pix_size is not None:
        #print("|filtering for min_pix_size",min_pix_size)
        for cl in cl_list[:]:
            if cl.cl_len<min_pix_size:
                cl_list.remove(cl)


    if max_cat_size is not None:
        #print("|filtering for min_pix_size",min_pix_size)
        if len(cl_list)>max_cat_size:
            selected=np.random.choice(len(cl_list), max_cat_size)
            for ID,cl in enumerate(cl_list[:]):
                if ID not in selected:
                    cl_list.remove(cl)



    if get_only_central_source==True:
        #The source at the center of the image is used
        central_cluster=get_cluster_closest_to_position(cl_list,x_c=image._center_x,y_c=image._center_y)
        if central_cluster is not None:
            cl_list=[central_cluster]
        else:
            cl_list=[]
        #print("|retaining only the central one",len(cl_list))


    region_catalog=None
    catalog=None
    off_set_id=0

    #if from_seg_map is True:
    #    off_set_id = 0
    #else:
    #    off_set_id = 1

    #if override_id is True:
    #    off_set_id = 1

    print ('off_set_id',off_set_id,'from_seg_map',from_seg_map,'override_id',override_id)



    if out_catalog is True:
        #print("|out catalog:",catalog_name)
        dtype_list=[]
        dtype_list.append(('file_name', file_name.dtype))
        dtype_list.append(('image_ID', np.int32))
        dtype_list.append(('src_ID',np.int32))
        dtype_list.append(('parent_ID', np.int32))
        dtype_list.append(('x_c',np.float32))
        dtype_list.append(('y_c',np.float32))
        dtype_list.append(('sig_x',np.float32))
        dtype_list.append(('sig_y',np.float32))
        dtype_list.append(('alpha',np.float32))
        dtype_list.append(('flux_tot',np.float32))
        dtype_list.append(('r_cluster', np.float32))
        dtype_list.append(('r_max', np.float32))
        dtype_list.append(('r_mean', np.float32))
        dtype_list.append(('h', np.float32))
        dtype_list.append(('signif_ave', np.float32))
        dtype_list.append(('denc_pd_ratio', np.float32))
        dtype_list.append(('denc_pb_ratio', np.float32))
        dtype_list.append(('denc_prandom_ratio', np.float32))
        dtype_list.append(('children_p', np.float32))
        dtype_list.append(('children_p_random', np.float32))
        dtype_list.append(('bkg_p', np.float32))
        dtype_list.append(('bkg_p_mean', np.float32))
        dtype_list.append(('n_points', np.int32))
        dtype=np.dtype(dtype_list)
        catalog=np.zeros(len(cl_list),dtype=dtype)

        region_catalog=''





        for ID,cl in enumerate(cl_list):
            #print('ID, cl.ID', ID, cl.ID)
            if override_id is True:
                cl.ID=ID+1

            #print ('ID, cl.ID',ID,cl.ID)

            if cl.parent_ID is None:
                parent_ID = -1
            else:
                parent_ID = cl.parent_ID


            region_catalog+='physical; ellipse %f %f %f %f %f # text = {%d} \n'%( cl.x_c, cl.y_c , cl.sig_x,cl.sig_y,cl.semi_major_angle,(cl.ID+1))
            if image_id is None:
                image_id=0

            for attr in ['h','signif_ave','denc_pb_ratio','denc_pd_ratio','denc_prandom_ratio','children_p','children_p_random','bkg_p','bkg_p_mean']:
                if hasattr(cl,attr):
                    pass
                else:
                    setattr(cl,attr,'-1')
                    #cl.h=None


            tuple_val=(file_name,
                       image_id,
                       cl.ID ,
                       parent_ID,
                       cl.x_c,
                       cl.y_c,
                       cl.sig_x,
                       cl.sig_y,
                       cl.semi_major_angle,
                       cl.flux.sum(),
                       cl.r_cluster,
                       cl.r_max,
                       cl.r_mean,
                       cl.h,
                       cl.signif_ave,
                       cl.denc_pb_ratio,
                       cl.denc_pd_ratio,
                       cl.denc_prandom_ratio,
                       cl.children_p,
                       cl.children_p_random,
                       cl.bkg_p,
                       cl.bkg_p_mean,
                       cl.n_points)

            catalog[ID]=tuple_val
            #print (cl.ID,cl.parent_ID)
    #print ('CICCIO catalog.py',catalog)
    #print("|final number of detected sources",len(cl_list))


    if out_seg_map is True:

        seg_map,seg_map_overlap=build_seg_map(image, _cl_list_full, off_set_id, out_seg_map_overlap=True)
    else:
        seg_map_overlap=None
        seg_map= None
    print('|total found',len(_cl_list_full))
    return seg_map,seg_map_overlap,catalog,region_catalog,cl_list

class DoSourceCatalogTask(AnalysisTask):

    def __init__(self, name='image_segmentation', func=do_source_catalog, parser=None, process=None):



        super(DoSourceCatalogTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-override_id', help='recompute the id not matching seg map anymore',
                                     action='store_true')

        self.parameters_list.add_par('-max_cat_size',  type=np.int ,help='max sources in the catalog, if exceed randomly picked',default=None )
        self.parameters_list.add_par('-min_pix_size',  type=np.int ,help='min source size in pixels',default=None )
        self.parameters_list.add_par('-significance_th',  type=np.float ,help='th for source significance',default=None )
        self.parameters_list.add_par('-out_catalog',help='output catalog to file',action='store_true')
        self.parameters_list.add_par('-out_seg_map',help='output segmentation map to file',action='store_true')
        self.parameters_list.add_par('-get_only_central_source',help='gets only the central source',action='store_true')
        self.parameters_list.add_par('-get_only_central_debl_source',help='gets only the central source',action='store_true')

        self.func=func