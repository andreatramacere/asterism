"""
This modules provides the implementation of the  Task class :class:`.DoSetBkgThreshTask`
used to estimate the background level in images.
The Task algorithm is implemented by the func :func:`.set_thresh`


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::
    DoSetBkgThreshTask


Summary
---------
.. autosummary::
   set_thresh
   DoSetBkgThreshTask



Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np

from astropy.stats import sigma_clipping,sigma_clipped_stats

from ....pipeline_manager.analysis_tasks import AnalysisTask
from ....core.stats.utils.array_stats import eval_bkg_stats,eval_skew_minimization_stats
from ....core.clustering.connectivity_based.connected_components import ConnectedComponents


__author__ = 'andrea tramacere'



def set_thresh(image,
               n_sigma_sclip=3.5,
               n_std_th=4.5,
               #min_size_ratio=0.1,
               sub_block_frac_size=None,
               use_fixed_th=None,
               plot=False,
               verbose=False,
               rms_map=None,
               resampling_size=1000000):
    """
    Function to estimate the backgorund threshold in a given image.

    * If ``sub_block_frac_size`` is provided, the image is partitioned in N-blocks with size=sub_block_frac_size*image,
      and the block with the lowest flux is used to estimate the bkg level. Otherwise, the full image is used.

    * If ``skewness_range`` is provided, then the method is ste to the skewness is minimization. The skewness is minimized
      using the function :func:`~asterism.core.stats.utils.array_stats.minimize_skewness`. The data are clipped over the
      range :math:`|m-s1,m+s1|`,  where `m` is the mean of the data, and `s1=std(data)*sigma_range`.
      The data are clipped in the range providing the minimum skewness .

    * If ``threshold`` is provided, then the backgorund level and the variance are directly evaluated from the background
      pixels

    * If ``resampling_size`` the bkg is estimated from a random resampling of the full image (`sub_block_frac_size` is None) or
      of the sub image (`sub_block_frac_size` is not None). The resampling is performed only if the nubmber of pixels in
      the image is greater than `resampling_size`



    * ``n_std_th`` is used to set the background threshold according to **bkg_th=bkg_level+n_std_th * bkg_std** where:
         * ``bkg_std`` is evaluated from the median absolute deviation (MAD) (clipped or not according to method) of the background pixels
         * ``bkg_level`` is the mean of the of the background pixels (clipped or not according to method)


    Parameters
    ----------
    image : :class:`~asterism.core.image_manager.image.Image` class object
    n_std_th : float (Optional)
        see above for description
    min_size_ratio :float (Optional)
        see above for description
    sub_block_frac_size : float (Optional)
        see above for description
    resampling_size : float (Optional)
        see above for description
    plot : bool (Optional)
    verbose : bool (Optional)

    Returns
    -------
    bkg_th : float
        bkg_th=bkg_level+n_std_th * bkg_sig
    bkg_level : float
        the mean of the flux in the bkg pixels
    bkg_sig : float
        the median absolute deviation (MAD) of the flux in the background pixels



    """
    if verbose == True:
        print("|setting threshold")
    if use_fixed_th is not None:
        th=use_fixed_th
        bkg_mean=use_fixed_th,
        bkg_sig=None,
        if verbose == True:
            print("|bkg_th", th)
    else:
        if verbose == True:
            print("|image flux min",image.array.min(),"image flux max",image.array.max())




        if plot==True:
            import pylab as plt
            f, ax = plt.subplots(2, )

        if sub_block_frac_size is not None:
            if verbose == True:
                print ("|using sub blocks")
            flux_bkg=image.get_lowest_flux_array_from_sub_stamp(sub_block_frac_size)
        else:
            flux_bkg=image.valid_flatten

        if verbose == True:
            print("|size", flux_bkg.size,image.array.size)
        if resampling_size is not None:
            if flux_bkg.size>resampling_size:
                flux_bkg=np.random.choice(flux_bkg,size=resampling_size,replace=False)

            flux_bkg=np.sort(flux_bkg)

        #flux_bkg = sigma_clipping.sigma_clip(flux_bkg, sigma=n_sigma_sclip, maxiters=5, sigma_lower=n_sigma_sclip)


        bkg_mean_pre, bkg_median_pre,bkg_sig_pre, = sigma_clipped_stats(flux_bkg,sigma=n_sigma_sclip)
        th_pre = bkg_mean_pre+(bkg_sig_pre*n_std_th)
        if verbose==True:
            print("|pre src removal")
            print("|bkg_lvl", bkg_mean_pre)
            print("|bkg_std", bkg_sig_pre)
            print("|bgk_median", bkg_median_pre)
            print("|")
        if plot == True:
            th_mean_pre=np.mean(th_pre)
            bkg_sig_mean=np.mean(bkg_sig_pre)
            test_plot(ax[0],
                      flux_bkg,
                      bkg_mean_pre,
                      bkg_sig=bkg_sig_mean,
                      th=th_mean_pre,
                      title='pre sigma clip')



        #print('cluster detection start',flux_bkg,th_pre)
        conn = ConnectedComponents(1)
        conn.run(image.array, th_pre,masked=image.masked,skip_cluster_building=True)
        if conn.bkg_blobs_val is not None:
            if image.array[conn.blobs_labels==conn.bkg_blobs_val].size>=image.valid_flatten.size:
                flux_bkg=image.array[conn.blobs_labels==conn.bkg_blobs_val].flatten()

        #print('cluster detection done')
        if resampling_size is not None:
            if flux_bkg.size>resampling_size:
                flux_bkg=np.random.choice(flux_bkg,size=resampling_size,replace=False)

            flux_bkg=np.sort(flux_bkg)

        #flux_bkg = sigma_clipping.sigma_clip(flux_bkg, sigma=n_sigma_sclip, iters=5, sigma_lower=n_sigma_sclip)

        bkg_mean, bkg_median,bkg_sig= sigma_clipped_stats(flux_bkg,sigma=n_sigma_sclip)
        if verbose==True:
            print("|post src removal")
            #print("|size", size)
            print("|bkg_lvl", bkg_mean)
            print("|bkg_std", bkg_sig)
            print("|bgk_median", bkg_median)
            print("|")


        if rms_map is not None:
            bkg_sig = rms_map
            if verbose == True:
                print("|using rms map")
                print("|average bkg_std in rms map", np.mean(rms_map))

        th = bkg_mean + n_std_th * bkg_sig

        #th=(2.5 * bkg_median) - (1.5 * bkg_mean)

        if verbose == True:
            print("|th shape",np.shape(th))
            print("|bkg_th (mean value if rms map is provided)", np.mean(th))
            print("|-")

        if plot==True:
            th_mean=np.mean(th)
            bkg_sig_mean=np.mean(bkg_sig)
            test_plot(ax[1],
                      flux_bkg,
                      bkg_mean,
                      bkg_sig=bkg_sig_mean,
                      th=th_mean,
                      title='post sigma-clip')

            plt.tight_layout()
            plt.show()

    #


    return th,bkg_mean,bkg_sig



def test_plot(ax,
              flux_bkg,
              bkg_lvl,
              bkg_sig=None,
              th=None,
              title=None):

    if ax is not None:
        ax.set_title(label=title)

    n, bins, patches =ax.hist(flux_bkg, 20)
    if th is not None:
        ax.plot([th,th],[0,n.max()],label='th=%f'%th)
    ax.plot([bkg_lvl + bkg_sig, bkg_lvl + bkg_sig], [0, n.max()], label='bkg lvl+sig=%f'%(bkg_lvl + bkg_sig))
    ax.plot([bkg_lvl, bkg_lvl], [0, n.max()], label='bkg lvl=%f' % bkg_lvl)

    ax.legend()



class DoSetBkgThreshTask(AnalysisTask):

    def __init__(self,name='bkg_treshold',func=set_thresh,parser=None):
        super(DoSetBkgThreshTask,self).__init__(name=name,func=func,parser=parser)
        self.parameters_list.add_par('-sub_block_frac_size',
                                     type=np.float ,
                                     help='''if provided, the bkg pixels are extracted from the sub-block with lowest integrated flux, having a size equal
                                          to sub_block_frac_size time the image size''',
                                     default=None)

        #self.parameters_list.add_par('-min_size_ratio',
        #                             type=np.float,
        #                             default=0.1,
        #                             help='''provides the minimum size to stop the clipping in the skewness minimization''')

        self.parameters_list.add_par('-n_std_th',
                                     type=np.float ,
                                     help='''number of bakground deviation to set threshold, th=bkg_level+n_std_th*bkg_sig''',
                                     default=4.5 )

        self.parameters_list.add_par('-resampling_size',
                                     type=np.int ,
                                     help='''if the image pixels number>resampling_size,
                                            then a random resampling of pixels is performed, with a final number= resampling_size''',
                                     default=1000000)

        self.parameters_list.add_par('-n_sigma_sclip',
                                     type=np.float,
                                     help='''if not None, |skewness| is minimized in the range |bkg_lvl,bgk_lvl+skewness_range*bkg_std| ''',
                                     default=3.0 )

        self.parameters_list.add_par('-use_fixed_th',
                                     type=np.float,
                                     help='''if not None, this value is used as th''',
                                     default=None)

        self.parameters_list.add_par('-plot',  help='', action='store_true')
        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')
