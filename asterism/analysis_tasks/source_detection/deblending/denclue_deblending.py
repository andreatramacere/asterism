"""
Overview
--------
This module provides the implementation of the :class:`Table`

Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::
    DoDENCLUEDeblendingTask



mary::
    Table


Summary
---------
.. autosummary::
    :nosignatures:

    DoDENCLUEDeblendingTask
    do_denclue_deblending
    do_denclue_of_cluster
    gauss_laplace_down_sampling

Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import copy

import numpy as np
from skimage.morphology import watershed
from sklearn.cluster import spectral_clustering
from scipy import ndimage

from .deblending import DeblendedProducts, test_plot
from .deblending_downsample import set_coordinates_and_target
from ....core.clustering.density_based.denclue import DENCLUE
from ....pipeline_manager.analysis_tasks import AnalysisTask
from ....core.image_processing.features import BlobDetectionGaussLaplace
from ....core.geometry.distance import dist_eval
from ....core.clustering.connectivity_based.connected_components import ConnectedComponents
from ....core.image_processing.filters import GaussianFilter

__author__ = 'andrea tramacere'


def azimuthalAverage(image, center=None):
    """
    Calculate the azimuthally averaged radial profile.

    image - The 2D image
    center - The [x,y] pixel coordinates used as the center. The default is
             None, which then uses the center of the image (including
             fracitonal pixels).

    """
    # Calculate the indices from the image
    y, x = np.indices(image.shape)

    if not center:
        center = np.array([(x.max() - x.min()) / 2.0, (x.max() - x.min()) / 2.0])

    r = np.hypot(x - center[0], y - center[1])

    # Get sorted radii
    ind = np.argsort(r.flat)
    r_sorted = r.flat[ind]
    i_sorted = image.flat[ind]

    # Get the integer part of the radii (bin size = 1)
    r_int = r_sorted.astype(int)

    # Find all pixels that fall within each radial bin.
    deltar = r_int[1:] - r_int[:-1]  # Assumes all radii represented
    rind = np.where(deltar)[0]  # location of changed radius
    nr = rind[1:] - rind[:-1]  # number of radius bin

    # Cumulative sum to figure out sums for each radius bin
    csim = np.cumsum(i_sorted, dtype=float)
    tbin = csim[rind[1:]] - csim[rind[:-1]]

    radial_prof = tbin / nr

    return radial_prof


def do_psd(arr, sim_scale, image=None):
    import pylab as plt

    from scipy import fftpack
    # arr=np.log10(arr+arr.min()+1)
    # print ('shape',arr)
    if arr.ndim == 2:

        F1 = fftpack.fft2(arr)

        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        F2 = fftpack.fftshift(F1)

        # Calculate a 2D power spectrum
        psd2D = np.abs(F2) ** 2

        # Calculate the azimuthally averaged 1D power spectrum
        psd1D = azimuthalAverage(psd2D)
        size = psd1D.size
    else:
        # size=np.int(arr.size*0.5)
        # print ('size',size)
        F1 = fftpack.fft(arr)

        # Now shift the quadrants around so that low spatial frequencies are in
        # the center of the 2D fourier transformed image.
        # F2 = fftpack.fftshift( F1 )

        psd1D = np.abs(F1) ** 2

    # print ('F1',psd1D.shape,arr.size)
    sample_freq = fftpack.fftfreq(size * 2 + 1)
    sample_freq = sample_freq[sample_freq > 0]
    if sim_scale is None:
        sim_scale = min(image.shape)

    scales = 1.0 / sample_freq
    # pidxs = scales<sim_scale*0.5
    pidxs = scales > 0
    print(sample_freq.size, psd1D.size)
    psd1D = psd1D[pidxs][::-1]
    scales = scales[pidxs]

    h_scale_1 = None
    h_scale_2 = None
    if pidxs.sum() > 0:

        i = psd1D.cumsum()
        i = i / i.max()

        # print ('i',i)
        ID_1 = np.argwhere(i > 0.10).min()
        ID_2 = np.argwhere(i < 0.99).max()

        print('ID', ID_1, ID_2)
        print('scale', scales[ID_1], scales[ID_2], 'sim', sim_scale)
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
        if arr.ndim == 2:
            ax1.imshow(arr)
        else:
            ax1.plot(arr)
            if image is not None:
                ax3.imshow(image, vmax=image.max() * 0.5)

        ax2.semilogy(scales, psd1D / psd1D.max())
        ax2.semilogy(scales, i)
        plt.show()
        h_scale_1 = scales[ID_1]
        h_scale_2 = scales[ID_2]

    return h_scale_1, h_scale_2


def test_fft(image):
    import pylab as plt

    from scipy import fftpack
    F1 = fftpack.fft2(image)

    # Now shift the quadrants around so that low spatial frequencies are in
    # the center of the 2D fourier transformed image.
    F2 = fftpack.fftshift(F1)

    # Calculate a 2D power spectrum
    psd2D = np.abs(F2) ** 2

    # Calculate the azimuthally averaged 1D power spectrum
    psd1D = azimuthalAverage(psd2D)
    x, y = image.shape
    sample_freq = fftpack.fftfreq(x)

    pidxs = np.where(sample_freq > 0)

    freqs = sample_freq[pidxs]
    psd1D = psd1D[pidxs]

    i = psd1D.cumsum()
    i = i / i.max()
    print('freqs', 1.0 / freqs)
    ID = np.argwhere(i > 0.99).min()
    print('ID', ID)
    print('scale', 1.0 / freqs[ID])
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(image)
    ax2.semilogy(psd1D)
    plt.show()


class DenclueDeblender(object):

    def __init__(self, parent_cluster,
                 kernel,
                 eps,
                 attr_dbs_K,
                 k_table_size,
                 R_max_frac,
                 R_max_kern_th,
                 max_calls_method,
                 max_calls_par,
                 scale_finder,
                 bkg_std,
                 pure_denclue=True,
                 do_downsampling=False,
                 down_sampler=None,
                 verbose=False):

        self.pure_denclue = pure_denclue

        if R_max_frac is not None:
            R_max_method = 'fixed'
            R_max = R_max_frac * parent_cluster.r_cluster

        elif R_max_kern_th is not None:
            R_max_method = 'kern_rel_th'
            R_max = None
        else:
            R_max_method = 'auto'
            R_max = None

        _coords = parent_cluster.cartesian.coords
        _weight = parent_cluster.pts_weight

        lm_xy, lm_f, lm_radii, h = scale_finder.run(parent_cluster,bkg_std, verbose=verbose)

        self.down_sampler = down_sampler
        self.do_downsampling = do_downsampling

        if do_downsampling == True and down_sampler is not None:
            # _lm_xy=copy.deepcopy(lm_xy)
            # _lm_f = copy.deepcopy(lm_f)
            if lm_xy is not None:
                _lm_xy = lm_xy[:]
            else:
                _lm_xy = None

            if lm_f is not None:
                _lm_f = lm_f[:]
            else:
                _lm_f = None

            _coords, _weight, _t_coords, _t_weight = down_sampler.run(parent_cluster,
                                                                      h,
                                                                      local_maxima_xy=_lm_xy,
                                                                      local_maxima_flux=_lm_f,
                                                                      pure_denclue=self.pure_denclue)



        else:
            _t_coords = None
            _t_weight = None

        self.h = h
        self.R_max = R_max
        self.eps = eps
        self.coords = _coords
        self.weight = _weight
        self.t_coords = _t_coords
        self.t_weight = _t_weight
        self.lm_xy = lm_xy
        self.lm_f = lm_f
        self.lm_radii = lm_radii
        self.kernel=kernel
        self.denclue_debl = DENCLUE(eps=eps,
                                    h=h,
                                    dbscan_K=attr_dbs_K,
                                    kernel=kernel,
                                    k_table_size=k_table_size,
                                    R_max_method=R_max_method,
                                    R_max_th_rel=R_max_kern_th,
                                    R_max=R_max,
                                    max_calls_method=max_calls_method,
                                    max_calls_par=max_calls_par)

        if verbose == True:
            print('|R_max,R_max_method,R_max_frac', R_max, R_max_method, R_max_frac)
            print("|max_calls_method", max_calls_method)
            print("|eps", eps)
            print("|h,h_min,h_max,h_frac", h)

    def run(self, parent_cluster, mask_unchanged, digitize_attractors, verbose, attractors_th=None,multiprocessing_th=500):
        if verbose == True:
            print('|*DenclueDeblender*')
        self.denclue_debl.run(self.coords,
                              attractors_th=attractors_th,
                              weight_array=self.weight,
                              mask_unchanged=mask_unchanged,
                              R_max=self.R_max,
                              make_convolved_image=True,
                              digitize_attractors=digitize_attractors,
                              target_coordinates=self.t_coords,
                              point_type=parent_cluster.point_type,
                              target_coordinates_w_array=self.t_weight,
                              verbose=verbose,
                              multiprocessing_th=multiprocessing_th)
        off_x = None
        off_y = None
        parent_cluster_image = None
        # import  pylab as plt
        # plt.imshow(self.denclue_debl.convolved_image)
        # plt.show()
        # if verbose == True:
        #    print('|')
        parent_cluster.h = self.h
        parent_cluster.kernel = self.kernel
        return self.denclue_debl.clusters_list, off_x, off_y, parent_cluster_image





class DenclueSpectralDeblender(DenclueDeblender):
    def __init__(self, *args, **kwargs):
        super(DenclueSpectralDeblender, self).__init__(*args, pure_denclue=False, **kwargs)

    def run(self, parent_cluster, mask_unchanged, digitize_attractors, verbose, attractors_th=None):
        if verbose == True:
            print('|*DenclueSpectralDeblender*')
        # if self.t_coords is None:
        #    _coords=self.coords
        #    _weight_array=self.weight
        # else:
        #    _coords=self.t_coords
        #    _weight_array=self.t_weight

        self.denclue_debl.run(self.coords,
                              weight_array=self.weight,
                              mask_unchanged=mask_unchanged,
                              R_max=self.R_max,
                              make_convolved_image=False,
                              attractors_th=attractors_th,
                              digitize_attractors=digitize_attractors,
                              target_coordinates=self.t_coords,
                              point_type=parent_cluster.point_type,
                              target_coordinates_w_array=self.t_weight,
                              verbose=verbose)

        # from .denclue_deblending import  denclue_plot
        # denclue_plot(denclue_debl,parent_cluster)

        parent_cluster_image, off_x, off_y, masked_pixels = parent_cluster.get_cluster_image_array(
            bkg=parent_cluster.flux.min())
        # print('|*DenclueWatershedDeblender*')
        if verbose == True:
            print('|cluster of attractors found from DENCLUE', len(self.denclue_debl.attractors_clusters_list))
        # test_fft(parent_cluster_image)

        from sklearn.feature_extraction import image

        img = parent_cluster_image.astype(float)
        # masked_pixels=img<=parent_cluster.flux.min()
        # print(masked_pixels.shape, img.shape)
        graph = image.img_to_graph(img, mask=~masked_pixels)
        label_im = np.zeros(img.shape, dtype=np.int)
        # graph.data = np.exp(-graph.data / graph.data.std())
        # graph.data = np.gradient(graph.data / graph.data.std())
        # graph.data[np.isfinite(graph.data)]=0.
        if masked_pixels.sum() > 0:
            labels = spectral_clustering(graph, n_clusters=len(self.denclue_debl.attractors_clusters_list),
                                         n_components=len(self.denclue_debl.attractors_clusters_list) + 1)
            # print('spectral clusters',len(labels),labels.max())
            labels += 1
            # print('spectral clusters', len(labels),labels.min(), labels.max())

            # print('label_im', label_im.min(), label_im.max(),label_im.size)

            label_im[~masked_pixels] = labels
        else:
            label_im[~masked_pixels] = 1

        # print('label_im', label_im.min(), label_im.max(), label_im.size)
        # print('label_im', mask.sum(),label_im.min(), label_im.max())
        # import pylab as plt
        # plt.matshow(~masked_pixels)
        # plt.show()
        # plt.matshow(label_im)
        # plt.show()
        # label_im = -np.ones(mask.shape)
        # labels[mask] = labels

        # print("off_x,off_y", off_x, off_y,parent_cluster_image.shape)
        # markers = np.zeros(parent_cluster_image.shape)
        # m = 1
        # print ('shape',markers.shape,parent_cluster_image.shape)
        # for cl in self.denclue_debl.attractors_clusters_list:
        #    x_c = np.int32(cl.x_c)
        #    y_c = np.int32(cl.y_c)
        #    print('x_c,y_c',)
        #    if parent_cluster.check_is_in_cluster(x_c, y_c):
        #        markers[y_c - off_y, x_c - off_x] = m
        #        m += 1
        #        print ('markers',y_c - off_y, x_c - off_x)
        # labels = watershed(-parent_cluster_image, markers, mask=~masked_pixels, compactness=0.)

        # if label_im.max()>1:
        conn = ConnectedComponents(1)
        conn.run(parent_cluster_image, -1, seg_map=label_im, seg_map_bkg=0)
        children_clusters_list = conn.clusters_list
        for cl in children_clusters_list:
            cl.position_array += [off_x, off_y]
        # else:
        #    children_clusters_list=[parent_cluster]
        if verbose == True:
            print('|cluster of sub clusters found from SPECTRAL CLUSTERING', len(children_clusters_list))
            print('|')

        return children_clusters_list, off_x, off_y, parent_cluster_image


class DenclueWatershedDeblender(DenclueDeblender):
    def __init__(self, *args, **kwargs):
        super(DenclueWatershedDeblender, self).__init__(*args, pure_denclue=False, **kwargs)

    def run(self, parent_cluster, mask_unchanged, digitize_attractors, verbose, watershed_compactness=0.,
            attractors_th=None,multiprocessing_th=500):

        if verbose == True:
            print('|*DenclueWatershedDeblender pre-downsampling*', self.denclue_debl.dbscan_K)

        if self.down_sampler is not None and  self.do_downsampling is True:
            self.denclue_debl.dbscan_K = int(max(1,  self.denclue_debl.dbscan_K / int(self.h ** 2)) )

        if verbose == True:
            print('|*DenclueWatershedDeblender post-downsampling*', self.denclue_debl.dbscan_K)

        self.denclue_debl.run(self.coords,
                              weight_array=self.weight,
                              mask_unchanged=mask_unchanged,
                              R_max=self.R_max,
                              attractors_th=attractors_th,
                              make_convolved_image=False,
                              digitize_attractors=digitize_attractors,
                              target_coordinates=self.t_coords,
                              point_type=parent_cluster.point_type,
                              target_coordinates_w_array=self.t_weight,
                              verbose=verbose,
                              multiprocessing_th=multiprocessing_th)

        parent_cluster_image, off_x, off_y, masked_pixels = parent_cluster.get_cluster_image_array(
            bkg=parent_cluster.flux.min())
        if verbose == True:
            print('|*DenclueWatershedDeblender*')
            print('|cluster of attractors found from DENCLUE', len(self.denclue_debl.attractors_clusters_list))
        # test_fft(parent_cluster_image)

        markers = np.zeros(parent_cluster_image.shape)
        m = 1
        for cl in self.denclue_debl.attractors_clusters_list:

            x_c = np.int32(cl.x_c)
            y_c = np.int32(cl.y_c)
            if parent_cluster.check_is_in_cluster(x_c, y_c):
                markers[y_c - off_y, x_c - off_x] = m
                m += 1
            if verbose == True:
                print('|cl ID,', cl.ID, 'size', cl.n_points)

        conn = ndimage.generate_binary_structure(2, 2)
        # print('CICCIO',watershed_compactness)
        labels = watershed(-parent_cluster_image, markers, mask=~masked_pixels, compactness=watershed_compactness,
                           connectivity=conn)

        conn = ConnectedComponents(1)
        conn.run(parent_cluster_image, -1, seg_map=labels, seg_map_bkg=0)
        children_clusters_list = conn.clusters_list
        parent_cluster.h=self.h
        parent_cluster.kernel=self.kernel
        for cl in children_clusters_list:
            cl.position_array += [off_x, off_y]
            cl.h=self.h
            cl.kernel=self.kernel
            # print(print('cl.cartesian.x.astype(np.int32)', cl.ID, cl.position_array[:, 1].min()))
        if verbose == True:
            print('|cluster of sub clusters found from WATERSHED', len(children_clusters_list))
            print('| off_x, off_y', off_x, off_y)
            print('|')

        # print('target',self.t_coords.shape,self.t_coords)

        return children_clusters_list, off_x, off_y, parent_cluster_image


def do_morphological_correction(clusters_list, parent_cluster, parent_cluster_image, off_x, off_y,h,kernel):
    print('|morphological correction')

    if parent_cluster_image is None:
        parent_cluster_image, off_x, off_y, masked_pixels = parent_cluster.get_cluster_image_array(
            bkg=parent_cluster.flux.min())

    for cl in clusters_list:
        cl.position_array -= [off_x, off_y]
        print('cl.ID', cl.ID)
    ID_min = min([cl.ID for cl in clusters_list])
    bkg_val = ID_min - 1
    labels = np.ones(parent_cluster_image.shape, dtype=np.int16) * bkg_val
    print('ID_min', ID_min, 'label min', labels.min())

    for cl in clusters_list:
        # print ('cl ID',cl.ID)
        labels[cl.position_array[:, 1].astype(np.int16), cl.position_array[:, 0].astype(np.int16)] = cl.ID

    sources_cluster_list = []
    for ID, cl in enumerate(clusters_list):
        sources_cluster_list.append(cl.SourceCluster_factory(ID=cl.ID))

    area = np.zeros(len(sources_cluster_list), dtype=np.int16)
    print('|pre-correction areas')
    for ID, cl in enumerate(sources_cluster_list):
        area[ID] = np.sum([labels == cl.ID])
        print('|ID,area', cl.ID, area[ID])

    cl_ID_area_max = sources_cluster_list[np.argmax(area)].ID
    print('cl_ID_area_max', cl_ID_area_max)
    for s_cl in sources_cluster_list:
        if s_cl.ID != cl_ID_area_max:
            f_th_1 = parent_cluster_image[
                s_cl.contour_shape[:, 1].astype(np.int16), s_cl.contour_shape[:, 0].astype(np.int16)].min()
            f_th_2 = parent_cluster_image[
                s_cl.contour_shape[:, 1].astype(np.int16), s_cl.contour_shape[:, 0].astype(np.int16)].max()
            f_th = np.mean([f_th_1, f_th_2])
            # f_th=parent_cluster_image[s_cl.contour_shape[:, 1].astype(np.int16), s_cl.contour_shape[:,0].astype(np.int16)].max()
            msk = np.logical_and(parent_cluster_image < f_th, labels == s_cl.ID)

            print('|ID,ID_max', s_cl.ID, cl_ID_area_max)
            labels[msk] = cl_ID_area_max

    # area = np.zeros(len(clusters_list))
    print('|post-correction areas')
    for ID, cl in enumerate(sources_cluster_list):
        area[ID] = np.sum([labels == cl.ID])
        print('|ID,area', cl.ID, area[ID])

    conn = ConnectedComponents(1)
    conn.run(parent_cluster_image, bkg_val, seg_map=labels, seg_map_bkg=bkg_val)
    for cl in conn.clusters_list:
        cl.position_array += [off_x, off_y]
        cl.h = h
        cl.kernel = kernel
        # print(cl.position_array - [off_x, off_y])
    print('| ', )




    return conn.clusters_list, off_x, off_y


def do_denclue_of_cluster(parent_cluster,
                          segmentation_method,
                          min_size,
                          kernel,
                          eps,
                          attr_dbs_K,
                          mask_unchanged,
                          k_table_size,
                          max_calls_method,
                          max_calls_par,
                          R_max_frac,
                          R_max_kern_th,
                          scale_finder,
                          bkg_std,
                          attractors_th=None,
                          watershed_compactness=0,
                          morphological_correction=False,
                          plot=False,
                          digitize_attractors=False,
                          do_downsampling=False,
                          down_sampler=None,
                          verbose=False,
                          multiprocessing_th=500):
    """
    This function sets-up the parameters for the :class:`.DENCLUE`  partitioning of the parent clusters.
    The following features are


    Parameters
    ----------
    parent_cluster
    min_size
    kernel
    eps
    h_frac
    attr_dbs_K
    attr_dbs_eps
    mask_unchanged
    k_table_size
    R_max_frac
    R_max_kern_th
    plot
    digitize_attractors
    do_gl_downsampling
    gl_down_sampler

    Returns
    -------

    """
    if verbose == True:
        print("|debelnding for cluster", parent_cluster.ID, "r_cluter", parent_cluster.r_cluster, "r_max",
              parent_cluster.r_max, 'size', parent_cluster.cl_len, min_size)

    _segmentation_methods_dic_ = {}

    _segmentation_methods_dic_['denclue'] = DenclueDeblender
    _segmentation_methods_dic_['watershed'] = DenclueWatershedDeblender
    _segmentation_methods_dic_['spectral'] = DenclueSpectralDeblender

    if segmentation_method not in _segmentation_methods_dic_.keys():
        raise RuntimeError('segmentation method for deblending', segmentation_method, 'not in allowed',
                           _segmentation_methods_dic_.keys())

    children_clusters_list = []
    cl_off_x = 0
    cl_off_y = 0
    h = None

    if parent_cluster.cl_len > min_size:
        print('|segmentation method', segmentation_method)
        try:
            deblender = _segmentation_methods_dic_[segmentation_method](parent_cluster,
                                                                        kernel,
                                                                        eps,
                                                                        attr_dbs_K,
                                                                        k_table_size,
                                                                        R_max_frac,
                                                                        R_max_kern_th,
                                                                        max_calls_method,
                                                                        max_calls_par,
                                                                        scale_finder,
                                                                        bkg_std,
                                                                        do_downsampling=do_downsampling,
                                                                        down_sampler=down_sampler,
                                                                        verbose=verbose)

            if attractors_th is not None:
                attractors_th=parent_cluster.flux.min()+attractors_th*np.mean(bkg_std)

            if segmentation_method == 'watershed':
                children_clusters_list, off_x, off_y, parent_cluster_image = deblender.run(parent_cluster,
                                                                                           mask_unchanged,
                                                                                           digitize_attractors,
                                                                                           verbose,
                                                                                           attractors_th=attractors_th,
                                                                                           watershed_compactness=watershed_compactness,
                                                                                           multiprocessing_th=multiprocessing_th)

            else:
                children_clusters_list, off_x, off_y, parent_cluster_image = deblender.run(parent_cluster,
                                                                                           mask_unchanged,
                                                                                           digitize_attractors,
                                                                                           verbose,
                                                                                           attractors_th=attractors_th,
                                                                                           multiprocessing_th=multiprocessing_th)

            h = deblender.h

        except Exception as e:
            # TODO raise Exception
            raise (RuntimeError)
            print('failed deblending', e)
            print('deblending failed set this in catalog')

        if morphological_correction == True and len(children_clusters_list) > 1:
            # children_clusters_list,off_x, off_y=do_morphological_correction_1(children_clusters_list, parent_cluster, parent_cluster_image, off_x, off_y)
            children_clusters_list, off_x, off_y = do_morphological_correction(children_clusters_list,
                                                                               parent_cluster,
                                                                               parent_cluster_image,
                                                                               off_x,
                                                                               off_y,
                                                                               deblender.h,
                                                                               deblender.kernel)

        if verbose == True:
            print("|denclue done")
            print("|------------")

        if plot is True and len(children_clusters_list) > 0:
            test_plot(parent_cluster,
                      children_clusters_list,
                      denclue_debl=deblender.denclue_debl,
                      parent_cluster_image=parent_cluster_image,
                      target_coordinates=deblender.t_coords,
                      off_x=off_x,
                      off_y=off_y,
                      lm_xy=deblender.lm_xy,
                      lm_radii=deblender.lm_radii)

        if len(children_clusters_list) == 1:
            children_clusters_list = []

    return children_clusters_list, cl_off_x, cl_off_y, h


def do_denclue_deblending(clusters_list,
                          segmentation_method,
                          max_calls_method,
                          bkg_std,
                          max_calls_par=0.1,
                          eps=0.1,
                          kernel='gauss',
                          attractors_th=None,
                          attr_dbs_K=4,
                          watershed_compactness=0,
                          morphological_correction=False,
                          do_downsampling=False,
                          down_sampler=None,
                          scale_finder=None,
                          min_size=9,
                          mask_unchanged=0.5,
                          k_table_size=None,
                          R_max_frac=1.0,
                          R_max_kern_th=None,
                          plot=False,
                          verbose=False,
                          digitize_attractors=False,
                          multiprocessing_th=500):
    """
    This function implements the top-level algorithm for the Denclue-based deblending:
     * each parent cluster in the `cluster_list` is partitioned by the :func:`do_denclue_of_cluster` function.
     * the parent cluster with his children clusters are used to build :class:`DeblendedProducts` object
     * a list of class:`DeblendedProducts` object is returned


    Parameters
    ----------
    clusters_list
    eps
    h_frac
    kernel
    gl_downsampling
    attr_dbs_eps
    attr_dbs_K
    children_min_frac_integ_flux
    children_min_frac_peak_flux
    children_min_frac_size
    children_bright_frac_peak_flux
    min_size
    mask_unchanged
    k_table_size
    R_max_frac
    R_max_kern_th
    plot
    denclue_catalog
    verbose
    do_denclue_deblending
    out_catalog
    digitize_attractors

    Returns
    -------

    """

    deblended_prod_list = []

    for parent_cluster in clusters_list:
        print("|-")

        denclue_clusters, off_x, off_y, h = do_denclue_of_cluster(parent_cluster,
                                                                  segmentation_method,
                                                                  min_size,
                                                                  kernel,
                                                                  eps,
                                                                  attr_dbs_K,
                                                                  mask_unchanged,
                                                                  k_table_size,
                                                                  max_calls_method,
                                                                  max_calls_par,
                                                                  attractors_th=attractors_th,
                                                                  morphological_correction=morphological_correction,
                                                                  down_sampler=down_sampler,
                                                                  scale_finder=scale_finder,
                                                                  bkg_std=bkg_std,
                                                                  watershed_compactness=watershed_compactness,
                                                                  R_max_frac=R_max_frac,
                                                                  R_max_kern_th=R_max_kern_th,
                                                                  plot=plot,
                                                                  digitize_attractors=digitize_attractors,
                                                                  do_downsampling=do_downsampling,
                                                                  verbose=verbose,
                                                                  multiprocessing_th=multiprocessing_th)

        deblended_prod_list.append(DeblendedProducts(parent=parent_cluster,
                                                     children_list=denclue_clusters,
                                                     offset_x=off_x,
                                                     offset_y=off_y,
                                                     h=h))

    return deblended_prod_list


class DoDENCLUEDeblendingTask(AnalysisTask):
    """


    """

    def __init__(self, name='denclue_deblending', func=do_denclue_deblending, parser=None, process=None):
        super(DoDENCLUEDeblendingTask, self).__init__(name=name, func=func, parser=parser, process=process)
        self.parameters_list.add_par('-segmentation_method', type=str, default='denclue')
        self.parameters_list.add_par('-do_downsampling',
                                     help='if True, a GaussLaplace downsampling of the target coordiantes is performed ',
                                     action='store_true')
        self.parameters_list.add_par('-multiprocessing_th', type=np.int,
                                     help='',
                                     default=None)
        self.parameters_list.add_par('-morphological_correction', help='', action='store_true')
        self.parameters_list.add_par('-eps', type=np.float,
                                     help='sets the stop threshold for the recursive attractor update rule',
                                     default=0.01)
        self.parameters_list.add_par('-attractors_th', type=np.float,
                                     help='',
                                     default=None)
        self.parameters_list.add_par('-max_calls_method', default='sqrt', type=str)
        self.parameters_list.add_par('-max_calls_par', default=0.1, type=float)
        self.parameters_list.add_par('-digitize_attractors',
                                     help='if True, will approximate the final attractor position to the coordinate of the closest image pixel ',
                                     action='store_true')
        self.parameters_list.add_par('-kernel', type=str, help='smoothing  kernel', default='gauss')
        self.parameters_list.add_par('-k_table_size', type=np.int,
                                     help='if not None, the kernel is precomputed in a look-up table over a grid with size equal to  k_table_size',
                                     default=None)
        self.parameters_list.add_par('-mask_unchanged', type=np.float,
                                     help='th value to flag and attractor as unchanged', default=None)
        self.parameters_list.add_par('-min_size', type=np.int,
                                     help=' sets the minimum size in pixels to perform a deblending', default=9)
        self.parameters_list.add_par('-attr_dbs_K', type=np.int, help='K for dbscan of attractors', default=4)
        self.parameters_list.add_par('-watershed_compactness', type=np.float, help='watershed compactness parameter',
                                     default=0.0)
        self.parameters_list.add_par('-verbose', help='set  verbosity on', action='store_true')
        self.parameters_list.add_par('-plot', help='plot ', action='store_true')
        group_method = self.parameters_list.add_mex_group('R_max_method')
        group_method.add_par('-R_max_frac', type=np.float,
                             help='max kernel influence radius as fraction of parent cluster r_max', default=None)
        group_method.add_par('-R_max_kern_th', type=np.float,
                             help='max kernel influence radius when kernel<=th with th relative to kenerl max value',
                             default=None)

        self.func = func
