"""

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

__author__ = 'andrea tramacere'


import  numpy as np
from skimage.morphology import watershed
from skimage.morphology import extrema
from skimage.measure import label
from scipy import ndimage

from .deblending import  DeblendedProducts
from ....core.clustering.connectivity_based.connected_components import ConnectedComponents
from ....pipeline_manager.analysis_tasks import AnalysisTask
from .deblending import test_plot








class ExtremaDeblender(object):

    def __init__(self,
                 scale_finder=None,
                 parent_cluster=None,
                 s_min=None,
                 s_max=None,
                 verbose=False):

        self.s_min = s_min
        self.s_max = s_max
        self.parent_cluster = parent_cluster

        self.verbose = verbose








    def run(self,s_min=None,s_max=None,parent_cluster=None,verbose=None,N=100,scale_finder=None,bkg_std=None):
        if s_min is None:
            s_min=self.s_min
        if s_max is None:
            s_max=self.s_max
        if parent_cluster is None:
            parent_cluster=self.parent_cluster

        if verbose is None:
            verbose=self.verbose

        parent_cluster_image, off_x, off_y, masked_pixels = parent_cluster.get_cluster_image_array(
            bkg=parent_cluster.flux.min())

        if scale_finder is not None:
            lm_xy, lm_f, lm_radii, h = scale_finder.run(parent_cluster,bkg_std, verbose=verbose)



        else:
            local_maxima = extrema.h_maxima(parent_cluster_image, parent_cluster_image.min())
            label_maxima = label(local_maxima)


            _y_c, _x_c = np.where(label_maxima > 0)
            _lm_xy = np.column_stack((_x_c,_y_c))
            lm_f=parent_cluster_image[_y_c,_x_c]
            #h_min=_lm_f.min()*s_min
            #h_max=_lm_f.max()*s_max
        #print(lm_f)
        h_min = min(lm_f) * s_min
        h_max = max(lm_f) * s_max

        _n_h=np.zeros(N,dtype=np.int)
        #parent_cluster_image+=parent_cluster_image.min()
        #h_min=parent_cluster_image.min()*s_min
        #h_max=parent_cluster_image.max()*s_max
        delta=(h_max-h_min)/N
        if verbose is True:
            print('|h_min,h_max',h_min,h_max)
        for ID,_h in enumerate(np.linspace(h_min,h_max,N)):

            local_maxima = extrema.h_maxima(parent_cluster_image, _h)
            label_maxima = label(local_maxima)

            _n_h[ID]=np.sum(label_maxima>0)
            #if verbose == True:
            #print('ID,->',ID,_h,np.sum(label_maxima>0))

        m=np.argmax(np.bincount(_n_h))
        #u, indices = np.unique(_n_h, return_inverse=True)
        #m=u[np.argmax(np.bincount(indices))]
        #print(np.bincount(_n_h),m)
        h=np.argwhere(_n_h==m).max()*delta+h_min
        if verbose==True:
            print('|h_min,h_maxn,h,m,id', h_min, h_max,h)
        local_maxima = extrema.h_maxima(parent_cluster_image, h)
        label_maxima = label(local_maxima)
        y_c, x_c = np.where(label_maxima > 0)
        self.lm_xy = np.column_stack((x_c, y_c))

        markers = np.zeros(parent_cluster_image.shape)
        m = 1

        for _xy in self.lm_xy:
            x_c = np.int32(_xy[0])
            y_c = np.int32(_xy[1])
            #print('x_c,y_c', )
            if parent_cluster.check_is_in_cluster(x_c+off_x, y_c+off_y):
                markers[y_c, x_c] = m
                m += 1
                #print('markers', y_c , x_c)

        conn = ndimage.generate_binary_structure(2, 2)

        labels = watershed(-parent_cluster_image, markers, mask=~masked_pixels, compactness=0.0, connectivity=conn)


        conn = ConnectedComponents(1)
        conn.run(parent_cluster_image, -1, seg_map=labels, seg_map_bkg=0)
        children_clusters_list = conn.clusters_list
        for cl in children_clusters_list:
            cl.position_array += [off_x, off_y]

        print('|sub clusters found from extrema', len(children_clusters_list))
        print('|')

        #print('target',self.t_coords.shape,self.t_coords)

        off_x=0
        off_y=0

        return children_clusters_list, off_x, off_y, parent_cluster_image


def do_extrema_deblending(clusters_list,
                          s_min,
                          s_max,
                          N=100,
                          min_size=9,
                          plot=False,
                          verbose=False,
                          scale_finder=None,
                          use_scale_finder=True,
                          bkg_std=None):
    """
    This function implements the top level  algorithm for the gauss_laplace_watershed_deblending:
        * each parent cluster in the `cluster_list` is partitioned by the :func:`do_cluster_glw_segmentation` function.
        * the parent cluster with his children clusters are used to build :class:`DeblendedProducts` object
        * a list of class:`DeblendedProducts` objects is returned


    Parameters
    ----------
    clusters_list
    h_frac
    h_min
    th_rel
    min_size
    plot
    verbose

    Returns
    -------
    deblended_prod_list : list of :class:`DeblendedProducts` objects

    """
    deblender=ExtremaDeblender()

    deblended_prod_list = []

    if use_scale_finder == False:
        scale_finder=None
    for parent_cluster in clusters_list:


        children_clusters_list = []
        cl_off_x = 0
        cl_off_y = 0

        if parent_cluster.cl_len > min_size:
            print('|parent cluster', parent_cluster.ID,parent_cluster.cl_len)
            try:
                children_clusters_list,cl_off_x,cl_off_y,parent_cluster_image=deblender.run(s_min=s_min,
                                                                                      s_max=s_max,
                                                                                      N=N,
                                                                                      parent_cluster=parent_cluster,
                                                                                      verbose=verbose,
                                                                                      scale_finder=scale_finder)

                if plot is True:
                    test_plot(parent_cluster,
                              children_clusters_list,
                              parent_cluster_image=parent_cluster_image,
                              target_coordinates=None,
                              off_x=cl_off_x,
                              off_y=cl_off_x,
                              lm_xy=deblender.lm_xy)
            except Exception as e:
                # TODO raise Exception
                raise RuntimeError
                print ('deblending exception',e)

        deblended_prod_list.append(DeblendedProducts(parent=parent_cluster,
                                                     children_list=children_clusters_list,
                                                     offset_x=cl_off_x,
                                                     offset_y=cl_off_y))




    return deblended_prod_list



class DoExtremaDeblendingTask(AnalysisTask):

    def __init__(self, name='extrema_deblending', func=do_extrema_deblending, parser=None, process=None):



        super(DoExtremaDeblendingTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-min_size', type=np.int, help='sets the minimum size in pixels to perform a deblending', default=9)
        self.parameters_list.add_par('-s_min', type=np.float, help='scale parameter for extrem', default=0.1)
        self.parameters_list.add_par('-s_max', type=np.float, help='scale parameter for extrem', default=2.0)
        self.parameters_list.add_par('-N', type=np.int, help='scale parameter for extrem', default=100)
        self.parameters_list.add_par('-use_scale_finder', help='', action='store_true')
        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')
        self.parameters_list.add_par('-plot',help='plot ',action='store_true')



        self.func=func