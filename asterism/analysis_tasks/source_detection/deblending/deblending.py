
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ....core.clustering.density_based.denclue import DENCLUE_cluster
from ....core.clustering.density_based.denclue_tools import cluster_significance
from ....core.cython_tools.denclue import _dist_eval
from ....pipeline_manager.analysis_tasks import AnalysisTask
from itertools import combinations
import numpy as np
import scipy as sp
__author__ = 'andrea tramacere'


def test_plot(parent_cluster,
              deblended_cluster_list,
              denclue_debl=None,
              plot_cnt=True,
              parent_cluster_image=None,
              target_coordinates=None,
              off_x=0,
              off_y=0,
              lm_xy=None,
              lm_radii=None):

    from asterism.plotting.plot_tools import plot_contour, show_image
    import pylab as plt



    fig, (ax, ax1,ax2) = plt.subplots(1, 3)



    sources_cluster_list = []


    if parent_cluster_image is None:
        parent_cluster_image, off_x, off_y, selected = parent_cluster.get_cluster_image_array(
            bkg=parent_cluster.flux.min())


    # print('ID_min', ID_min,'label min',labels.min())

    ID_min = min([cl.ID for cl in deblended_cluster_list])
    bkg_val = ID_min - 1

    #cluster coordinates
    labels = np.ones(parent_cluster_image.shape, dtype=np.int16) * bkg_val
    for cl in deblended_cluster_list:
    #    # print ('cl ID',cl.ID)
       labels[cl.position_array[:, 1].astype(np.int16)-off_y, cl.position_array[:, 0].astype(np.int16)-off_x] = cl.ID

    #image cooridantes
    show_image(labels,ax=ax1)
    show_image(parent_cluster_image,ax=ax,vmin=parent_cluster.flux.min(),vmax=parent_cluster.flux.max()*.1)




    #cluster coordinates
    for ID1, cl in enumerate(deblended_cluster_list):
        sources_cluster_list.append(cl.SourceCluster_factory(ID=ID1))

    # cluster coordinates
    if parent_cluster is not None:
        if plot_cnt==True:
            plot_contour(ax, parent_cluster.contour_shape[:, 0]- off_x, parent_cluster.contour_shape[:, 1]- off_y, 'w')
        ax.annotate('P', xy=(parent_cluster.x_c- off_x, parent_cluster.y_c- off_y), color='w')


    if denclue_debl is not None:
        for ID1, attr_c in enumerate(denclue_debl.attractors_clusters_list):
            ax.annotate('attr cl', xy=(attr_c.x_c - off_x, attr_c.y_c - off_y), color='w')
            ax.plot(attr_c.x_c - off_x, attr_c.y_c - off_y,'+',color='w')

        if denclue_debl.attractors_position_array is not None:
            x=denclue_debl.attractors_position_array[:,0]
            y=denclue_debl.attractors_position_array[:,1]
            #dx
            #dy
            ax.scatter(x-off_x,y-off_y,marker='o', edgecolor='black', linewidth='0',facecolor='white',s=10)

    if lm_xy is not None:
        ax.scatter(lm_xy[:,0],lm_xy[:,1], marker='o', edgecolor='white', linewidth='1', facecolor='white', s=20)



    for cl in sources_cluster_list:
        if cl.contour_shape is not None:
            plot_contour(ax, cl.contour_shape[:, 0]  -off_x, cl.contour_shape[:, 1]-off_y , color='w')
            ax.plot(cl.x_c-off_x, cl.y_c-off_y, '+', c='white', ms=10)
            #print (cl.x_c-off_x)
        ax.annotate('C.ID=%d deblended cluster' % (cl.ID), xy=(cl.x_c-off_x, cl.y_c-off_y), color='w')

    if target_coordinates is not None:
        x_t = target_coordinates[:, 0]
        y_t = target_coordinates[:, 1]
        ax.scatter(x_t - off_x, y_t - off_y, marker='+', color='green',linewidths=0.1,s=1)

    if lm_radii is not None:
        for r, x, y in zip(lm_radii, lm_xy[:, 0], lm_xy[:, 1]):
            c = plt.Circle((x, y), r, linewidth=1, fill=False)
            ax.add_patch(c)
    #print('c')
    show_image(parent_cluster_image, ax=ax2,vmin=parent_cluster.flux.min(),vmax=parent_cluster.flux.max()*.1)
    #print('d')
    plt.show()
    #print('f')
    return


class DeblendedProducts(object):

    def __init__(self, parent, children_list, offset_x=0, offset_y=0, h=None):
        self.parent = parent
        self.children_list = children_list
        self.offset_x = offset_x
        self.offset_y = offset_y
        self.h = h


def add_denclue_signif(parent_cluster,
                       children_cluster,
                       h,
                       kernel,
                       bkg_lvl,
                       bkg_sig,
                       bkg_th):

    _x_px = parent_cluster.cartesian.x.astype(np.int32)
    _y_px = parent_cluster.cartesian.y.astype(np.int32)
    _x_cx = children_cluster.cartesian.x.astype(np.int32)
    _y_cx = children_cluster.cartesian.y.astype(np.int32)

    if np.ndim(bkg_sig) == 2:
        w_bkg_p = np.ones(parent_cluster.pts_weight.size)
        #w_bkg_c = np.ones(children_cluster.pts_weight.size)

        for ID,p in enumerate(_x_px):
            w_bkg_p[ID]=bkg_sig[_y_px[ID],_x_px[ID]]

        #for ID, p in enumerate(_x_cx):
            #w_bkg_c[ID]=bkg_sig[_y_px[ID],_x_px[ID]]
            #w_chl[ID] = parent_cluster.pts_weight[ID] - bkg_threshold[_y_px[ID],_x_px[ID]]

    elif np.ndim(bkg_sig) == 1 or np.ndim(bkg_sig) == 0:
        w_bkg_p = np.ones(parent_cluster.pts_weight.size) * bkg_sig
        #w_bkg_c = np.ones(children_cluster.pts_weight.size) * bkg_sig
    else:
        raise RuntimeError('bkg_threshold dim. must be 0,1 or 2, found', np.ndim(bkg_sig))

    #w_bkg_random=np.random.normal(0,np.mean(bkg_sig),w_bkg_p.size)
    th=(np.mean(bkg_th)-np.mean(bkg_lvl))/np.mean(bkg_sig)
    #w_children_random = np.random.normal(0, np.mean(bkg_sig)*c_bkg*1.0, w_bkg_p.size)
    w_children_random= sp.stats.truncnorm.rvs(th,th*100.,0,np.mean(bkg_sig),size=w_bkg_p.size)-bkg_lvl
    #w_bkg=np.ones(parent_cluster.pts_weight.size)*bkg_threshold

    # dist = _dist_eval(children_cluster.cartesian.coords,
    #                   children_cluster.x_c,
    #                   children_cluster.y_c,
    #                   children_cluster.cartesian.coords[:, 1].size)
    #
    # selected_centroids = []
    # s = children_cluster.sig_x
    # while len(selected_centroids) < 1:
    #     selected_centroids = children_cluster.cartesian.coords[dist < s]
    #     s *= 1.1
    #
    # pb_ratio = np.zeros(selected_centroids.shape[0])
    #



    bkg_p = cluster_significance(position_array=parent_cluster.cartesian.coords,
                                 weight_array=w_bkg_p,
                                 h=h,
                                 x_t=children_cluster.x_c,
                                 y_t=children_cluster.y_c,
                                 kernel_type=kernel)

    dist = _dist_eval(children_cluster.cartesian.coords,
                      children_cluster.x_c,
                      children_cluster.y_c,
                      children_cluster.cartesian.coords[:, 1].size)


    selected_centroids = []
    s = children_cluster.sig_x

    while len(selected_centroids) < 1:
         selected_centroids = children_cluster.cartesian.coords[dist < s]
         s *= 1.1
    bkg_p_ave = np.zeros(selected_centroids.shape[0])

    for ID, pc in enumerate(selected_centroids):
        bkg_p_ave[ID] = cluster_significance(position_array=parent_cluster.cartesian.coords,
                                         weight_array=w_bkg_p,
                                         h=h,
                                         x_t=pc[0],
                                         y_t=pc[1],
                                         kernel_type=kernel)

    bkg_p_mean = np.mean(bkg_p_ave)
    bkg_p_std = np.std(bkg_p_ave)
    # print(pb_ratio)
    # children_cluster.denc_pb_ratio = np.mean(pb_ratio)

    w_parent=parent_cluster.pts_weight-bkg_lvl
    children_p = cluster_significance(position_array=parent_cluster.cartesian.coords,
                                     weight_array=w_parent,
                                     h=h,
                                     x_t=children_cluster.x_c,
                                     y_t=children_cluster.y_c,
                                     kernel_type=kernel)






    children_p_random = cluster_significance(position_array=parent_cluster.cartesian.coords,
                                      weight_array=w_children_random,
                                      h=h,
                                      x_t=children_cluster.x_c,
                                      y_t=children_cluster.y_c,
                                      kernel_type=kernel)



    #r = -2.0*np.log(bkg_p/children_p)
    #w1 = w_chl.sum() / w_bkg_p.sum()
    #w = w_chl.sum() / w_bkg.sum()
    #w =  children_p/bkg_p
    children_cluster.denc_pd_ratio = children_p/bkg_p_mean
    children_cluster.denc_pb_ratio = children_p/bkg_p
    children_cluster.denc_prandom_ratio = children_p/children_p_random
    children_cluster.children_p_random = children_p_random
    children_cluster.children_p = children_p
    children_cluster.bkg_p = bkg_p
    children_cluster.bkg_p_mean = bkg_p_mean

def add_signif_map(cl, image,bkg_lvl,bkg_sig):
    _x_px = cl.cartesian.x.astype(np.int32)
    _y_px = cl.cartesian.y.astype(np.int32)
    _p = tuple([_y_px, _x_px])
    if np.ndim(bkg_sig) == 2:
        cl.signif_map = (image.array[_p]-bkg_lvl) / bkg_sig[_p]
        #cl.signif_ave = np.sum(image.array[_p]-bkg_lvl)/np.sum( bkg_sig[_p])
    elif np.ndim(bkg_sig) == 1 or np.ndim(bkg_sig) == 0:
        cl.signif_map = (image.array[_p]-bkg_lvl) / bkg_sig
        #cl.signif_ave = cl.signif_map.sum()/len(_p)
    else:
        raise RuntimeError('bkg_threshold dim. must be 0,1 or 2, found', np.ndim(bkg_sig))
    #msk = cl.flux > 0.
    cl.signif_ave=np.mean(cl.signif_map)


def build_source_cluster(deblended_cluster_list,
                         parent,
                         offset_x=0,
                         offset_y=0,
                         image=None,
                         bkg_threshold=None,
                         bkg_lvl=None,
                         bkg_sig=None):
    src_cl_list = []
    for ID, children_cluster in enumerate(deblended_cluster_list):

        children_cluster.position_array = children_cluster.position_array + [offset_x, offset_y]
        src_cl_list.append(children_cluster.SourceCluster_factory(parent_ID=parent.ID))
        #if image is not None and bkg_threshold is not None and hasattr(children_cluster,'h') :
        #    add_signif_map(src_cl_list[-1], image,bkg_lvl,bkg_sig)
        #    add_denclue_signif(parent,
        #                       src_cl_list[-1],
        #                       h=children_cluster.h,
        #                       kernel=children_cluster.kernel,
        #                       bkg_lvl=bkg_lvl,
        #                       bkg_sig=bkg_sig,
        #                       bkg_th=bkg_threshold)

    return src_cl_list




def find_direct_children(parent_cluster,children_src_clusters_list):
    for cl in children_src_clusters_list:
        if parent_cluster.check_is_in_cluster(cl.x_c,cl.y_c)[0] == True:
            return cl

    return None


def validate_children_clusters(parent_cluster,
                               children_clusters_list,
                               abs_size_th=None,
                               overlap_max=None,
                               sig_th=None,
                               denc_prandom_ratio_th=None,
                               denc_pb_ratio_th=None,
                               denc_pd_ratio_th=None,
                               verbose=False):
    """

    Returns:

    """

    validated_list=[]
    orphan_list=[]


    #valid=False



    if len(children_clusters_list)<2:
        return [],[]

    if verbose==True:
        print ("|children  clusters found",len(children_clusters_list))
    #print([cl.ID for cl in children_clusters_list])
    for ID,children_source_cluster in enumerate(children_clusters_list):


        frac_overlap = children_source_cluster._overlap.sum() / children_source_cluster.cl_len

        valid=True
        #print(children_source_cluster.signif_map.size,children_source_cluster.cl_len)

        valid_abs_size=True
        valid_sig=True
        valid_overlap=True
        valid_probab_b= True
        valid_probab_p= True
        valid_prandom_th= True

        if overlap_max is not None:
             if frac_overlap<=overlap_max:
                 valid_overlap=True
             else:
                 valid_overlap=False


        # abs size
        if abs_size_th is not None:
            if children_source_cluster.cl_len < abs_size_th:
                valid_abs_size = False
            else:
                valid_abs_size = True


        if sig_th is not None:
            #msk = children_source_cluster.flux > 0.
            signif = np.mean(children_source_cluster.signif_map)

            if signif > sig_th:
                valid_sig = True
            else:
                valid_sig=False
        else:
            signif=None

        if denc_pb_ratio_th is not None:

            #print('cl denc_signif', children_source_cluster.denc_signif)
            if children_source_cluster.denc_pb_ratio > denc_pb_ratio_th:
                valid_probab_b = True
            else:
                valid_probab_b = False

        if denc_pd_ratio_th is not None:
        #
              #print('cl denc_signif', children_source_cluster.denc_signif)
            if children_source_cluster.denc_pd_ratio > denc_pd_ratio_th:
                valid_probab_p = True
            else:
                valid_probab_p = False


        if denc_prandom_ratio_th is not None:
        #
              #print('cl denc_signif', children_source_cluster.denc_signif)
            if children_source_cluster.denc_prandom_ratio > denc_prandom_ratio_th:
                valid_prandom_th = True
            else:
                valid_prandom_th = False


        #valid_probab=np.logical_or(valid_probab_b,valid_probab_p)

        valid = valid_abs_size*valid_sig*valid_overlap*valid_probab_b*valid_probab_p*valid_prandom_th

        if valid == True:

            validated_list.append(children_source_cluster)
        else:
            orphan_list.append(children_source_cluster)

        if verbose == True:
            print("|test for subcluster",ID, 'x_c,y_c',children_source_cluster.x_c,children_source_cluster.y_c)
            print("|overlap frac", frac_overlap, 'max', overlap_max, valid_overlap)
            print("|abs  size", children_source_cluster.cl_len, ' th', abs_size_th, valid_abs_size)
            print("|signif ", signif, 'th',sig_th, valid_sig)
            print("|denc_pb_ratio ", children_source_cluster.denc_pb_ratio, 'th', denc_pb_ratio_th, valid_probab_b)
            print("|denc_pd_ratio ", children_source_cluster.denc_pd_ratio, 'th', denc_pd_ratio_th, valid_probab_p)
            print("|denc_prandom_ratio ", children_source_cluster.denc_prandom_ratio,'th', denc_prandom_ratio_th, valid_prandom_th)
            print("|valid=",valid)
            print("|-")

    return validated_list,orphan_list


def assign_orphane_clusters(parent,validated_denclue_clusters_list,orphan_list,image,bkg_lvl,bkg_sig,bkg_th):
    """

    Returns:

    """
    print ("|merging orphan",type(parent))
    for orphan in  orphan_list:
        closest_to_orphan=orphan.get_closest_cluster(validated_denclue_clusters_list)
        closest_to_orphan.merge_with(orphan)
        add_signif_map(closest_to_orphan,image,bkg_lvl,bkg_sig)
        add_denclue_signif(parent,
                           closest_to_orphan,
                           h=parent.h,
                           kernel=parent.kernel,
                           bkg_lvl=bkg_lvl,
                           bkg_sig=bkg_sig,
                           bkg_th=bkg_th)




def deblend_parent(parent_cluster,
                   children_src_clusters_list,
                   validate_children,
                   image=None,
                   bkg_threshold=None,
                   bkg_lvl=None,
                   bkg_sig=None,
                   abs_size_th=None,
                   overlap_max=None,
                   sig_th=None,
                   denc_pb_ratio_th=None,
                   denc_pd_ratio_th=None,
                   denc_prandom_ratio_th=None,
                   #offset_x=0,
                   #offset_y=0,
                   verbose=False,
                   plot=False):
    """

    from the initial list of children are removed the not-valid ones (orphanes)
    and the final list of validated children is returned, with the following steps

        1) srcs clusters are built from basic clustering clusters
        2) the direct children (i.e. with center within parent) is identified
        3) all the children except the direct are validated, and the list of children is divide in validated and orphanes
        4) if  validated are None:
                only the direct is returned, or the parent is direct is None
           else:
                the list of [direct + validated..] is returned
                where direct=parent-validated


    Parameters
    ----------

    parent_cluster:
    deblended_cluster_list:
    children_min_frac_integ_flux:
    children_min_frac_peak_flux:
    children_min_frac_size:
    offset_x:
    offset_y:

    Returns
    -------
    final_deblended_cluster_list:

    """

    validated_cluster_list=[]
    orphan_list=[]
    #children_src_clusters_list=[]
    if len(children_src_clusters_list)>1:
        if verbose==True:
            print ("|analysing sub clusters for parent cluster",parent_cluster.ID)


        _overlapping_map(children_src_clusters_list, parent_cluster)

        if validate_children == True:

            direct_children_cluster=find_direct_children(parent_cluster,children_src_clusters_list)

            if direct_children_cluster is not None:
                children_src_clusters_list.remove(direct_children_cluster)
                validated_cluster_list=[direct_children_cluster]

            _list, orphan_list = validate_children_clusters(parent_cluster,
                                                            children_src_clusters_list,
                                                            abs_size_th=abs_size_th,
                                                            overlap_max=overlap_max,
                                                            denc_pb_ratio_th=denc_pb_ratio_th,
                                                            denc_pd_ratio_th=denc_pd_ratio_th,
                                                            denc_prandom_ratio_th=denc_prandom_ratio_th,
                                                            sig_th=sig_th,
                                                            verbose=verbose)



            validated_cluster_list.extend(_list)
            if plot==True:
                plot_overlap(children_src_clusters_list,parent_cluster)
        else:
            validated_cluster_list=children_src_clusters_list
            orphan_list=[]
    else:
        validated_cluster_list = [parent_cluster]

    if len(validated_cluster_list)>0:

        if verbose==True:
            print ("| *children  clusters validated*",len(validated_cluster_list))

        if len(orphan_list)>0:
            assign_orphane_clusters(parent_cluster,validated_cluster_list, orphan_list,image,bkg_lvl,bkg_sig,bkg_threshold)


    if len(validated_cluster_list) ==0:
        validated_cluster_list = [parent_cluster]

    #if plot is True:
    #    test_plot(parent_cluster,children_src_clusters_list,offset_x,offset_y)


    return validated_cluster_list


def ellips_contain_ineq(x_c, y_c, a, b, angle, x, y):
    cosa = np.cos(np.deg2rad(angle))
    sina = np.sin(np.deg2rad(angle))
    # dd = d   / 2 * d / 2
    # DD = D / 2 * D / 2

    a1 = cosa * (x - x_c) + sina * (y - y_c)
    b1 = sina * (x - x_c) - cosa * (y - y_c)

    return ((a1 * a1) / (a * a)) + ((b1 * b1) / (b * b))

    # return ((b*b)*(x-x_c)*(x-x_c)) + ((a*a)*(y-y_c)*(y-y_c))


def _find_ovralap(cl_0,cl_1):
    _scalce_factor = cl_0.r_max / cl_0.sig_x * 1.0
    a = _scalce_factor * cl_0.sig_x
    b = _scalce_factor * cl_0.sig_y
    return ellips_contain_ineq(cl_0.x_c, cl_0.y_c, a, b, cl_0.semi_major_angle, cl_1.cartesian.x,
                               cl_1.cartesian.y) < 1.0

def _overlapping_map(clusters_list, parent_cluster):
    comb = combinations(np.arange(len(clusters_list)), 2)
    for c in list(comb):
        clusters_list[c[1]]._overlap = np.logical_or(clusters_list[c[1]]._overlap,_find_ovralap(clusters_list[c[0]],clusters_list[c[1]]))
        #print(clusters_list[c[1]].overlap.sum())
        clusters_list[c[0]]._overlap = np.logical_or(clusters_list[c[0]]._overlap,_find_ovralap(clusters_list[c[1]], clusters_list[c[0]]))




def plot_overlap(clusters_list, parent_cluster):

    import pylab as plt
    from matplotlib.patches import Ellipse
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    parent_cluster_image, off_x, off_y, selected = parent_cluster.get_cluster_image_array(
        bkg=parent_cluster.flux.min())
    _seg_map = np.zeros(parent_cluster_image.shape, dtype=np.int32)
    _overlap_map = np.zeros(parent_cluster_image.shape, dtype=np.int)

    y_size, x_size = parent_cluster_image.shape
    ax1.imshow(parent_cluster_image, interpolation='nearest')

    for ID, cl in enumerate(clusters_list):
        _x_px = cl.cartesian.x.astype(np.int32) - off_x
        _y_px = cl.cartesian.y.astype(np.int32) - off_y
        _seg_map[_y_px, _x_px] = cl.ID + 1
        _overlap_map[_y_px[cl.overlap], _x_px[cl.overlap]] = 1

        _scalce_factor = cl.r_max / cl.sig_x * 1.0
        a=_scalce_factor*cl.sig_x
        b = _scalce_factor * cl.sig_y
        ellipse = Ellipse(xy=(cl.x_c- off_x, cl.y_c- off_y), width=a * 2.0, height=b * 2.0,angle=cl.semi_major_angle, edgecolor='y', fc='None')
        ax1.add_patch(ellipse)
        ellipse = Ellipse(xy=(cl.x_c - off_x, cl.y_c - off_y), width= cl.sig_x, height= cl.sig_y,
                          angle=cl.semi_major_angle, edgecolor='y', fc='None')
        ax1.add_patch(ellipse)
    ax2.imshow(_seg_map, interpolation='nearest')
    masked_data = np.ma.masked_where(_overlap_map < 1, _overlap_map)
    ax2.imshow(masked_data, interpolation='none',alpha=0.5)
    ax3.imshow(_overlap_map, interpolation='nearest')
    for ax in [ax1,ax2,ax3]:
        ax.set_ylim([0, y_size - 1])
        ax.set_xlim([0, x_size - 1])
    plt.show()


def do_deblending_validation(deblended_prod_list,
                             bkg_threshold=None,
                             bkg_lvl=None,
                             bkg_sig=None,
                             image=None,
                             validate_children=False,
                             abs_size_th=None,
                             overlap_max=None,
                             sig_th=None,
                             denc_pb_ratio_th=None,
                             denc_pd_ratio_th=None,
                             denc_prandom_ratio_th=None,
                             verbose=False,
                             plot=False):
    """
    Top level function for the deblending validation.



    Parameters
    ----------
    deblended_prod_list
    validate_children
    children_min_frac_integ_flux
    children_min_frac_peak_flux
    children_compact_frac_size
    children_ext_frac_size
    children_bright_frac_peak_flux
    verbose

    Returns
    -------

    """
    final_list=[]
    for p in deblended_prod_list:

        children_src_clusters_list = build_source_cluster(p.children_list,
                                                          p.parent,
                                                          offset_x=p.offset_x,
                                                          offset_y=p.offset_y,
                                                          image=image,
                                                          bkg_threshold=bkg_threshold,
                                                          bkg_lvl=bkg_lvl,
                                                          bkg_sig=bkg_sig)

        _l = len(children_src_clusters_list)

        start=0
        while True:
            if verbose==True:
                print('|---> Validation ',start, 'cl len',len(children_src_clusters_list),'parent',p.parent.ID)
            _list = deblend_parent(p.parent,
                                   children_src_clusters_list,
                                   image=image,
                                   validate_children=validate_children,
                                   bkg_threshold=bkg_threshold,
                                   bkg_lvl=bkg_lvl,
                                   bkg_sig=bkg_sig,
                                   abs_size_th=abs_size_th,
                                   overlap_max=overlap_max,
                                   sig_th=sig_th,
                                   denc_pb_ratio_th=denc_pb_ratio_th,
                                   denc_pd_ratio_th=denc_pd_ratio_th,
                                   denc_prandom_ratio_th=denc_prandom_ratio_th,
                                   #offset_x=p.offset_x,
                                   #offset_y=p.offset_y,
                                   verbose=verbose,
                                   plot=plot)



            #_overlapping_map(_list, p.parent, plot=plot)

            #print('CICCIO', len(_list), _l,start)
            if _l == len(_list):
                break
            else:
                _l = len(_list)
                children_src_clusters_list = _list[:]
                start+=1



        final_list.extend(_list)

        for deblended_cluster in _list:
            deblended_cluster.h = p.h


    CL_ID = 0
    #print('CICCIO final_list', len(final_list), _l, start,p.h)
    for deblended_cluster in final_list:
        deblended_cluster.ID = CL_ID
    return final_list


class DoDeblendingValidationTask(AnalysisTask):

    def __init__(self, name='deblending_validation', func=do_deblending_validation, parser=None, process=None):



        super(DoDeblendingValidationTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-validate_children', help='if  True, then the resulting children sub-clusters are processed to be validated', action='store_true')

        self.parameters_list.add_par('-abs_size_th', type=np.int, help='min absolute size to validate a children ',
                                     default=None)

        self.parameters_list.add_par('-overlap_max', type=np.float,
                                     help='max fractional overlapping below which to validate a children',
                                     default=None)

        self.parameters_list.add_par('-sig_th', type=np.float,
                                     help='min significance th to validate a children',
                                     default=None)

        self.parameters_list.add_par('-denc_pd_ratio_th', type=np.float,
                                     help='denc_pd th',
                                     default=None)

        self.parameters_list.add_par('-denc_pb_ratio_th', type=np.float,
                                     help='denc_pb th ',
                                     default=None)

        self.parameters_list.add_par('-denc_prandom_ratio_th', type=np.float,
                                     help=' denc_prandom th',
                                     default=None)

        self.parameters_list.add_par('-verbose', help='set  verbosity on', action='store_true')
        self.parameters_list.add_par('-plot', help='plot ', action='store_true')

        self.func=func





def do_set_deblending_method(method,):
    allowed_methods=DoSetDeblendingMethodTask._get_allowed_methods()
    if method not in allowed_methods:
        raise RuntimeError('denclue method is %s, but allowed methods are%s'%(method,allowed_methods))

    return method


class DoSetDeblendingMethodTask(AnalysisTask):

    def __init__(self, name='set_deblending_method', func=do_set_deblending_method, parser=None, process=None):
        super(DoSetDeblendingMethodTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-method',type=str, help='allowed methods: %s'%self._get_allowed_methods,default='denclue')
        self.func=func

    @staticmethod
    def _get_allowed_methods():
        return ['gauss_laplace_watershed', 'denclue','no_deblending','extrema','combo']

