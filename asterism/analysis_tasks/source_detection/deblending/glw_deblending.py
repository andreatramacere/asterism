"""

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

__author__ = 'andrea tramacere'


import  numpy as np
from skimage.morphology import watershed
from scipy import ndimage

from .deblending import  DeblendedProducts
from ....core.clustering.connectivity_based.connected_components import ConnectedComponents
from ....pipeline_manager.analysis_tasks import AnalysisTask
from .denclue_deblending import test_plot








class GLWDeblender(object):

    def __init__(self,
                 parent_cluster=None,
                 scale_finder=None,
                 verbose=False):

        self.scale_finder = scale_finder

        self.parent_cluster = parent_cluster

        self.verbose = verbose








    def run(self,scale_finder=None,parent_cluster=None,verbose=None,bkg_std=None):
        if scale_finder is None:
            scale_finder=self.scale_finder
        if parent_cluster is None:
            parent_cluster=self.parent_cluster

        if verbose is None:
            verbose=self.verbose

        lm_xy, lm_f, lm_radii,h=scale_finder.run(parent_cluster,bkg_std,verbose=verbose)

        self.lm_xy = lm_xy
        self.lm_f = lm_f
        self.lm_radii = lm_radii

        parent_cluster_image, off_x, off_y, masked_pixels = parent_cluster.get_cluster_image_array(
            bkg=parent_cluster.flux.min())

        markers = np.zeros(parent_cluster_image.shape)
        m = 1

        for _xy in lm_xy:
            x_c = np.int32(_xy[0])
            y_c = np.int32(_xy[1])
            #print('x_c,y_c', )
            if parent_cluster.check_is_in_cluster(x_c+off_x, y_c+off_y):
                markers[y_c , x_c] = m
                m += 1
                #print('markers', y_c , x_c)

        conn = ndimage.generate_binary_structure(2, 2)

        labels = watershed(-parent_cluster_image, markers, mask=~masked_pixels, compactness=0.0, connectivity=conn)


        conn = ConnectedComponents(1)
        conn.run(parent_cluster_image, -1, seg_map=labels, seg_map_bkg=0)
        children_clusters_list = conn.clusters_list
        for cl in children_clusters_list:
            cl.position_array += [off_x, off_y]
            cl.h = h
            cl.kernel = 'gauss'
        print('|cluster of sub clusters found from GLW', len(children_clusters_list))
        print('|')

        # print('target',self.t_coords.shape,self.t_coords)



        return children_clusters_list, off_x, off_y, parent_cluster_image


def do_gauss_laplace_watershed_deblending(clusters_list,
                                          scale_finder,
                                          min_size=9,
                                          bkg_std=None,
                                          plot=False,
                                          verbose=False):
    """
    This function implements the top level  algorithm for the gauss_laplace_watershed_deblending:
        * each parent cluster in the `cluster_list` is partitioned by the :func:`do_cluster_glw_segmentation` function.
        * the parent cluster with his children clusters are used to build :class:`DeblendedProducts` object
        * a list of class:`DeblendedProducts` objects is returned


    Parameters
    ----------
    clusters_list
    h_frac
    h_min
    th_rel
    min_size
    plot
    verbose

    Returns
    -------
    deblended_prod_list : list of :class:`DeblendedProducts` objects

    """

    deblender=GLWDeblender()

    deblended_prod_list = []

    for parent_cluster in clusters_list:


        children_clusters_list = []
        cl_off_x = 0
        cl_off_y = 0

        if parent_cluster.cl_len > min_size:
            print('|parent cluster', parent_cluster.ID,parent_cluster.cl_len)
            try:
                children_clusters_list,off_x,off_y,parent_cluster_image=deblender.run(scale_finder=scale_finder,
                                                                       parent_cluster=parent_cluster,

                                                                        verbose=verbose)

                if plot is True:
                    test_plot(parent_cluster,
                              children_clusters_list,
                              parent_cluster_image=parent_cluster_image,
                              target_coordinates=None,
                              off_x=off_x,
                              off_y=off_y,
                              lm_xy=deblender.lm_xy,
                              lm_radii=deblender.lm_radii)
            except:
                # TODO raise Exception
                print ('deblending failed set this in catalog')

        deblended_prod_list.append(DeblendedProducts(parent=parent_cluster,
                                                     children_list=children_clusters_list,
                                                     offset_x=cl_off_x,
                                                     offset_y=cl_off_y))




    return deblended_prod_list



class DoGLWDeblendingTask(AnalysisTask):

    def __init__(self, name='gauss_laplace_watershed_deblending', func=do_gauss_laplace_watershed_deblending, parser=None, process=None):



        super(DoGLWDeblendingTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-min_size', type=np.int, help='sets the minimum size in pixels to perform a deblending', default=9)
        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')
        self.parameters_list.add_par('-plot',help='plot ',action='store_true')



        self.func=func