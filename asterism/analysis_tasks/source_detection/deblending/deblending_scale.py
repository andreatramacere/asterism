"""
Overview
--------
This module provides the implementation of the :class:`Table`

Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::
    DoDENCLUEDeblendingTask



mary::
    Table


Summary
---------
.. autosummary::
    :nosignatures:

    DoDENCLUEDeblendingTask
    do_denclue_deblending
    do_denclue_of_cluster
    gauss_laplace_down_sampling

Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np
from scipy import  ndimage

from .deblending import  DeblendedProducts
from ....core.clustering.density_based.denclue import  DENCLUE
from ....pipeline_manager.analysis_tasks import AnalysisTask
from ....core.image_processing.features import BlobDetectionGaussLaplace
from ....core.geometry.distance import dist_eval
__author__ = 'andrea tramacere'



def plot_final_clusters(parent_cluster,deblended_clusters_list):
    from asterism.plotting.plot_tools import plot_contour,show_image
    import pylab as plt
    image,off_set_x,off_set_y,masked_pixels=parent_cluster.get_cluster_Image(border=1,bkg=0)
    dx=off_set_x
    dy=off_set_y
    im,ax=show_image(image.array)
    for cl in deblended_clusters_list:
        if cl.contour_shape is not None:
            plot_contour(ax,cl.contour_shape[:,0]-dx,cl.contour_shape[:,1]-dy,'w')
    plt.show()


def find_cluster_scale(parent_cluster,
                       h_min_frac,
                       h_max_frac,
                       h_min,
                       h_max,
                       bkg_std,
                       n_samples=10,
                       verbose=False,
                       gl_th_rel=0.1,
                       gl_th_pct=0.2):
    """

    """



    if verbose == True:
        print("|")
        print("|*cluster scale finder  start*")
        print("| h_min_frac", h_min_frac)
        print("| h_max_frac", h_max_frac)
        print("| h_min", h_min)
        print("| h_min", h_max)
        print("| n_samples", n_samples)
        print("| gl_th_rel", gl_th_rel)
        print("| gl_th_pct", gl_th_pct)

    #build image
    bkg = parent_cluster.flux.min()
    image, off_x, off_y, masked = parent_cluster.get_cluster_image_array(bkg=bkg)

    h_min_ = parent_cluster.r_cluster * h_min_frac
    #h_max_ = parent_cluster.r_cluster * h_max_frac
    h_max_ = parent_cluster.r_cluster * h_max_frac

    #print('|h1,h2=', h_min_, h_max_)

    h_min_ = max(h_min_, h_min)
    h_max_ = min(h_max_, h_max)
    h_1=h_min_
    h_2=h_max_


    local_maxima_radii=None

    max_trial=100
    trial=0
    if gl_th_pct is not None:
        v_min = np.percentile(parent_cluster.flux, (gl_th_pct*100))
        #v_min = bkg_std*gl_th_pct+parent_cluster.flux.min()
    else:
        v_min=None
    #
    h_cond_1=True
    h_cond_2=True
    ratio=5.0
    k_max=ratio**(1.0/max_trial)
    k_min=1.0/k_max
    if verbose == True:
        print('|th from gl_th_rel', parent_cluster.flux.max()*gl_th_rel, 'with f min,max', parent_cluster.flux.min(), parent_cluster.flux.max())
        print('|th from gl_th_pct',v_min, 'with f min,max', parent_cluster.flux.min(), parent_cluster.flux.max() )
        print('|GaussLaplace local maxima')
        print('|r_cluster=', parent_cluster.r_cluster)
        print('|h1,h2=', h_min_, h_max_)


    while local_maxima_radii is None and trial<max_trial and (h_cond_1 or h_cond_2):
        blob_log_det=BlobDetectionGaussLaplace(min_sigma=h_min_,
                                           max_sigma=h_max_,
                                           num_sigma=n_samples,
                                           threshold_rel=gl_th_rel,
                                           threshold_abs=v_min,
                                           overlap=0.5)


        local_maxima_xy,local_maxima_flux,local_maxima_radii=blob_log_det.apply(image,coords_in_xy=True,verbose=verbose)
        h_min_*=k_min
        h_max_*=k_max
        h_cond_1 = h_min_ > h_min
        h_cond_2 = h_max_ < h_max
        h_max_ = min(h_max_, h_max)
        h_min_ = max(h_min_, h_min)
        trial+= 1
        #print('ciccio',h_max_,h_min_)
        if local_maxima_radii is None:
            if verbose == True:
                print('!!!!! Warning, no blob detected, changing h_min=%f ,h_max=%f'%(h_min_,h_max_),'trials',trial)
                #print ('local_maxima_radii',local_maxima_radii)
                #print ('->')
    if local_maxima_radii is not None:
        h = local_maxima_radii.mean()
        if verbose == True:
            print('|h from scale_setter', h)
    else:
        h = (h_1+ h_2) * 0.5
        if verbose == True:
            print ('!!!!! Warning, no blob detected, using (h_min,h_max) average',h,'trials',trial)


    if h_min is not None:
        h = max(h, h_min)

    if h_max is not None:
        h = min(h, h_max)

    if verbose == True:
        print('|final h after mix max check', h)

    return local_maxima_xy,local_maxima_flux,local_maxima_radii,h


class ScaleFinder(object):
    def __init__(self,
                 h_min_frac,
                 h_max_frac,
                 h_min,
                 h_max,
                 h_scale,
                 n_samples,
                 gl_th_rel,
                 gl_th_pct):
        self.h_min=h_min
        self.h_max = h_max
        self.h_min_frac = h_min_frac
        self.h_max_frac = h_max_frac
        self.h_scale=h_scale
        self.n_samples = n_samples
        self.gl_th_rel = gl_th_rel
        self.gl_th_pct = gl_th_pct

    def run(self, parent_cluster,bkg_std, verbose=False):
        local_maxima_xy, local_maxima_flux, local_maxima_radii,h=find_cluster_scale(parent_cluster,
                                                                                    h_min_frac=self.h_min_frac,
                                                                                    h_max_frac=self.h_max_frac,
                                                                                    h_min=self.h_min,
                                                                                    h_max=self.h_max,
                                                                                    bkg_std=bkg_std,
                                                                                    n_samples=self.n_samples,
                                                                                    gl_th_rel=self.gl_th_rel,
                                                                                    gl_th_pct=self.gl_th_pct,
                                                                                    verbose=verbose)



        h=h*self.h_scale
        if verbose == True:
           print('|final h after h_scale', h)
        return local_maxima_xy,local_maxima_flux,local_maxima_radii,h


def build_scale_finder(
        h_min_frac,
        h_max_frac,
        h_min,
        h_max,
        h_scale,
        n_samples,
        gl_th_rel,
        gl_th_pct,
        verbose=False):

    if verbose==True:
        print("| h_min_frac",h_min_frac)
        print("| h_max_frac", h_max_frac)
        print("| h_min", h_min)
        print("| h_max", h_max)
        print("| h_scale", h_scale)
        print("| n_samples",n_samples)
        print("| gl_th_rel", gl_th_rel)
        print("| gl_th_pct", gl_th_pct)

    return ScaleFinder(h_min_frac,
                       h_max_frac,
                       h_min,
                       h_max,
                       h_scale,
                       n_samples,
                       gl_th_rel,
                       gl_th_pct)



class DoFindClusterScaleTask(AnalysisTask):
    """


    """
    def __init__(self, name='cluster_scale_finder', func=build_scale_finder, parser=None, process=None):



        super(DoFindClusterScaleTask,self).__init__(name=name,func=func,parser=parser,process=process)


        self.parameters_list.add_par('-h_min_frac', type=np.float,
                                     help=' ', default=0.1)
        self.parameters_list.add_par('-h_max_frac', type=np.float, help=' ',
                                     default=2.0)
        self.parameters_list.add_par('-h_min', type=np.float,
                                    help=' ', default=1.0)
        self.parameters_list.add_par('-h_max', type=np.float,
                                     help=' ', default=10.1)
        self.parameters_list.add_par('-h_scale', type=np.float,
                                     help=' ', default=1.0)
        self.parameters_list.add_par('-n_samples', type=np.int,
                                     help=' ', default=10)
        self.parameters_list.add_par('-gl_th_rel', type=np.float, help='relative threshold for the GaussLaplace local maxima detection', default=0.1)

        self.parameters_list.add_par('-gl_th_pct', type=np.float, help='precentile for absolute threshold for the GaussLaplace local maxima detection ', default=0.2)

        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')

        self.func=func

