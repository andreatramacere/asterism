"""
Overview
--------
This module provides the implementation of the :class:`Table`

Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::
    DoDENCLUEDeblendingTask



mary::
    Table


Summary
---------
.. autosummary::
    :nosignatures:

    DoDENCLUEDeblendingTask
    do_denclue_deblending
    do_denclue_of_cluster
    gauss_laplace_down_sampling

Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import warnings
import  numpy as np
from .deblending import  DeblendedProducts
from scipy import  ndimage

from ....core.clustering.density_based.denclue import  DENCLUE
from ....pipeline_manager.analysis_tasks import AnalysisTask
from ....core.image_processing.features import BlobDetectionGaussLaplace
from ....core.geometry.distance import dist_eval
__author__ = 'andrea tramacere'




def get_local_mean(image, r, c, rows, cols, h):

    r2 = min(rows - 1, r + h)

    c2 = min(cols - 1, c +h)
    return image[r:r2+1,c:c2+1].mean()


def set_coordinates_and_target(coordinates,
                               coordinates_w,
                               ds_coordiantes=None,
                               ds_coordiantes_w=None,
                               local_maxima_coord=None,
                               local_maxima_coord_w=None,
                               use_grid_as_target=False,
                               pure_denclue=False,
                               off_x=0,
                               off_y=0,):
    _coords=coordinates
    _coords_w=coordinates_w
    _target_coords=ds_coordiantes
    _target_coords_w=ds_coordiantes_w


    if local_maxima_coord is not None:
        _local_maxima_coord = local_maxima_coord + [off_x, off_y]

    #local_maxima_coord= None
    if ds_coordiantes is not None and local_maxima_coord is not None:
        if use_grid_as_target==True:
            _target_coords=np.row_stack((ds_coordiantes,_local_maxima_coord))
            _target_coords_w=np.concatenate((ds_coordiantes_w,local_maxima_coord_w))
            _coords=np.row_stack((ds_coordiantes,_local_maxima_coord))
            _coords_w=np.concatenate((ds_coordiantes_w,local_maxima_coord_w))
        else:
            _target_coords=np.row_stack((ds_coordiantes,_local_maxima_coord))
            _target_coords_w=np.concatenate((ds_coordiantes_w,local_maxima_coord_w))
            _coords=coordinates
            _coords_w=coordinates_w

    if ds_coordiantes is not None and local_maxima_coord is None:
        if use_grid_as_target == True:
            _target_coords = ds_coordiantes
            _target_coords_w = ds_coordiantes_w
            _coords = ds_coordiantes
            _coords_w = ds_coordiantes_w

    if ds_coordiantes is  None:
        _target_coords = coordinates
        _target_coords_w = coordinates_w




    if pure_denclue==True:
        _coords = coordinates
        _coords_w = coordinates_w

    #print(_coords.shape, _target_coords.shape)
    return _coords, _coords_w, _target_coords, _target_coords_w


def do_cl_down_sampling(parent_cluster,
                        cluster_image,
                        masked,
                        h,
                        verbose=False,
                        mesh_grid_pix=None,
                        mesh_grid_frac=None,
                        max_h_fraction=1.0,
                        min_size=None,
                        off_x=0,
                        off_y=0):
    """
    Performs the down sampling of the target coordinates to use in the Denclue.

     * the cluster image is extracted
     * if mesh_grid_pix is not provided and mesh_grid_frac is provided
        mesh_grid_pix=sqrt(1.0/mesh_grid_frac)
     * mesh_grid is constrained as
        mesh_grid<=  h * max_h_fraction
        mesh_grid>= 1.-
     * local maxima coordinates are added to target coordinates
     * image is resampled to interp_image with a step equal to step_size, evaluating the average flux within the box with side=step_size
       and corner on grid_coordinate
     * target_coordinates_w_array are obtained from the interp_image flux at grid coordinates


    Parameters
    ----------
    parent_cluster :
    h : float
        sets the  side of the grid and the max_sigma=h, and min_sigma=h*0.5 used
        in the :class:`BlobDetectionGaussLaplace`

    mesh_grid_pix: int
        sets mesh = mesh_grid_pix

    mesh_grid_frac: float
        sets mesh to have number of vertices=cluster_size*mesh_grid_frac

    mesh_grid_frac: float
        sets mesh to have number of vertices=cluster_size*mesh_grid_frac


    max_h_fraction: float
        sets lower limit for mesh i.e mesh>= max_h_fraction*h

    Returns
    -------
    target_coordinates :
    target_coordinates_w_array :
    local_maxima_xy :
    local_maxima_xy_flux :

    """

    target_coordinates=None
    target_coordinates_w_array=None

    if verbose == True:
        print("|")
        print("|*downsampling start*")
        print("|mesh_grid_pix",mesh_grid_pix)
        print("|mesh_grid_frac", mesh_grid_frac)
        print("|max_h_fraction", max_h_fraction)
        print('|initial cluster size', parent_cluster.cl_len)



    # set mesh_grid
    if mesh_grid_pix is None:
        mesh_grid_pix=1.0

    if mesh_grid_frac is not None:
        reduced_size = np.sqrt(np.float(parent_cluster.cl_len) * mesh_grid_frac)
        mesh_grid_pix = np.sqrt(np.float(parent_cluster.cl_len)) / reduced_size
        if verbose == True:
            print("|mesh_grid_pix=%d set from mesh_grid_frac=%f" % (mesh_grid_pix, mesh_grid_frac))


    mesh_grid_pix_min = h * max_h_fraction
    mesh_grid = max(mesh_grid_pix, mesh_grid_pix_min)

    if verbose == True:
        print("|h scale", h)
        print("|initial mesh_grid_pix", mesh_grid_pix)
        print("|mesh_grid_pix min (as a fraction of h)", mesh_grid_pix_min)
        print("|h, max_h_fraction=", h, max_h_fraction)
        print("|mesh_grid set to", mesh_grid)

    mesh_grid = min(min(cluster_image.shape) * 0.5, mesh_grid)
    mesh_grid = max(1., mesh_grid)

    if verbose == True:
        print("|after checking for image size mesh_grid set to", mesh_grid)

    if min_size is not None:
        #print ('ciccio',parent_cluster.cl_len,min_size)
        if parent_cluster.cl_len<min_size:
            mesh_grid=-1

    if verbose == True:
        print("|*final mesh_grid set to*", mesh_grid)

    if mesh_grid <= 1.0:
        target_coordinates=None
        target_coordinates_w_array=None
    else:
        row_sel = np.arange(0, cluster_image.shape[0], mesh_grid).astype(np.int16)
        col_sel = np.arange(0, cluster_image.shape[1], mesh_grid).astype(np.int16)
        if row_sel.size>masked.shape[0] or col_sel.size>masked.shape[1]:
            msg='interpolated coordinate problem'
            warnings.warn(msg)
            row_sel=row_sel[:masked.shape[0]]
            col_sel=col_sel[:masked.shape[1]]

        #print(masked.shape,(row_sel.size,col_sel.size),row_sel.max(),col_sel.max())
        if row_sel.max() >= masked.shape[0] or col_sel.max() >= masked.shape[1]:
            msg='interpolated coordinate problem'
            warnings.warn(msg)
            row_sel = row_sel[row_sel<masked.shape[0]]
            col_sel = col_sel[col_sel<masked.shape[1]]

        y_sel = np.arange(0, cluster_image.shape[0], mesh_grid)
        x_sel = np.arange(0, cluster_image.shape[1], mesh_grid)
        x_t=[]
        y_t=[]
        interp_image = np.zeros(cluster_image.shape, dtype=cluster_image.dtype)
        mgrid = np.zeros(cluster_image.shape, dtype=np.bool)
        for ID_r,row in enumerate(row_sel):


            for ID_c,col in enumerate(col_sel):
                if ~masked[row,col]:
                    mgrid[row][col] = True
                    interp_image[row, col] = get_local_mean(cluster_image, row, col, interp_image.shape[0], interp_image.shape[1],
                                                            np.round(mesh_grid).astype(np.int)+1)
                    x_t.append(x_sel[ID_c])
                    y_t.append(y_sel[ID_r])

        target_coordinates_w_array = interp_image[mgrid]


        x_t = np.array(x_t) + off_x
        y_t = np.array(y_t) + off_y

        target_coordinates = np.column_stack((x_t , y_t ))

    if verbose == True:
        if target_coordinates is not None:
            print("|dowsampled shape", target_coordinates.shape)
        print("|*downsampling stop*")
        print("|")

    return target_coordinates, target_coordinates_w_array


class DownSampler(object):
    def __init__(self,
                 mesh_grid_pix,
                 mesh_grid_frac,
                 min_size,
                 max_h_fraction,
                 use_grid_as_target,
                 downsample_target_fraction,
                 downsample_target_th,
                 verbose=None):

        self.verbose=verbose
        self.mesh_grid_pix = mesh_grid_pix
        self.mesh_grid_frac = mesh_grid_frac
        self.min_size = min_size
        self.max_h_fraction = max_h_fraction
        self.use_grid_as_target = use_grid_as_target
        self.downsample_target_fraction=downsample_target_fraction
        self.downsample_target_th=downsample_target_th

    def run(self, parent_cluster, h,
            verbose=None,
            pure_denclue=False,
            local_maxima_xy=None,
            local_maxima_flux=None):
        if verbose is None:
            verbose=self.verbose
        else:
            verbose=verbose

        _coords = parent_cluster.cartesian.coords
        _weight = parent_cluster.pts_weight

        print('|pre-downsanple coords shape', _coords.shape[0])
        bkg = parent_cluster.flux.min()
        image, off_x, off_y, masked = parent_cluster.get_cluster_image_array(bkg=bkg)

        #print ('|run dwonsampler,')
        ds_coord, ds_weight = do_cl_down_sampling(parent_cluster,
                                                  image,
                                                  masked,
                                                  h,
                                                  mesh_grid_pix=self.mesh_grid_pix,
                                                  mesh_grid_frac=self.mesh_grid_frac,
                                                  min_size=self.min_size,
                                                  max_h_fraction=self.max_h_fraction,
                                                  verbose=verbose,
                                                  off_x=off_x,
                                                  off_y=off_y)



        _coords, _weight, _t_coords, _t_weight = set_coordinates_and_target(_coords,
                                                                            _weight,
                                                                            ds_coordiantes=ds_coord,
                                                                            ds_coordiantes_w=ds_weight,
                                                                            local_maxima_coord=local_maxima_xy,
                                                                            local_maxima_coord_w=local_maxima_flux,
                                                                            use_grid_as_target=self.use_grid_as_target,
                                                                            pure_denclue=pure_denclue,
                                                                            off_x=off_x,
                                                                            off_y=off_y)
        _target_size=_t_coords.shape[0]*self.downsample_target_fraction

        if self.downsample_target_fraction<1.0  and _t_coords.shape[0]>_target_size:
            _size=np.int(_t_coords.shape[0]*self.downsample_target_fraction)
            _selected=np.random.choice(_t_coords.shape[0], _size, replace=False)
            _t_coords=_t_coords[_selected]
            _t_weight=_t_weight[_selected]

        #print('|coords shape',_target_size)
        #if verbose == True:
        print("|dowsampled shape target", _t_coords.shape)
        print("|dowsampled shape coords",_coords.shape)
        print("|")

        return _coords, _weight, _t_coords, _t_weight


def build_downsampler(
    max_h_fraction=1.0,
    downsample_target_fraction=1.0,
    downsample_target_th=100,
    mesh_grid_pix = None,
    use_grid_as_target=False,
    mesh_grid_frac=None,
    min_size=400,
    verbose=False):

    if verbose==True:
        print("| mesh_grid_pix",mesh_grid_pix)
        print("| mesh_grid_frac", mesh_grid_frac)
        print("| use_grid_as_target",use_grid_as_target)
        print("| min_size", min_size)
        print("| max_h_fraction", max_h_fraction)
        print("| downsample_target_fraction", downsample_target_fraction)
        print("| downsample_target_fraction", downsample_target_th)

    return DownSampler(mesh_grid_pix,
                       mesh_grid_frac,
                       min_size,
                       max_h_fraction,
                       use_grid_as_target,
                       downsample_target_fraction,
                       downsample_target_th,
                       verbose)

class DoDownsamplingTask(AnalysisTask):
    """


    """
    def __init__(self, name='down_sampling', func=build_downsampler, parser=None, process=None):



        super(DoDownsamplingTask,self).__init__(name=name,func=func,parser=parser,process=process)


        self.parameters_list.add_par('-min_size', type=np.int,
                                     help='min size to apply downsample', default=400)

        self.parameters_list.add_par('-max_h_fraction', type=np.float, help='max mesh grid  size as a fraction of kernel width', default=1.0)


        self.parameters_list.add_par('-use_grid_as_target',
                                     help='fraction of targed to use', action='store_true')

        self.parameters_list.add_par('-downsample_target_fraction',type=np.float,
                                     help='uses also ds coordinates as target', default=1.0)

        self.parameters_list.add_par('-downsample_target_th', type=np.int,
                                     help='uses also ds coordinates as target', default=100)

        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')
        group_method = self.parameters_list.add_mex_group('mesh_grid')
        group_method.add_par('-mesh_grid_pix', type=np.float,
                             help='mesh of the grid in pix', default=None)
        group_method.add_par('-mesh_grid_frac', type=np.float,
                             help='mesh of the grid derived from fraction of image size',
                             default=None)


        self.func=func

