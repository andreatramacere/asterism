"""
This modules provides the implementation of the :class:`.DoImageProcessing` class
used to handle the image processing :class:`.AnalysisTask`.
The tasks is implemented by the :func:`.do_image_processing`


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.analysis_tasks.image_processing.image_processing.DoImageProcessing




Summary
---------
.. autosummary::
   do_image_processing
   DoImageProcessing


Module API
-----------
"""


from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ...pipeline_manager.analysis_tasks import AnalysisTask
from ...core.image_processing.filters import *
from ...core.image_manager.image import Image
__author__ = 'andrea tramacere'

def do_image_processing(image,
                        #do_asinh_contarst_filter=False,
                        #asinh_ontrast_filter_beta=50,
                        do_median_filter=True,
                        median_filter_window_size=2,
                        do_gauss_filter=False,
                        gauss_filter_sigma=1,
                        do_gauss_laplace_filter=False,
                        gauss_laplace_filter_s=0.1,
                        do_nlm_filter=False,
                        nlm_filter_h=0.1,
                        nlm_filter_patch_size=7,
                        nlm_filter_patch_distance=11,
                        verbose=False):
    """
    Function that implements the application of the different Filters.
    The filters are :class:`.filters.ImageFilter` derived objects, and are applied to :class:`.Image` objects
    using the :meth:`.Image.filter` with  `inplace=True`

    Parameters
    ----------
    image : :class:`.Image` obj:
        input image obj

    do_median_filter : bool
        boolean par to set on :class:`.MedianFilter` filter

    median_filter_window_size : int
        par for the  `.MedianFilter` filter

    do_gauss_filter : bool
        boolean par to set on :class:`.GaussianFilter` filter

    gauss_filter_sigma float :
        par for the :class:`.GaussianFilter` filter

    do_gauss_laplace_filter : bool
        boolean par to set on :class:`.GaussLaplaceFilter` filter

    gauss_laplace_filter_s
        par for the :class:`.GaussLaplaceFilter` filter

    do_nlm_filter : bool
        boolean par to set on :class:`.NonLocalMeansDenoising` filter

    nlm_filter_h
        par for the :class:`.NonLocalMeansDenoising` filter

    nlm_filter_patch_size
         par for the :class:`.NonLocalMeansDenoising` filter

    nlm_filter_patch_distance
         par for the :class:`.NonLocalMeansDenoising` filter

    Returns
    -------

    """

    if  isinstance(image,Image):
        pass

    else:
        raise RuntimeError('image should be instance of Image %s'%Image)

    filters=[]



    if do_gauss_filter is True:

        gauss_filter=GaussianFilter(sigma=gauss_filter_sigma)
        filters.append(gauss_filter)

    if do_median_filter is True:

        median_filter=MedianFilter(window_size=median_filter_window_size)
        filters.append(median_filter)

    if do_nlm_filter is True:
        nl_filter=NonLocalMeansDenoising(h=nlm_filter_h,patch_distance=nlm_filter_patch_distance,patch_size=nlm_filter_patch_size)
        filters.append(nl_filter)

    if do_gauss_laplace_filter is True:
        gl_filter=GaussLaplaceFilter(sigma=gauss_laplace_filter_s)
        filters.append(gl_filter)

    for filt in filters:
        if verbose==True:
            print("| filtering with ",filt.name)
        image.filter(filt,inplace=True)
        #image.show()

class DoImageProcessing(AnalysisTask):
    """
     :class:`.AnalysisTask` derived class that implements the   image processing Task:


    Parameters
    ----------
    name : str
        the name for the Process

    func : callable
        The function that implements the task algorithm , :func:`.do_image_processing` in this case

    image_id : int
        id of the image


    parser :



    """
    def __init__(self,name='do_image_processing',func=do_image_processing,parser=None):
        super(DoImageProcessing,self).__init__(name=name,func=func,parser=parser)
        #self.parameters_list.add_par('-do_asinh_contarst_filter',help='set  contrast filter on',action='store_true')
        #self.parameters_list.add_par('-asinh_ontrast_filter_beta',  type=np.float ,help='beta param for contrat filter', default=50.0 )
        self.parameters_list.add_par('-do_median_filter',help='set  median filter on',action='store_true')
        self.parameters_list.add_par('-do_gauss_laplace_filter',help='set  median filter on',action='store_true')
        self.parameters_list.add_par('-gauss_laplace_filter_s', type=np.float ,help='s param for gauss_laplace filter', default=0.1)
        self.parameters_list.add_par('-median_filter_window_size',  type=np.int ,help='median filter size in pixels', default=2 )
        self.parameters_list.add_par('-do_gauss_filter',help='set  gaussian filter on',action='store_true')
        self.parameters_list.add_par('-gauss_filter_sigma',  type=np.float ,help='gauss filter sigma size in pixels', default=1.0 )
        self.parameters_list.add_par('-do_nlm_filter',help='set  non local mean denosing filter on',action='store_true')
        self.parameters_list.add_par('-nlm_filter_h',  type=np.float ,help='', default=0.1 )
        self.parameters_list.add_par('-nlm_filter_patch_size',  type=np.int ,help='', default=7 )
        self.parameters_list.add_par('-nlm_filter_patch_distance',  type=np.int ,help='', default=11 )
