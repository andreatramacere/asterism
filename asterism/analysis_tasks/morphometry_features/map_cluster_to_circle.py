
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ...pipeline_manager.analysis_tasks import AnalysisTask
from ...core.image_processing.geometry_transformation import RotateImage,MapToCircle
from ...core.clustering.connectivity_based.connected_components import  ConnectedComponents
from ...core.clustering.density_based.dbscan_binned import DBSCANBinned
from ...core.clustering.connectivity_based import connected_components
from ...core.clustering.cluster_tools import get_cluster_closest_to_position

import  numpy as np

__author__ = 'andrea tramacere'

def do_map_cluster_to_circle_func(cluster,image_bkg_value,cluster_th_level,eval):
    """
    Maps a cluster to circular shape: deprojection

    Args:
        cluster:
        bkg_lvl:

    Returns:

    """


    #Cluster Image

    image_circular,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=image_bkg_value,border=2)
    #bkg=cluster.flux.min()
    #Performs geom transformation of the image
    map_to_circle=MapToCircle(a=cluster.sig_x,b=cluster.sig_y,angle=cluster.semi_major_angle)
    image_circular.geom_transformation(map_to_circle,inplace=True)


    x_c=image_circular._center_x
    y_c=image_circular._center_y

    #conn=ConnectedComponents(min_size=4)
    #conn.run(image_circular.array,bkg_lvl,masked=image_circular.masked)



    #masked_bg=image_circular.array<cluster_th_level
    #masked=np.logical_or(masked_bg,image_circular.masked)

    conn=ConnectedComponents(1)
    conn.run(image_circular.array,cluster_th_level,masked=image_circular.masked)



    clusters_list=[]

    print('|clusters found segmenting rotated image',len(conn.clusters_list),'with bkg level',cluster_th_level)

    if conn.clusters_list!=[]:
        central_cluster=get_cluster_closest_to_position(conn.clusters_list,x_c=x_c,y_c=y_c)
        return  central_cluster.SourceCluster_factory(ID=cluster.ID)
    else:
        return  None

    #for ID,cl in enumerate(conn.clusters_list):
    #    #print("ID",ID)
    #    clusters_list.append(cl.SourceCluster_factory(ID=ID))


    #if len(conn.clusters_list)>-1:
    #    print ('cl_id',cluster.ID)
    #   from asterism.plotting.plot_tools import show_image,plot_contour
    #    #image,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=image_bkg_value,border=2)
    #    image_circular.show()
    #    import pylab as plt
    #    im,ax=show_image(image_circular.array)


    #    for cl in clusters_list:
    #        if hasattr(cl,'contour_shape'):
    #            if cl.contour_shape is not None:
    #                plot_contour(ax,cl.contour_shape[:,0],cl.contour_shape[:,1],'w')


    #        ax.scatter(cl.x_c,cl.y_c,marker='s', edgecolor='black', linewidth='0',facecolor='white',s=10)

    #    plt.show()

    #central_cluster=get_cluster_closest_to_position(clusters_list,x_c=x_c,y_c=y_c)

    return  central_cluster.SourceCluster_factory(ID=cluster.ID)


class DoMapClusterToCircle(AnalysisTask):

    def __init__(self, name='map_cluster_to_circle', func=do_map_cluster_to_circle_func, parser=None, process=None):
        super(DoMapClusterToCircle,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-eval',help=' ',action='store_true')
        self.func=func

