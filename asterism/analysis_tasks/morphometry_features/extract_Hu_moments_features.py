from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.non_parametric.Hu_moments import get_attractors_Hu_moments_morphometric_features,get_cluster_contour_Hu_moments_morphometric_features,get_cluster_image_Hu_moments_morphometric_features
from ...pipeline_manager.analysis_tasks import AnalysisTask

__author__ = 'andrea tramacere'



def do_extract_Hu_moments_features(prod_original,prod_mapped_to_circle,image_id,extract,gabor_prod=None,unsharp_prod=None):

    feat_list=[]


    for p,flag in zip([prod_original,prod_mapped_to_circle,gabor_prod],['ic','depr','gabor','unsh']):
        if p is None:
            attr_polar=None
            attr_cartesian=None
            cluster=None
            image=None
            x_c_image=None
            y_c_image=None
        else:
            attr_polar=p.attr_polar
            attr_cartesian=p.attr_cartesian
            cluster=p.cluster
            image=p.image
            x_c_image=p.x_c_image
            y_c_image=p.y_c_image

        #contour
        feat_list.append(get_cluster_contour_Hu_moments_morphometric_features(cluster,group_name='cnt_log_Hu',name_flag=flag))

        #image
        feat_list.append(get_cluster_image_Hu_moments_morphometric_features(image,[x_c_image,y_c_image],group_name='img_log_Hu',name_flag=flag))

        #attractors
        feat_list.append(get_attractors_Hu_moments_morphometric_features(attr_polar,group_name='attr_polar_log_Hu',name_flag=flag))
        feat_list.append(get_attractors_Hu_moments_morphometric_features(attr_cartesian,group_name='attr_log_Hu',name_flag=flag))


    return feat_list



class DoExtractHuMomentsFeatures(AnalysisTask):

    def __init__(self, name='extract_Hu_moments_features', func=do_extract_Hu_moments_features, parser=None, process=None):
        super(DoExtractHuMomentsFeatures,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-extract',help='sets on feature extraction for this group',action='store_true')
        self.func=func

