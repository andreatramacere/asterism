from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.non_parametric.attractors_polar_distr import get_attractors_r_distr_morphometric_features,get_attractors_theta_distr_morphometric_features
from asterism.core.morphometry.non_parametric.gabor_features import get_gabor_morphometric_features
from ...pipeline_manager.analysis_tasks import AnalysisTask

__author__ = 'andrea tramacere'



def do_extract_attractors_radial_distr_features(prod_original,
                                                prod_mapped_to_circle,
                                                image_id,
                                                extract,
                                                gabor_prod=None,
                                                unsharp_prod=None):

    feat_list=[]




    for p,flag in zip([prod_original,prod_mapped_to_circle,gabor_prod],['ic','depr','gabor_cl']):
        if p is None:
            attr_polar=None
            non_core_attr_polar=None
            n_points=None
        else:
            attr_polar=p.attr_polar
            non_core_attr_polar=p.non_core_attr_polar
            n_points=p.cluster.n_points

        feat_list.append(get_attractors_r_distr_morphometric_features(attr_polar,non_core_attr_polar,n_points,name_flag=flag))
        feat_list.append(get_attractors_theta_distr_morphometric_features(non_core_attr_polar,name_flag=flag))


    if gabor_prod is not None:
        gabor_w1=gabor_prod.gabor_w1
        gabor_w2=gabor_prod.gabor_w2
    else:
        gabor_w1=[-99.]*4
        gabor_w2=[-99.]*4

    feat_list.append(get_gabor_morphometric_features(gabor_w1,gabor_w2,name_flag='gabor_cl'))

    return feat_list

class DoExtractAttractorsRadialDistrFeatures(AnalysisTask):

    def __init__(self, name='extract_gal_shape_features', func=do_extract_attractors_radial_distr_features, parser=None, process=None):
        super(DoExtractAttractorsRadialDistrFeatures,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-extract',help='sets on feature extraction for this group',action='store_true')
        self.func=func

