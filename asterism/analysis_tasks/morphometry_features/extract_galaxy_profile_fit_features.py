from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.galaxy_profile_fit import get_galaxy_profile_fit_morphometric_features
from ...pipeline_manager.analysis_tasks import AnalysisTask

__author__ = 'andrea tramacere'





def do_extract_galaxy_profile_fit_features(prod_original,prod_mapped_to_circle,image_id,extract,gabor_prod=None):

    feat_list=[]


    for p,flag in zip([prod_original,prod_mapped_to_circle],['ic','depr']):
        if p is None:
            cluster=None
        else:
            surf_brigth_prof=p.surf_brigth_prof
            image=p.image
            image_centroid=[p.x_c_image,p.y_c_image]

            feat_list.append(get_galaxy_profile_fit_morphometric_features(surf_brigth_prof,
                                                                          cluster_image=image,
                                                                          image_centroid=image_centroid,
                                                                          name_flag=flag))


    return feat_list

class DoExtractGalaxyProfileFitFeatures(AnalysisTask):

    def __init__(self, name='extract_gal_shape_features', func=do_extract_galaxy_profile_fit_features, parser=None, process=None):
        super(DoExtractGalaxyProfileFitFeatures,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-extract',help='sets on feature extraction for this group',action='store_true')
        self.func=func