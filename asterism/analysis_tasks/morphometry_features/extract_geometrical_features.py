"""
Test
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.non_parametric.geometry_features import get_cluster_geometric_morphometric_features
from ...pipeline_manager.analysis_tasks import AnalysisTask

__author__ = 'andrea tramacere'



def do_extract_geometric_features(prod_original,prod_mapped_to_circle,image_id,extract,gabor_prod=None,unsharp_prod=None):
    """
    Extracts geometric features using the :func:`get_cluster_geometric_morphometric_features`

    Args:
        prod_original  (:class:`asterism.analysis_processes.morphometric_features_input_products_euclid.MorphometryInputData`):
        prod_mapped_to_circle  (:class:`asterism.analysis_processes.morphometric_features_input_products_euclid.MorphometryInputData`):
        image_id (int): id of the image

    Returns:
        feat_list ( list of :class:`asterism.core.morphometry.features.MorphometricFeatures`]):
        list of MorphometricFeatures objects for the geometric group

    """


    feat_list=[]
    for p,flag in zip([prod_original,prod_mapped_to_circle,unsharp_prod],['ic','depr','unsh']):
        if p is None:
            cluster=None
        else:
            cluster=p.cluster

        feat_list.append(get_cluster_geometric_morphometric_features(cluster,group_name='geom',name_flag=flag))


    return feat_list

class DoExtractGeometricFeatures(AnalysisTask):

    def __init__(self, name='extract_profile_fit_features', func=do_extract_geometric_features, parser=None, process=None):
        super(DoExtractGeometricFeatures,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-extract',help='sets on feature extraction for this group',action='store_true')
        self.func=func



