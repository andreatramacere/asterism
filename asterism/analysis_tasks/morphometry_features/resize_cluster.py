
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ...pipeline_manager.analysis_tasks import AnalysisTask
from ...core.image_processing.geometry_transformation import RescaleImage
from ...core.clustering.connectivity_based.connected_components import  ConnectedComponents
from ...core.clustering.cluster_tools import get_cluster_closest_to_position
from ...core.image_manager.image import Image
import  numpy as np

__author__ = 'andrea tramacere'




def do_resize_cluster_func(cluster,image_bkg_value,cluster_th_level,size=25):
    """
    Maps a cluster to circular shape: deprojection

    Args:
        cluster:
        bkg_lvl:

    Returns:

    """


    #Cluster Image
    if cluster.r_max>size and size is not None:
        image_resized,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=image_bkg_value,border=2)

        seg_map_resized=Image.from_array(np.zeros(image_resized.shape))
        seg_map_resized.array[[cluster.cartesian.y.astype(np.int32)-off_set_y, cluster.cartesian.x.astype(np.int32)-off_set_x]] = 1

        print("resizing size",size)
        print('cluster f min,max',cluster.flux.min(),cluster.flux.max())
        print('im_original f min,max',image_resized.array[~masked_pixels].min(),image_resized.array[~masked_pixels].max())
        resize=RescaleImage(scale=float(size)/cluster.r_max)
        image_resized.geom_transformation(resize,inplace=True,order=3,preserve_range=True,clip=False)
        seg_map_resized.geom_transformation(resize,inplace=True,order=3,preserve_range=True,clip=False)
        print('im_res f min,max',image_resized.array.min(),image_resized.array.max())


        conn = ConnectedComponents(1)
        conn.run(image_resized.array, 0, seg_map=seg_map_resized.array, seg_map_bkg=0)

        clusters_list=[]

        print('|clusters found segmenting rotated image',len(conn.clusters_list),'with bkg level',cluster_th_level)




        central_cluster=get_cluster_closest_to_position(conn.clusters_list,x_c=image_resized._center_x,y_c=image_resized._center_y)
        print('|central_cluster',central_cluster.size)

        return central_cluster.SourceCluster_factory(ID=cluster.ID)

    else:
        return cluster

    # if len(conn.clusters_list)>-1:

    #for ID,cl in enumerate(conn.clusters_list):
    #   #print("ID",ID)
    #   clusters_list.append(cl.SourceCluster_factory(ID=ID))


    #     print ('cl_id',cluster.ID)
    #     from asterism.plotting.plot_tools import show_image,plot_contour
    #     #image,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=image_bkg_value,border=2)
    #     #image_resized.show()
    #     import pylab as plt
    #     im,ax=show_image(image_resized.array)
    #
    #
    #     for cl in clusters_list:
    #         if hasattr(cl,'contour_shape'):
    #             if cl.contour_shape is not None:
    #                plot_contour(ax,cl.contour_shape[:,0],cl.contour_shape[:,1],'w')
    #
    #         if cl==central_cluster:
    #             print (cl.x_c,cl.y_c)
    #
    #         ax.scatter(cl.x_c,cl.y_c,marker='s', edgecolor='black', linewidth='0',facecolor='white',s=10)
    #
    #     plt.show()
    #
    # #




class DoResizeCluster(AnalysisTask):

    def __init__(self, name='resize_cluster', func=do_resize_cluster_func, parser=None, process=None):
        super(DoResizeCluster,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-size',  type=np.int ,help='',default=25)
        self.func=func

