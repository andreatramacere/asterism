from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np
from asterism.core.morphometry.non_parametric.morphology import get_cluster_morphology_morphometric_features
from ...pipeline_manager.analysis_tasks import AnalysisTask

__author__ = 'andrea tramacere'





def do_extract_morphology_features(prod_original,
                                   prod_mapped_to_circle,
                                   image_id,
                                   extract,
                                   gabor_prod=None,
                                   blurring_scale=0.3,
                                   clump_cover_factor=0.05,
                                   petrosian_factor=1.0,
                                   unsharp_prod=None,
                                   plot=False,
                                   delta_clump=5,
                                   mesh_asymm=1,
                                   delta_asymm=3.0):

    feat_list=[]


    #contour

    for p,flag in zip([prod_original,prod_mapped_to_circle,unsharp_prod],['ic','depr','unsh']):
        if p is None:
            cluster=None
            image=None
            image_centroid=None
            surf_brigth_prof=None
            bkg_image=None
            bkg_level=None
            binary_image=None
        else:
            surf_brigth_prof=p.surf_brigth_prof
            cluster=p.cluster
            image=p.image
            bkg_image=p.bkg_image
            image_centroid=[p.x_c_image,p.y_c_image]
            binary_image=p.binary_image
            bkg_level=p.bkg_level

        feat_list.append(get_cluster_morphology_morphometric_features(cluster,
                                                                      image,
                                                                      bkg_image,
                                                                      bkg_level,
                                                                      binary_image,
                                                                      image_centroid,
                                                                      surf_brigth_prof,
                                                                      clump_cover_factor=clump_cover_factor,
                                                                      blurring_scale=blurring_scale,
                                                                      petrosian_factor=petrosian_factor,
                                                                      mesh_asymm=mesh_asymm,
                                                                      delta_asymm=delta_asymm,
                                                                      delta_clump=delta_clump,
                                                                      name_flag=flag,
                                                                      plot=plot))

    return feat_list



class DoExtractMorphologyFeatures(AnalysisTask):

    def __init__(self, name='extract_morph_features', func=do_extract_morphology_features, parser=None, process=None):
        super(DoExtractMorphologyFeatures,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-blurring_scale', type=np.float,help='', default=0.3)
        self.parameters_list.add_par('-mesh_asymm', type=np.int, help='', default=1)
        self.parameters_list.add_par('-delta_asymm', type=np.float, help='', default=3.0)
        self.parameters_list.add_par('-delta_clump', type=np.int, help='', default=5)
        self.parameters_list.add_par('-petrosian_factor', type=np.float, help='', default=1.0)
        self.parameters_list.add_par('-clump_cover_factor', type=np.float, help='', default=0.05)
        self.parameters_list.add_par('-extract',help='sets on feature extraction for this group',action='store_true')
        self.parameters_list.add_par('-plot', help='', action='store_true')
        self.func=func