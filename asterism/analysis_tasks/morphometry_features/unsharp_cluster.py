

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ...pipeline_manager.analysis_tasks import AnalysisTask
from ...core.image_processing.filters import UnsharpFilter
from ...core.clustering.connectivity_based.connected_components import  ConnectedComponents
from ...core.clustering.cluster_tools import get_cluster_closest_to_position

import  numpy as np

__author__ = 'andrea tramacere'




def do_unsharp_cluster_func(original_cluster,image_bkg_value,cluster_th_level,u=0.8,sigma_factor=1.0,eval=True):
    """
    Maps a cluster to circular shape: deprojection

    Args:
        cluster:
        bkg_lvl:

    Returns:

    """



    image_original,off_set_x,off_set_y,masked_pixels=original_cluster.get_cluster_Image(bkg=image_bkg_value,border=2)
    print(original_cluster.flux.min(),original_cluster.flux.max())
    #image_resized.show()
    #bkg=cluster.flux.min()
    #Performs geom transformation of the image
    #print("resizing size",size)
    unsh=UnsharpFilter(sigma=original_cluster.r_cluster*sigma_factor,k=u)
    image_f=image_original.filter(unsh,inplace=False)



    x_c=image_f._center_x
    y_c=image_f._center_y

    #conn=ConnectedComponents(min_size=4)
    #conn.run(image_circular.array,bkg_lvl,masked=image_circular.masked)


    cluster_th_level=original_cluster.flux.min()
    #masked_bg=image_circular.array<cluster_th_level
    #masked=np.logical_or(masked_bg,image_circular.masked)
    conn=ConnectedComponents(1)
    conn.run(image_f.array,cluster_th_level,masked=image_original.masked)



    clusters_list=[]

    print('|clusters found segmenting unsharp image',len(conn.clusters_list),'with bkg level',cluster_th_level)




    central_cluster=get_cluster_closest_to_position(conn.clusters_list,x_c=x_c,y_c=y_c)

    # import pylab as plt
    # from asterism.plotting.plot_tools import show_image,plot_contour
    # fig,(ax1,ax2)=plt.subplots(1,2)
    # ax1.imshow(image_original.array,interpolation='nearest')
    # ax2.imshow(image_f.array,interpolation='nearest')
    # plot_contour(ax1,original_cluster.contour_shape[:,0]-off_set_x,original_cluster.contour_shape[:,1]-off_set_y,'w')
    # for cl in conn.clusters_list:
    #         cl=cl.SourceCluster_factory()
    #         if hasattr(cl,'contour_shape'):
    #             if cl.contour_shape is not None:
    #                 plot_contour(ax2,cl.contour_shape[:,0],cl.contour_shape[:,1],'w')
    # plt.show()
    if central_cluster is not None:
        for ID,pos in enumerate(central_cluster.position_array):
            central_cluster.weight_array[ID]=image_original.array[np.int(pos[1]),np.int(pos[0])]
        print('central cluster cl_size',central_cluster.size)
        return central_cluster.SourceCluster_factory(ID=original_cluster.ID,x_off_set=off_set_x,y_off_set=off_set_y)
    else:
        return original_cluster



class DoUnsharpCluster(AnalysisTask):

    def __init__(self, name='unsharp_cluster', func=do_unsharp_cluster_func, parser=None, process=None):
        super(DoUnsharpCluster,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-eval',help=' ',action='store_true')
        self.parameters_list.add_par('-u',  type=np.float ,help='',default=0.5)
        self.parameters_list.add_par('-sigma_factor',  type=np.float  ,help='',default=1.0)
        self.func=func

