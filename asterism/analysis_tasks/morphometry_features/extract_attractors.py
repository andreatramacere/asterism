
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np

from ...core.clustering.density_based.denclue import  DENCLUE
from ...pipeline_manager.analysis_tasks import AnalysisTask
from ...core.image_processing.features import BlobDetectionGaussLaplace,PeakLocalMaxima
from ...core.image_processing.filters import GaussLaplaceFilter
from ...core.clustering.density_based.denclue_tools import  donwsample_cluster_target_attractors
from ...core.stats.histogram.radial_distributions import  radial_polar_local_maxima
__author__ = 'andrea tramacere'

def plot_attr(denclue_attr_finder,cluster,core,lm,ID,R_in,target_coordinates=None,plot=True,ax=None,show_plot=False):
    """

    Parameters
    ----------
    denclue_attr_finder
    cluster
    core
    lm
    ID
    R_in

    Returns
    -------

    """
    from asterism.plotting.plot_tools import show_image,plot_contour
    from matplotlib.patches import Ellipse
    import pylab as plt


    image,dx,dy,masked=cluster.get_cluster_image_array()
    im,ax=show_image(image,ax=ax)
    for cl in denclue_attr_finder.attractors_clusters_list:
        x=cl.coords[:,0]
        y=cl.coords[:,1]
        #print (cl.ID)
        ax.plot(x-dx,y-dy,'.',ms=0.5,c='w')




    #ellipse=Ellipse(xy=(cluster.x_c-dx,cluster.y_c-dy), width=R_in*2, height=R_in*2,edgecolor='y',fc='None')
    #ax.add_patch(ellipse)

    #if core is not None:
    #    x=core.position_array[:,0]
    #    y=core.position_array[:,1]
    #    ax.scatter(x-dx,y-dy,marker='o', edgecolor='black', linewidth='0',facecolor='black',s=15)
    #if lm is not None:
    #    ax.scatter(lm[:,0],lm[:,1],marker='o', edgecolor=None, linewidth='0',facecolor='white',s=20)

    #plot_contour(ax,cluster.contour_shape[:,0]-dx,cluster.contour_shape[:,1]-dy,'gray')
    #if target_coordinates is not None:
    #    x_t=target_coordinates[:,0]-dx
    #    y_t=target_coordinates[:,1]-dy
    #    ax.plot(x_t,y_t,'.',ms=1.0,c='w')

    ax.set_title(ID+1)
    if show_plot==True:
        plt.show()



def radial_local_maxima(polar_r,polar_theta,flux_values,r_cluster):
    ids=radial_polar_local_maxima(polar_r,polar_theta,flux_values,N_bins=r_cluster*2,use_equal_area_annuli=True)
    return ids

def find_core_attractor_cluster(cluster,attractors_clusters,R):
    within_list,dist_array_within =cluster.get_clusters_within_centroid(attractors_clusters,R=R,centre=[cluster.x_c,cluster.y_c])

    if  len(within_list)>0:
        core_id=np.argmin(dist_array_within['dist_min'])
        core_cl=within_list[core_id]
    else:
        core_cl=None


    return core_cl

def find_core_local_maxima(cluster,local_maxima_xy,R):
    return local_maxima_xy[cluster.check_is_within_r_from_center(x_test=local_maxima_xy[:,0],y_test=local_maxima_xy[:,1],R=R)]



def find_attractors_and_lm(cluster,\
                           denclue_attr_finder,\
                           h_1,\
                           h_2,\
                           R_in,\
                           mask_unchanged,
                           downsampling=False,\
                           weight_cluster=None,\
                           check_core_cluster=True,
                           image_id=None):
    """

    Parameters
    ----------
    cluster
    denclue_attr_finder
    h_1
    h_2
    mask_unchanged
    downsampling
    image_id

    Returns
    -------

    """
    blob_log_det=BlobDetectionGaussLaplace(min_sigma=h_1,
                                           max_sigma=h_2,
                                           overlap=1.0,
                                           threshold_rel=0.05)




    image,dx,dy,masked=cluster.get_cluster_image_array()
    local_maxima_xy,local_maxima_flux,local_maxima_radii=blob_log_det.apply(image,coords_in_xy=True)
    if local_maxima_xy is not None:
        local_maxima_xy_core= find_core_local_maxima(cluster,local_maxima_xy+[dx,dy],R_in)
        print("core lm",len(local_maxima_xy_core))
    else:
        local_maxima_xy_core=None



    if downsampling==True:
        print ("|downsampling",h_1,h_2)
        print("|size", cluster.cl_len)
        target_coordinates,target_coordinates_w_array=donwsample_cluster_target_attractors(cluster,h_2)
        #ids=radial_local_maxima(cluster.polar.r,
        #                        cluster.polar.theta,
        #                        cluster.pts_weight,
        #                        cluster.r_max)
        #target_coordinates=cluster.cartesian.coords[ids]
        #target_coordinates_w_array=cluster.pts_weight[ids]
        print("|size",target_coordinates.size)
    else:
        target_coordinates,target_coordinates_w_array=None,None

    if weight_cluster is not None:
        target_coordinates=weight_cluster.cartesian.coords
        target_coordinates_w_array=weight_cluster.pts_weight

    eps=denclue_attr_finder.eps

    denclue_attr_finder.run(cluster.cartesian.coords,\
                                h=h_1,
                                eps=eps,
                                attractors_th=0.01,
                                make_convolved_image=False,
                                get_only_attractors=False,\
                                weight_array=cluster.pts_weight,\
                                mask_unchanged=mask_unchanged,\
                                target_coordinates=target_coordinates,\
                                target_coordinates_w_array=target_coordinates_w_array,\
                                digitize_attractors=False)




    core_cl=find_core_attractor_cluster(cluster,denclue_attr_finder.attractors_clusters_list,R_in)


    if check_core_cluster ==True:
        max_trials=10
        trials  =0

        while core_cl is None and eps>0.05 and trials<max_trials:
            trials+=1
            eps=eps*0.95
            h_1=h_1*1.05
            mask_unchanged=eps
            denclue_attr_finder.run(cluster.cartesian.coords,\
                                    h=h_1,
                                    eps=eps,
                                    attractors_th=0.01,
                                    make_convolved_image=False,
                                    get_only_attractors=False,\
                                    weight_array=cluster.pts_weight,\
                                    mask_unchanged=mask_unchanged,\
                                    target_coordinates=target_coordinates,\
                                    target_coordinates_w_array=target_coordinates_w_array,\
                                    digitize_attractors=False)


            core_cl=find_core_attractor_cluster(cluster,denclue_attr_finder.attractors_clusters_list,R_in)

            if core_cl is not None:
                ratio=core_cl.r_cluster/cluster.r_cluster
                print ("|h=",h_1," core cl=",core_cl.ID, " core radius=",core_cl.r_cluster," ratio=",ratio)

        if core_cl is None:
             core_cl=find_core_attractor_cluster(cluster,denclue_attr_finder.attractors_clusters_list,cluster.r_max)


        if core_cl is not None:
            ratio=core_cl.r_cluster/cluster.r_cluster

            non_core_attrs=0
            for cl in denclue_attr_finder.attractors_clusters_list:
                if cl!=core_cl:
                  non_core_attrs+=cl.n_points

            ratio_attr=np.float(non_core_attrs)/np.float(core_cl.n_points)
            ratio_attr_1=np.float(non_core_attrs)/np.float(cluster.n_points)
            print ("|h=",h_1," core cl=",core_cl.ID, " core radius",core_cl.r_cluster,' ratio=',ratio,' ratio attr=',ratio_attr,ratio_attr_1)




    return denclue_attr_finder.attractors_clusters_list,core_cl,local_maxima_xy,local_maxima_xy_core,target_coordinates


def make_denclue_attr_finder(cluster,\
                             kernel,\
                             eps,\
                             h_frac,\
                             h_min,\
                             attr_dbs_K,\
                             attr_dbs_eps,\
                             mask_unchanged,\
                             k_table_size,\
                             R_max_frac,\
                             R_max_kern_th,\
                             digitize_attractors=False,
                             downsampling=False,\
                             image_id=None):
    """

    Parameters
    ----------
    cluster
    kernel
    eps
    h_frac
    h_min
    attr_dbs_K
    attr_dbs_eps
    mask_unchanged
    k_table_size
    R_max_frac
    R_max_kern_th
    digitize_attractors
    downsampling
    image_id

    Returns
    -------

    """
    print ("|set denclue par for cluster",cluster.ID, "r_cluter",cluster.r_cluster)




    h_start=h_frac*cluster.r_cluster

    if h_start<h_min:
        h_start=h_min



    if R_max_frac is not None:
        R_max_method='fixed'
        R_max=R_max_frac*cluster.r_cluster
        R_max=max(h_start,R_max)
    elif R_max_kern_th is not None:
        R_max_method='kern_rel_th'
        R_max=None
    else:
        R_max_method='auto'
        R_max=None

    print ("eps",eps)

    denclue_attr_finder=DENCLUE(eps=eps,
                         h=h_start,
                         kernel=kernel,
                         k_table_size=k_table_size,
                         R_max_method=R_max_method,
                         R_max_th_rel=R_max_kern_th,
                         R_max=R_max,
                         dbscan_eps=attr_dbs_eps,
                         dbscan_K=attr_dbs_K)

    print ("| h_start",h_start)

    return denclue_attr_finder,h_start


def attractors_seg_map(image,cluster,core_cl,attractors_cl_list):
    seg_map = np.zeros(image._shape, dtype=np.int32)
    cl_image=np.zeros(image._shape, dtype=np.float32)
    _image, dx, dy, masked = cluster.get_cluster_image_array(border=2)
    (y1,x1)=image._shape
    (y2,x2)= _image.shape

    ddx=np.int(max(0,np.abs(x2-x1)*0.5-1))
    ddy=np.int(max(0,np.abs(y2-y1)*0.5-1))
    for x, y in cluster.cartesian.coords.astype(np.int32):
        #print(x,y)
        cl_image[y, x] = _image[y-dy,x-dx]

    for cl in attractors_cl_list[:]:
        if cl==core_cl:
            v=1
        else:
            v=2
        for x, y in cl.cartesian.coords.astype(np.int32):
            seg_map[y, x] = v

    #import  pylab as plt
    #plt.imshow(cl_image)
    #plt.show()
    #plt.imshow(seg_map)
    #plt.show()
    return seg_map,cl_image

def do_find_denclue_attractors(cluster,\
                               image=None,
                               eval=True,
                               eps=0.1, \
                               h_frac=0.25,\
                               h_min=1.5,\
                               kernel='gauss',\
                               attr_dbs_eps=1.0,\
                               attr_dbs_K=4.0,\
                               core_ratio_1=0.05,\
                               core_ratio_2=0.20,\
                               mask_unchanged=0.5,\
                               k_table_size=None,\
                               min_size=9.0,\
                               R_max_frac=1.0,\
                               R_max_kern_th=None,\
                               plot=False,\
                               show_plot=False,
                               ax=None,
                               out_seg_map=False,
                               verbose=False, \
                               check_core_cluster=False,
                               weight_cluster=None,
                               digitize_attractors=False,\
                               downsampling=False,\
                               image_id=None):





    print("|- Image ID",image_id,cluster.cl_len)
    if cluster is None:
        return None,None,None,None,None,None
    if cluster.cl_len<min_size:
        return None,None,None,None,None,None


    denclue_attr_finder,h_start=make_denclue_attr_finder(cluster,\
                                                 kernel,\
                                                 eps,\
                                                 h_frac,\
                                                 h_min,\
                                                 attr_dbs_K,\
                                                 attr_dbs_eps,\
                                                 mask_unchanged,\
                                                 k_table_size,\
                                                 R_max_frac,\
                                                 R_max_kern_th,\
                                                 digitize_attractors=digitize_attractors,\
                                                 downsampling=downsampling,
                                                 image_id=0)

    h_max=2.0*h_start
    R_in=cluster.sig_x*0.5

    attractors_cl_list,core_cl,local_maxima_xy,local_maxima_xy_core,target_coordinates=find_attractors_and_lm(cluster, \
                                                                                           denclue_attr_finder, \
                                                                                           h_start, \
                                                                                           h_max,\
                                                                                           R_in,\
                                                                                           mask_unchanged, \
                                                                                           weight_cluster=weight_cluster,
                                                                                           downsampling=downsampling, \
                                                                                           image_id=image_id,
                                                                                           check_core_cluster=check_core_cluster )


    print("|-")

    attractors_src_cl_list=[]
    core_src_attr_cl=None
    for ID1,cl in enumerate(attractors_cl_list):
        if cl==core_cl:
            core_src_attr_cl=cl.SourceCluster_factory(ID=cl.ID)
            attractors_src_cl_list.append(core_src_attr_cl)
        else:

            attractors_src_cl_list.append(cl.SourceCluster_factory(ID=cl.ID))


    if plot == True:

        plot_attr(denclue_attr_finder,cluster,core_cl,local_maxima_xy,image_id,R_in,target_coordinates=target_coordinates,ax=ax,show_plot=plot)


    if out_seg_map == True:
        seg_map,src_stamp = attractors_seg_map(image,cluster,core_src_attr_cl,attractors_src_cl_list)
    else:
        seg_map=None
        src_stamp=None


    return attractors_src_cl_list,core_src_attr_cl,local_maxima_xy,local_maxima_xy_core,seg_map,src_stamp












class DoDENCLUEAttractorsTask(AnalysisTask):

    def __init__(self, name='find_denclue_attractors', func=do_find_denclue_attractors, parser=None, process=None):



        super(DoDENCLUEAttractorsTask,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-eval',help=' ',action='store_true')
        self.parameters_list.add_par('-out_seg_map',help='output attractors segmentation map to file',action='store_true')
        self.parameters_list.add_par('-min_size',type=np.int ,help=' ',default=9)
        self.parameters_list.add_par('-downsampling',help=' ',action='store_true')
        self.parameters_list.add_par('-eps',  type=np.float ,help='eps dbscan par',default=0.01)
        self.parameters_list.add_par('-kernel',type=str, help='smoothing  kernle',default='gauss')
        self.parameters_list.add_par('-k_table_size',type=np.int, help='if not None, a lookup table of the kernel is used with size k_table_size', default=None)
        self.parameters_list.add_par('-h_frac',  type=np.float ,help='kernel width as a fraction of parent cluster radius',default=0.25 )
        self.parameters_list.add_par('-h_min',  type=np.float ,help='min abs size for kernel width',default=1.5)
        self.parameters_list.add_par('-mask_unchanged',  type=np.float ,help='th value to flag and attractor as unchanged',default=0.15 )
        self.parameters_list.add_par('-check_core_cluster', help='switch on core cluster recursive refinement ',action='store_true')
        self.parameters_list.add_par('-core_ratio_1',type=np.float, help='minimum value for core cluster ratio', default=0.05)
        self.parameters_list.add_par('-core_ratio_2',type=np.float, help='maximum value for core cluster ratio', default=0.20)
        self.parameters_list.add_par('-attr_dbs_eps',  type=np.float ,help='eps for dbscan of attractors',default=1.0 )
        self.parameters_list.add_par('-attr_dbs_K',  type=np.float ,help='K for dbscan of attractors',default=4.0 )
        self.parameters_list.add_par('-verbose',help='set  verbosity on',action='store_true')
        self.parameters_list.add_par('-plot',help='plot ',action='store_true')
        self.parameters_list.add_par('-show_plot',help='shows the plot ',action='store_true')
        group_method=self.parameters_list.add_mex_group('R_max_method')
        group_method.add_par('-R_max_frac',  type=np.float ,help='max kernel influence radius as fraction of parent cluster r_max',default=None )
        group_method.add_par('-R_max_kern_th', type=np.float, help='max kernel influence radius when kernel<=th with th relative to kenerl max value',default=None)


        self.func=func

