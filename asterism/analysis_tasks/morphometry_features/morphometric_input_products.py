"""

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ...pipeline_manager.analysis_processes import AnalysisProcess
from ...core.photometry.surface_brightness_profile import ImageBrigthnessEllipiticalRadialProfile
from ...pipeline_manager.analysis_products import *
from ...plotting.plot_tools import  analysis_plot
from ...pipeline_manager.analysis_tasks import AnalysisTask

import  numpy as np
import copy

__author__ = 'andrea tramacere'



class MorphometryInputData(object):
    """
    Class to containing products to extract morphometric  features

    """

    def __init__(self,
                 image,
                 cluster,
                 seg_map,
                 attractors_cluster_list,
                 core_attractors_cluster,
                 centroid='core',
                 gabor_w1=None,
                 gabor_w2=None,
                 image_bkg_value=0,
                 N_bins_profile=20,
                 from_seg_map=False,
                 plot=False):

        __valid_centroid__ = ['geom', 'peak', 'core']

        self.gabor_w1 = gabor_w1
        self.gabor_w2 = gabor_w2

        if np.shape(centroid) == (2,):
            self.x_c = centroid[0]
            self.y_c = centroid[1]
        elif centroid in __valid_centroid__:

            if centroid == 'geom':
                self.x_c = cluster.x_c
                self.y_c = cluster.y_c

            elif centroid == 'peak':
                self.x_c = cluster.x_p
                self.y_c = cluster.y_p

            elif centroid == 'core':
                if core_attractors_cluster is not None:
                    self.x_c = core_attractors_cluster.x_c
                    self.y_c = core_attractors_cluster.y_c
                else:
                    self.x_c = cluster.x_c
                    self.y_c = cluster.y_c
        else:
            raise RuntimeError("centroid param", centroid,
                               " not valid either you provide a tuple (x,y) or you provide a valid method %s" % (
                                   __valid_centroid__))

        #print('cl ID', cluster.ID, from_seg_map)
        self.cluster = cluster
        #print('N_bins_profile', N_bins_profile)
        self.cluster._add_polar_coords(center=[self.x_c, self.y_c])

        self.core_attractors_cluster = core_attractors_cluster

        self.attractors_cluster_list = attractors_cluster_list

        r_max = self.cluster.r_max * 1.5
        self.image, self.bkg_image, self.binary_image, self.bkg_level,self.x_c_image, self.y_c_image = self._get_image(image, cluster, seg_map,
                                                                                     r_max,from_seg_map=from_seg_map)

        #self.image=self.image-self.bkg_image
        self.surf_brigth_prof = ImageBrigthnessEllipiticalRadialProfile()

        a_max = r_max
        b_max = self.cluster.sig_y / self.cluster.sig_x * a_max
        self.surf_brigth_prof.eval(self.image-self.bkg_level, self.x_c_image, self.y_c_image, a_max=a_max,
                                   b_max=b_max,N_bins=N_bins_profile, theta=np.deg2rad(self.cluster.semi_major_angle))

        if attractors_cluster_list is not None:

            self.attr_cartesian, self.attr_polar = self._merge_clusters_geom_repr(attractors_cluster_list)
        else:

            self.attr_cartesian = None
            self.attr_polar = None
            self.attr_w = 1

        if self.core_attractors_cluster is not None:

            self.non_core_attractors_cluster_list = [cl for cl in attractors_cluster_list if
                                                     cl != self.core_attractors_cluster]
            self.non_core_attr_cartesian, self.non_core_attr_polar = self._merge_clusters_geom_repr(
                self.non_core_attractors_cluster_list)
            self.non_core_attr_w = 1
        else:
            self.non_core_attractors_cluster_list = []
            self.non_core_attr_cartesian = None
            self.non_core_attr_polar = None
            self.non_core_attr_w = None

    def _fill_holes(self):
        pass

    def _get_image(self, image, cluster, seg_map, r_max,from_seg_map=False, buffer=10):
        (r, c) = image.array.shape
        dy = r_max + buffer
        dx = r_max + buffer
        r1 = np.int(max(0, self.y_c - dy))
        r2 = np.int(min(r, self.y_c + dy))
        c1 = np.int(max(0, self.x_c - dx))
        c2 = np.int(min(r, self.x_c + dx))

        _image = np.copy(image.array[r1:r2, c1:c2])

        _seg_map = np.copy(seg_map[r1:r2, c1:c2])

        _x_c = self.x_c - c1
        _y_c = self.y_c - r1

        # print('cl', cluster.ID, cluster.x_c, cluster.y_c,_x_c,_y_c,from_seg_map)

        #import pylab as plt
        #plt.imshow(_image, interpolation='nearest', clim=(_image.min(), _image.max()))
        #plt.show()
        #plt.imshow(_seg_map, interpolation='nearest', clim=(_seg_map.min(), _seg_map.max()))
        #plt.show()

        bkg_arr = _image[_seg_map == 0]

        id_offset = 1
        if from_seg_map is True:
            id_offset = 0
        print('from_seg_map',from_seg_map)
        print ('cluster.ID',cluster.ID,np.unique(_seg_map))
        msk = np.logical_and(_seg_map != cluster.ID + id_offset, _seg_map > 0)
        _binary_image=np.copy(_seg_map)
        _binary_image[_seg_map!=cluster.ID + id_offset]=0
        _binary_image[_seg_map== cluster.ID + id_offset] = 1
        #plt.imshow(msk, interpolation='nearest')
        #plt.show()
        size = msk.sum()
        _image[msk] = bkg_arr[np.random.randint(0, bkg_arr.size, size)]

        _bkg_image = np.zeros(_image.shape)
        # msk = np.logical_and(_seg_map == cluster.ID + id_offset, _seg_map > 0)
        # size = msk.sum()
        _bkg_image = bkg_arr[np.random.randint(0, bkg_arr.size, _bkg_image.shape)]

        #plt.imshow(_image,interpolation='nearest',clim=(_image.min(), _image.max()))
        #plt.show()

        #plt.imshow(_bkg_image, interpolation='nearest', clim=(_bkg_image.min(), _bkg_image.max()))
        #plt.show()

        #plt.imshow(_binary_image, interpolation='nearest')
        #plt.show()
        _bgk_level=np.median(bkg_arr.ravel())
        #print('_bkg_level',_bgk_level)
        return _image, _bkg_image,_binary_image,_bgk_level, _x_c, _y_c

    # def _get_image(self,cluster,image_bkg_value=0):
    #    print('image_bkg_value',image_bkg_value)

    #    cluster_image,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=image_bkg_value,border=max(3,3))
    #    x_c_image=self.x_c-off_set_x
    #    y_c_image=self.y_c-off_set_y
    #    return  cluster_image.array,x_c_image,y_c_image

    def _merge_clusters_geom_repr(self, cluster_list):
        new_coords_cartesian = None
        new_coords_polar = None

        for (ID, cl) in enumerate(cluster_list):
            if ID == 0:
                new_coords_cartesian = copy.deepcopy(cl.cartesian)
                # if cl.polar is None:
                #    cl._add_polar_coords(center=[self.x_c,self.y_c])
                # new_coords_polar=copy.deepcopy(cl.polar)
            else:
                new_coords_cartesian._expand(cl.cartesian.coords)
                # if cl.polar is None:
                #    cl._add_polar_coords(center=[self.x_c,self.y_c])
                # new_coords_polar._expand(cl.polar.coords)

        if new_coords_cartesian is not None:
            new_coords_polar = new_coords_cartesian.to_polar(x_c=self.x_c, y_c=self.y_c)

        return new_coords_cartesian, new_coords_polar


def do_morphometric_input_data_func(image,
                                    seg_map=None,
                                    cluster=None,
                                    image_bkg_value=0,
                                    from_seg_map=False,
                                    N_bins_profile=20,
                                    centroid_type='peak',
                                    plot=False):
    """

    Parameters
    ----------
    process
    image
    image_id
    cluster
    no_plot
    bkg_image
    bkg_lvl

    Returns
    -------

    """
    #print ('N_bins_profile',N_bins_profile)
    if cluster is None:
        original_cluster_shape_input_prod = None
    else:

        original_cluster = cluster
        # Original cluster products
        # original_cluster=process.resize_cluster.run(cluster=cluster,image_bkg_value=image_bkg_value,cluster_th_level=cluster_th_level)
        # cluster._add_polar_coords(center=[cluster.x_c,cluster.y_c])
        original_cluster_shape_input_prod = MorphometryInputData(image,
                                                                 original_cluster,
                                                                 seg_map,
                                                                 None,
                                                                 None,
                                                                 N_bins_profile=N_bins_profile,
                                                                 image_bkg_value=image_bkg_value,
                                                                 from_seg_map=from_seg_map,
                                                                 centroid=centroid_type)

    return original_cluster_shape_input_prod



class DoMorphometryInputData(AnalysisTask):

    def __init__(self, name='extract_morph_features', func=do_morphometric_input_data_func, parser=None, process=None):
        super(DoMorphometryInputData,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-plot', help='', action='store_true')
        self.parameters_list.add_par('-N_bins_profile', type=np.int, help='', default=20)
        self.parameters_list.add_par('-centroid_type', type=str, help='', default='peak')
        self.func=func