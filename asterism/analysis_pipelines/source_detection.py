"""
Module: source_detection
========================



Overview
--------

This modules provides the implementation of the :class:`.SrcDetectionPipeline` class
used to create the pipeline for the source detection.
The process in the pipeline are orchestrated by the :func:`.do_src_detection_pipeline_func`


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::
    asterism.analysis_pipelines.source_detection.SrcDetectionPipeline




Summary
---------
.. autosummary::
   do_src_detection_pipeline_func
   SrcDetectionPipeline


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ..pipeline_manager.analysis_pipeline import AnalysisPipeline
from ..analysis_processes.source_detection import   DoSrcDetectionProcess
from ..core.image_manager.image import Image
from ..plotting.plot_tools import  analysis_plot

__author__ = 'andrea tramacere'



def do_src_detection_pipeline_func(pipeline,image,image_id=None,input_seg_map=None,input_rms_map=None,file_name='',**kwargs):
    """
    Function that implements the algorithm for the :class:`SrcDetectionPipeline` class

    user guide: :ref:`src_detection_pipeline_user_guide`

    Parameters
    -----------

    pipeline : :class:`~asterism.analysis_pipelines.source_detection.SrcDetectionPipeline`
        instance of the base :class:`.AnalysisPipeline` class

    image : :class:`~asterism.core.image_manager.image.Image`
        instance of the class used as input for different processes

    image_id :  int
        id of the image

    **kwargs:

    Returns
    -------

    src_detection_analysis_products : :class:`~asterism.pipeline_manager.analysis_products.AnalysisProduct`
        class instance storing the analysis products for this pipeline

    """

    if isinstance(image,Image)==True:
        pass
    else:
        raise TypeError('image is not instance of Image class')


    src_detection_analysis_products=pipeline.do_src_detection.run(image,image_id=image_id,input_seg_map=input_seg_map,input_rms_map=input_rms_map,file_name=file_name)

    return  src_detection_analysis_products



class SrcDetectionPipeline(AnalysisPipeline):
    """
    Class  that implements the source detection pipeline.

    Processes in the Pipeline
        - do_src_detection                 :class:`~asterism.analysis_processes.source_detection.DoSrcDetectionProcess`

    user guide: :ref:`src_detection_pipeline_user_guide`

    Parameters
    ----------
    name : str
        the name for the Process

    func : :func:
        The function that handles tha processes

    image_id : int
        id of the image

    plot_func : :func:
        plotting function for this process


    parser :

    argv :

    conf_file:


    """
    def __init__(self,name='scr_detection_script',func=do_src_detection_pipeline_func,plot_func=analysis_plot,parser=None,argv=None,conf_file=None):
        super(SrcDetectionPipeline,self).__init__(name,func,plot_func=analysis_plot,parser=parser,argv=argv,conf_file=conf_file)
        self.add_analysis_process(DoSrcDetectionProcess,'do_src_detection' )






