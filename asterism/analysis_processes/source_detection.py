"""

This modules provides the implementation of the :class:`.DoSrcDetectionProcess` class
used to handle the source detection process.


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.analysis_processes.source_detection




Summary
---------
.. autosummary::
   do_src_detection_process_func
   DoSrcDetectionProcess


Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ..analysis_tasks.source_detection.image_segmentation.catalog import DoSourceCatalogTask
from ..analysis_tasks.image_processing.image_processing import DoImageProcessing
from ..analysis_tasks.source_detection.background_estimation.background_estimation import DoSetBkgThreshTask
from ..analysis_tasks.source_detection.deblending.deblending_downsample import DoDownsamplingTask
from ..analysis_tasks.source_detection.deblending.deblending_scale import DoFindClusterScaleTask
from ..analysis_tasks.source_detection.deblending.denclue_deblending import DoDENCLUEDeblendingTask
from ..analysis_tasks.source_detection.deblending.glw_deblending import DoGLWDeblendingTask
from ..analysis_tasks.source_detection.deblending.deblending import  DoSetDeblendingMethodTask
from ..analysis_tasks.source_detection.deblending.deblending import  DoDeblendingValidationTask
from ..analysis_tasks.source_detection.deblending.extrema_deblending import DoExtremaDeblendingTask

from ..analysis_tasks.source_detection.image_segmentation.image_segmentation import  DoImageSegmentationTask
from ..pipeline_manager.analysis_processes import AnalysisProcess
from asterism.pipeline_manager.analysis_products import *
from ..plotting.plot_tools import  analysis_plot
from ..core.image_manager.image import Image

__author__ = 'andrea tramacere'


def deblending(deb_method,process,cluster_list,scale_finder,down_sampler,bkg_std):
    if deb_method=='gauss_laplace_watershed':
        debl_prod_list = process.glw_deblending.run(clusters_list=cluster_list,
                                                    scale_finder=scale_finder,
                                                    bkg_std=bkg_std)
    elif deb_method=='denclue':
        debl_prod_list= process.denclue_deblending.run(clusters_list=cluster_list,
                                                       down_sampler=down_sampler,
                                                       scale_finder=scale_finder,
                                                       bkg_std=bkg_std)
    elif deb_method=='extrema':
        debl_prod_list=process.extrema_deblending.run(clusters_list=cluster_list,
                                                      scale_finder=scale_finder,
                                                      bkg_std=bkg_std)
    elif deb_method=='combo':
        debl_prod_list = process.extrema_deblending.run(clusters_list=cluster_list,
                                                        scale_finder=scale_finder,
                                                        bkg_std=bkg_std)
        _cl_list = []
        for p in debl_prod_list[:]:
            if len(p.children_list)>1:
                _cl_list.append(p.parent)
                debl_prod_list.remove(p)

        _db_ls = process.denclue_deblending.run(clusters_list=_cl_list,
                                                scale_finder=scale_finder,
                                                down_sampler=down_sampler,
                                                bkg_std=bkg_std)

        debl_prod_list.extend(_db_ls)
    elif deb_method == 'no_deblending':
        debl_prod_list=None

    return debl_prod_list

def do_src_detection_process_func(process,image,image_id=None,no_plot=False,input_rms_map=None,input_seg_map=None,file_name=''):
    """
    This function implements the logic of  the algorithm for the detection :class:DoSrcDetectionProcess.

    The following Tasks are used:

        * image_processing :class:`.DoImageProcessing`
        * set_thresh :class:`.DoSetBkgThreshTask`
        * image_segmentation  :class:`.DoImageSegmentationTask`
        * source_catalog   :class:`.DoSourceCatalogTask`
        * do_denclue_deblending :class:`.DoDENCLUEDeblendingTask`
        * do_gauss_laplace_deblending :class:`.DoGAUSSLAPLACEDeblendingTask`


    user guide: :ref:`src_detection_process_user_guide`

    Parameters
    ----------
    process : :class:`.AnalysisProcess`

    image : :class:`.Image` instance
        The input image

    image_id : int
        id of the image

    no_plo :t bool, optional (defaule=False):

    input_rms_map : 2D np.array, optional
        numpy array of the image of the bkg rms

    bkg_lvl : float,optional
        value of the background image, estimated for the current image


    Returns
    -------
    products : :class:`AnalysisProcessProducts`
        instance of the :class:`AnalysisProcessProducts` storing into the member prod_list :class:`FeaturesArray` instance
        with all the features  and features names extracted


    """


    #Type checking for image
    if isinstance(image,Image)==True:
        pass
    else:
        raise TypeError('image is not instance of Image class')

    #print('=> file_name 2', file_name, type(file_name))
    #image=process.image_processing.run(image=image)
    #sex_srcs=None
    #if process.sextractor.get_par_value('sex') is True:
    #    sex_srcs=process.sextractor.run(image=image,sex=True)




    #if input_rms_map==None:
    #    bkg_image=image

    #from_seg_map = False
    #Bkg level
    #if input_seg_map is None:
    process.image_processing.run(image=image)

    bkg_threshold,bkg_mode,bkg_sig=process.set_thresh.run(image=image,rms_map=input_rms_map)
    if input_seg_map is None:
        from_seg_map = False
    else:
        from_seg_map=True
    #    bkg_threshold,bkg_mode,bkg_sig=(0,0,0)

    # Image Processing



    #segmentation
    cluster_list,K,selected_coords=process.image_segmentation.run(image=image,bkg_threshold=bkg_threshold,image_id=image_id,input_seg_map=input_seg_map)

    #catalog
    override_id = process.source_catalog.get_par_value('override_id')

    seg_map,seg_map_overlap,segmentation_catalog,reg_catalog,cluster_list=process.source_catalog.run(image=image,
                                                                                                     cl_list=cluster_list,
                                                                                                     catalog_name='segmentation_catalog',
                                                                                                     image_id=image_id,
                                                                                                     from_seg_map=from_seg_map,
                                                                                                     file_name=file_name,
                                                                                                     override_id=override_id)


    #deblending
    seg_map_debl=None
    catalog_debl=None
    reg_catalog_debl=None
    deb_method=process.set_deblending_method.run()
    down_sampler=process.down_sampling.run()
    scale_finder=process.cluster_scale_finder.run()





    debl_prod_list=deblending(deb_method,process,cluster_list,scale_finder,down_sampler,bkg_sig)

    if debl_prod_list is not None:
        debl_cluster_list=process.deblending_validation.run(deblended_prod_list=debl_prod_list,
                                                            bkg_threshold=bkg_threshold,
                                                            bkg_lvl=bkg_mode,
                                                            bkg_sig=bkg_sig,
                                                            image=image)

        #debl_prod_list = deblending(deb_method, process, cluster_list, scale_finder, down_sampler)


        #debl_cluster_list = process.deblending_validation.run(deblended_prod_list=debl_prod_list,
        #                                                      bkg_threshold=bkg_threshold,
        #                                                      bkg_lvl=bkg_mode,
        #                                                      bkg_sig=bkg_sig,
        #                                                      image=image)

        if process.source_catalog.get_par_value('get_only_central_debl_source')==True:
            get_only_central_source=True
        else:
            get_only_central_source=False

        seg_map_debl, seg_map_overlap, catalog_debl, reg_catalog_debl, cluster_list = process.source_catalog.run(
            cl_list=debl_cluster_list,
            catalog_name='deblended_catalog',
            image_id=image_id,
            from_seg_map=False,
            file_name=file_name,
            out_seg_map_overlap=True,
            override_id=True,
            get_only_central_source=get_only_central_source)



    #plotting
    if (process.do_plot.get_par_value('plot') == True or process.do_plot.get_par_value('save_plot') is True) and no_plot==False:
        print ('plot start')
        fig_name='detection_plot'
        fig=process.run_plotting(process.plot_func,
                                 image,
                                 selected_coords,
                                 image.array[~image.masked],
                                 cluster_list,
                                 title=process.image_segmentation.get_par_value('method'),
                                 plot=process.do_plot.get_par_value('plot'),
                                 gal_id=image_id,
                                 threshold=bkg_threshold,
                                 K=K,
                                 fig_name=fig_name,
                                 msk=None)
        print('plot done' )
    else:
        fig=None
        fig_name=None


    #final products
    analysis_products_collection=AnalysisProductsCollection()

    analysis_products_collection.add_product(AnalysisProductFigure([fig],[fig_name],name='src_detection_figures'))

    if segmentation_catalog is not None:
        analysis_products_collection.add_product(AnalysisProductCatalog(segmentation_catalog, name='segmentation_catalog'))

    if reg_catalog is not None:
        analysis_products_collection.add_product(AnalysisProductRegionCatalog(reg_catalog, name='reg_catalog'))

    if catalog_debl is not None:
        analysis_products_collection.add_product(AnalysisProductCatalog(catalog_debl,name='deblended_catalog'))


    if seg_map is not None:
        analysis_products_collection.add_product(AnalysisProductFitsImage(seg_map, name='segmentation_map'))

    if seg_map_debl is not None:
        analysis_products_collection.add_product(AnalysisProductFitsImage(seg_map_debl, name='segmentation_map_debl'))

    if seg_map_overlap is not None:
            analysis_products_collection.add_product(AnalysisProductFitsImage(seg_map_overlap, name='segmentation_map_debl_overlap'))


    if reg_catalog_debl is not None:
        analysis_products_collection.add_product(AnalysisProductRegionCatalog(reg_catalog_debl, name='reg_catalog_debl'))


    analysis_products_collection.add_product(AnalysisProductGeneric(cluster_list,name='cluster_list'))
    analysis_products_collection.add_product(AnalysisProductGeneric(bkg_threshold,name='bkg_threshold'))
    analysis_products_collection.add_product(AnalysisProductGeneric(bkg_mode,name='bkg_mode'))
    analysis_products_collection.add_product(AnalysisProductGeneric(K,name='K'))
    analysis_products_collection.add_product(AnalysisProductGeneric(bkg_sig,name='bkg_sig'))


    return analysis_products_collection






class DoSrcDetectionProcess(AnalysisProcess):
    """
    Class that implements the src detection process

    Tasks in the Process:

        * image_processing :class:`.DoImageProcessing`
        * set_thresh :class:`.DoSetBkgThreshTask`
        * image_segmentation  :class:`.DoImageSegmentationTask`
        * source_catalog   :class:`.DoSourceCatalogTask`
        * do_denclue_deblending :class:`.DoDENCLUEDeblendingTask`
        * do_gauss_laplace_deblending :class:`.DoGAUSSLAPLACEDeblendingTask`

    user guide: :ref:`src_detection_process_user_guide`


    Parameters
    ----------
    name : str
        the name for the Process

    func : callable
        The function that handles tha tasks, :func:`.do_src_detection_process_func` in this case

    image_id : int
        id of the image

    plot_func : callable
        plotting function for this process


    parser :

    add_plot_task  : bool


    """

    def __init__(self,name='do_src_detection',func=do_src_detection_process_func,plot_func=analysis_plot,parser=None,add_plot_task=True):
        super(DoSrcDetectionProcess,self).__init__(name,func,plot_func=plot_func,parser=parser,add_plot_task=add_plot_task)

        self.add_analysis_task(DoImageProcessing,'image_processing')

        self.add_analysis_task(DoSourceCatalogTask,'source_catalog' )

        self.add_analysis_task(DoImageSegmentationTask, 'image_segmentation')

        self.add_analysis_task(DoSetDeblendingMethodTask,'set_deblending_method')

        self.add_analysis_task(DoDownsamplingTask, 'down_sampling')

        self.add_analysis_task(DoDeblendingValidationTask, 'deblending_validation')

        self.add_analysis_task(DoGLWDeblendingTask,'glw_deblending')

        self.add_analysis_task(DoExtremaDeblendingTask, 'extrema_deblending')

        self.add_analysis_task(DoFindClusterScaleTask, 'cluster_scale_finder')

        self.add_analysis_task(DoDENCLUEDeblendingTask,'denclue_deblending' )

        self.add_analysis_task(DoSetBkgThreshTask,'set_thresh' )














