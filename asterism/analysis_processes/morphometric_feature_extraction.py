"""




Overview
--------

This modules provides the implementation of the :class:`.DoMorphometricFeaturesExtraction` class
used to handle the process for the morphometric features extraction.
The tasks in the process are orchestrated by the :func:`.do_morphometric_features_extraction_process_func`




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.analysis_processes.morphometric_feature_extraction.DoMorphometricFeaturesExtraction




Summary
---------
.. autosummary::
   do_morphometric_features_extraction_process_func
   FeaturesArray
   DoMorphometricFeaturesExtraction


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ..pipeline_manager.analysis_processes import AnalysisProcess
from asterism.pipeline_manager.analysis_products import *
from ..analysis_tasks.morphometry_features.extract_geometrical_features import  DoExtractGeometricFeatures
from ..analysis_tasks.morphometry_features.extract_Hu_moments_features import  DoExtractHuMomentsFeatures
from ..analysis_tasks.morphometry_features.extract_galaxy_profile_fit_features import  DoExtractGalaxyProfileFitFeatures
from ..analysis_tasks.morphometry_features.extract_attr_polar_distr_features import  DoExtractAttractorsRadialDistrFeatures
from ..analysis_tasks.morphometry_features.extract_morphology_features import DoExtractMorphologyFeatures
from ..core.morphometry.features import MorphometricFeatures,MorphometricFeaturesArray
from ..plotting.plot_tools import  analysis_plot
import  numpy as np

__author__ = 'andrea tramacere'





def do_morphometric_features_extraction_process_func(process,
                                                     image,
                                                     image_id=None,
                                                     cl_id=None,
                                                     x_c=None,
                                                     y_c=None,
                                                     morphometry_input_data_original=None,
                                                     morphometry_input_data_gabor=None,
                                                     morphometry_input_data_deproj=None,
                                                     morphometry_input_data_unsharp=None,
                                                     file_name='',

                                                     no_plot=False,
                                                     bkg_image=None,
                                                     bkg_lvl=None):
    """
    Function for the morphometric feature extraction Process. The following :class:`.AnalysisTask` derived tasks are used

        - extract_geometric_features :class:`.DoExtractGeometricFeatures`
        - extract_Hu_moments_features :class:`.DoExtractHuMomentsFeatures`
        - extract_morphological_features  :class:`.DoExtractMorphologyFeatures`
        - extract_galaxy_profile_fit_features   :class:`.DoExtractGalaxyProfileFitFeatures`
        - extract_arr_radial_distr_features :class:`.DoExtractAttractorsRadialDistrFeatures`

    Features are from each task are returned as list of :class:`asterism.core.morphometry.features.MorphometricFeatures` objects
    Each task function is calling  feature extraction function from `asterism.core.morphometry`. These functions
    have an internal try/execpt structure organinze in a such way that if a feture extraction fails, np.nan array
    is returned, with the corresponding columns names


    Parameters
    ----------
    process : :class:`.AnalysisProcess`
        instance of the analysis process

    image : :class:`.Image` instance
        The input image

    image_id : int
        id of the image

    no_plo :t bool, optional (defaule=False):


    morphometry_input_data_original :class:`asterism.analysis_processes.morphometric_features_input_products.MorphometryInputData`
        class instance storing the input data for the morphometric feature extraction

    morphometry_input_data_deproj :class:`asterism.analysis_processes.morphometric_features_input_products.MorphometryInputData`
        class instance storing the input data for the morphometric feature extraction

    bkg_image : 2D np.array, optional
        numpy array of the image of the bkg image

    bkg_lvl : float,optional
        value of the background image, estimated for the current image


    Returns
    -------
    products : :class:`AnalysisProcessProducts`
        instance of the :class:`AnalysisProcessProducts` storing into the member prod_list :class:`FeaturesArray` instance
        with all the features  and features names extracted


    """


    fig=None
    fig_name=None






    tasks_list=[process.extract_geometric_features,
                process.extract_Hu_moments_features,
                process.extract_morphological_features,
                process.extract_galaxy_profile_fit_features,
                process.extract_arr_radial_distr_features]


    #list of MorphometricFeatures objs
    if image_id is None:
        image_id=0
    if cl_id is None:
        cl_id=0

    if x_c is None:
        x_c=-1.0
    if y_c is None:
        y_c=-1.0

    print('file name finale',file_name)
    features_list=[MorphometricFeatures(group_name='id',names=['file_name','image','cluster'],values=[file_name,image_id,cl_id],dtypes=[file_name.dtype,np.int64,np.int64])]
    features_list.append(MorphometricFeatures(group_name='det_coord',names=['x','y'],values=[x_c,y_c],dtypes=[np.float32,np.float32]))
    morphometric_features_array=MorphometricFeaturesArray(features_list)
    for task in tasks_list:
        if task.get_par_value('extract') == True:
            print ('|extracting ',task.name)
            features_list.extend(task.run(prod_original=morphometry_input_data_original,
                                          prod_mapped_to_circle=morphometry_input_data_deproj,
                                          gabor_prod=morphometry_input_data_gabor,
                                          unsharp_prod=morphometry_input_data_unsharp,
                                          image_id=image_id))


    #l=0
    #_dt_list=[]
    #for morph_feature in features_list:
    #    l+=len(morph_feature.values)
    #    _dt_list.extend(morph_feature.dtype.descr)
    #print('===> final',l,len(_dt_list))
    morphometric_features_array.add(features_list)




    analysis_products_collection=AnalysisProductsCollection()

    analysis_products_collection.add_product(AnalysisProductRecArray(morphometric_features_array.values_tuple,morphometric_features_array.dtype, name='morph_features'))

    return analysis_products_collection



class DoMorphometricFeaturesExtraction(AnalysisProcess):
    """
    Derived class  orchestrating the tasks

        - extract_geometric_features :class:`.DoExtractGeometricFeatures`
        - extract_Hu_moments_features :class:`.DoExtractHuMomentsFeatures`
        - extract_morphological_features  :class:`.DoExtractMorphologyFeatures`
        - extract_galaxy_profile_fit_features   :class:`.DoExtractGalaxyProfileFitFeatures`
        - extract_arr_radial_distr_features :class:`.DoExtractAttractorsRadialDistrFeatures`


    Parameters
    ----------
    name : str
        the name for the Process

    func : :function:
        The function that handles tha tasks

    image_id : int
        id of the image

    plot_func : :function:
        plotting function for this process


    parser :

    add_plot_task  : bool


    """

    def __init__(self,name='do_gal_shape_extraction',func=do_morphometric_features_extraction_process_func,plot_func=analysis_plot,parser=None,add_plot_task=True):
        super(DoMorphometricFeaturesExtraction,self).__init__(name,func,plot_func=plot_func,parser=parser,add_plot_task=add_plot_task)

        self.add_analysis_task(DoExtractGeometricFeatures, 'extract_geometric_features')
        self.add_analysis_task(DoExtractHuMomentsFeatures, 'extract_Hu_moments_features')
        self.add_analysis_task(DoExtractMorphologyFeatures, 'extract_morphological_features')
        self.add_analysis_task(DoExtractGalaxyProfileFitFeatures, 'extract_galaxy_profile_fit_features')
        self.add_analysis_task(DoExtractAttractorsRadialDistrFeatures, 'extract_arr_radial_distr_features')
