"""
Module:
========================



Overview
--------

This modules provides the implementation of the :class:



Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::

Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ..pipeline_manager.analysis_processes import AnalysisProcess
from ..plotting.plot_tools import  analysis_plot
from ..analysis_tasks.morphometry_features.morphometric_input_products import DoMorphometryInputData


__author__ = 'andrea tramacere'


def do_morphometric_input_products_process_func(process,
                                                image,
                                                image_id=None,
                                                seg_map=None,
                                                cluster=None,
                                                no_plot=False,
                                                bkg_image=None,
                                                image_bkg_value=0,
                                                cluster_th_level=0,
                                                from_seg_map=False,
                                                **kwargs):


    return process.morphometric_input_products_task.run(image=image,
                                                        seg_map=seg_map,
                                                        cluster=cluster,
                                                        image_bkg_value=image_bkg_value,
                                                        from_seg_map=from_seg_map,
                                                        )










class DoMorphometricInputProducts(AnalysisProcess):

    def __init__(self,name='do_morphometric_input_products',func=do_morphometric_input_products_process_func,plot_func=analysis_plot,parser=None,add_plot_task=True):
        super(DoMorphometricInputProducts,self).__init__(name,func,plot_func=plot_func,parser=parser,add_plot_task=add_plot_task)
        self.add_analysis_task(DoMorphometryInputData, 'morphometric_input_products_task')
