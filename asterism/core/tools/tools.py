"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

__author__ = 'andrea tramacere'


def iter_input(func):
    #print("iter_input start",func)
    def func_wrapper(self,input_obj,**kwargs):

        #print("-----------------")
        #print("wrapper <","args=",args," kw=",kwargs,">")
        #print("-----------------")

        if type(input_obj)==list:
            pass
        else:
            input_obj=[input_obj]
        #print(type(a))
        return func(self,input_obj,kwargs)
    #print("iter_input stop")
    return func_wrapper




def check_choice_name(par_name,choice_name,allowed_names):
    if choice_name not in allowed_names:
            raise RuntimeError(''' %s='%s' is not allowed, allowed names=%s'''%(par_name,choice_name,allowed_names))
