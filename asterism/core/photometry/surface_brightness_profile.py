"""
Module: name
========================



Overview
--------

This modules provides an

The :class:`.class_name` is used to...



Classes relations
---------------------------------------
.. figure::  classes_rel_pkg.sub_pkg.modname.png
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::   classes_pkg.sub_pkg.modname.png




Summary
---------
.. autosummary::
   some func
   some classes


Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


from photutils import  aperture_photometry
from photutils import EllipticalAperture
from asterism.core.stats.histogram.radial_distributions import radial_profile_image
from asterism.core.image_processing.geometry_transformation import RescaleImage
import numpy as np
import photutils

__author__ = 'andrea tramacere'


class ImageBrigthnessEllipiticalRadialProfile(object):
    #will be improved with photUtils

    def __init__(self,aperture=None,N_bins=20):

        self.aperture=aperture


    def eval(self,image_array,x_c,y_c,a_max,b_max,theta,N_bins=None,plot=False):


        self.I,self.r,self.s,self.apertures=image_brigthness_radial_profile(image_array,x_c,y_c,a_max,b_max,theta,plot=plot,N_bins=N_bins)

    def plot(self):
        pass

def image_brigthness_radial_profile(image_array,x_c,y_c,a_max,b_max,theta,N_bins=20,plot=False):


    if N_bins is None:
        N_bins=int(a_max)

    #c_lin=1.0
    #c_sq=1.0

    #if oversampling_ratio is not None:
    #    c_lin=np.float(oversampling_ratio)
    #    c_sq=c_lin*c_lin

    #    rescale =RescaleImage(scale=oversampling_ratio)
    #    print('check rescale',image_array.sum())
    #    image_array=rescale.apply(image_array,preserve_integral_flux=True)
    #    print('check rescale',image_array.sum())

    #    x_c=x_c*oversampling_ratio
    #    y_c=y_c*oversampling_ratio
    #    a_max=a_max*oversampling_ratio
    #    b_max=b_max*oversampling_ratio

    a = np.linspace(1, a_max, N_bins)
    b = np.linspace(1, b_max, N_bins)
    positions = [x_c, y_c]
    apertures = [EllipticalAperture(positions, a=ai, b=bi, theta=theta) for (ai, bi) in zip(a, b)]

    #print ('theta',theta)
    r = a

    if photutils.__version__ < "0.3":
        t = [aperture_photometry(image_array, ap) for ap in apertures]
    else:
        t=aperture_photometry(image_array, apertures)
    #print ('t',t)
    #print('t', t[0][0])
    if photutils.__version__ < "0.3":
        flux = np.array([t[i][0][0] for i in range(N_bins)])

    else:
        flux=np.array([t[0][i] for i in range(3,N_bins+3)])
    #print('flux', flux)
    flux[1:]=flux[1:]-flux[:-1]
    #print ('flux',flux)
    #flux=np.array([t[0][i]-t[0][i-1] for i in range(4,N_bins+3)])
    area=np.array([apertures[i].area() for i in range(N_bins)])
    area[1:]=area[1:]-area[:-1]
    I=flux/area
    #print('I',I)
    if plot ==True:
        import  pylab as plt
        f,ax=plt.subplots(1,1)
        ax.imshow(image_array,interpolation='nearest')
        for a in apertures:
            a.plot(ax=ax)
        plt.show()


    return  I,r,area,apertures

