"""
This modules implements functions for the statistical operations on arrays

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np
from scipy import stats
import  math as m
import time
__author__ = 'andrea tramacere'


def eval_mad(arr):
    """

    Parameters
    ----------
    arr

    Returns
    -------

    """
    return np.median(np.fabs(arr-np.median(arr)))*1.5

def eval_skew_minimization_stats(data):
    """
    Returns stats after skewness minimization

    Parameters
    ----------
    data : 1d numpy array

    Returns
    -------
    bkg_lvl : float, bkg level
    bkg_sig : float, bkg std dev
    skew : skewness of the bkg
    size : size of the array used to eval the stats
    """
    if data.size>2:
        return data.mean(),eval_mad(data),stats.skew(data),data.size
    else:
        return np.nan,np.nan,np.nan,np.nan


def eval_bkg_stats(data):
    """
    Returns stats after minimization

    Parameters
    ----------
    data : 1d numpy array

    Returns
    -------
    bkg_lvl : float, bkg level
    bkg_sig : float, bkg std dev
    mediab : median of the bkg
    size : size of the array used to eval the stats
    """
    if data.size>2:
        return data.mean(),eval_mad(data),np.median(data),data.size
    else:
        return np.nan,np.nan,np.nan,np.nan

def minimize_skewness(data,sigma_range,min_size_ratio,grid_size=50):
    """
    the minimum   skewness in the range |m-s1,m+s1| is chosen, where m is the mean of the data,
    and s1=std(data)*`sigma_range`

    Parameters
    ----------
    data : 1d numpy array

    sigma_range : float

    min_size_ratio : float
        min size of the skewness minimized  data set. If size<data.size*`min_size_ratio`, then
        the  a less restrictive skew is chosen until the minimun size condition is satisfied

    grid_size : Optional[int]
        size of the grid to evaluate the skewness

    Returns
    -------

    """
    mean,sig,skew,size=eval_skew_minimization_stats(data)


    print ("|minimization of |skewness|")
    print ("|initial skewness",skew)
    print ("|initial mean",mean)
    print ("|initial std",sig)




    if sigma_range is not None:
        skew_range=sigma_range*sig
        th_skew_min=mean-skew_range
        th_skew_max=mean+skew_range
    else:

        th_skew_min=data.min()
        th_skew_max=data.max()
        skew_range=th_skew_max-th_skew_min

    print("|range of threshold for skewness minimization th_skew in [%f,%f]"%(th_skew_min,th_skew_max))

    start_size=float(data.size)
    print ("|start size: ",data.size,mean,sig,skew,size)


    th_array=np.linspace(mean,skew_range,grid_size)
    skew_arr=np.zeros(grid_size)
    size_ratio_arr=np.zeros(grid_size)
    mean_array=np.zeros(grid_size)
    sig_array=np.zeros(grid_size)
    th_max=np.zeros(grid_size)
    th_min=np.zeros(grid_size)
    for ID,th in enumerate(th_array):

        m1=data<mean+th
        m2=data>mean-th
        th_min[ID]=mean-th
        th_max[ID]=mean+th


        mean,sig,skew,size=eval_skew_minimization_stats(data[m1*m2])

        if np.isnan(mean):
            mean,sig,skew,size=eval_skew_minimization_stats(data)

        mean_array[ID]=mean
        sig_array[ID]=sig
        skew_arr[ID]=m.fabs(skew)
        size_ratio_arr[ID]=np.float(size)/start_size

    mnan=np.logical_not(np.isnan(mean_array))

    th_array=th_array[mnan]
    skew_arr=skew_arr[mnan]
    size_ratio_arr=size_ratio_arr[mnan]
    mean_array=mean_array[mnan]
    sig_array=sig_array[mnan]
    th_min=th_min[mnan]
    th_max=th_max[mnan]

    id_sort=np.argsort(skew_arr)
    skew_arr= skew_arr[id_sort]
    size_ratio_arr= size_ratio_arr[id_sort]
    th_array=th_array[id_sort]
    mean_array=mean_array[id_sort]
    sig_array=sig_array[id_sort]
    th_min=th_min[id_sort]
    th_max=th_max[id_sort]

    id_min=0


    print ("|min skewness=%f for th_skew=%f"%(skew_arr[id_min],th_array[id_min]))
    print ("|size ratio=%f, min_size ratio=%f"%(size_ratio_arr[id_min],min_size_ratio))





    if size_ratio_arr[id_min]<min_size_ratio:
        print ("|size  smaller then for min_size")
        if np.where(size_ratio_arr>=min_size_ratio)[0].size==0:
            id_min=np.argmax(size_ratio_arr)
        else:
            id_min=np.where(size_ratio_arr>=min_size_ratio)[0][0]

        print ("|new size ratio",size_ratio_arr[id_min])
        print ("|updated values")
        print ("|min skewness=%f for th_skew=%f"%(skew_arr[id_min],th_array[id_min]))






    return mean_array[id_min],sig_array[id_min],skew_arr[id_min],th_min[id_min],th_max[id_min]


def sigma_clipping():
    pass
