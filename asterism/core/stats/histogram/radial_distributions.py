"""

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np
import math
from scipy import interpolate
from ...geometry.distance import dist_eval
from .hisogram_1d import *

__author__ = 'andrea tramacere'





def equal_area_annuli(R,N,i=None):
    """
    provides the radius r[i] for N concentric annular regions in such a way that
    each annular region surface s[i]=pi*(r[i]**2-r[i-1]**2)=pi*R**2/N

    Args:
        R (float): the outern radius
        N (int): the total number of annular regions
        i (int) or array of (int): the index for the annular region or the array of indices

    Returns:
        r (float): the radius r[i] (if is scalar) or the the full array r if i is an array
    """


    return np.sqrt((R**2/np.float64(N))*i)


def radial_histogram(r_coords,r_max=None,N_bins=10,use_equal_area_annuli=False,r_norm=False):

    if r_max is None:
        r_max=r_coords.max()
    if r_norm==True:
        r_coords=r_coords/r_max

    r_max=r_coords.max()
    r_min=r_coords.min()

    if equal_area_annuli==True:
        r_bins=equal_area_annuli(r_max,N_bins,np.arange(N_bins+1))
    else:
        r_bins=np.linspace(r_min,r_max,N_bins+1)



    return histogram(r_coords,bins=r_bins)

def radial_histogram_kde(r_coords,N_bins=100,r_norm=False,r_max=None,bw=None,r_min=None):
    if r_min is not None:
        r_min=r_min/r_coords.max()

    if r_max is None:
        r_max=r_coords.max()

    if r_norm==True:
        r_coords=r_coords/r_max

    return histogram_kde(r_coords,N_bins,bw=bw,v_min=r_min,v_max=r_max)





def theta_histogram(theta_coords,N_bins=10):
    return histogram(theta_coords,bins=N_bins)

def theta_histogram_kde(theta_coords,N_bins=100,bw=None):
    return histogram_kde(theta_coords,N_bins,bw=bw)





def frac_flux_in_bin(image_array,r_dist,r_bin):
    """

    Parameters
    ----------
    image_array
    r_dist_array
    r_dist_max
    r_bin

    Returns
    -------

    """
    in_bin=r_dist<=r_bin

    f=image_array[in_bin]



    return f.sum()



def radial_profile_image(image_array,x_c,y_c,r_max=None, N_bins=10, use_equal_area_annuli=False,intensity=True,pix_size=1.0,prune_zeros=True):
    """
    Function to evaluate the radial  profile, given radial_coordinates of points/pixels and their fluxes values
    if intesity==True, then the flux in the annular region is divided by the number of pixels

    Parameters
    ----------
    polar_r
    flux_values
    r_max
    N_bins
    use_equal_area_annuli
    intensity

    Returns
    -------
    flux_values
    r


    """




    N_bins=np.int(N_bins)

    if N_bins==0:
        N_bins=1


    r_dist=np.zeros(image_array.shape)


    for r in range(image_array.shape[0]):
        c=np.arange(image_array.shape[1])
        xy=np.column_stack((c,np.ones(c.shape)*r))
        r_dist[r]=dist_eval(xy,x_c=x_c,y_c=y_c)

    if r_max is None:
        r_max=r_dist.max()

    if use_equal_area_annuli==True:
        r_bins=equal_area_annuli(r_max,N_bins,np.arange(N_bins+1))
    else:
        r_bins=np.linspace(0,r_max,N_bins+1)






    fluxes=np.zeros(r_bins[1:].shape)
    surf=np.zeros(r_bins[1:].shape)



    #
    for ID in range(1,r_bins.size):



        #print('r_1,r_2',r_bins[ID-1],r_bins[ID])
        f_up=frac_flux_in_bin(image_array,r_dist,r_bins[ID])

        if ID>1:
            f_down=frac_flux_in_bin(image_array,r_dist,r_bins[ID-1])


            fluxes[ID-1]=f_up -f_down
        else:
            fluxes[ID-1]=f_up

        surf[ID-1]=np.pi*((r_bins[ID]*r_bins[ID])-(r_bins[ID-1]*r_bins[ID-1]))
        #print("delta_f",fluxes[ID-1]/surf[ID-1])
        #print()

        #print(ID,fluxes[ID-1],r_bins[ID-1],r_bins[ID])





    r=(r_bins[1:]+r_bins[:-1])*0.5
    if prune_zeros ==True:

        selected=fluxes>0
    else:
        selected=np.ones(fluxes.shape,dtype=bool)

    r=r[selected]
    fluxes=fluxes[selected]
    surf=surf[selected]

    if intensity==True:
        fluxes*=1.0/surf


    return fluxes,r,surf


def radial_polar_local_maxima(polar_r,polar_theta,flux_values,r_max=None, N_bins=10, use_equal_area_annuli=False,):
    if r_max is None:
        r_max=polar_r.max()

    N_bins=np.int(N_bins)

    if N_bins==0:
        N_bins=1

    if use_equal_area_annuli==True:
        r_bins=equal_area_annuli(r_max,N_bins,np.arange(N_bins+1))
    else:
        r_bins=np.linspace(0,r_max,N_bins+1)

    lm_ids=[]

    for ID in range(1,r_bins.size):


        msk_up=polar_r<=r_bins[ID]

        if ID>1:
            msk_down=polar_r<=r_bins[ID-1]
            m_ring=np.logical_and(~msk_down,msk_up)
            f_ring=flux_values[m_ring]
            if f_ring.size>0:
                f_m=f_ring.mean()
                f_std=f_ring.std()
                f_th=f_ring.min()+f_std
                m_max=np.logical_and(m_ring,flux_values>f_th)

        else:

            m_max=msk_up



        lm_ids.extend(np.argwhere(m_max).flatten().tolist())

    return lm_ids

def radial_profile(polar_r, flux_values,r_max=None, N_bins=10, use_equal_area_annuli=False,intensity=True,pix_size=1.0):
    """
    Function to evaluate the radial  profile, given radial_coordinates of points/pixels and their fluxes values
    if intesity==True, then the flux in the annular region is divided by the number of pixels

    Parameters
    ----------
    polar_r
    flux_values
    r_max
    N_bins
    use_equal_area_annuli
    intensity

    Returns
    -------
    flux_values
    r


    """
    if r_max is None:
        r_max=polar_r.max()

    N_bins=np.int(N_bins)

    if N_bins==0:
        N_bins=1

    if use_equal_area_annuli==True:
        r_bins=equal_area_annuli(r_max,N_bins,np.arange(N_bins+1))
    else:
        r_bins=np.linspace(0,r_max,N_bins+1)







    fluxes=np.zeros(r_bins[1:].shape)
    N=np.zeros(r_bins[1:].shape)

    for ID in range(1,r_bins.size):

        f_up=flux_values[polar_r<=r_bins[ID]]


        if ID>1:
            f_down=flux_values[polar_r<=r_bins[ID-1]]

            N[ID-1]=f_up.size-f_down.size
            fluxes[ID-1]=f_up.sum() -f_down.sum()
        else:
            fluxes[ID-1]=f_up.sum()
            N[ID-1]=f_up.size






    r=(r_bins[1:]+r_bins[:-1])*0.5
    selected=fluxes>0
    r=r[selected]
    fluxes=fluxes[selected]
    N=N[selected]

    if intensity==True:
        fluxes*=1.0/N


    return fluxes,r,N




