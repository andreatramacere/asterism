"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from scipy import stats
import  numpy as np

__author__ = 'andrea tramacere'



class  Histogram(object):

    def __init__(self,n_bins=10,kde=None,normed=False):
        """
        Base class for histogram. !!! Not yet decided which KDE engine to use (Scipy/sklearn)
        Returns
        -------

        """

        __allowed_kernels__='gauss'

        self.bins=n_bins
        self.kde=kde
        self.normed=normed


        if self.kde not in __allowed_kernels__:
            raise RuntimeError('kernel ',self.kde, 'not in allowed',__allowed_kernels__)



    def eval(self,x):


        if self.kde =='gauss':
            self.counts,self.x_bins,self.x=histogram_kde(x,bins=self.bins)
            if self.normed==True:
                self.counts *= x.size()
        else:
            self.counts,self.x_bins,self.x=histogram(x,bins=self.bins,normed=self.normed)



    def plot(self):
        pass








def histogram_kde(v,bins=100,bw=None,v_min=None,v_max=None):
    if v_min  is None:
        v_min=v.min()
    if v_max is None:
        v_max = v.max()
    x_bins=np.linspace(v_min,v_max,bins+1)

    x=(x_bins[1:]+x_bins[:-1])*0.5
    pdf = stats.gaussian_kde(v,bw_method=bw)(x)
    return pdf,x_bins,x



def histogram(v=None,bins=None,normed=False):
    x=(bins[1:]+bins[:-1])*0.5
    return np.histogram(v,bins=bins,normed=normed),x



