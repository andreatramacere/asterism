#include <stdlib.h>

double* c_gauss(double* weight, double* u, double h,int N);
double* c_look_up_table(double* weight, double *table ,double* u, double dx,int N, int k_table_size);

void dist_eval(double* dist,double* xy, double x_c,double y_c, int N);
void dist_eval2(double* dist,double* xy, double x_c,double y_c, int N);
