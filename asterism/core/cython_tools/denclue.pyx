#cython: boundscheck=False, wraparound=False, nonecheck=False


cimport cython
import numpy as np
cimport numpy as np
from libc.math cimport exp,cos,fabs,sqrt,M_PI
from cpython cimport bool
from cpython cimport array
from libc.stdlib cimport malloc, free

#cdef extern from "vfastexp.h":
#    double exp_approx "EXP" (double)



#cdef extern from "kern.h":
#    struct res:
#        pass

#cdef extern from "kern.h":
#   res  cGauss  (double *, double *, double h, double *, double * ,size_t N)

cdef extern from "kern.h":
   void dist_eval(double* dist,double* xy, double x_c,double y_c, int N)
   void dist_eval2(double* dist,double* xy, double x_c,double y_c, int N)
   double* c_gauss(double* weight, double* u, double h,int N)
   double* c_look_up_table(double* weight, double *table ,double* u, double dx,int N, int k_table_size)

ctypedef np.float64_t DTYPE_f




cdef double* gauss   (double* weight, double* u, double h,int N):


    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    if N==0:
        #print 'NULL'
        return NULL

    for i in range(N):
        f[i] =weight[i]*exp(-0.5*u[i]*u[i])
        #print i,dist[i],h, f[i]

    return f

cdef double* logistic   (double* weight, double* u, double h,int N):


    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    if N==0:
        #print 'NULL'
        return NULL

    for i in range(N):
        f[i] =weight[i]*1.0/(exp(u[i])+2.0+exp(-u[i]))
        #print i,dist[i],h, f[i]

    return f





cdef double* mex   (double* weight, double* u2, double h,int N):



    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i
    
    cdef double minima,min_val

    if N==0:
        return NULL

    for i in range(N):
        f[i]= weight[i]*(1-u2[i])*exp(-0.5*u2[i])
       
    return f



cdef double* gauss_cos   (double* weight, double* u, double h,int N):



    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i

    if N==0:
        return NULL

    for i in range(N):
        c=cos(np.pi*u[i])
        f[i] =weight[i]*exp(-0.5*u[i]*u[i])*c

    return f




cdef double* gauss_cos2   (double* weight, double* u, double h,int N):



    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    if N==0:
        return NULL

    for i in range(N):
        c=cos(np.pi*u[i])
        f[i] =weight[i]*exp(-u[i]*u[i]*0.5)*c*c


    return f







cdef double* discr   (double* weight, double* u, double h,int N):



    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i

    if N==0:
        return NULL

    for i in range(N):
        if u[i]<1.0:
            f[i] =weight[i]
        else:
            f[i] =0.0


    return f

cdef double* epanen   (double* weight, double* u2, double h,int    N):
    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    cdef double t,a


    if N==0:
        return NULL

    for i in range(N):
        
        if u2[i]<1:
            f[i]=weight[i]*(1-u2[i])
        else:
            f[i]=0.0
            
    return f

cdef double* biweight   (double* weight, double* u2, double h,int    N):
    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    cdef double t,a


    if N==0:
        return NULL

    for i in range(N):
        if u2[i]<1:
            t=(1-u2[i])
            f[i]=weight[i]*t*t
        else:
            f[i]=0.0


    return f


cdef double* triweight   (double* weight, double* u2, double h,int    N):
    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    cdef double t,a


    if N==0:
        return NULL

    for i in range(N):
        if u2[i]<1:
            t=(1-u2[i])
            f[i]=weight[i]*t*t*t
        else:
            f[i]=0.0


    return f


cdef double* tricube   (double* weight, double* u, double h,int    N):
    cdef double *f = <double *>malloc(N * sizeof(double))
    cdef int i


    cdef double t,a


    if N==0:
        return NULL

    for i in range(N):
        if u[i]<1:
            f_u=fabs(u[i])
            t=(1-f_u*f_u*f_u)
            f[i]=weight[i]*t*t*t
        else:
            f[i]=0.0


    return f



cdef double* look_up_K_table   (double* weight, double* u, double* k_table_x, double* k_table_y,int k_table_size,int    N):


    #cdef double *f = <double *>malloc(N * sizeof(double))
    #cdef Py_ssize_t  i
    #cdef Py_ssize_t  ID
    #cdef double dx,y
    #cdef int  delta
    #cdef Py_ssize_t ID

    dx=k_table_x[1]-k_table_x[0]

    return c_look_up_table(weight, k_table_y , u,  dx, N,  k_table_size)

    #if N==0:
    #    return NULL
    #
    
    #for i in range(N):
    #    ID=int(u[i]/dx)
    #   if ID>=0 and ID<k_table_size:
    #        if ID<k_table_size-1:
    #            delta=1
    #        else:
    #            delta=0
    #     y=(k_table_y[ID]+k_table_y[ID+delta])*0.5
    #     f[i]=k_table_y[ID]*weight[i]
    #
    #    else:
    #        f[i]=0


    #return f





def _dist_eval(np.ndarray[DTYPE_f, ndim=2,mode="c"] xy,
               double x_c,
               double y_c,
               int N):
    cdef np.ndarray[DTYPE_f, ndim=1] dist = np.zeros(N,dtype=np.float64)

    dist_eval(&dist[0],&xy[0,0] ,x_c,y_c,N)

    return dist

def _dist_eval2(np.ndarray[DTYPE_f, ndim=2,mode="c"] xy,
               double x_c,
               double y_c,
               int N):
    cdef np.ndarray[DTYPE_f, ndim=1] dist = np.zeros(N,dtype=np.float64)

    dist_eval2(&dist[0],&xy[0,0] ,x_c,y_c,N)

    return dist



cdef double* eval_kernel(char* kern_name,double* weight,double* u, double h,int    N):    
    
    
    if kern_name==b'gauss':
        return c_gauss(weight,u,h,N)

    
    elif kern_name==b'mex':
        return mex(weight,u,h,N)


    elif kern_name==b'uniform':
        return discr(weight,u,h,N)


    elif kern_name==b'epanen':
        return epanen(weight,u,h,N)

    elif kern_name==b'biweight':
        return biweight(weight,u,h,N)    

    elif kern_name==b'triweight':
        return triweight(weight,u,h,N)    
        
    elif kern_name==b'tricube':
        return tricube(weight,u,h,N)
    
    
    elif kern_name==b'gauss_cos':
        return gauss_cos(weight,u,h,N)

    elif kern_name==b'gauss_cos2':
        return gauss_cos2(weight,u,h,N)
    
    
    elif kern_name==b'logistic':
        return logistic(weight,u,h,N)

def _eval_kernel(char* kern_name,np.ndarray[DTYPE_f, ndim=1] weight,np.ndarray[DTYPE_f, ndim=1] u, double h,int    N):
    cdef np.ndarray[DTYPE_f, ndim=1] res = np.zeros(N,dtype=np.float64)
    f= eval_kernel(kern_name,<double*> weight.data, <double*> u.data ,h,N)
    for i in range(N):
        res[i]=f[i]
    return res


#def _convolve_kernel(char* kern_name,np.ndarray[DTYPE_f, ndim=1] weight,np.ndarray[DTYPE_f, ndim=1] dist, double h,int    N):
#    cdef double res
#    f= eval_kernel(kern_name,<double*> weight.data, <double*> dist.data ,h,N)
#    res=0    
#    for i in range(N):
#        res+=f[i]
#    return res

#def cython_convolve_ph_position(np.ndarray[DTYPE_f, ndim=1] weight,
#                            np.ndarray[DTYPE_f, ndim=1]dist,
#                            int N,
#                            double h,
#                            char* kern_name,):
#    cdef double f_conv
#    cdef double *f = <double *>malloc(N * sizeof(double))
#    f_conv=0.0
#    f=eval_kernel(kern_name,<double*> weight.data, <double*> dist.data ,h,N)
#    
#    for i in range(N):
#        f_conv+=f[i]
#
#    free(f)
#    return f_conv



cdef double* eval_weighted_position(double* f, double* x,double* y,int N):

    cdef int i
    cdef double f_sum
    cdef double x_sum
    cdef double y_sum
    cdef double x_t
    cdef double y_t
    cdef  double *res = <double *>malloc(3 * sizeof(double))
    #print"res", res
    f_sum=0
    x_sum=0
    y_sum=0
    for i in range(N):
       
        f_sum=f_sum+f[i]
        x_sum+=f[i]*x[i]
        y_sum+=f[i]*y[i]

    if f_sum==0.0:
        res[0]=0
        res[1]=0
        res[2]=0
        return res
        
    x_t=x_sum/f_sum
    y_t=y_sum/f_sum
    
    res[0]=x_t
    res[1]=y_t
    res[2]=f_sum
    #print "f",f_sum
    #print "res[0]", res[0]
    return  res



                          
                      




def  _update_position(np.ndarray[DTYPE_f, ndim=1, mode="c"] weight,
                      np.ndarray[DTYPE_f, ndim=1, mode="c"] u,
                      np.ndarray[DTYPE_f, ndim=1, mode="c"] x,
                      np.ndarray[DTYPE_f, ndim=1, mode="c"] y,
                      int N_points,
                      double h,
                      char* kernel_name,
                      np.ndarray[DTYPE_f, ndim=1, mode="c"] k_table_x,
                      np.ndarray[DTYPE_f, ndim=1, mode="c"] k_table_y,
                      int k_table_size,
                      bool f_is_precomputed):

   
    
    cdef bool free_f
    
    cdef double pos[3]
    
    free_f=False

    
    
    if  k_table_size<=0 and f_is_precomputed == False:
        
        f=eval_kernel(kernel_name,<double*> weight.data,<double*> u.data,h,N_points)
        free_f=True
   
    
    elif k_table_x is not  None and k_table_y is not None and  k_table_size >0 and f_is_precomputed == False:
        
        f= look_up_K_table(<double*> weight.data,
                  <double*> u.data,
                  <double*> k_table_x.data,
                  <double*> k_table_y.data,
                  k_table_size,
                  N_points)
        free_f=True
    
    elif f_is_precomputed ==True:
        f=<double*> weight.data
        f_free=False
    else:
        raise ("wrong!!!")
        
       
    #print f[0],x[0],y[0]
    pos=eval_weighted_position(f,<double*> x.data,<double*> y.data,N_points)
    
    #print weight
    #print N_points

    if free_f == True:
        free(f)
        
    return pos[0],pos[1],pos[2]




def  _get_probab(np.ndarray[DTYPE_f, ndim=1, mode="c"] weight,
                 np.ndarray[DTYPE_f, ndim=1, mode="c"] u,
                 np.ndarray[DTYPE_f, ndim=1, mode="c"] x,
                 np.ndarray[DTYPE_f, ndim=1, mode="c"] y,
                 int N_points,
                 double h,
                 char* kernel_name):



    cdef bool free_f

    cdef double f_sum




    f=eval_kernel(kernel_name,<double*> weight.data,<double*> u.data,h,N_points)
    free_f=True


    f_sum=0
    for i in range(N_points):
        f_sum+=f[i]

    if free_f == True:
        free(f)

    return f_sum

