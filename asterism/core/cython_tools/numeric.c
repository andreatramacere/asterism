#include "kern.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>





void dist_eval(double* dist,double* xy, double x_c,double y_c, int N){
    int i;
    double dx,dy;
    int cols=2;
    for (i=0;i< N; i++){
        //for (j=0;j <n;j++){

        dx=x_c - *(xy + i*cols + 0);
        dy=y_c - *(xy + i*cols + 1);

        dist[i]=sqrt(dx*dx+dy*dy);
    }

    return;
}

void dist_eval2(double* dist,double* xy, double x_c,double y_c, int N){
    int i;
    double dx,dy;
    int cols=2;
    for (i=0;i< N; i++){
        //for (j=0;j <n;j++){

        dx=x_c - *(xy + i*cols + 0);
        dy=y_c - *(xy + i*cols + 1);

        dist[i]=dx*dx+dy*dy;
    }

    return;
}

double* c_gauss   (double* weight, double* u, double h,int N){


    double *f = (double *)malloc(N * sizeof(double));
    int i;


    if (N==0){
        //#print 'NULL'
        return 0;
    }
    for (i=0;i< N; i++){
        f[i] =weight[i]*exp(-0.5*u[i]*u[i]);
        //print i,dist[i],h, f[i]
    }
    return f;
}

double* c_look_up_table   (double* weight, double *table ,double* u, double dx,int N, int table_size){


    double *f = (double *)malloc(N * sizeof(double));
    int i,ID,delta;


    if (N==0){
        //#print 'NULL'
        return 0;
    }
    for (i=0;i< N; i++){
        ID=(int)(u[i]/dx);
        if (ID>=0 && ID<table_size){
            if (ID<table_size-1){
                    delta=1;
                }
            else{
                    delta=0;
                }
         //y=(k_table_y[ID]+k_table_y[ID+delta])*0.5;
         f[i]=table[ID]*weight[i];
        }
        else{
            f[i]=0.0;
        }


        //print i,dist[i],h, f[i]
    }
    return f;
}