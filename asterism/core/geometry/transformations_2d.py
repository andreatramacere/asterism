"""
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np

__author__ = 'andrea tramacere'




def cartesian_to_polar_2d(x,y,x_c,y_c):
    theta=np.arctan2(y-y_c,x-x_c)
    r=np.sqrt((x-x_c)*(x-x_c)+(y-y_c)*(y-y_c))
    #print (theta)
    theta_rad=np.unwrap(theta)
    #print (theta)
    return r,theta_rad



def polar_to_cartesian_2d(r,theta_rad,x_c=0,y_c=0):
    x=r*np.cos(theta_rad)
    y=r*np.sin(theta_rad)
    return x+x_c,y+y_c


def affine_transformation(x_y_array,new_ar):
    transform=np.array([[1.0/new_ar,0],[0,1.0]])
    return np.asarray(x_y_array*np.asmatrix(transform))


def rotate(theta,rot_angle):
    return theta+rot_angle

