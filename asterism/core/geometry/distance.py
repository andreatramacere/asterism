"""

"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


from sklearn.neighbors import DistanceMetric
from scipy.spatial.distance import  cdist
import numpy as np
__author__ = 'andrea tramacere'

def cdist_like_eval(ref,pos,metric='euclidean',selected=None):
    #print(ref.shape,pos.shape)
    if metric=='geod_approx':
        b_vec = (ref[0][1] + pos[:, 1]) / 2.0

        b_cos = np.cos(b_vec)
        if selected is None:
            d=cdist([ref], [pos],'euclidean') * b_cos
        else:
            d = cdist([ref], [pos][selected], 'euclidean') * b_cos[selected]

    elif metric=='euclidean':
        if selected is None:
            d= cdist([ref],[pos],metric)[0]
        else:
            d= cdist([ref],[pos][selected],metric)[0]
    else:
        raise RuntimeError('Metric %s unsupported'%metric)
    return d

def dist_eval(coords2d,index=None,x_c=None,y_c=None,metric='euclidean',selected=None):

    if index is None and x_c is  None and  y_c is  None:
        raise RuntimeError("you must provide either index or x_c,y_c")

    if index is None and (x_c is None or y_c is None):
        raise RuntimeError("you must provide either index or x_c,y_c")


    if index is not None:
        x_ref=[coords2d[index]]
    else:
        x_ref=[[x_c,y_c]]

    if metric=='haversine':
        if index is not None:
            x_ref = [coords2d[index][:,[1,0]]]
        else:
            x_ref = [[y_c, x_c]]
        dist = DistanceMetric.get_metric('haversine')

        if selected is None:
            d = dist.pairwise(x_ref, coords2d[:,[1,0]])[0]
        else:
            d = dist.pairwise(x_ref, coords2d[selected][:,[1,0]])[0]

    elif metric=='geod_approx':
        b_vec = (x_ref[0][1] + coords2d[:, 1]) / 2.0

        b_cos = np.cos(b_vec)
        if selected is None:
            d=cdist(x_ref, coords2d,'euclidean') * b_cos
        else:
            d = cdist(x_ref, coords2d[selected], 'euclidean') * b_cos[selected]

        d=d[0]

    elif metric=='euclidean':

        if selected is None:
           d= cdist(x_ref,coords2d,metric)[0]
        else:
           d= cdist(x_ref,coords2d[selected],metric)[0]


    else:
        raise RuntimeError('Metric %s unsupported'%metric)
    return d