"""
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from scipy import ndimage
import numpy as np
from skimage import transform
from ..observables.quantity import Angle
from ..image_manager.image import Image
#from ..image_manager.utils import check_Image

__author__ = 'andrea tramacere'




class ImageTransform(object):
    def __init__(self,func,name=''):

        self._func=func
        self.name=name

    def apply(self,image,fill=None,**kwg):
        Image_obj=None
        if isinstance(image,Image):
            image_array=image._masked_array
            Image_obj=image

        elif type(image)==np.ndarray:
            image_array=image
        elif type(image)==np.ma.core.MaskedArray:
            image_array=image
        else:
            raise TypeError("image is neither Image istance, nor numpy.ndarray")
        if len(image_array.shape)!=2:
            raise RuntimeError("array to rotate has not twodimensional shape")

        return self._func(image_array,fill=fill,Image_obj=Image_obj,**kwg)


class RotateImage(ImageTransform):
    """

    """
    def __init__(self,angle=90.0,units='deg',mode='constant',resize=True,rotation_center=None,fill=0,order=0):
        super(RotateImage,self).__init__(func=self.func,name='RotateImage')


        self.angle=Angle(value=angle,units=units)


        self.mode=mode
        self.resize=resize
        self.fill=fill
        self.order=order
        if rotation_center is not None:
            if len(rotation_center)==2:
                self._rotation_center=rotation_center
            else:
                raise RuntimeError('you must provide as rotation center an iterable object with length=2')

        else:
            self._rotation_center=None


    @property
    def center(self):
        return self._rotation_center

    @center.setter
    def center(self,rotation_center_x,rotation_center_y):
        self._rotation_center_x=rotation_center_x
        self._rotation_center_y=rotation_center_y

    @property
    def center_x(self):
        if self._rotation_center is not None:
            return self._rotation_center[0]

    @property
    def center_y(self):
        if self._rotation_center is not None:
            return self._rotation_center[1]

    def func(self,image_array,fill=None,center=None,Image_obj=None):
        #print("|rotation")
        Image_rotation_center=None
        if Image_obj is not None:
            Image_rotation_center=Image_obj.rotation_center

        if center is None:
            if self.center is not None:
                center=self.center
            elif Image_rotation_center:
                center=Image_rotation_center
            else:
                r,c=image_array.shape
                r=r*0.5
                c=c*0.5
                center=[r,c]

        if fill is None:
            fill=self.fill

        #print("|rotation center",center,self.angle.deg,fill)
        return  transform.rotate(image_array, self.angle.deg,resize=self.resize,center=center,mode=self.mode,cval=fill,preserve_range=True,order=self.order)






class RescaleImage(ImageTransform):
    def __init__(self,scale=None,fill=0):
        self.scale=scale

        super(RescaleImage,self).__init__(func=self.func,name='RescaleImage')
        self.fill=fill

    def func(self,image,scale=None, order=1, mode='constant', fill=0., clip=True, preserve_range=True,Image_obj=None,preserve_integral_flux=False):
        if fill is None:
            fill=self.fill

        if self.scale is not None and scale is None:
            scale=self.scale
        #print("order",order)
        new_image=transform.rescale(image,scale,order=order,mode=mode,cval=fill,clip=clip,preserve_range=preserve_range)

        if preserve_integral_flux == True:
            new_image*=1.0/(scale*scale)

        return new_image



class MapToCircle(ImageTransform):

    def __init__(self,a,b,angle,angle_units='deg',mode='constant',rotation_center=None,fill=0,resize=True):
        super(MapToCircle,self).__init__(func=self.func,name='MapToCircle')

        self.a=a
        self.b=b
        self._rotation=RotateImage(angle=angle,units=angle_units,mode=mode,resize=resize,rotation_center=rotation_center,fill=fill)



    def func(self,image,fill=None,Image_obj=None):
        '''

        Parameters
        ----------
        image
        fill
        Image_obj

        Returns
        -------

        '''
        print("|map to circle")
        if fill is None:
            fill=self.fill


        new_ar=self.a/self.b

        if Image_obj is not None:

            Image_obj.geom_transformation(self._rotation,inplace=True)
            #Image_obj.show()
            rotated=Image_obj._masked_array

        else:
            rotated= self._rotation.apply(image,fill=fill)

        original_shape=rotated.shape
        transform=np.array([[1.0,0],[0,new_ar]])
        new_shape=np.int16((original_shape[0]*1.0,original_shape[1]*1.0/new_ar))
        print("|new shape",new_shape)

        return  ndimage.interpolation.affine_transform(rotated,transform,output_shape=new_shape)




class FlipImageX(ImageTransform):

    def __init__(self,x_c=None,fill=0):
        super(FlipImageX,self).__init__(func=self.func,name='FlipImage')

        self.x_c=x_c

    def func(self,image,x_c=None,fill=None,Image_obj=None):
        r,c=image.shape
        new_image=np.zeros((r,c))
        if x_c is None:
            if self.x_c is not None:
                x_c=self.x_c

            elif Image_obj is not None:
                x_c= Image_obj.rotation_center[0]
            else:
                x_c=c/2
        for x in range(c):

            delta=x-x_c
            #print(x,x_c,delta,x_c-delta<c,c)
            if x_c-delta<=c:
                new_image[:,x]=image[:,x_c-delta]

        return new_image

class FlipImageY(ImageTransform):

    def __init__(self,y_c=None,fill=0):
        super(FlipImageY,self).__init__(func=self.func,name='FlipImage')

        self.y_c=y_c

    def func(self,image,y_c=None,fill=None,Image_obj=None):
        r,c=image.shape
        new_image=np.zeros((r,c))
        if y_c is None:
            if self.y_c is not None:
                y_c=self.y_c

            elif Image_obj is not None:
                y_c= Image_obj.rotation_center[0]
            else:
                y_c=r/2
        for y in range(r):

            delta=y-y_c
            #print(x,x_c,delta,x_c-delta<c,c)
            if y_c-delta<=r:
                new_image[y]=image[y_c-delta]

        return new_image




def test():
    from asterism import data_dir
    from ..image_processing.filters import  GaussianFilter
    from ..image_manager.image import Image
    import  pylab as plt
    #fig,(ax1,ax2)=plt.subplots(1,2)
    image=Image.from_fits_file(data_dir+'/galaxy.fits')
    print("with inplace=False, the Image._data has not changed")
    rotation=RotateImage(angle=30)
    rotated=image.geom_transformation(rotation,inplace=False)
    image.show()
    print("with inplace=True, the Image._data has changed")
    image.geom_transformation(rotation,inplace=True)
    image.show()

    return image,rotation