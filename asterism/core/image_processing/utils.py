"""

"""
# from __future__ import division, absolute_import, print_function
# from asterism.core.image_processing import geometry_transformation
# from asterism.core.image_processing import filter
#
# __author__ = 'andrea tramacere'
#
# def check_ImageTransformation(image_transformation):
#     if isinstance(image_transformation,geometry_transformation.ImageTransform):
#         pass
#     else:
#         raise RuntimeError ("the provided transformation is not an ImageTransform object")
#
# def check_ImageFilter(image_filter):
#     if isinstance(image_filter,filter.ImageFilter):
#         pass
#     else:
#         raise RuntimeError ("the provided filter is not an ImageFilter object")
#
#


from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


import math as m

import numpy as np


def find_low_flux_sub_image(image,sub_block_frac_size=0.1,stack_average_max_id=1):
    """
    This Functions splits an input image in a number of sub-blocks. Each sub-block has a
    size that is a fraction of the input image size, give by the parameter `sub_block_frac_size`.
    Sub-images are sorted according to their integrated flux. The lowest integrated flux sub-image
    is returned, or the averaged/stacked `id_max` sub-images.


    Parameters
    ----------
    image : array-like:
        The image from which to extract the sub_image for background estimation

    sub_block_frac_size : float
        The size  of the sub_images relative to the input image size

    stack_average_max_id : Optional[int] :
        Number of lowest integrated flux images to stack/average

    Returns
    -------
    sub_im : array-like

    """

    print ("|bkg sub image shape-->",   image.shape)
    (y,x)=image.shape
    sub_block_size=x*y*sub_block_frac_size
    sub_block_side=m.sqrt(sub_block_size)

    x_n=np.int(x/sub_block_side)
    x_n=max(1,x_n)
    y_n=np.int(y/sub_block_side)
    y_n=max(1,y_n)
    N_imgs=x_n*y_n

    print("|sub block side",sub_block_side)
    print("|N_imgs",N_imgs)

    x_grid=np.arange(0,x-sub_block_side,sub_block_side)
    y_grid=np.arange(0,y-sub_block_side,sub_block_side)

    flux_arr=np.zeros((N_imgs,3))


    ID=0
    #import  pylab as plt

    for x_r  in x_grid:
        for y_r in y_grid:

            flux=image[y_r:y_r+sub_block_side,x_r:x_r+sub_block_side].sum()

            flux_arr[ID]=flux,x_r,y_r
            ID+=1

    flux_sorted_id=np.argsort(flux_arr[:,0])
    flux_arr=flux_arr[flux_sorted_id]

    x_r=flux_arr[0][1]
    y_r=flux_arr[0][2]
    sub_im=np.copy(image[y_r:y_r+sub_block_side,x_r:x_r+sub_block_side])



    # sum and averages
    if stack_average_max_id>1:


        for sub_i in range(1,stack_average_max_id):
            x_r=flux_arr[sub_i][1]
            y_r=flux_arr[sub_i][2]


            sub_im+=image[y_r:y_r+sub_block_side,x_r:x_r+sub_block_side]




    return sub_im/float(stack_average_max_id)