"""
Overview
--------

This modules provides .....




Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.core.image_processing.filters




Summary
---------
.. autosummary::
    ImageFilter

Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from scipy.ndimage import gaussian_filter,median_filter,gaussian_laplace,gaussian_gradient_magnitude

try:
    from skimage.restoration import  nl_means_denoising
except:
    from skimage.restoration import _nl_means_denoising

import numpy as np

from ..image_manager.image import Image



__author__ = 'andrea tramacere'




class ImageFilter(object):
    """
    Class to handle image fitering
    """
    def __init__(self,func,name=''):

        self._func=func
        self.name=name



    def apply(self,image,):

        Image_obj=None
        if isinstance(image,Image):
            image_array=image._masked_array
            Image_obj=image

        elif type(image)==np.ndarray:
            image_array=image
        elif type(image)==np.ma.core.MaskedArray:
            image_array=image
        else:
            raise TypeError("image is neither Image istance, nor numpy.ndarray")
        if len(image_array.shape)!=2:
            raise RuntimeError("array to rotate has not twodimensional shape")

        return self._func(image_array)

    

class NonLocalMeansDenoising(ImageFilter):
    """

    """
    def __init__(self,h=1.0,patch_distance=7,patch_size=11):
        super(NonLocalMeansDenoising,self).__init__(func=self.func,name='NonLocalMeansDenoising')
        self.h=h
        self.patch_size=patch_size
        self.patch_distance=patch_distance

    def func(self,image_array):
        return  nl_means_denoising(image_array,h=self.h,patch_size=self.patch_size,patch_distance=self.patch_distance)


class GaussianFilter(ImageFilter):
    """

    """
    def __init__(self,sigma=1.0):
        super(GaussianFilter,self).__init__(func=self.func,name='GaussianFilter')
        self.sigma=sigma

    def func(self,image_array):
        return  gaussian_filter(image_array,sigma=self.sigma)



class GaussLaplaceFilter(ImageFilter):
    """

    """
    def __init__(self,sigma=1.0):
        super(GaussLaplaceFilter,self).__init__(func=self.func,name='GaussLaplaceFilter')
        self.sigma=sigma

    def func(self,image_array):
        return  gaussian_laplace(image_array,sigma=self.sigma)



class InvGaussLaplaceFilter(ImageFilter):
    """

    """
    def __init__(self,sigma=1.0):
        super(InvGaussLaplaceFilter,self).__init__(func=self.func,name='InvGaussLaplaceFilter')
        self.sigma=sigma

    def func(self,image_array):
        return  -gaussian_laplace(image_array,sigma=self.sigma)


class AbsGaussLaplaceFilter(ImageFilter):
    """

    """
    def __init__(self,sigma=1.0):
        super(AbsGaussLaplaceFilter,self).__init__(func=self.func,name='AbsGaussLaplaceFilter')
        self.sigma=sigma

    def func(self,image_array):
        return  np.fabs(gaussian_laplace(image_array,sigma=self.sigma))




class GaussGradientMagFilter(ImageFilter):
    """

    """
    def __init__(self,sigma=1.0):
        super(GaussGradientMagFilter,self).__init__(func=self.func,name='GaussGradientMagFilter')
        self.sigma=sigma

    def func(self,image_array):
        return  gaussian_gradient_magnitude(image_array,sigma=self.sigma)




class MedianFilter(ImageFilter):
    """

    """

    def __init__(self,window_size):
        super(MedianFilter,self).__init__(func=self.func,name='MedianFilter')
        self.window_size=window_size

    def func(self,image_array):
        return median_filter(image_array,size=self.window_size)


class UnsharpFilter(ImageFilter):
    """

    """
    def __init__(self,sigma=1.0,k=0.5):
        super(UnsharpFilter,self).__init__(func=self.func,name='UnsharpFilter')
        self.sigma=sigma
        self.k=k

    def func(self,image_array):
        print(type(image_array))
        image=  image_array-self.k*gaussian_filter(image_array,sigma=self.sigma)

        image[image<0]=0
        return image

class Map(ImageFilter):
    """

    """

    def __init__(self,func=None,min=None,max=None,out_range_min=0,out_range_max=1):
        if func is None:
           self.func=  lambda x: x
        else:
            self.func=func

        self.min=min
        self.max=max
        self.out_range_min=out_range_min
        self.out_range_max=out_range_max

    def apply(self,image_array,inplace=False):

        """
        NOT TESTED
        Args:
            image_array:
            inplace:

        Returns:

        """
        if self.min is None:
            min=image_array.min()
        else:
            min=self.min

        if self.max is None:
            max=image_array.max()
        else:
            max=self.max


        sel1=image_array<min
        image_array[sel1]=self.out_range_min
        sel1=image_array<=min

        sel2=image_array>max
        image_array[sel2]=self.out_range_max
        sel2=image_array>=max

        sel2=np.logical_not(sel2)
        sel1=np.logical_not(sel1)
        image_array[sel1*sel2]= self.func(image_array[sel1*sel2]-min)/self.func(max-min)

        sel1=image_array<=min
        image_array[sel1]=self.out_range_min
        sel2=image_array>=max
        image_array[sel2]=self.out_range_max


        return image_array


def test():
    from asterism import data_dir
    from asterism.core.image_manager.image import Image
    import pylab as plt
    fig,(ax1,ax2)=plt.subplots(1,2)
    image=Image.from_fits_file(data_dir+'/galaxy.fits')
    gauss_filter=GaussianFilter(sigma=2.0)
    ax1.imshow(image._data,interpolation='nearest')
    filtered_image=gauss_filter.apply(image._data)
    ax2.imshow(filtered_image,interpolation='nearest')
    plt.show()

    pass
