"""
Module: features
========================



Overview
--------

This modules provides the implementation of the :class:`.MorphometricFeatures` class
used to handle the storage of morphometric features.
This class, actually implements a list of :Feature:`.Feature` objects




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::
   Feature
   MorphometricFeatures


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import numpy as np

__author__ = 'andrea tramacere'




class MorphometricFeaturesArray(object):
    """
    Class storing features and features names built from a list
    of :class:`asterism.core.morphometry.features.MorphometricFeatures` objects

    """

    def __init__(self,morph_features_list):
        self.dtype=[]
        self.values_tuple=()
        self.add(morph_features_list)


    def add(self,morph_features_list):
        _list=[]
        _dt_list=[]

        for morph_feature in morph_features_list:
            _list.extend(morph_feature.values)
            _dt_list.extend(morph_feature.dtype.descr)

        self.dtype= np.dtype(_dt_list)

        self.values_tuple=tuple(_list)


class MorphometricFeatures(object):

    def __init__(self,
                 group_name,\
                 names,\
                 dtypes,\
                 null_value=None,\
                 failed_value=None,\
                 name_flag=None,
                 values=None):
        self.names=names
        self.dtype=self._build_dtype(group_name,names,dtypes,name_flag=name_flag)
        self.null_value=null_value
        self.failed_value=failed_value

        if values is  None:
            self.values=values
        else:
            self.values=tuple(values)

    def _build_dtype(self,group_name,names,dtypes,name_flag=None):
        _list=[]
        for name,dt in zip(names,dtypes):
            f_name=group_name+'_'+name
            if name_flag is not None:
                f_name+='_'+name_flag
            _list.append((f_name,dt))

        return np.dtype(_list)

    def fill(self,values_tuple):

        self.values=tuple(values_tuple)

    def fill_null(self):
        values_tuple=[self.null_value]*len(self.dtype.names)
        self.values=tuple(values_tuple)

    def fill_failed(self):
        values_tuple=[self.failed_value]*len(self.dtype.names)
        self.values=tuple(values_tuple)

    @classmethod
    def feature_factory(cls,
                        feat_names,
                        feat_dt,
                        group_name,
                        name_flag=None,
                        null_value=None,
                        failed_value=None):




        return MorphometricFeatures(group_name,
                                 feat_names,
                                 feat_dt,
                                 name_flag=name_flag,
                                 null_value=null_value,
                                 failed_value=failed_value)


