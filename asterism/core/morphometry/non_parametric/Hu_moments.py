"""
Module: Hu_moments
========================



Overview
--------

This modules provides the implementation of the functions for the extraction of
morphometric features, related to Hu_moments




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::
    eval_log_Hu_moments
    extract_Hu_moments_img
    extract_Hu_moments_features_attr
    extract_Hu_moments_features_cnt
    get_cluster_contour_Hu_moments_morphometric_features

Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.features import MorphometricFeatures

#import cv2
#from skimage.measure import moments_central,moments_normalized,moments_hu
from skimage import  measure
import numpy as np
from scipy.stats import moment

__author__ = 'andrea tramacere'



def eval_log_Hu_moments(yx_array,weight=None):

    #print yx_array

    if weight!=None:
        yx_array=np.copy(yx_array)
        yx_array[:,0]=yx_array[:,0]*weight
        yx_array[:,1]=yx_array[:,0]*weight

    x_bar=yx_array[:,1].mean()
    y_bar=yx_array[:,0].mean()
    mu=np.zeros((4,4))
    for n  in range(4):
        for m in range(4):
            _k=(yx_array[:,0]-y_bar)**n *(yx_array[:,1]-x_bar)**m
            mu[n,m]=_k.sum()


    nu = measure.moments_normalized(mu)
    HuMoments = measure.moments_hu(nu)

    #return nu,hu
    # shift image by dx=2, dy=3
    #moments=cv2.moments(yx_array)
    #HuMoments=cv2.HuMoments(moments)
    #print HuMoments
    #print"--> Hu<0", HuMoments[HuMoments<=0]
    HuMoments[np.fabs(HuMoments)<1E-50]=1E-50
    #HuMoments=HuMoments[0]
    #print HuMoments
    log_HuMoments=np.log10(np.fabs(HuMoments)) *np.sign(HuMoments)
    log_HuMoments=log_HuMoments.reshape(7,)
    #print "--> log_HuMoments",log_HuMoments/np.linalg.norm(log_HuMoments)
    return log_HuMoments
    #/np.linalg.norm(log_HuMoments)

def extract_Hu_moments_img(cluster_image,x_c,y_c):
    mu=measure.moments_central(cluster_image,y_c,x_c, 3)
    nu=measure.moments_normalized(mu,3)
    hu=measure.moments_hu(nu)
    log_hu=np.log10(np.fabs(hu)) *np.sign(hu)
    return tuple(log_hu)





def get_cluster_contour_Hu_moments_morphometric_features(cluster,group_name='cnt',name_flag=None):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """
    feat_names=['log_Hu_0','log_Hu_1','log_Hu_2','log_Hu_3','log_Hu_4','log_Hu_5','log_Hu_6']
    feat_dt=[np.float32]*len(feat_names)

    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-99.,
                                                failed_value=np.nan)

    if cluster is None:
        m_feat.fill_null()
    elif cluster.contour_shape is  None:
        m_feat.fill_null()
    else:
        try:
            feature_values=eval_log_Hu_moments(cluster.contour_shape)
            m_feat.fill(feature_values)
        except  Exception as e :
            raise Warning("!Warning  get_attractors_Hu_moments_morphometric_features failed",e)
            m_feat.fill_failed()


    return m_feat




def get_cluster_image_Hu_moments_morphometric_features(cluster_image,centr,group_name='cl_image',name_flag=None):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """
    feat_names=['log_Hu_0','log_Hu_1','log_Hu_2','log_Hu_3','log_Hu_4','log_Hu_5','log_Hu_6']
    feat_dt=[np.float32]*len(feat_names)

    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-99.,
                                                failed_value=np.nan)

    (x_c, y_c)=centr
    if cluster_image is None:
        m_feat.fill_null()
    else:
        try:
            feature_values=extract_Hu_moments_img(cluster_image,x_c,y_c)
            m_feat.fill(feature_values)
        except  Exception as e :
            raise Warning("!Warning  get_attractors_Hu_moments_morphometric_features failed",e.message)
            m_feat.fill_failed()


    return m_feat



def get_attractors_Hu_moments_morphometric_features(attractors_matrix,group_name='attr',name_flag=None):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """
    feat_names=['log_Hu_0','log_Hu_1','log_Hu_2','log_Hu_3','log_Hu_4','log_Hu_5','log_Hu_6']
    feat_dt=[np.float32]*len(feat_names)

    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-99.,
                                                failed_value=np.nan)


    if attractors_matrix is None:
        m_feat.fill_null()
    else:
        try:
            feature_values=eval_log_Hu_moments(attractors_matrix.coords)
            m_feat.fill(feature_values)
        except Exception as e :
            m_feat.fill_failed()
            raise Warning("!Warning  get_attractors_Hu_moments_morphometric_features failed",e)



    return m_feat


