"""
Module:
========================



Overview
--------

This modules provides the implementation of the


Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::



Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.features import MorphometricFeatures
import  traceback
__author__ = 'andrea tramacere'



from scipy import stats
import  numpy as np
from asterism.core.stats.histogram.radial_distributions import theta_histogram_kde,radial_histogram_kde


def eval_attractors_distr_features(values,distr,x):



    if values is None:
        mode=0
        x_min_non_zero=0
        std=0
        delta_x_non_zero=0
        kurt=0
        skew=0
        skew_p_val=0
    else:



        mode=x[np.argmax(distr)]

        skew=stats.skew(values,bias=True)

        kurt=stats.kurtosis(values,bias=True)

        std=np.std(values,ddof=1)

        if distr[distr>0].size>0:
            x_min_non_zero=x[distr>0].min()
            x_max_non_zero=x[distr>0].max()
        else:
            x_max_non_zero=0.0
            x_min_non_zero=0.0
        #print('--->',x_max_non_zero,x_min_non_zero,x.min(),x.max())
        delta_x_non_zero=x_max_non_zero-x_min_non_zero



        if values.size<8 :
            skew_p_val=0
            skew_z_score=0
        else:
             skew_z_score,skew_p_val=stats.skewtest(values)
        print ('| std,delta non zero',std,delta_x_non_zero)
    return (mode,std,skew,skew_p_val,kurt,x_min_non_zero,delta_x_non_zero)




def get_attractors_theta_distr_morphometric_features(polar_coords,group_name='theta_distr_attr',name_flag=None):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """

    feat_names=['sig','skew','skew_p_val','kurt','delta_theta_non_zero']
    feat_dt=[np.float32]*len(feat_names)
    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-1.0,
                                                failed_value=np.nan)

    if polar_coords is None:
        m_feat.fill_null()
    else:
        try:
            theta_pdf,theta_pdf_bins,theta_pdf_x=theta_histogram_kde(polar_coords.theta,bw=0.1)

            print('|theta')
            mode,std,skew,skew_p_val,kurt,x_min_non_zero,delta_x_non_zero=eval_attractors_distr_features(polar_coords.theta,theta_pdf,theta_pdf_x)
            m_feat.fill([std,skew,skew_p_val,kurt,delta_x_non_zero])

            #import pylab as plt
            #plt.plot(theta_pdf_x,theta_pdf)
            #plt.show()

        except Exception as e:
            print(traceback.format_exc())
            raise Warning("!Warning cluster_attractors_polar distr failed",e)
            m_feat.fill_failed()



    return m_feat





def get_attractors_r_distr_morphometric_features(polar_coords,polar_coords_non_core,cls_size,group_name='r_distr_attr',name_flag=None):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes


    """

    feat_names=['non_core_ratio','mode','sig','skew','skew_p_val','kurt','delta_r_non_zero']
    feat_dt=[np.float32]*len(feat_names)
    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-1.0,
                                                failed_value=np.nan)

    if polar_coords is None:
        m_feat.fill_null()
    else:
        try:
            print('|r')
            if polar_coords_non_core is None:
                ratio=0.0
                feature_values=[ratio]
                mode,std,skew,skew_p_val,kurt,x_min_non_zero,delta_x_non_zero=eval_attractors_distr_features(None,None,None)
            else:
                ratio=np.float(polar_coords_non_core.r.size)/np.float(cls_size)
                feature_values=[ratio]
                r_pdf,r_pdf_bins,r_pdf_x=radial_histogram_kde(polar_coords_non_core.r,r_norm=True,bw=0.1)

                mode,std,skew,skew_p_val,kurt,x_min_non_zero,delta_x_non_zero=eval_attractors_distr_features(polar_coords_non_core.r/polar_coords_non_core.r.max(),r_pdf,r_pdf_x)

                #import pylab as plt
                #plt.plot(r_pdf_x,r_pdf)
                #plt.show()


            feature_values.extend([mode,std,skew,skew_p_val,kurt,delta_x_non_zero])



            m_feat.fill(feature_values)
        except Exception as e:
            print(traceback.format_exc())
            raise Warning("!Warning cluster_attractors_polar distr failed",e)
            m_feat.fill_failed()



    return m_feat

