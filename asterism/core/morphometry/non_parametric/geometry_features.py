"""
Module: geometry_features
========================



Overview
--------

This modules provides the implementation of the functions for the extraction of
morphometric features, related to geometry




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::
   do_morphometric_features_extraction_process_func
   FeaturesArray
   DoMorphometricFeaturesExtraction


Module API
-----------
"""


from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.features import MorphometricFeatures
import  numpy as np
#import cv2
__author__ = 'andrea tramacere'



def eval_geometric_features(sig_x,sig_y,n_points,contour_coords=None):



    ecc=np.sqrt(1-(sig_y/sig_x)**2)


    if contour_coords is not None:
        contour_perimeter=len(contour_coords)
        contour_area= n_points+contour_perimeter
    else:
        contour_area=None


    if (contour_coords is None or contour_area is None):
        #print "-> c_s",self.contour_shape
        #print "-> c_a",self.contour_area
        geom_compactness=-1
        area_ratio=-1
        perimeter_ratio=-1
        AR=-1

    else:


        min_size,max_size,AR,theta=eval_AR(np.float32(contour_coords ))

        if contour_area>0:
            geom_compactness=(contour_perimeter*contour_perimeter)/float(contour_area)
            area_ratio=(max_size*min_size)/contour_area
            perimeter_ratio=(2*min_size+2*max_size)/contour_perimeter
        else:
            geom_compactness=-1
            area_ratio=-1
            perimeter_ratio=-1



    return  [n_points,ecc,geom_compactness,AR,area_ratio,perimeter_ratio]


def eval_AR(contour_coords):
    #(x,y),(w,h),theta=cv2.minAreaRect(contour_coords)
    w=1
    h=1
    theta=0
    min_size=min(w,h)
    max_size=max(w,h)

    if min_size!=0:
        AR=max_size/min_size
    else:
        AR=-1
    #print min_size,max_size,AR
    return min_size,max_size,AR,theta


def get_cluster_geometric_morphometric_features(cluster,group_name='geom',name_flag=None):

    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """

    feat_names=['pix_size','ecc','comp','ar','contour_ratio','ratio','r_max']
    feat_dt=[np.float32]*len(feat_names)

    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-99.,
                                                failed_value=np.nan)


    if cluster is None:
        m_feat.fill_null()
    else:
        try:
            feature_values=eval_geometric_features(cluster.sig_x,cluster.sig_y,cluster.n_points,contour_coords=cluster.contour_shape)
            feature_values.append(cluster.r_max)
            m_feat.fill(feature_values)
        except Exception as e:
            raise Warning("!Warning cluster_geometric_morphometric_features failed",e)
            m_feat.fill_failed()


    return m_feat

