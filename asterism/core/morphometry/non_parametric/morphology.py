"""
Module: morphology
========================



Overview
--------

This modules provides the implementation of the functions for the extraction of
morphometric features, related to Hu_moments




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::
   do_morphometric_features_extraction_process_func
   FeaturesArray
   DoMorphometricFeaturesExtraction


Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  copy
from photutils import CircularAperture,aperture_photometry,EllipticalAperture

from asterism.core.image_processing.filters import GaussianFilter,MedianFilter
from asterism.core.image_processing.geometry_transformation import FlipImageX, FlipImageY, RotateImage
from asterism.core.morphometry.features import MorphometricFeatures
from asterism.core.stats.histogram.radial_distributions import radial_profile_image
import traceback
from scipy.stats import spearmanr
import numpy as np
import photutils

__author__ = 'andrea tramacere'









def get_roi(image,aperture):
    #rows, cols = np.indices(image.shape)
    #print ('iamge x min,max',cols.min(),cols.max())

    roi = aperture.to_mask(method='center')[0]
    roi_cutout = roi.cutout(image)

    rows, cols = np.indices(roi_cutout.shape)
    #print('cutout x min,max', cols.min(), cols.max())
    msk = np.ravel(roi.data > 0)

    #print ('mks',msk.sum(),msk.size)
    flux_array = roi_cutout.flatten()[msk]
    x_coords = cols.flatten()[msk]
    y_coords = rows.flatten()[msk]
    x_c= ( x_coords.min() + x_coords.max())*0.5
    y_c= ( y_coords.min() + y_coords.max())*0.5
    print('x_c,y_c',x_c,y_c)

    import  pylab as plt
    fig,(ax1,ax2,ax3)=plt.subplots(1,3)
    ax1.imshow(image,interpolation='nearest')
    ax2.imshow(roi_cutout,interpolation='nearest')
    ax3.imshow(roi.data > 0,interpolation='nearest')
    plt.show()

    return flux_array,x_coords,y_coords,x_c,y_c,roi.data > 0


def get_hole(image,aperture):
    masks = aperture.to_mask(method='center')
    mask = masks[0]
    return mask.to_image(shape=image.shape)



# NEEDED by the photutils v0.2.2 version
def get_roi_old_v_ph(image,aperture):

    rows, cols = np.indices(image.shape)
    _selected=ellips_contain_ineq(aperture.positions[0][0],
                                  aperture.positions[0][1],
                                  aperture.a,
                                  aperture.b,
                                  aperture.theta,
                                  cols,
                                  rows)<1.0

    #x_c = (cols.flatten().min() + cols.flatten().max()) * 0.5
    #y_c = (rows.flatten().min() + rows.flatten().max()) * 0.5


    flux_array = image[_selected]
    x_coords = cols[_selected]
    y_coords = rows[_selected]
    x_c = (x_coords.min() + x_coords.max()) * 0.5
    y_c = (y_coords.min() + y_coords.max()) * 0.5

    return flux_array.flatten(),x_coords.flatten(),y_coords.flatten(),x_c,y_c,flux_array.flatten() > 0


def ellips_contain_ineq(x_c, y_c, a, b, angle, x, y):

    cosa = np.cos(angle)
    sina = np.sin(angle)
    # dd = d   / 2 * d / 2
    # DD = D / 2 * D / 2
    a1 = cosa * (x - x_c) + sina * (y - y_c)
    b1 = sina * (x - x_c) - cosa * (y - y_c)

    return ((a1 * a1) / (a * a)) + ((b1 * b1) / (b * b))

def get_hole_old(image,aperture):

    hole=np.zeros(image.shape)
    rows, cols = np.indices(image.shape)
    _selected=ellips_contain_ineq(aperture.positions[0][0],
                                  aperture.positions[0][1],
                                  aperture.a,
                                  aperture.b,
                                  aperture.theta,
                                  cols,
                                  rows)<1.0

    hole[rows.flatten()[_selected.flatten()],cols.flatten()[_selected.flatten()]]=1

    return hole

# =============================================================





def get_mid_value(r,r1):
    #print ('r',r.shape)
    _r=None
    if  np.size(r)>=2:
        #print('a')
        _r= (r[0]+r[1])*0.5
    elif np.size(r)==1:
        #print('b')
        _r= r[0]
    else:
        pass
    if _r is None:
        _r=max(r1)
    #print('_r',_r)
    return _r

def eval_r_Petrosian(I, r, s, image_array=None, plot=False,apertures=None,petrosian_factor=1.5):
    """

    Parameters
    ----------
    r_coords
    r_max
    flux_array

    Returns
    -------

    """
    #see http://spiff.rit.edu/classes/phys443/lectures/gal_1/petro/petro.html

    I_a = (I * s).cumsum()/s.cumsum()
    L_ratio = (I * s).cumsum() / (I * s).sum()


    _r = np.linspace(r.min(), r.max(), 1000)
    I_a=np.interp(_r, r, I_a)
    I = np.interp(_r, r, I)
    L_ratio=np.interp(_r, r, L_ratio)

    r_20 = _r[np.argwhere(L_ratio >= 0.2)]
    r_20=get_mid_value(r_20.flatten(),r)




    r_80 = _r[np.argwhere(L_ratio >= 0.8)]
    r_80 = get_mid_value(r_80.flatten(),r)

    r_90 = _r[np.argwhere(L_ratio >= 0.9)]
    r_90 = get_mid_value(r_90.flatten(),r)


    r_50 = _r[np.argwhere(L_ratio >= 0.5)]
    r_50 = get_mid_value(r_50.flatten(),r)

    r_petrosian = _r[np.argwhere(I_a/I >= 5.0)]
    r_petrosian=get_mid_value(r_petrosian.flatten(),r)
    #print('r_petrosian', r_petrosian,)

    r_petrosian*=petrosian_factor

    #print(r_20,r_80,r_petrosian)
    if plot == True:
        import pylab as plt
        fig, (ax1, ax2) = plt.subplots(1, 2)
        ax1.imshow(image_array, interpolation='nearest')
        ax2.plot(_r, I/I_a, '-o')
        #ax2.plot(_r, I, '-')
        #ax2.plot(_r,L_ratio,'-')
        #ax2.plot(r,I_a,'-*')
        #ax2.plot(r,I_a/I,'-')
        ax2.plot([r_20, r_20], [0, 1.0], '--', label='r20')
        ax2.plot([r_50, r_50], [0, 1.0], '--', label='r50')
        ax2.plot([r_80, r_80], [0, 1.0], '--', label='r80')
        ax2.plot([r_90, r_90], [0, 1.0], '--', label='r90')
        ax2.plot([r_petrosian, r_petrosian], [0, 1.0], '--', label='Petrosian')
        #ax2.set_ylim([0, I.max()])
        ax2.legend()
        if apertures is not None:
            for a in apertures:
                a.plot(ax=ax1, lw=0.5, color='w')

        plt.tight_layout()
        plt.show()



    return r_20, r_80, r_50, r_90, r_petrosian,

def eval_clumpiness(cluster_image, cluster_blurred_image,bkg_image,bkg_image_blurred,r_petrosian,r_inner):
    """

    Parameters
    ----------
    cluster_image
    cluster_blurred_image

    Returns
    -------

    """

    #cluster_blurred_image=cluster_blurred_image*(cluster_image.sum()/cluster_blurred_image.sum())
    #bkg_image_blurred=bkg_image_blurred*(cluster_image.sum()/bkg_image_blurred.sum())
    #bkg_image=bkg_image*(cluster_image.sum()/bkg_image.sum())
    #print('K',cluster_image.sum()/cluster_blurred_image.sum())
    #print('_img', cluster_blurred_image.sum() / cluster_image.sum(),cluster_image.sum()/cluster_blurred_image.sum())

    _img = np.copy(cluster_image)
    im_s = _img - cluster_blurred_image
    #print('_img', cluster_blurred_image.sum() / cluster_image.sum())
    msk=im_s < 0.0
    im_s[msk] = 0.0
    _img[msk] = 0.0
    #print('_img', cluster_blurred_image.sum() / cluster_image.sum())
    im_b = bkg_image-bkg_image_blurred
    #msk = im_b < 0.0
    #im_b[msk] = 0.0

    #print('_img', cluster_blurred_image.sum() / cluster_image.sum())
    #_cluster_image=np.copy(cluster_image)

    #_cluster_image[im_s < 0.0]=0

    f0 =  aperture_photometry(_img, r_petrosian)['aperture_sum'][0]
    #f0 -= aperture_photometry(_img, r_inner)['aperture_sum'][0]

    f=aperture_photometry(im_s, r_petrosian)['aperture_sum'][0]
    #f-=aperture_photometry(im_s, r_inner)['aperture_sum'][0]

    f_b=aperture_photometry(im_b, r_petrosian)['aperture_sum'][0]
    #   f_b-= aperture_photometry(im_b, r_inner)['aperture_sum'][0]

    #diff = cluster_image - cluster_blurred_image
    #diff_bg= bkg_image-bkg_image_blurred

    #c=diff[msk].sum() / cluster_image[msk].sum()
    #c_bg=diff_bg[msk].sum()/cluster_image[msk].sum()

    #print('fs', f,f0)
    c1 = 1.0*(f-f_b)/f0

    print ('clumpiness',c1)
    plot=False
    if plot==True:
        import pylab as plt
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
        ax1.imshow(cluster_image, interpolation='nearest')
        ax2.imshow(cluster_blurred_image, interpolation='nearest')
        ax3.imshow(im_s, interpolation='nearest')
        r_petrosian.plot()
        r_inner.plot()
        plt.show()
    return c1

def set_asymm_grid(r,c,x_c,y_c,delta_x,delta_y,mesh):

    n_grid_x_min = max(0, x_c - delta_x)
    n_grid_x_max = min(c, x_c + delta_x)

    n_grid_y_min = max(0, y_c - delta_y)
    n_grid_y_max = min(r, y_c + delta_y)

    grid_x = np.linspace(n_grid_x_min, n_grid_x_max, num=(1+2*mesh))
    # asymm_index_arr_x=np.ones(grid_x.size)*-1

    grid_y = np.linspace(n_grid_y_min, n_grid_y_max, num=(1+2*mesh))

    return grid_x,grid_y

def eval_asymmetry(src_image, x_c, y_c, aperture,delta_x=3,delta_y=3,mesh=1,f0=None,bkg_image=None,shape=False):
    """

    Parameters
    ----------
    cluster_image
    x_c
    N

    Returns
    -------

    """

    #import pylab as plt
    #plt.imshow(src_image,interpolation='nearest')
    #aperture.plot()
    #plt.show()

    # f2=np.fabs(cluster_image).sum()
    (r, c) = src_image.shape

    #print ('delta,mesh->',delta_x,mesh)
    #image_x_c = np.int(c * 0.5)
    #image_y_c = np.int(r * 0.5)


    # asymm_index_arr_y=np.ones(grid_y.size)*-1


    if shape == False:


        if bkg_image is not None:
            _src_image=src_image-bkg_image
        else:
            _src_image=src_image

    else:

        _src_image=src_image
        #import pylab as plt
        #plt.imshow(_src_image, interpolation='nearest')
        #plt.show()

    #import pylab as plt

    grid_x, grid_y=set_asymm_grid(r, c, x_c, y_c, delta_x, delta_y,mesh)

    rot = RotateImage(angle=180.0, resize=False)

    if f0 is None:
        f0 = aperture_photometry(np.fabs(_src_image), aperture)['aperture_sum'][0]
    #print ('f0',f0)
    #import time
    #t = time.time()
    #from PIL import Image

    ID_min_y=-1
    ID_min_x=-1
    ID_center_x=np.int(mesh)
    ID_center_y=np.int(mesh)
    MAX_TRIAL=10
    TRIAL=0
    while not (ID_min_y==ID_center_y and ID_min_x==ID_center_x) and TRIAL<MAX_TRIAL:
        TRIAL+=1
        asymm_array = np.ones(grid_x.size * grid_y.size, dtype=np.float).reshape(grid_y.size ,grid_x.size)*2.0
        ID_center=(2*int(mesh)+1)*int(mesh)+int(mesh)
        #print('x_gri,y_grid',grid_x,grid_y,ID_center,asymm_array.shape)
        for ID_X, x in enumerate(grid_x):


            for ID_Y, y in enumerate(grid_y):



                i_rot = rot.apply(_src_image, center=[x , y],fill=src_image.min())
                #i_rot=Image.fromarray(_src_image)
                #i_rot.rotate(180, expand=1)
                # print('check rot/crop asysmm',i_crop.sum(),i_rot.sum())
                #f_rot=aperture_photometry(np.fabs(i_rot-src_image), aperture)['aperture_sum'][0]/f0
                #f1=np.fabs(f_rot-f0)

                asymm_array[ID_Y,ID_X]=aperture_photometry(np.fabs(i_rot-_src_image), aperture)['aperture_sum'][0]

                #fig,(ax1,ax2,ax3)=plt.subplots(1,3)
                #ax1.imshow(src_image,interpolation='nearest')
                #ax1.plot(x,y,'+')
                #ax2.imshow(i_rot,interpolation='nearest')
                #ax3.imshow(i_rot-_src_image,interpolation='nearest')
                #aperture.plot()
                #print(x, y, asymm_array[ID_X+(ID_Y*grid_x.size)]/f0)
                #plt.show()

        (ID_min_y,ID_min_x)=np.unravel_index(np.argmin(asymm_array, axis=None), asymm_array.shape)
        x_c = grid_x[ID_min_x]
        y_c = grid_y[ID_min_y]
        #print('->',ID_min_x,ID_center_x,ID_min_y,ID_center_y,ID_min_y==ID_center_y and ID_min_x==ID_center_x)
        grid_x, grid_y = set_asymm_grid(r, c, x_c, y_c, delta_x, delta_y,mesh)
        tmp_min=asymm_array.min()
        #print('t rot',_src_image.shape, (time.time() - t)/(ID_X*ID_Y))
        #print("asymm_index min/max", asymm_array.min()/f0,asymm_array.max()/f0)
    if TRIAL==MAX_TRIAL:
        min_val=tmp_min
    else:
        min_val= asymm_array[ID_min_y,ID_min_x]
    #print('DONE!')
    return np.fabs(min_val)/f0,f0


def eval_Gini(flux_array):
    """
    The Gini factor :cite:`Lotz:2004` indicates the distribution of the light among pixels. A value of Gini equal to 1
    would indicate that  all the light is concentrated in one pixel, on the contrary, a lower value would indicates that
    the light is distributed more evenly amongst the pixels, with Gini factor equal to 0, if all the pixels have the s
    ame flux value.

    Parameters
    ----------
    cluster_image :

    Returns
    -------
    Gini factor : float


    .. bibliography:: references.bib
    """

    #print (mask.data)
    #print (flux_array)

    #print (flux_array.size,mask.cutout(cluster_image).size)

    flux_sorted_ids = np.argsort(flux_array.flatten())
    n = flux_sorted_ids.size
    if n > 1:
        ID = np.arange(n)
        ID = ID + 1
        flux_sorted = flux_array[flux_sorted_ids]
        g_down = np.fabs(flux_sorted.mean()) * n * (n - 1)

        g_up = (2 * ID - n - 1) * np.fabs(flux_sorted)

        g = g_up.sum() / g_down
    else:
        g = 1.0
    print ('Gini',g)
    return g


def eval_M20(flux_array, x, y, x_c, y_c):
    """

    Parameters
    ----------
    flux_array
    x
    y
    x_c
    y_c

    Returns
    -------

    """
    flux_sorted_ids = np.argsort(flux_array)[::-1]
    flux_sorted = flux_array[flux_sorted_ids]
    f_tot = flux_sorted.sum()

    ID_max = flux_sorted.size
    ID = 1
    while flux_sorted[0:ID].sum() / f_tot < 0.2 and ID < ID_max:
        ID += 1

    ID_20 = ID - 1
    x = x[flux_sorted_ids]
    y = y[flux_sorted_ids]
    dist = (x - x_c) ** 2 + (y - y_c) ** 2
    m_tot = (flux_sorted * dist).sum()
    m_20 = (flux_sorted * dist)[0:ID_20].sum() / m_tot



    if m_20 <= 0.0:
        m_20= -100
    else:
        m_20=np.log10(m_20)

    print('M20', m_20)

    return m_20


def eval_Concentration(r_20, r_80, r_50, r_90):
    """
    Evaluates the concentration parameter:
    r_X=major semiaxis of the ellipictal aperture containing a X fraction of
      the total galaxy flux

    Parameters
    ----------
    r_80
    r_20
    r_50
    r_90



    Returns
    -------
    c1= 1.0 * np.log10(r_80 / r_20)
    c2= 1.0 * np.log10(r_90 / r_50)

    """

    if r_20 > 0 and r_80 > 0:

        c1 = 0.5 * np.log10(r_80 / r_20)

    else:

        c1 = 0

    if r_50 > 0 and r_90 > 0:

        c2 = 0.5 * np.log10(r_90 / r_50)

    else:

        c2 = 0
    print('Conc',c1)
    return c1, c2



def find_peak_centroid(image,centroid,delta=15):
    r,c=image.shape
    x_c = np.int(centroid[0])
    y_c = np.int(centroid[1])

    c_min = max(0, x_c - delta)
    c_max = min(c, x_c + delta)



    r_min = max(0, y_c - delta)
    r_max = min(r, y_c + delta)

    #print (x_c,y_c,c_min, c_max, r_min, r_max)

    _image=image[:, c_min:c_max][r_min:r_max]

    _y_c,_x_c=np.unravel_index(np.argmax( _image), _image.shape)
    #print(_y_c,_x_c)
    #import pylab as plt
    #fig, (ax1, ax2) = plt.subplots(1, 2)
    #ax1.imshow(_image, interpolation='nearest')
    #ax2.imshow(_image, interpolation='nearest')

    #plt.show()


    _y_c=_y_c+(r_min)
    _x_c=_x_c+(c_min)

    return _x_c,_y_c



def get_cluster_morphology_morphometric_features(cluster,
                                                 cluster_image,
                                                 bkg_image,
                                                 bkg_level,
                                                 binary_image,
                                                 image_centroid,
                                                 surf_bright_profile,
                                                 group_name='morph',
                                                 name_flag=None,
                                                 blurring_scale=0.1,
                                                 clump_cover_factor=0.05,
                                                 petrosian_factor=1.0,
                                                 delta_asymm=3.0,
                                                 delta_clump=5,
                                                 mesh_asymm=1,
                                                 plot=False):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    cluster : :class:`asterism.core.clustering.source_cluster.SourceCluster2Dim'
        cluster object from whicht to extract the features

    cluster_image : 2d array
        image used to evaluate the asymmetry and the clumpiness


    image_centroid : tuppolar_coords.theta,theta_pdf,theta_pdf_xle (x,y)
        tuple storing the source centroid coordinate for the cluster_image,

    surf_bright_profile : :class: `asterism.core.photometry.surface_brightness_profile.ImageBrigthnessEllipiticalRadialProfile`
        instance object for the surface brightness profile

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """

    feat_names = ['Gini', 'M20', 'conc_1', 'conc_2', 'clumpiness', 'r_Petrosian_to_r_max', 'asymm', 'asymm_s', 'r_20_to_r_max',
                  'r_80_to_r_max', 'r_Petrosian']
    feat_dt = [np.float32] * len(feat_names)

    m_feat = MorphometricFeatures.feature_factory(feat_names,
                                                  feat_dt,
                                                  group_name,
                                                  name_flag=name_flag,
                                                  null_value=-99.,
                                                  failed_value=np.nan)

    #import  pylab as plt
    # fig,(ax1,ax2,ax3)=plt.subplots(1,3)
    # ax1.imshow(cluster_image,interpolation='nearest')
    # ax2.imshow(cluster_blurred_image,interpolation='nearest')
     #ax3.imshow(cluster_blurred_image-cluster_image,interpolation='nearest')
    #print(type(cluster_image))
    #plt.imshow(cluster_image,interpolation='nearest')
    #plt.show()
    #plt.show()

    if photutils.__version__ < "0.3":
        print ('photutils<=0.3',photutils.__version__)
    else:
        print ('photutils>=0.3',photutils.__version__)


    if cluster is None and cluster_image is None and surf_bright_profile is None:
        m_feat.fill_null()
    else:
        try:
            feature_values = []

            r_20, r_80, r_50, r_90, r_Petrosian = eval_r_Petrosian(surf_bright_profile.I, surf_bright_profile.r,
                                                                   surf_bright_profile.s, image_array=cluster_image,
                                                                   plot=plot,apertures=surf_bright_profile.apertures,
                                                                   petrosian_factor=petrosian_factor)
            base_aperture=surf_bright_profile.apertures[-1]
            #positions = image_centroid

            a=base_aperture.a
            b=base_aperture.b
            theta=base_aperture.theta
            a_p=r_Petrosian
            b_p=(b/a)*r_Petrosian
            petrosian_aperture = EllipticalAperture(image_centroid, a=a_p, b=b_p, theta=theta)




            _image=cluster_image-bkg_level
            _bkg_image=bkg_image-bkg_level

            print('a_p,b_p',a_p,b_p)
            print('image x_c,y_c', image_centroid[0], image_centroid[0])

            if photutils.__version__ <= "0.2.2":
                 flux_array, x_coords, y_coords, roi_x_c, roi_y_c, msk = get_roi_old_v_ph(_image, petrosian_aperture)
            else:
                 flux_array, x_coords, y_coords, roi_x_c, roi_y_c, msk = get_roi(_image, petrosian_aperture)

            print ('flux_array.mean(),x_coords.mean(),y_coords.mean(),roi_x_c,roi_y_c',flux_array.mean(),
                   x_coords.mean(),
                   y_coords.mean(),
                   roi_x_c,
                   roi_y_c)
            #roi_cutout, flux_array, x_coords, y_coords,roi_x_c,roi_y_c,msk=get_roi(_image,petrosian_aperture)

            #roi_cutout_bkg, _flux_array, _x_coords, _y_coords,_msk = get_roi(bkg_image, petrosian_aperture)

            #roi_x_c= image_centroid[0]
            #roi_y_c= image_centroid[1]

            #print (msk.shape,roi_cutout.shape)

            #print ('roi_x_c,roi_y_c',roi_x_c,roi_y_c,x_coords.min(),x_coords.max())

            #Gini
            feature_values.append(eval_Gini(flux_array))

            #M20
            feature_values.append(
                eval_M20(flux_array, x_coords, y_coords, roi_x_c, roi_y_c))

            #C1,C2
            feature_values.extend(eval_Concentration(r_20, r_80, r_50, r_90))

            #Clumpiness
            #print ('cetroid',image_centroid)
            r_blurring=r_Petrosian
            _x_c,_y_c=image_centroid= find_peak_centroid(_image,image_centroid,)
            print('peak cetroid', _x_c,_y_c)

            a_i = r_blurring * clump_cover_factor
            #b_i = (b / a) * a_i
            #circular
            inner_aperture = EllipticalAperture([_x_c,_y_c], a=a_i, b=a_i, theta=theta)

            #masks = inner_aperture.to_mask(method='center')
            #mask = masks[0]

            if photutils.__version__ < "0.3":
                #print ('photutils<=0.3',photutils.__version__)
                _image_hole = get_hole_old(_image,inner_aperture)
            else:
                #print('photutils>=0.3', photutils.__version__)
                _image_hole = get_hole(_image, inner_aperture)



            _image_c=np.copy(_image)
            _bkg_image_c=_=np.copy(_bkg_image)

            _image_c[_image_hole==1]=0
            _bkg_image_c[_image_hole==1]=0

            R=r_blurring * blurring_scale
            #print('R',np.int(r_blurring * blurring_scale))

            _image_blurred = GaussianFilter(R).apply(_image )
            _bkg_image_blurred = GaussianFilter(R).apply(_bkg_image)

            _image_blurred[_image_hole==1]=0
            _bkg_image_blurred[_image_hole==1]=0

            #import pylab as plt
            #fig, (ax1,ax2) =plt.subplots(1,2)
            #ax1.imshow(_image_c,interpolation='nearest')
            #ax2.imshow(_image, interpolation='nearest')
            #plt.show()
            feature_values.append(eval_clumpiness(_image_c, _image_blurred,_bkg_image_c,_bkg_image_blurred,petrosian_aperture,inner_aperture))

            #r_Petrosian_to_r_max
            feature_values.append(r_Petrosian / cluster.r_max)

            # asymm
            i_src,f0 = eval_asymmetry(_image,
                                   image_centroid[0],
                                   image_centroid[1],
                                   petrosian_aperture,
                                   delta_x=delta_asymm,
                                   delta_y=delta_asymm,
                                   mesh=mesh_asymm,
                                   bkg_image=None)

            #f0 = aperture_photometry(np.fabs(_image), petrosian_aperture)['aperture_sum'][0]
            #f0 returned by eval_asymmetry is bkg subtracted if bkg_image is passed
            i_bkg,f0 = eval_asymmetry(_bkg_image,
                                   image_centroid[0],
                                   image_centroid[1],
                                   petrosian_aperture,
                                   f0=f0,
                                   delta_x=delta_asymm,
                                   delta_y=delta_asymm,
                                   mesh=mesh_asymm,
                                   bkg_image=None)

            #import pylab as plt
            #plt.imshow(bkg_image,interpolation='nearest')
            #plt.show()
            print('asymm index', i_src-i_bkg)
            feature_values.append( i_src-i_bkg)
            #feature_values.append(np.fabs(0.))
            i_shape,f0= eval_asymmetry(binary_image,
                                   image_centroid[0],
                                   image_centroid[1],
                                   petrosian_aperture,
                                   delta_x=delta_asymm,
                                   delta_y=delta_asymm,
                                   mesh=mesh_asymm,
                                   bkg_image=None,
                                   shape=True)

            feature_values.append(i_shape)
            print('shape asymm index', i_shape)

            feature_values.append(r_20 / cluster.r_max)

            feature_values.append(r_80 / cluster.r_max)

            feature_values.append(r_Petrosian)

            print ('n features',len(feature_values))
            m_feat.fill(feature_values)
        except Exception as e:
            m_feat.fill_failed()
            print(traceback.format_exc())
            raise Warning("!Warning cluster_geometric_morphometric_features failed ", e.message)

    return m_feat
