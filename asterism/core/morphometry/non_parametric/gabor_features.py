"""
Module: gabor_features
========================



Overview
--------

This modules provides the implementation of the functions for the extraction of




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::

Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.morphometry.features import MorphometricFeatures

#import cv2
from skimage import  measure
import numpy as np

__author__ = 'andrea tramacere'





def get_gabor_morphometric_features(gw_1,gw_2,group_name='gabor',name_flag=None):
    """

    """
    feat_names=['w1_%d'%id for id in range(len(gw_1))]
    feat_names.extend(['w2_%d'%id for id in range(len(gw_2))])
    feat_dt=[np.float32]*len(feat_names)



    m_feat= MorphometricFeatures.feature_factory(feat_names,
                                                feat_dt,
                                                group_name,
                                                name_flag=name_flag,
                                                null_value=-99.,
                                                failed_value=np.nan)


    if gw_1 is None and gw_2 is None:
        m_feat.fill_null()
    else:
        try:
            values=np.append(gw_1,gw_2)
            m_feat.fill(values)
        except Exception as e :
            m_feat.fill_failed()
            raise Warning("!Warning get_gabor_morphometric_features failed",e)




    return m_feat







