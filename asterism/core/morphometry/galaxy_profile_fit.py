"""
Module: galaxy_profile_fit
========================



Overview
--------

This modules provides the implementation of the functions for the extraction of
morphometric features, related to galxy profile fit




Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::
   do_morphometric_features_extraction_process_func
   FeaturesArray
   DoMorphometricFeaturesExtraction


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str



from asterism.core.morphometry.features import MorphometricFeatures
from asterism.core.stats.histogram.radial_distributions import radial_profile
import  numpy as np
from scipy import optimize
#from leastsqbound import leastsqbound

__author__ = 'andrea tramacere'


def plot_fit(r,radial_flux_density,best_fit_par,w=None,cluster_image=None,image_centroid=None):
    import pylab as plt

    fig,(ax1,ax2)=plt.subplots(1,2)


    ax1.imshow(cluster_image,interpolation='nearest')
    #for r_c in r/9.0:
    #    circle=plt.Circle((image_centroid[0],image_centroid[1]), radius=r_c,edgecolor='y',fc='None')
    #    ax1.add_patch(circle)

    if w is None:
        ax2.plot(np.log10(r ),np.log10(radial_flux_density ),'o')
    else:
        ax2.errorbar(np.log10(r ), np.log10(radial_flux_density ), yerr=0.434*(w/radial_flux_density), fmt='o')
    r_plot=np.linspace(r.min(),r.max(),50)
    y=sersic_func(best_fit_par,r_plot)
    ax2.plot(np.log10(r_plot),np.log10(y))
    plt.show()




def b_sersic(n):

    return 1.999*n-0.327


def sersic_func(p,r):

        return p[0]*np.exp(-b_sersic(p[2])*((r/p[1])**(1.0/p[2])-1) )

def sersic_err_func(p,r_hist,hist,w=None):


    if w is None:
        w=np.ones(r_hist.size)

    return  (hist-sersic_func(p,r_hist))/w

def fit_sersic(radial_flux_density,r,w=None):



    n=1
    #alpha=5
    #r_b=cluster.sig_x/5

    r_e=r.mean()/4.0
    I_0=radial_flux_density.max()

    #gamma=0.1
    #b=0.1

    p=[I_0,r_e,n]
    print ("|fit sersic start pars I_0,r_e,n]",p)
    print ("|r_hist size",r.size)



    #res = optimize.leastsq(sersic_err_func, p, args=(r,radial_flux_density,w))
    #out= res[0]
    #bounds = [(0, None), (0,None),(0.0,20.0)]
    #res = leastsqbound(sersic_err_func, p, args=(r,radial_flux_density,w),bounds=bounds)
    #out= res[0]
    res=optimize.least_squares(sersic_err_func, p, args=(r,radial_flux_density,w),bounds=([0.0,0.0,0.],[np.inf,np.inf,20.0]))
    out=res.x
    print ("|fit sersic best fit  pars I_0,r_e,n]",out)
    chi2=sersic_err_func(out,r,radial_flux_density)
    chi2_red=(chi2**2).sum()/(r.size - len(out))
    print ("|chi2_red",chi2_red)
    print ("|")





    return out,chi2_red


def fit_sersic_profile(surf_bright_profile,cluster_image=None,image_centroid=None):
    best_fit_par=[-99,-99,-99]
    chi2_red=-1.0

    #try:
    msk=surf_bright_profile.I!=0
    r=surf_bright_profile.r[msk]
    radial_flux_density=surf_bright_profile.I[msk]

    if r.size>3:
        w=None
        best_fit_par,chi2_red=fit_sersic(radial_flux_density,r,w=w)

        plot_fit(r,radial_flux_density,best_fit_par,w=w,cluster_image=cluster_image,image_centroid=image_centroid)


        #else:
        #    pass



    #except:
    #    print('failed')
    #    pass



    return  best_fit_par[-1],chi2_red




def get_galaxy_profile_fit_morphometric_features(surf_bright_profile,group_name='model',name_flag=None,cluster_image=None,image_centroid=None):
    """
    Function to extract galaxy morphometric features for :.... . The try/except statement allows to return
    np.nan features

    Parameters
    ----------
    surf_bright_profile : :class: `asterism.core.photometry.surface_brightness_profile.ImageBrigthnessEllipiticalRadialProfile`
        instance object for the surface brightness profile

    group_name : str
        the group name for the features (see :class:`.MorphometricFeatures`)


    name_flag : str
        the flag name for the features (see :class:`.MorphometricFeatures`)

    Returns
    -------
    MorphometricFeatures :   :class:`.MorphometricFeatures`
        The class storing the features with their names and dtypes



    """

    feat_names=['sersic_index','sersic_index_chi2_red']
    feat_dt=[np.float32,np.float32]
    feature_values=[np.nan for name in feat_names]

    #try:
    feature_values=fit_sersic_profile(surf_bright_profile,cluster_image=cluster_image,image_centroid=image_centroid)
    #except:
    #raise Warning("!Warning galaxy_profile_fit_morphometric_features failed")


    return  MorphometricFeatures(group_name=group_name,names=feat_names,values=feature_values,name_flag=name_flag,dtypes=feat_dt)



