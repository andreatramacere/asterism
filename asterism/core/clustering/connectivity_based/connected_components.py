"""
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.clustering.source_cluster import SourceCluster2Dim
from  ..cluster import  BaseCluster,PostionMatrix

import  numpy as np
from skimage import measure

__author__ = 'andrea tramacere'




class CONNECTED_cluster(BaseCluster):

    def __init__(self,ID,position_array,metric,weight_array=None,bkg_th=None):
        """

        Args:
            ID:
            members_ID_list:
            position_array:
            dist_method:
            K:
            eps:
            weight_array:

        Returns:

        """

        super(CONNECTED_cluster,self).__init__(ID,position_array,weight_array=weight_array,bkg_th=bkg_th)







    def SourceCluster_factory(self,x_off_set=0,y_off_set=0,ID=None,parent_ID=None,weight_array=None):
        if weight_array is None:
            weight_array=self.weight_array

        if ID is None:
            ID=self.ID
        if parent_ID is  None:
            parent_ID=self.parent_ID

        cls=SourceCluster2Dim.build_from_cartesian_position_array(ID,position_array=self.position_array+[x_off_set,y_off_set],weight_array=weight_array,parent_ID=parent_ID)

        cls.point_type = None
        cls.bkg_th=self.bkg_th

        return cls



class ConnectedComponents(object):

        def __init__(self,min_size,metric='euclidean'):
            if min_size is None:
                self.min_size=1
            else:
                self.min_size=min_size

        def run(self,image,th,masked=None,seg_map=None,seg_map_bkg=None,min_pix_size=None,skip_cluster_building=False,verbose=False):

            self.clusters_list = []

            if min_pix_size is None:
                min_pix_size=self.min_size

            if seg_map is None:

                if masked is not None:
                    self.masked=masked
                    self.n_evts=np.logical_not(self.masked).sum()
                else:
                    self.masked=None
                    self.n_evts=image.size



                (rows,cols)=image.shape

                if self.masked is None:
                    blobs = image >= th
                else:
                    blobs=np.logical_and(image >= th,~self.masked)

                import time
                #t=time.time()
                self.blobs_labels,labels_num = measure.label(blobs.astype(np.int0), background=0,connectivity=1,return_num=True)
                #print('done measure label', time.time() - t,labels_num)
            else:
                self.blobs_labels=seg_map.astype(np.int)


            if np.size(self.blobs_labels)>0:
                #t = time.time()
                bkg_id=self.blobs_labels.min()

                self.bkg_blobs_val=bkg_id

                pm=PostionMatrix(labels=self.blobs_labels,image_array=image,bkg_blobs_val=self.bkg_blobs_val)
                #print('done PM list', time.time() - t)

                ID_array,ID_counts=np.unique(self.blobs_labels[self.blobs_labels>bkg_id],return_counts=True)
                ID_array=ID_array[ID_counts>min_pix_size]
                #print (ID_array,ID_counts)
                #t = time.time()
                if skip_cluster_building==False:
                    for ID,CL_ID in enumerate(ID_array):
                        #print ('-> Cl ID',CL_ID)
                        self.build_cluster_list(CL_ID,th,min_pix_size,pm,ID=None)


                del(pm)
                #print('done cl list', time.time() - t)
            else:
                self.bkg_blobs_val = None
            #for cl in self.clusters_list:
            #    print('->cl.ID',cl.ID)
            if verbose==True:
                print ("|found clusters",len(self.clusters_list))


        def build_cluster_list(self,cluster_ID,bkg_level,min_size,pm,ID=None):


            selceted= pm.ID==cluster_ID
            x=pm.x[selceted]
            y=pm.y[selceted]
            weight_array=pm.flux[selceted]

            if ID is None:
               ID=cluster_ID

            #print ('sem map ID',cluster_ID,cluster_ID)
            if x.size>min_size:
                try:
                    self.clusters_list.append( CONNECTED_cluster(ID,
                                  np.column_stack((x,y)),
                                  'euclidean',
                                  weight_array=weight_array,
                                  bkg_th=bkg_level))

                except:
                    #TODO raise Exception
                    print ('Raise Exception')



