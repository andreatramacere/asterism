"""
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import numpy as np
from asterism.core.geometry import  distance
from .dbscan import  DBSCAN_cluster
from .dbscan import BaseDBSCAN
from sklearn.cluster import DBSCAN

import  time
__author__ = 'andrea tramacere'




class DBSCANUnbinned(BaseDBSCAN):

    def __init__(self,eps,K,buffer_size=None,metric='euclidean',check_core_is_already_cluster_min_size=100,K_pix=False):
        """

        Args:
            position_array (ndarray): 2dim array
            dist_method (func(ID)): a method to evaluate  distances between position_array[ID] and all of the position_array elements
            K (float):
            eps (float):
            weight_array (array): 1d array with same size of position_array rows
            buffer_size (esp): the scanning distance is limited to a box centered in current ID and size  of buffer_size

        Returns ():

        """

        super(DBSCANUnbinned,self).__init__(eps,K,buffer_size=buffer_size,metric=metric,K_pix=K_pix)

        self.check_core_is_already_cluster_min_size=check_core_is_already_cluster_min_size







    def run(self,position_array,weight_array=None,store_position_array_IDs=True,verbose=False):

        self.position_array=position_array
        self.weight_array=weight_array

        (self.n_evts,self.n_coords)=position_array.shape
        self.IDs=np.arange(self.n_evts)

        self.visited=np.zeros(self.n_evts,dtype=np.bool)
        self.point_type=np.ones(self.n_evts,dtype=np.int)*-1
        self.position_array_cl_ID = np.ones(self.n_evts, dtype=np.int) * -1

        self.clusters_list=[]


        if verbose==True:
            start=time.time()
            print("| K",self.K)
            print("| n_evts",self.n_evts)

        # cluster_ID=0
        #
        # for ID in range(self.n_evts):
        #
        #     if self.visited[ID]==False:
        #         cluster_members_list=self.scan(ID)
        #         if len(cluster_members_list)>0 and len(cluster_members_list)>self.min_cluster_size:
        #             if store_position_array_IDs==True:
        #                 IDs=cluster_members_list
        #             else:
        #                 IDs=None
        #
        #
        #             self.clusters_list.append(self.build_cluster(cluster_ID,cluster_members_list,IDs=IDs))
        #             cluster_ID+=1

        #print("core_samples_mask size", np.sum(self.point_type == -1), np.sum(self.point_type == 0),
        #      np.sum(self.point_type == 1))
        if self.metric=='haversine' or self.metric=='geod_approx':
            self.metric='haversine'
            db = DBSCAN(eps=self.eps, min_samples=self.K,metric=self.metric,algorithm='auto').fit(position_array[:,[1,0]],sample_weight=self.weight_array)
        else:
            db = DBSCAN(eps=self.eps, min_samples=self.K, metric=self.metric, algorithm='auto').fit(position_array,sample_weight=self.weight_array)
        labels = db.labels_
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

        #print (np.unique(db.labels_))

        self.point_type[db.labels_==-1]=-1
        self.point_type[db.labels_>-1]=0
        self.point_type[core_samples_mask]=1

        #print("core_samples_mask size", np.sum(self.point_type == -1), np.sum(self.point_type == 0),
        #      np.sum(self.point_type == 1))

        cluster_ID=0
        for ID in range(n_clusters_):
            cluster_members_list= np.argwhere(labels==ID).flatten().tolist()

            #print ("ID,",ID,self.metric)
            #print (cluster_members_list)
            if store_position_array_IDs==True:
                IDs=np.array(cluster_members_list)
            else:
                IDs=None
            self.clusters_list.append(self.build_cluster(cluster_ID,cluster_members_list,IDs=IDs))

            self.position_array_cl_ID[cluster_members_list] = cluster_ID
            cluster_ID+=1


        if verbose==True:
            print ("| computational time",time.time()-start)
            print ("| clusters found",len(self.clusters_list))




    def build_cluster(self,cluster_ID,cluster_members_list,IDs=None):
        if self.weight_array is not None:
            w=self.weight_array[cluster_members_list]
        else:
            w=None

        return DBSCAN_cluster(cluster_ID,
                              self.position_array[cluster_members_list],
                              self.metric,
                              self.K,
                              self.eps,
                              self.point_type[cluster_members_list],
                              weight_array=w,
                              bkg_th=self.K,
                              position_array_IDs=IDs)


    def scan(self,ID):

        cluster_members_list=[]

        is_core,neigh_id_array=self.set_point_type(ID)




        if is_core==True:
            core_seed=neigh_id_array
            cluster_members_list=self.grow_core(core_seed)
            #if self.check_core_is_already_cluster(core_seed) and neigh_id_array.size>self.check_core_is_already_cluster_min_size:
            #    #print("core is already cluster",)
            #    cluster_members_list=core_seed
            #else:
            #    cluster_members_list=self.grow_core(core_seed)



        return cluster_members_list


    def check_core_is_already_cluster(self,core_list):

        x_c=self.position_array[core_list][:,0].mean()
        y_c=self.position_array[core_list][:,1].mean()

        D= distance.dist_eval(self.position_array,x_c=x_c,y_c=y_c,metric=self.metric)
        D_core= distance.dist_eval(self.position_array[core_list],x_c=x_c,y_c=y_c,metric=self.metric)
        in_core_1= np.where(D<=self.eps)[0]
        in_core_2= np.where(D<=self.eps*D_core.max())[0]
        all_in=False
        if len(in_core_1)==len(in_core_2):
            all_in =True
            for ID in core_list:
                    self.visited[ID]=True
                    self.point_type[ID]=1


        print ("check_core_is_already_cluster",self.eps,D.max(),x_c,y_c,all_in,len(in_core_1),len(in_core_2),len(core_list))
        return  all_in




    def grow_core(self,core_seed):

        core_seed.sort()
        cluster_members_list=core_seed
        start_size=cluster_members_list.size
        end_size=0
        id_start=0

        while start_size!=end_size :

            start_size=cluster_members_list.size
            for ID in cluster_members_list[:]:


                if self.visited[ID] == False:

                    density_reach=self.point_type[ID]==2
                    is_core,neigh_id_array=self.set_point_type(ID,density_reach=density_reach)

                    cluster_members_list= self.expand_list(cluster_members_list,neigh_id_array)




            end_size=cluster_members_list.size


        return cluster_members_list




    def expand_list(self,list_to_expand,candidate_list):
        return np.unique(np.append(list_to_expand,candidate_list))

        #self.calls_expand+=1
        #for ID in candidate_list:
        #    self.calls_expand+=1
        #    if ID not in list_to_expand:
        #        self.calls_expand+=1
        #        list_to_expand.append(ID)



    def get_neigh(self,ID):
        if self.buffer_size is None:
            neigh_id_array=np.where(self.dist_eval(index=ID)<=self.eps)[0]
            #print ("ID",ID,neigh_id_array)
            #print ("ID",ID,neigh_id_array)
        else:
            selected=self.get_buffer_mask(ID)
            masked_IDs=self.IDs[selected]
            neigh_id_array=np.where(self.dist_eval(index=ID,selected=selected)<=self.eps)[0]
            neigh_id_array=masked_IDs[neigh_id_array]


        return neigh_id_array



    def set_point_type(self,ID,density_reach=False):
        """
        set point type (core,border,noise), according to local density and K
        Args:
            ID:
            neigh_id_array:
            density_reach:

        Returns:

        """
        is_core=False
        neigh_id_array=self.get_neigh(ID)

        self.visited[ID]=True

        if self.weight_array is None:
            density=np.float64(neigh_id_array.size)
            K=self.K
        else:
            density=self.weight_array[neigh_id_array].sum()

            if self.K_pix is True:
                density=density/np.float64(neigh_id_array.size)
                K=self.K
            else:
                K=self.K*np.float64(neigh_id_array.size)



        if density>=K:
            is_core=True
            self.point_type[ID]=2
        elif density<self.K and density_reach==True:
            self.point_type[ID]=1
        else:
           self.point_type[ID]=0


        return is_core,neigh_id_array





    def dist_eval(self,index,selected=None):
        d=distance.dist_eval(self.position_array,index=index,metric=self.metric,selected=selected)

        return d

    def get_buffer_mask(self,ID):
        x_c=self.position_array[:,0][ID]
        y_c=self.position_array[:,1][ID]


        y_min=max((y_c-self.buffer_size),self.position_array[:,1].min())
        y_max=min((y_c+self.buffer_size),self.position_array[:,1].max())

        x_min=max((x_c-self.buffer_size),self.position_array[:,0].min())
        x_max=min((x_c+self.buffer_size),self.position_array[:,0].max())

        sel1=self.position_array[:,1]>=y_min
        sel2=self.position_array[:,1]<=y_max
        sel3=self.position_array[:,0]>=x_min
        sel4=self.position_array[:,0]<=x_max

        return sel1*sel2*sel3*sel4

