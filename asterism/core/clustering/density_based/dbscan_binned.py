"""
Overview
--------

This modules provides the implementation of `.DBSCANBinned` class, to perform
a DBSCAN clustering on a binned dataset, as is in the case of an image.




Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.core.clustering.density_based.dbscan_binned.DBSCANBinned




Summary
---------
.. autosummary::
   DBSCANBinned


Module API
-----------
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

import  numpy as np
import  time
#import  itertools

from .dbscan import DBSCAN_cluster
from .dbscan import BaseDBSCAN
from ..cluster import PostionMatrix
from ...geometry import  distance


__author__ = 'andrea tramacere'




class DBSCANBinned(BaseDBSCAN):

        def __init__(self,eps,K,metric='euclidean',K_pix=False):
            super(DBSCANBinned,self).__init__(eps,K,metric=metric,K_pix=K_pix)

            # for binned eps it is integer
            self.eps=np.int32(self.eps)








        def run(self,image,masked=None,verbose=False):

            self.clusters_list = []


            if masked is not None:
                self.masked=masked
                self.n_evts=np.logical_not(self.masked).sum()
            else:
                self.masked=None
                self.n_evts=image.size





            (rows,cols)=image.shape

            #all points set to visited=False
            self.visited=np.zeros(image.shape,dtype=np.bool)

            #all masked points set to visited=True
            self.visited[self.masked]=True

            #self.added=np.zeros(image.shape,dtype=np.bool)
            #self.added[self.masked]=True

            self.cluster_map=np.ones(image.shape,dtype=np.int16)*-1
            self.point_type_map=np.ones(image.shape,dtype=np.int16)

            cl_ID=0

            if verbose==True:
                start=time.time()
                print("| dbscan matrix",self.K)
                print("| n_evts",self.n_evts)
            for r in range(rows):
                for c in range(cols):
                    if self.visited[r,c]==False and self.masked[r,c]==False:
                        is_core,r1,r2,c1,c2=self.get_neigh_and_set_point_type(image, r, c, rows, cols,masked=self.masked)

                        if is_core==True:
                            self.start_new_cluster(cl_ID,image,r,c,r1,r2,c1,c2,rows,cols)
                            cl_ID+=1


            pm=PostionMatrix(labels=self.cluster_map,image_array=image,point_type=self.point_type_map)

            if verbose==True:
                print ("| computational time",time.time()-start)
                print ("| clusters found",cl_ID)

            for cl_ID in range(cl_ID):




                selected=pm.ID==cl_ID



                if selected.sum()>self.min_cluster_size:
                    x=pm.x[selected]
                    y=pm.y[selected]
                    weight_array=pm.flux[selected]
                    point_type=pm.point_type[selected]
                    position_array=np.column_stack((x,y))


                    self.clusters_list.append(self.build_cluster(cl_ID,position_array,weight_array,point_type,self.K))

            if verbose==True:
                print ("| clusters built",len(self.clusters_list))

            del(pm)

        def build_cluster(self,cluster_ID,position_array,weight_array,point_type,bkg_level):
            return DBSCAN_cluster(cluster_ID,
                                  position_array,
                                  'euclidean',
                                  self.K,
                                  self.eps,
                                  point_type,
                                  weight_array=weight_array,
                                  bkg_th=self.K)





        def add_to_cluster_as_core(self,cl_ID,seed_list,r1,r2,c1,c2):
            m1=~self.masked[r1:r2+1,c1:c2+1]==True
            m2=self.cluster_map[r1:r2+1,c1:c2+1]==-1
            r,c=np.where(m2*m1)
            r=r+r1
            c=c+c1
            seed_list.extend(np.column_stack((r,c)).tolist())
            self.cluster_map[r,c]=cl_ID


        def start_new_cluster(self,cl_ID,image,r,c,r1,r2,c1,c2,rows,cols):
            self.cluster_map[r,c]=cl_ID
            seed_list=[]
            self.add_to_cluster_as_core(cl_ID,seed_list,r1,r2,c1,c2)


            for rc in seed_list:
                r=rc[0]
                c=rc[1]
                if self.visited[r,c]==False:

                    is_core,r1,r2,c1,c2=self.get_neigh_and_set_point_type(image, r, c, rows, cols,masked=self.masked)

                    if is_core == True:
                        self.add_to_cluster_as_core(cl_ID,seed_list,r1,r2,c1,c2)



                    else:
                        self.cluster_map[r,c]=cl_ID



        def dist_eval(self,index,selected=None):
            d=distance.dist_eval(self.position_array,index=index,metric=self.metric,selected=selected)
            return  d


        def get_neigh_and_set_point_type(self, image, r, c, rows, cols, masked=None,density_reach=False):
            r1=max(0,r-self.eps)
            r2=min(rows-1,r+self.eps)
            c1=max(0,c-self.eps)
            c2=min(cols-1,c+self.eps)

            if masked is not None:
                block=image[r1:r2+1,c1:c2+1][~masked[r1:r2+1,c1:c2+1]]
            else:
                block=image[r1:r2+1,c1:c2+1]

            self.visited[r,c]=True

            f=block.sum()
            s=(r2-r1)*(c2-c1)

            if block.size==0:

                return False,r1,r2,c1,c2

            #print("->",f,s)
            if self.K_pix == True:
                s=np.float64(s)
                f=np.float64(f)/s
                K=self.K
            else:
                K=self.K*s

            if f<K:
                is_core=False
                self.point_type_map[r,c]=2
            else:
                #print("->",f,K,s)
                is_core=True

                if density_reach == True:
                    self.point_type_map[r,c]=1
                else:
                    self.point_type_map[r,c]=0

            return is_core,r1,r2,c1,c2