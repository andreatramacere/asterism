"""
"""
from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


from asterism.core.geometry import  distance
try:
    from ...cython_tools.denclue import _update_position,_eval_kernel,_dist_eval,_get_probab
except:
    pass

import multiprocessing as mp
from sklearn.utils._joblib import Parallel
from sklearn.utils._joblib import delayed

from ...tools.tools import check_choice_name
from math import  sqrt
import  numpy as np

_allowed_kernels_=['mex','epanen','biweight','triweight','tricube','gauss','gauss_cos','gauss_cos2','uniform','logistic']
_allowed_R_max_methods_=['kern_rel_th','fixed','auto']

_allowed_max_calls_methods_={}
_allowed_max_calls_methods_['log10']=lambda frac,size:  max(1,int(np.log10(size)))
_allowed_max_calls_methods_['sqrt']=lambda frac,size:  max(1,int(np.sqrt(size)))
_allowed_max_calls_methods_['frac']=lambda frac,size: max(1,np.int(frac*size))
_allowed_max_calls_methods_['abs']=lambda  frac,size: max(1,np.int(frac))
_quad_kernel_list_=['mex','epanen','biweight','triweight','gauss']

__author__ = 'andrea tramacere'



def set_max_calls(max_calls_methods,size,frac):
    if max_calls_methods not in _allowed_max_calls_methods_.keys():
        raise RuntimeError('max_calls_methods',max_calls_methods,'not in allowed',_allowed_max_calls_methods_)
    else:
        _max_calls= _allowed_max_calls_methods_[max_calls_methods](frac, size)

    return _max_calls

def donwsample_cluster_target_attractors(cluster,step):

    bkg=cluster.flux.min()
    image,off_x,off_y,masked=cluster.get_cluster_image_array(bkg=bkg)

    row_sel=np.arange(0,image.shape[0],step,dtype=np.int16)
    col_sel=np.arange(0,image.shape[1],step,dtype=np.int16)

    mgrid=np.zeros(image.shape, dtype=np.bool)
    for row in row_sel:
        mgrid[row][col_sel]=True

    y_t,x_t=np.where(np.logical_not(masked)*mgrid)


    target_coordinates_w_array=image[np.int16(y_t),np.int16(x_t)]


    x_t=x_t+off_x
    y_t=y_t+off_y
    target_coordinates=np.column_stack((x_t.astype(np.int16),y_t.astype(np.int16)))

    return target_coordinates,target_coordinates_w_array

def mask_unchanged_attractors(th,attractors_position_array,position_array):
    dx=np.fabs(attractors_position_array[:,0]-position_array[:,0])
    dy=np.fabs(attractors_position_array[:,1]-position_array[:,1])

    d=dx*dx+dy*dy

    selected=d>(th)
    return selected

def set_kernel_R_max(kernel_type,h,K_table_size=1000,th_rel=1E-5):

    th_ID=[]

    n_h=5.0
    while len(th_ID)<1:

        k_x,k_y=eval_kernel_table(h,n_h*h,K_table_size,kern_type=kernel_type)

        th_ID=np.argwhere(np.fabs(k_y/k_y.max())<th_rel)

        n_h+=1

    #print('->',th_ID,len(th_ID))
    for ID in range(th_ID.size-1,1,-1):
        delta=th_ID[ID]-th_ID[ID-1]
        if delta>1:
            break

    ID_final=th_ID[ID][0]
    d_th=k_x[ID_final]

    R_max=d_th*h
    #print('->',np.shape(d_th),np.shape(k_x),np.shape(th_ID))
    #print("|R_max=%f for kernel th=%f"%(R_max,th_rel))

    #import pylab as plt
    #plt.plot(k_x,k_y)
    #plt.plot([d_th,d_th],[0,k_y.max()])
    #plt.show()
    return R_max


def eval_kernel_table(h,R_max,K_table_size,kern_type=u'gauss'):
    u=np.linspace(0,R_max,K_table_size)/h
    k_table_x=u
    k_table_y= _eval_kernel(kern_type.encode('utf-8'),np.ones(K_table_size,dtype=np.float64),np.float64(u),h,K_table_size)



    return k_table_x,k_table_y




def convolved_map(position_array,
                    kernel_type,
                    h,
                    weight_array=None,
                    binning=None,
                    verbose=False,
                    R_max=None,
                    metric='euclidean'):

    border=1
    x_p=position_array[:,0]
    y_p=position_array[:,1]


    off_set_x=x_p.min()-border
    off_set_y=y_p.min()-border

    if binning is None:
        rows=int(y_p.max()-y_p.min())+1+2*border
        cols=int(x_p.max()-x_p.min())+1+2*border

    else:
        rows = int(y_p.max() - y_p.min())/binning + 1 + 2 * border
        cols = int(x_p.max() - x_p.min())/binning + 1 + 2 * border
        print (rows,cols)


    image=np.zeros((rows,cols),dtype=np.float32)

    n_rows=position_array.shape[0]

    if weight_array is None:
        weight_array=np.ones(x_p.size,dtype=np.float64)

    f_min=0
    for row_id in range(n_rows):

        d,w,x,y = get_local_data(position_array,weight_array,x_p[row_id],y_p[row_id],R_max,metric=metric)
        u = d / h
        # if kernel_type in _quad_kernel_list_:
        #    u *= 1.0 / h
        f = _eval_kernel(kernel_type.encode('utf-8'),w.astype(np.float64),u.astype(np.float64),h,d.size)
        #print(y_p[row_id]-off_set_y,x_p[row_id]-off_set_x)
        image[np.int(y_p[row_id]-off_set_y),np.int(x_p[row_id]-off_set_x)]=f.sum()
        f_min=min(f.sum(),f_min)
    image[image==0]=f_min
    return image,off_set_x,off_set_y


def multiprocess(processes, samples, x, widths):
    pool = mp.Pool(processes=processes)
    results = [pool.apply_async(find_attractor_position, args=(samples, x, w)) for w in widths]
    results = [p.get() for p in results]
    results.sort() # to sort the results by input window width
    return results


def find_attractors(position_array,
                    kernel_type,
                    h,
                    eps,
                    max_calls,
                    metric='euclidean',
                    k_table_x=None,
                    k_table_y=None,
                    k_table_size=0,
                    weight_array=None,
                    verbose=False,
                    R_max=None,
                    precomputed=False,
                    target_position_array=None,
                    target_weight_array=None,
                    multiprocessing_th=500):


    if verbose==True:
        print("| find attractors")

    if verbose==True:
        print("|position array size ",position_array.shape[0])


    check_choice_name('kernel',kernel_type,_allowed_kernels_)
    attractor_position_matrix=np.zeros((position_array.shape))
    signif_array=np.zeros((position_array.shape[0]))
    n_rows=position_array.shape[0]



    if weight_array is None:
        weight_array=np.ones(n_rows)
    else:
        weight_array=weight_array+weight_array.min()





    if target_position_array is not None and target_weight_array is not None:
        xy_position_array=target_position_array
        w_array=target_weight_array
    else:
        xy_position_array=position_array
        w_array=weight_array

    if verbose==True:
       print("|target position array size ",xy_position_array.shape[0])

    if k_table_size is None:
        k_table_size=-1

    tot_calls=0



    #for row_id in range(n_rows):

    #x_0=position_array[row_id][0]
    #y_0=position_array[row_id][1]


    kw=dict(metric = metric,
    k_table_x = k_table_x,
    k_table_y = k_table_y,
    k_table_size = k_table_size,
    f_is_precomputed = precomputed)
    run_parallel=False

    if multiprocessing_th is not None:
        if position_array.shape[0]>multiprocessing_th:
            run_parallel=True

    if verbose == True:
        print('- run_parallel',run_parallel, 'size',position_array.shape[0])

    # if run_parallel is True:
    #     pool = mp.Pool(processes= mp.cpu_count())
    #     results = [pool.apply_async(find_attractor_position,
    #                                 (xy_position_array.astype(np.float64),
    #                                 w_array.astype(np.float64),
    #                                 position_array[row_id][0],
    #                                 position_array[row_id][1],
    #                                 row_id,
    #                                 kernel_type,
    #                                 h,
    #                                 eps,
    #                                 R_max,
    #                                 max_calls),
    #                                 kw,
    #                                 ) for row_id in range(n_rows)]
    #
    #     #row_id,x_attr, y_attr, calls, f =
    #
    #     results = [p.get() for p in results]
    #     results.sort()
    #     for row_id in range(n_rows):
    #         attractor_position_matrix[row_id]=[results[row_id][1],results[row_id][2]]
    #         signif_array[row_id]=results[row_id][4]
    #         tot_calls+=results[row_id][3]
    #     pool.close()

    if run_parallel is True:
        results = Parallel(n_jobs=mp.cpu_count())(
            delayed(find_attractor_position)
            (xy_position_array.astype(np.float64),
              w_array.astype(np.float64),
              position_array[row_id][0],
              position_array[row_id][1],
              row_id,
              kernel_type,
              h,
              eps,
              R_max,
              max_calls,
              kw) for row_id in range(n_rows))

        results.sort()
        for row_id in range(n_rows):
            attractor_position_matrix[row_id] = [results[row_id][1], results[row_id][2]]
            signif_array[row_id] = results[row_id][4]
            tot_calls += results[row_id][3]

    else:
        for row_id in range(n_rows):
            results = find_attractor_position(xy_position_array.astype(np.float64),
                                              w_array.astype(np.float64),
                                              position_array[row_id][0],
                                              position_array[row_id][1],
                                              row_id,
                                              kernel_type,
                                              h,
                                              eps,
                                              R_max,
                                              max_calls,
                                              kw)

            attractor_position_matrix[row_id] = [results[1], results[2]]
            signif_array[row_id] = results[4]
            tot_calls += results[3]

    if verbose == True:
        print("n call mean", np.float(tot_calls) / weight_array.size)


    return attractor_position_matrix,signif_array


def get_local_data(position_array,weight_array,x_c,y_c,R_max,metric='euclidean',kernel_type='gauss'):

    #if kernel_type in _quad_kernel_list_:
    #    dist=_dist_eval2(position_array,x_c,y_c,position_array[:,1].size)
    #else:
    dist = _dist_eval(position_array, x_c, y_c, position_array[:, 1].size)

    if R_max is not None:
        #if kernel_type in _quad_kernel_list_:
        selected= dist<=R_max
        #else:
        #    selected=dist<=R_max*R_max

        w=weight_array[selected]

        d=dist[selected]

        x=position_array[:,0][selected]
        y=position_array[:,1][selected]


        return d,w,x,y
    else:
        return dist,weight_array,position_array[:,0],position_array[:,1]



def normalize_probability(proba_array,h,kernel_type,w):
    if kernel_type == 'gauss':
        proba_array = proba_array / (2.0 * np.pi * h * h)
    elif kernel_type == 'uniform':
        proba_array= proba_array
    else:

        raise RuntimeError('Probability implemented only for gauss kernel')

    return proba_array/w.size


def cluster_significance(position_array,
                         weight_array,
                         h,
                         x_t,
                         y_t,
                         kernel_type):

    d, w, x, y = get_local_data(position_array, weight_array, x_t, y_t, R_max=None, kernel_type=kernel_type)
    u = d / h

    proba_cluster = _get_probab(w.astype(np.float64),
                                u,
                                x.astype(np.float64),
                                y.astype(np.float64),
                                w.size,
                                h,
                                kernel_type.encode('utf-8'))

    proba_cluster = normalize_probability(proba_cluster, h, kernel_type,w.astype(np.float64))
    return proba_cluster

def find_attractor_position(position_array,
                           weight_array,
                           x_t,
                           y_t,
                           row_id,
                           kernel_type,
                           h,
                           eps,
                           R_max,
                           max_calls,
                           metric='euclidean',
                           k_table_x=None,
                           k_table_y=None,
                           k_table_size=0,
                           verbose=False,
                           f_is_precomputed=False):




        #if kernel_type!='gauss':
        #    #print('h',h)
        #    h=h*20.0





        dist_eps=eps
        calls=0
        f=0

        while dist_eps>=eps and calls<max_calls:
            d,w,x,y=get_local_data(position_array,weight_array,x_t,y_t,R_max,metric=metric,kernel_type=kernel_type)
            u = d/h
            #if kernel_type in _quad_kernel_list_:
            #    u *= 1.0/h

            if w.size>1:

                x_t_1, y_t_1, f = _update_position(w,
                                                   u,
                                                   x,
                                                   y,
                                                   w.size,
                                                   h,
                                                   kernel_type.encode('utf-8'),
                                                   k_table_x,
                                                   k_table_y,
                                                   k_table_size,
                                                   f_is_precomputed)

            else:
                x_t_1 = x_t
                y_t_1 = y_t

            dist_eps = sqrt((x_t_1-x_t)*(x_t_1-x_t)+(y_t_1-y_t)*(y_t_1-y_t))/h

            x_t = x_t_1
            y_t = y_t_1
            calls=calls+1

        if calls >= max_calls:
            if verbose is True:
                print("!Warning in find attractor ID=%d, calls=%d, eps=%f, dist_eps=%f, x_t_1=%f, y_t_1=%f"%(row_id,calls,eps,dist_eps,x_t_1,y_t_1))

        return row_id,x_t_1,y_t_1,calls,f



