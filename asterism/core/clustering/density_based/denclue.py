"""
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str


from .denclue_tools import *
from asterism.core.clustering.source_cluster import SourceCluster2Dim
import  numpy as np
from  ..cluster import  BaseCluster
from ...tools.tools import check_choice_name
from .dbscan_unbinned import DBSCANUnbinned
from ....core.image_processing.features import PeakLocalMaxima

import time

_allowed_kernels_=['mex','epanen','biweight','triweight','tricube','gauss','gauss_cos','gauss_cos2','uniform','logistic']
_allowed_R_max_methods_=['kern_rel_th','fixed','auto']

__author__ = 'andrea tramacere'

class DENCLUE_cluster(BaseCluster):

    def __init__(self,ID,position_array,kernel,h,eps,weight_array=None,point_type=None,position_array_IDs=None):
        """

        Args:
            ID:
            members_ID_list:
            position_array:
            dist_method:
            K:
            eps:
            weight_array:

        Returns:

        """
        super(DENCLUE_cluster,self).__init__(ID,position_array,weight_array=weight_array,bkg_th=None,position_array_IDs=position_array_IDs)

        self.kernel=kernel
        self.eps=eps
        self.h=h
        self.point_type = point_type




    def SourceCluster_factory(self,x_off_set=0,y_off_set=0,ID=None,parent_ID=None,weight_array=None):
        if weight_array is None:
            weight_array=self.weight_array

        if ID is None:
            ID=self.ID
        if parent_ID is  None:
            parent_ID=self.parent_ID

        cls=SourceCluster2Dim.build_from_cartesian_position_array(ID,position_array=self.position_array+[x_off_set,y_off_set],weight_array=weight_array,parent_ID=parent_ID)


        cls.kernel=self.kernel
        cls.eps=self.eps
        cls.h=self.h
        cls.position_array=self.position_array
        cls.position_array_IDs = self.position_array_IDs
        cls.weight_array=self.weight_array
        cls.ID=self.ID
        cls.point_type=self.point_type
        return cls




class DENCLUE(object):


    def __init__(self,eps,
                 h,
                 kernel,
                 k_table_size=None,
                 R_max_method='auto',
                 R_max_th_rel=None,
                 R_max=None,
                 dbscan_eps='auto',
                 max_calls_method='frac',
                 max_calls_par=0.1,
                 dbscan_K='auto',
                 metric='euclidean'):

        check_choice_name('kernel',kernel,_allowed_kernels_)
        check_choice_name('R_max_method',R_max_method,_allowed_R_max_methods_)

        self._allowed_kernels=_allowed_kernels_
        self._allowed_R_max_methods =_allowed_R_max_methods_
        self._R_max_th_rel=R_max_th_rel
        self.kernel=kernel
        self.R_max_method=R_max_method


        self.h=h

        self.eps=eps

        self.max_calls_method=max_calls_method

        self.max_calls_par=max_calls_par

        self.kernel=kernel

        self.clusters_list=[]

        self.set_R_max(h,R_max_method,R_max_th_rel=R_max_th_rel,R_max=R_max)

        self.set_kernel_table(k_table_size)

        self.metric=metric

        if dbscan_eps is None:
            self.dbscan_eps=self.h
        else:
            self.dbscan_eps=dbscan_eps

        if dbscan_K is None:
            self.dbscan_K=2
        else:
            self.dbscan_K=dbscan_K


    def run(self,
            position_array,
            position_array_IDs=None,
            point_type=None,
            weight_array=None,
            mask_unchanged=None,
            get_only_attractors=False,
            R_max=None,
            make_convolved_image=False,
            digitize_attractors=False,
            precomputed=False,
            target_coordinates=None,
            target_coordinates_w_array=None,
            eps=None,
            max_calls_method=None,
            max_calls_par=None,
            h=None,
            dbscan_eps='auto',
            dbscan_K=None,
            attractors_th=None,
            verbose=False,
            multiprocessing_th=500):


        if h is None:
            h=self.h


        if eps is None:
            eps=self.eps



        if R_max is None:
            if verbose == True:
                print("|setting R_max with method",self.R_max_method)
            self.set_R_max(h,self.R_max_method,R_max_th_rel=self._R_max_th_rel,R_max=self.R_max)
            R_max=self.R_max
            if verbose == True:
                print("|R_max set to",R_max)


        if max_calls_par is None:
            max_calls_par=self.max_calls_par

        if  max_calls_method is None:
           max_calls_method=self.max_calls_method

        max_calls = set_max_calls(max_calls_method, position_array.size, max_calls_par)
        if verbose == True:
            print("|set max calls with method",max_calls_method)

            print("|max calls set to", max_calls)

        if dbscan_eps is None:
            dbscan_eps=self.dbscan_eps

        if dbscan_eps=='auto':
            dbscan_eps=max(1.0,h*0.5)

        if dbscan_K is None:
            dbscan_K=self.dbscan_K

        if dbscan_K=='auto':
            dbscan_K=4

        if verbose==True:
            print("|running denclue with")
            print("|kernel ",self.kernel)
            print("|h ",h)
            print("|eps",eps)
            print("|max_calls",max_calls)
            print("|R_max set to",R_max)
            print("|DBSCAN eps",dbscan_eps)
            print("|DBSCAN K", dbscan_K)

        #if downsampling_th_size is not None:
        #    make_convolved_image= True

        if make_convolved_image is True:
            self.convolved_image,self.convolved_image_offset_x,self.convolved_image_offset_y=convolved_map(position_array,
                                                                                                           self.kernel,
                                                                                                           h,
                                                                                                           weight_array=weight_array,
                                                                                                           R_max=R_max)

        else:
            self.convolved_image=None

        t_start=time.time()
        #print('shape',target_coordinates.shape,target_coordinates_w_array.shape,attractors_th.shape)
        if attractors_th is not None:
            target_position_array = target_coordinates[target_coordinates_w_array > attractors_th]
            target_weight_array = target_coordinates_w_array[target_coordinates_w_array > attractors_th]
            if verbose==True:
                print('|targets size post filtering for attractors_th',target_position_array.shape[0])
        else:
            target_position_array = target_coordinates
            target_weight_array = target_coordinates_w_array

        attractors_position_array, signif_array = find_attractors(position_array,
                                                             self.kernel,
                                                             h,
                                                             eps,
                                                             max_calls,
                                                             weight_array=weight_array,
                                                             R_max=R_max,
                                                             k_table_x=self.k_table_x,
                                                             k_table_y=self.k_table_y,
                                                             k_table_size=self.k_table_size,
                                                             precomputed=precomputed,
                                                             metric=self.metric,
                                                             target_position_array=target_position_array,
                                                             target_weight_array=target_weight_array,
                                                             verbose=verbose,
                                                             multiprocessing_th=multiprocessing_th)

        s_ID=np.ones(position_array.shape[0],dtype=np.bool)
        if mask_unchanged is not None:
            if mask_unchanged<0:
                th_changed=eps*eps
            else:
                th_changed=mask_unchanged*mask_unchanged

            if verbose==True:
                print('|size pre masking unchanged',attractors_position_array.shape[0])

            selected=mask_unchanged_attractors(th_changed,attractors_position_array,position_array)
            s_ID*=selected
            attractors_position_array=attractors_position_array[selected]
            weight_array=weight_array[selected]
            signif_array=signif_array[selected]
            if verbose==True:
                print('|size post masking unchanged',attractors_position_array.shape[0])




        #if attractors_th is not None and attractors_position_array.size >0:
        #    if verbose==True:
        #        print('|size pre th filtering',attractors_position_array.shape[0])

        #    selected=signif_array>attractors_th
        #    s_ID *= selected
        #    attractors_position_array=attractors_position_array[selected]
        #    weight_array=weight_array[selected]
        #    if verbose==True:
        #        print('|size post th filtering',attractors_position_array.shape[0])

        if digitize_attractors ==True  and attractors_position_array.size >0:
            attractors_position_array=np.int16(attractors_position_array)


        if attractors_position_array.size>0:
            self.attractors_position_array=attractors_position_array
        else:
            self.attractors_position_array=None


        if get_only_attractors== False  and attractors_position_array.size>0:
            dbscan_attractors=DBSCANUnbinned(dbscan_eps, dbscan_K, buffer_size=None)
            dbscan_attractors.run(attractors_position_array,store_position_array_IDs=True,verbose=verbose)

            self.attractors_clusters_list=dbscan_attractors.clusters_list

            for ID,cl in enumerate(dbscan_attractors.clusters_list):
                self.clusters_list.append(self.build_cluster(ID,
                                                             position_array[s_ID],
                                                             cl.position_array_IDs,
                                                             weight_array=weight_array,
                                                             point_type=point_type,
                                                             position_array_IDs=position_array_IDs))
        else:
            self.attractors_clusters_list=[]



        if verbose == True:
            print("|comp. time",time.time()-t_start)


    def  set_kernel_table(self,k_table_size):
        if k_table_size is not None :
            #print("| use kernel table with size",k_table_size)
            self.k_table_size=k_table_size
            self.k_table_x,self.k_table_y=eval_kernel_table(self.h,self.R_max,k_table_size,kern_type=self.kernel)
        else:
            self.k_table_x=None
            self.k_table_y=None
            self.k_table_size=None

    def set_R_max(self,h,method,R_max_th_rel=None,R_max=None):
        check_choice_name('R_max_method',method,self._allowed_R_max_methods)

        if method=='auto':
            self.R_max=set_kernel_R_max(self.kernel,h)
        if method=='kern_rel_th':
            if R_max_th_rel is None:
                raise ValueError("th_rel can not be None")
            self.R_max=set_kernel_R_max(self.kernel,h,th_rel=R_max_th_rel)
        if method=='fixed':
            if R_max is None:
                raise ValueError("R_max can not be None")
            self.R_max=R_max

        self.R_max_method=method
        #print ("| set R_max to %f, method=%s"%(self.R_max,self.R_max_method))



    def build_cluster(self,cluster_ID,position_array,cluster_members_list,weight_array=None,point_type=None,position_array_IDs=None):
        if weight_array is not None:
            w=weight_array[cluster_members_list]
        else:
            w=None
        if position_array_IDs is not None:
            position_array_IDs = position_array_IDs[cluster_members_list]

        if point_type is not None:
            _point_type=point_type[cluster_members_list]
        else:
            _point_type=None

        return DENCLUE_cluster(cluster_ID,
                              position_array[cluster_members_list],
                              self.kernel,
                              self.h,
                              self.eps,
                              point_type=_point_type,
                              weight_array=w,
                              position_array_IDs=position_array_IDs)




