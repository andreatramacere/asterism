"""
Overview
--------

This modules provides the implementation of the :class:`.BaseCluster`, an high level class to handle  clusters objects.


Summary
---------
.. autosummary::
    BaseCluster



Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from asterism.core.clustering.cluster_tools import *
__author__ = 'andrea tramacere'

class PostionMatrix(object):
    def __init__(self,labels,image_array,point_type=None,bkg_blobs_val=-1):

        #print(labels.shape)
        msk=labels>bkg_blobs_val
        self.y,self.x=np.where(msk)
        self.ID=labels[msk].flatten()
        self.flux=image_array[msk].flatten().astype(np.float64)
        if point_type is not None:
            self.point_type=point_type[msk]



class BaseCluster(object):


    def __init__(self,ID,position_array,weight_array=None,bkg_th=None,position_array_IDs=None,metric='euclidean',parent_ID=None):

        self.position_array=position_array
        self.weight_array=weight_array
        self.bkg_th=bkg_th
        self.ID=ID
        self.parent_ID=parent_ID
        self.position_array_IDs=position_array_IDs
        self._metric=metric
        self.set_cluster_bary()
        self.size=position_array.shape[0]


    @property
    def coords(self):
        return self.position_array

    @property
    def n_points(self):
        return self.position_array.shape[0]


    def check_is_in_cluster(self,x_test,y_test):
        return check_is_in_cluster(x_test,y_test,x=self.position_array[:,0],y=self.position_array[:,1])



    def get_closest_cluster(self,clusters_list):
        return get_closest_cluster(self,clusters_list)





    def set_cluster_bary(self):

        self.x_c,\
        self.y_c,\
        self.sig_x,\
        self.sig_y,\
        self.r_cluster,\
        self.semi_major_angle,\
        self.pos_err,\
        self.r_max ,\
        self.r_mean=eval_cluster_bary(self.position_array,self._metric,weight=self.weight_array)




    def get_cluster_image_array(self,border=1,bkg=None):
        return get_cluster_image_array(self.position_array,border,pixel_values=self.flux,bkg=bkg)

    def add_contour_points(self):
        if self.position_array[:,0].size>1:
            self.contour_shape= cluster_border(self.position_array[:,0], self.position_array[:,1])
        else:
            self.contour_shape=None

