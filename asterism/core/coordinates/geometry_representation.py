"""

"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

__author__ = 'andrea tramacere'

from ..observables.quantity import Quantity,Angle
from ..geometry.transformations_2d   import *
from ..geometry.distance import dist_eval

import numpy as np



class GeomRepresentation(object):
    def __init__(foo):
        pass



    @property
    def name(self):
        if self._name is None:
            return self._representation_type
        else:
            return self._name

    @property
    def shape(self):
        return self.coords.shape

    @property
    def representation_type(self):
        return self._representation_type

    @property
    def allowed_representation_types(self):
        return self._allowed_representation_types


class Representation2D(GeomRepresentation):

    def __init__(self,
                 representation_type,
                 units=None,
                 twodim_matrix_coord=None,
                 first_coord=None,
                 second_coord=None,
                 allowed_representation_types=['cartesian', 'polar'],
                 copy=True,
                 name=None,
                 metric='euclidean',
                 rotation_center_x=0,
                 rotation_center_y=0,
                 coord_dtype=np.float64):

        self._metric=metric
        self._name=name
        self._N_coords=2
        self._coord_dtype=coord_dtype
        self._allowed_representation_types=allowed_representation_types
        self._check_representation_type(representation_type)
        self._representation_type=representation_type
        self._units=self._set_default_units()
        self._set_coords(xy=twodim_matrix_coord,x=first_coord,y=second_coord,copy=copy)
        self._set_rotation_center(rotation_center_x=rotation_center_x,rotation_center_y=rotation_center_y)
        self._coords_objs_list=['_first_coord','_second_coord']


    def __repr__(self,hide_values=True):
        repr_str='Representation2D, name=%s\n'%(self.name)
        repr_str+='representation type=%s\n'%self._representation_type
        repr_str+='allowed representations=%s\n'%self._allowed_representation_types
        repr_str+='shape='+str(self.shape)+'\n'
        for coord in self._coords_objs_list:
            if hasattr(self,coord):
                repr_str+='%s %s'%(coord,getattr(self,coord))
        if hide_values==False:
            repr_str+='first_coord=%s, \nsecond_coord=%s\n'%(self._first_coord,self._second_coord)

            #print (self._rotation_center_x,self._rotation_center_y)
        repr_str+='rotation_center_x=%f, rotation_center_y=%f\n'%(self._rotation_center_x,self._rotation_center_y)
        return repr_str


    @property
    def rotation_center(self):
        return self._rotation_center_x,self._rotation_center_y

    @rotation_center.setter
    def rotation_center(self,rotation_center):
         self._set_rotation_center(rotation_center_x=rotation_center[0],rotation_center_y=rotation_center[1])

    @property
    def x(self):
        if self.representation_type=='cartesian':
            return self._first_coord.values
        else:
            return None

    @property
    def y(self):
        if self.representation_type=='cartesian':
            return self._second_coord.values
        else:
            return None

    @property
    def r(self):
        if self.representation_type=='polar':
            return self._first_coord.values
        else:
            return None

    @property
    def theta(self):
        if self.representation_type=='polar':
            return self._second_coord.values
        else:
            return None

    @property
    def theta_units(self):
        if self.representation_type=='polar':
            return self._second_coord._units
        else:
            return None

    @property
    def coords(self,index=None):
        if index is not None:
            return self._coords[index]

        else:

            return self._coords


    def to_cartesian(self,x_c=None,y_c=None,name='cartesian'):
        if x_c is None:
            x_c=self._rotation_center_x
        if y_c is None:
            y_c=self._rotation_center_y

        if self.representation_type=='polar':
            x,y=polar_to_cartesian_2d(self.r,self.theta,x_c,y_c)
            return Representation2D('cartesian',first_coord=x,second_coord=y,name=name)


    def to_polar(self,x_c=None,y_c=None,name='polar'):
        if x_c is None:
            x_c=self._rotation_center_x
        if y_c is None:
            y_c=self._rotation_center_y
        if self.representation_type=='cartesian':
            r,theta=cartesian_to_polar_2d(self.x,self.y,x_c,y_c)
            return Representation2D('polar',first_coord=r,second_coord=theta,rotation_center_x=x_c,rotation_center_y=y_c,name=name)


    def _set_default_units(self):
        if self.representation_type=='polar':
            return[None,'rad']
        else:
            return [None,None]


    def geom_transformation(self,transformation,inplace=False):
        #check_ImageTransformation(transformation)

        if inplace==True:
            self._coords=transformation.apply(self)
        else:
            return transformation.apply(self)


    def dist_eval(self,index=None,x_c=None,y_c=None,metric=None,selected=None):
        if metric is None:
            metric=self._metric

        d=dist_eval(self.coords,index=index,x_c=x_c,y_c=y_c,metric=metric,selected=selected)

        return d


    def _expand(self,new_twodim_matrix_coord):
        new_twodim_matrix_coord=np.row_stack((self._coords,new_twodim_matrix_coord))

        self._set_coords(new_twodim_matrix_coord)
        self._set_rotation_center()

    def _set_coords(self,xy,x=None,y=None,copy=True):
        if xy is not None or (x is not None and y is not None):

            if xy is not None:
                if copy==True:
                    self._coords=np.copy(xy).astype(self._coord_dtype)
                else:
                    self._coords=xy
            else:
                self._coords=np.column_stack((x,y)).astype(self._coord_dtype)

        else:
            raise ValueError(""" either you provide 'xy' or you provide *both* 'x' and 'y' """)




        if self.representation_type=='polar':
            self._first_coord=Quantity(self._coords[:,0],self._units[0],copy=False,name='r')
            self._second_coord=Angle(self._coords[:,1],self._units[1],copy=False,name='theta')
        else:
            #print(self._coords,self._units)
            self._first_coord=Quantity(self._coords[:,0],self._units[0],copy=False,name='x')
            self._second_coord=Quantity(self._coords[:,1],self._units[1],copy=False,name='y')


    def _set_rotation_center(self,rotation_center_x=None,rotation_center_y=None):
        if rotation_center_x is not None:
            self._rotation_center_x=rotation_center_x

        if rotation_center_y is not None:
            self._rotation_center_y=rotation_center_y

    def _check_representation_type(self,repr):
        if repr not in self._allowed_representation_types:
            raise  ValueError(""" representation type %s not in allowed %s""",repr,self._allowed_representation_types)










