"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, open, super, range,
                      zip, round, input, pow, object, map, zip)

try:
    # Python 2
    from __builtin__ import str as builtin_str
except ImportError:
    # Python 3
    from builtins import str as builtin_str

from ..geometry.transformations_2d import *
from ..observables.quantity import Angle
from ..coordinates.geometry_representation import Representation2D
__author__ = 'andrea tramacere'



class GeomTransform(object):
    pass


class Rotation(GeomTransform):

    def __init__(self,angle=90.0,units='deg',rotation_center=None):



        self.angle=Angle(value=angle,units=units)


        if rotation_center is not None:
            if len(rotation_center)==2:
                self._rotation_center=rotation_center
            else:
                raise RuntimeError('you must provide as rotation center an iterable object with length=2')

        else:
            self._rotation_center=None


    @property
    def center(self):
        return self._rotation_center

    @center.setter
    def center(self,rotation_center_x,rotation_center_y):
        self._rotation_center_x= rotation_center_x
        self._rotation_center_y=rotation_center_y

    @property
    def center_x(self):
        if self._rotation_center is not None:
            return self._rotation_center[0]

    @property
    def center_y(self):
        if self._rotation_center is not None:
            return self._rotation_center[1]

    def apply(self,GeomRepr2d):

        if isinstance(GeomRepr2d,Representation2D):
            pass
        else:
            raise TypeError("GeomRepr2d is not instance of ",Representation2D)


        if Representation2D.representation_type=='polar':
            Polar2Drepr=Representation2D
        else:
            if self._rotation_center is None:
                Polar2Drepr=Representation2D.to_polar()
            else:
                Polar2Drepr=Representation2D.to_polar(x_c=self.center_x,y_c=self.center_y)

        #units of the angle to rotate
        angle_units=Polar2Drepr.theta_units


        rotated_theta=rotate(Polar2Drepr._second_coord,getattr(self.angle,angle_units))

        if Representation2D.representation_type=='cartesian':
            return Polar2Drepr.to_cartesian().coords
        else:
            Polar2Drepr._second_coord=rotated_theta
            return Polar2Drepr.coords


class TranslationCartesian(GeomTransform):


    def __init__(self,dx,dy):

         self._dx=dx
         self._dy=dy

    def apply(self,GeomRepr2d):

        if isinstance(GeomRepr2d,Representation2D):
            pass
        else:
            raise TypeError("GeomRepr2d is not instance of ",Representation2D)


        if Representation2D.representation_type=='cartesian':
            return GeomRepr2d.coords+[self.dx,self.dy]
        else:
            raise TypeError("Translation for polar not decided yet")


class Affine(GeomTransform):

    def __init__(self,new_aspect_ratio):

        self.new_aspect_ratio=new_aspect_ratio


    def apply(self,GeomRepr2d):
        if isinstance(GeomRepr2d,Representation2D):
            pass
        else:
            raise TypeError("GeomRepr2d is not instance of ",Representation2D)

        if Representation2D.representation_type=='cartesian':
            Cartesian2Drepr=Representation2D
        else:
            Cartesian2Drepr=Representation2D.to_cartesian()

        Cartesian2Drepr._set_coords(affine_transformation(Cartesian2Drepr.coords))

        if Representation2D.representation_type=='polar':
            return Cartesian2Drepr.to_polar().coords
        else:
            return Cartesian2Drepr.coords