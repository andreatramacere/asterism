"""
Module: name
========================



Overview
--------

This modules provides an

The :class:`.class_name` is used to...



Classes relations
---------------------------------------
.. figure::  classes_rel_pkg.sub_pkg.modname.png
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::   classes_pkg.sub_pkg.modname.png




Summary
---------
.. autosummary::
   some func
   some classes


Module API
-----------
"""

from __future__ import division, absolute_import, print_function
import  numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt

__author__ = 'andrea tramacere'



def plot_features_imp(feat_imp_array,max_features=None,plot=False):
    fig, (ax) = plt.subplots()
    feature_importance = 100.0 * (feat_imp_array['feature_score'] / feat_imp_array['feature_score'].max())
    sorted_idx = np.argsort(feature_importance)
    if max_features is not None:
        sorted_idx = sorted_idx[-max_features:]

    pos = np.arange(sorted_idx.shape[0]) + .5
    ax.barh(pos, feature_importance[sorted_idx], align='center')
    ax.set_yticks(pos)
    ax.set_yticklabels(np.array(feat_imp_array['feat_name'][sorted_idx]))

    ax.set_xlabel('Relative Importance')
    ax.set_title('Top %d feature Importance'%max_features)
    plt.tight_layout()
    if plot==True:
        plt.show()

    return fig



def plot_confusion_matrix(cm,labels, title='Confusion matrix', cmap=plt.cm.Blues,plot=False):
    fig, (ax1) = plt.subplots()
    imx=ax1.imshow(cm, interpolation='nearest', cmap=cmap)
    ax1.set_title(title)
    fig.colorbar(imx)
    tick_marks = np.arange(len(labels))
    ax1.set_xticks(tick_marks)
    ax1.set_yticks(tick_marks)
    ax1.set_xticklabels(labels, rotation=45)
    ax1.set_yticklabels(labels)
    plt.tight_layout()
    ax1.set_ylabel('True label')
    ax1.set_xlabel('Predicted label')
    if plot==True :
        plt.show()
    return fig

def plot_learning_curve(cv_score_array_learning,scorer_name,plot=False):

    train_sizes=cv_score_array_learning['cv_train_sizes']
    train_scores_mean=cv_score_array_learning['cv_train_score_mean']
    train_scores_std=cv_score_array_learning['cv_train_score_std']
    test_scores_mean=cv_score_array_learning['cv_test_score_mean']
    test_scores_std=cv_score_array_learning['cv_test_score_std']


    matplotlib.rcParams['xtick.labelsize'] = 18
    matplotlib.rcParams['ytick.labelsize'] = 18

    fig, (ax1) = plt.subplots()
    fig.suptitle('learning curve',fontsize=18)
    ax1.grid()


    #train_sizes=np.float32(train_sizes)/full_size
    ax1.fill_between(train_sizes,train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="g")
    ax1.plot(train_sizes , train_scores_mean, 'o-', color="g",
             label=" Training score")

    ax1.fill_between(train_sizes ,  test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="b")
    ax1.plot(train_sizes, test_scores_mean, 'o-', color="b",
             label=" CV score")


    ax1.legend(loc="best",fontsize=18)
    ax1.set_xlabel('train sizes ',fontsize=18)
    ax1.set_ylabel('%s'%scorer_name,fontsize=18)
    ymin, ymax = ax1.get_ylim()
    delta=ymax-ymin
    thick_space=int(delta/10)
    if thick_space>0.:
        ax1.yaxis.set_major_locator(ticker.MultipleLocator(thick_space))

    #ax1.set_ylim([-0.1,ymax])
    plt.tight_layout()
    if plot==True:
        plt.show()

    return fig