"""
Module: name
========================



Overview
--------

This modules provides an

The :class:`.class_name` is used to...



Classes relations
---------------------------------------
.. figure::  classes_rel_pkg.sub_pkg.modname.png
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::   classes_pkg.sub_pkg.modname.png




Summary
---------
.. autosummary::
   some func
   some classes


Module API
-----------
"""

from __future__ import division, absolute_import, print_function

from ..data_sets.features import FeaturesSetBase

from sklearn.metrics import classification_report,accuracy_score,precision_score
from sklearn.ensemble import RandomForestClassifier as RFC
from sklearn.ensemble import GradientBoostingClassifier as GB
from sklearn.ensemble import AdaBoostClassifier as AB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import numpy as np
from sklearn.feature_selection import RFECV,RFE
from sklearn.feature_selection import SelectKBest
from sklearn import svm
from sklearn.feature_selection import f_classif
from sklearn.grid_search import GridSearchCV
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.learning_curve import  learning_curve
from sklearn import svm

__author__ = 'andrea tramacere'


class Classifier(object):

    def __init__(self,cls,par_grid_dict=None):

        self.cls=cls
        self.par_grid_dict=par_grid_dict

    def grid_search(self,train,cv,par_grid_dict=None):
        if par_grid_dict is None:
            par_grid_dict=self.par_grid_dict


        g_search = GridSearchCV(self.cls, param_grid=par_grid_dict, cv=cv)
        g_search.fit(train.features, train.target)
        self.cls=g_search.best_estimator_
        print("The best parameters are %s with a score of %0.2f"% (g_search.best_params_, g_search.best_score_))
        return g_search.best_params_, g_search.best_score_


    def rec_feat_rem(self,train,estimator,filter=True,step=1,test=None):
        rfe = RFE(estimator=self.cls, step=1)
        rfe.fit(train.features, train.target)
        print("Optimal number of features : %d" % rfe.n_features_)

        if filter==True:
            train.select_columns_by_ID(rfe.support_)
            if test is not None:
                test.select_columns_by_ID(rfe.support_)
        return rfe.support_

    def rec_feat_rem_cv(self,train,cv,filter=True,test=None,scoring='accuracy'):
        #score_av=0
        rfecv = RFECV(estimator=self.cls, step=1, cv=cv,scoring=scoring)
        rfecv.fit(train.features, train.target)

        print("Optimal number of features : %d" % rfecv.n_features_)
        #for train_index, test_index in cv:
        #    self.cls.fit(train.features[train_index][:,rfecv.support_] ,train.target[train_index])
        #    pred=self.cls.predict(train.features[test_index][:,rfecv.support_])
        #    t_rep=classification_report(train.target[test_index],pred)
        #    acc= accuracy_score(train.target[test_index],pred)
        #    print ("rec feat rem report",t_rep)
        #    score_av+=acc

        #print ("with average cv acc",score_av/np.float(len(cv)))

        if filter==True:
            train.select_columns_by_ID(rfecv.support_)
            if test is not None:
                test.select_columns_by_ID(rfecv.support_)


        return rfecv.support_,rfecv.n_features_,rfecv.grid_scores_


    def dim_reduction(self,train,valid=None,n_components=100):
        lda = LDA(n_components=n_components)
        lda_X = lda.fit_transform(train.features,train.target)
        #train=FeaturesSetBase(lda_X,target_array=train.target)

        lda_X = lda.transform(valid.features)
        valid._features_array=lda_X


    def std_features(self,train,test=None):
        stdsc = StandardScaler()
        train._features_array = stdsc.fit_transform(train.features)
        if test is not None:
            test._features_array = stdsc.transform(test.features)




    def eval_best_K_features(self,features,k=None):
        if k is None:
            k=features._features_N_cols
        sel=SelectKBest(f_classif, k=k)
        sel.fit(features.features, features.target)
        #print (np.argsort(sel.scores_))
        #for ID in np.argsort(sel.scores_):
        #    print (features.features_names[ID],sel.scores_[ID])

        #if sort==True:
        #    features.sort_columns(np.argsort(sel.scores_)[::-1])
        #    self.cls.fit(features.features,features.target)
        #    sel.scores_=sel.scores_[np.argsort(sel.scores_)[::-1]]

        dt=[('feat_name','|S128'),('feature_score',np.float32)]
        arr=np.zeros(len(features.features_names),dtype=dt)

        for ID,name in enumerate(features.features_names):
            arr['feat_name'][ID]=name
            arr['feature_score'][ID]=sel.scores_[ID]


        return arr







    def do_learning_curve(self,train,cv):
        train_sizes,train_scores,test_scores=learning_curve(estimator=self.cls,X=train.features,y=train.target,train_sizes=np.linspace(0.1,1.0,10),cv=cv)
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(test_scores, axis=1)
        test_scores_std = np.std(test_scores, axis=1)
        dt=[('cv_train_sizes',np.int32),('cv_train_score_mean',np.float32),('cv_train_score_std',np.float32),('cv_test_score_mean',np.float32),('cv_test_score_std',np.float32)]
        cv_score_array=np.zeros((train_sizes.size),dtype=dt)
        cv_score_array['cv_train_sizes']=train_sizes
        cv_score_array['cv_train_score_mean']=train_scores_mean
        cv_score_array['cv_train_score_std']=train_scores_std
        cv_score_array['cv_test_score_mean']=test_scores_mean
        cv_score_array['cv_test_score_std']=test_scores_std

        return cv_score_array

    @classmethod
    def LDA_cls(cls,):
        tol_range = np.logspace(-10, 0, 100)
        param_grid = {'tol':tol_range}

        n_components_range=np.arange(1,100,10)
        param_grid['n_components']=n_components_range

        return cls(LDA(),par_grid_dict=param_grid)

    @classmethod
    def RFC_cls(cls,):
        n_estimators_range=np.arange(10,260,30)
        param_grid = {'n_estimators':n_estimators_range}

        return cls(RFC(),par_grid_dict=param_grid)

    @classmethod
    def GB_cls(cls,):
        n_estimators_range=np.arange(50,500,50)
        param_grid = {'n_estimators':n_estimators_range}

        #learning_rate_range=np.array([0.1])
        #param_grid['learning_rate']=learning_rate_range

        return cls(GB(learning_rate=0.1),par_grid_dict=param_grid)

    @classmethod
    def AB_cls(cls,):
        n_estimators_range=np.arange(10,260,30)
        param_grid = {'n_estimators':n_estimators_range}

        return cls(AB(),par_grid_dict=param_grid)

    @classmethod
    def SVC_linear_cls(cls,):

        C_range= np.logspace(-2, 10, 13)
        param_grid = {'C':C_range}
        svc = svm.LinearSVC()
        #rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)
        #poly_svc = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)
        #lin_svc = svm.LinearSVC(C=C).fit(X, y)

        return cls(svc,par_grid_dict=param_grid)

    @classmethod
    def SVC_rbf_cls(cls,):

        C_range= np.logspace(-2, 10, 13)
        param_grid = {'C':C_range}
        gamma_range= np.logspace(-9, 3, 13)
        param_grid['gamma']=gamma_range
        svc = svm.SVC(kernel='rbf')
        #lin_svc = svm.LinearSVC(C=C).fit(X, y)

        return cls(svc,par_grid_dict=param_grid)

