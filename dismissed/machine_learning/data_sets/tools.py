"""
Module: tools
==================

Summary
-------
.. autosummary::



Module API
-----------
"""
from __future__ import division, absolute_import, print_function
import fnmatch, re

__author__ = 'andrea tramacere'




def build_names_list(names_list,column_names_list,regex=True,skip=False):
    if skip==False:
        selected_names_list=[]
    else:
        selected_names_list=column_names_list[:]

    if regex==True:
        for use_name in names_list:
            regex = fnmatch.translate(use_name)
            matches = [name for name in column_names_list if re.match(regex, name)]
            #print ('matches',regex,matches)
            for match in matches:
                if skip==False:
                    if match not in selected_names_list:
                        selected_names_list.append(match)
                else:
                    if match  in selected_names_list:
                        #print ("removing",match)
                        selected_names_list.remove(match)
    else:
        for ID,name in enumerate(column_names_list):
            if name  in names_list:
                if skip==False:
                    selected_names_list.append(name)
                else :
                    selected_names_list.remove(name)
    #print(selected_names_list)
    return selected_names_list



