"""
Module: sampling
==================

Summary
-------
.. autosummary::


Module API
-----------
"""
from __future__ import division, absolute_import, print_function
import  numpy as np
from scipy import stats

__author__ = 'andrea tramacere'





def sampling_split(initial_array,sample_size,bins=50):
    """

    Args:
        initial_array:
        sample_size:
        bins:

    Returns:

    """

    bins=np.linspace(initial_array.min(),initial_array.max(),bins)


    #-----
    #histogram of the distribution of the features
    #-----
    (distr_hist, distr_bins) = np.histogram(initial_array,bins=bins)

    #-----
    #histrogram of the cum distr of the feature
    #the bis are the same of distr_hist
    #-----
    cumdistr_hist = np.float32(np.cumsum(distr_hist))/np.sum(distr_hist)
    cumdistr_bins = cumdistr_hist

    #-----
    #random sample of the codomine of the cumdistr
    #cumdistr_codomine_rand_sample=np.random.random(cumdistr_hist[0],cumdistr_hist[-1],out_size)
    #-----
    cumdistr_codomine_rand_sample=np.random.random(sample_size)

    #-----
    #This array maps the ID on the cumdistr_codomine_rand_sample
    #to the corresponding bin of the domine, and the bins are the same for
    #for both distr and cumdist, hence provides the sampling of distr
    #----
    cumdistr_bins=np.append(0,cumdistr_bins)
    rand_sample_ID_to_distr_bin_ID_map=np.digitize(cumdistr_codomine_rand_sample,cumdistr_bins)

    #this array maps each entry of the feature array to the distr_bins
    feature_ID_to_distr_bins_ID_map=np.digitize(initial_array,distr_bins)

    #the frequency of the bin in the random sample
    unique_rand_sample_bin_ID,freq_of_rand_sample_bin_ID=np.unique(rand_sample_ID_to_distr_bin_ID_map,return_counts=True)

    train_IDs_mask=np.zeros(initial_array.size,dtype=np.bool)

    for ID,bin_ID in enumerate(unique_rand_sample_bin_ID):

        #number of drawn elements in the bin
        rand_elements=freq_of_rand_sample_bin_ID[ID]

        #picks features with the same distr bin_ID of the cum_distr bin_ID
        selected_feature_ID=np.argwhere(feature_ID_to_distr_bins_ID_map==bin_ID)

        #rand_elements items are remove from that bin, ie assigned to the train
        selected_feature_ID=selected_feature_ID[:rand_elements]

        train_IDs_mask[selected_feature_ID]=True



    (ks_stat,ks_p_val)=stats.ks_2samp(initial_array,initial_array[train_IDs_mask])
    print ("KS p_value",ks_p_val)
    return train_IDs_mask,ks_p_val
