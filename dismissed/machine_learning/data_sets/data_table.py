"""
Summary
-------
.. autosummary::
   TableBase


Module API
-----------
"""
from __future__ import division, absolute_import, print_function
from  astropy.io import fits as pf
import numpy as np
from .tools import build_names_list
from numpy.lib import recfunctions

__author__ = 'andrea tramacere'








class TableBase(object):
    """

    Parameters
    ----------
    input_ndarray : {array-like}, shape=[_data_N_rows,_data_N_cols]
    id_column_name :  str
         name of the column storing  positional/ordinal information
    id_column_num : int
         id of the column storing  positional/ordinal information

    Attributes
    ----------
    _data_dtype : int
    _data_array : int
    _data_names : list
    _data_N_cols : int
    _data_N_rows : int
    _data_original_entry_ID : int
    data



    """


    def __init__(self,input_ndarray,\
                 id_column_name=None,\
                 id_column_num=None):




        self._data_dtype=None
        self._data_array=None
        self._data_names=None
        self._data_N_cols=None
        self._data_N_rows=None
        self._data_original_entry_ID=None
        self.set_data(input_ndarray,\
                       id_column_name=id_column_name,\
                       id_column_num=id_column_num)
        #self._clean_data_nan_inf()
        #self._clean_data_nan_inf()



    #------------------------------------
    # data Section
    #------------------------------------
    @property
    def data(self):
        """
        The numpy recarray storing the data

        """
        return self._data_array


    def filter_data(self,mask):
        print("| filtering data")
        print("| data initial Rows,Cols=",self._data_N_rows,self._data_N_cols)
        self._data_array=self._data_array[mask]
        self._data_N_rows=self._data_array.shape[0]
        print("| data filtered Rows,Cols=",self._data_N_rows,self._data_N_cols)
        print("")

    def set_data(self,data,target_col_num=None,target_col_name=None,id_column_name=None,id_column_num=None):
        """
        Parameters
        ----------
        data :
        target_col_name : str
            name of the column storing the target information
             .. warning:: this has been dismissed

        target_col_num : int
            id of the column storing the target information
             .. warning:: this has been dismissed

        id_column_name : str
            name of the column storing  positional/ordinal information

        id_column_num : int
            id of the column storing  positional/ordinal information


        sets the following attributes:
         - _data_dtype
         - _data_array (dtype np.object)
         - _data_names (list)
         - _data_N_cols (int)
         - _data_N_rows (int)
         -  _data_original_entry_ID 1d np.array np.int

        Columns storing ID positional/ordinal information, or target (i.e. class labels for
        classification, or target variables for regression are removed)

         - Removes the column storing ID positional information (id_column_name/id_column_num if given)


         - Removes the column storing target information (target_col_name/target_col_num if given)
         .. warning:: this has been removed

        The input data can be either a 2dim numpy array or, a numpy recarray.
        In the  former case, columns names are set to col_ plus the ordinal ID of the column, starting from zero



        """
        is_table,is_2dimarray,is_recarray=self._check_input_data_type(data)




        if is_2dimarray==True:
            if id_column_num is not None:
                data=np.delete(data,id_column_num,1)
            elif id_column_name is not None:
                raise RuntimeError("input data has no column names, please provide id_col_num ")



            selected_cols=np.arange(data.shape[1])
            self._data_dtype= [('col_%d'%ID,data.dtype) for ID,foo in enumerate(selected_cols)]
            self._data_array=np.zeros(data.shape[0],dtype=self._data_dtype)
            for ID,col_name in enumerate(self._data_names):
                self._data_array[col_name]=data[:,ID]

        else:


            #if target_col_name is not None:



            if id_column_name is None and id_column_num is None:
                pass
            elif id_column_name is not None:

                self._data_array=recfunctions.drop_fields(data,[id_column_name])
            elif id_column_num is not None:
                self._data_array=recfunctions.drop_fields(data,[data.dtype.names[id_column_num]])
            else:
                self._data_array=np.copy(data)

            self._data_names=list(data.dtype.names)
            self._data_dtype=self._data_dtype




        self._data_array=recfunctions.append_fields(self._data_array,'original_entry_ID',np.arange(self._data_array.size),usemask=False)
        self._data_names= list(self._data_array.dtype.names)

        self._data_N_rows=self._data_array.size
        self._data_N_cols=len(self._data_names)

        print("| input data built")
        print("| data Rows,Cols",self._data_N_rows,self._data_N_cols)
        #print("| column names", self._data_names)
        #for name in self._data_names:
        #    print("|",name)
        #print("")



    def add_column(self,values_array,column_name,dtype=None):
        """
        Adds a column to the data, using numpy recfunctions

        Parameters
        ----------
        values_array : {array-like}, shape =[self._data_N_rows]
        column_name : str
        dtype :



        """


        self._data_array=recfunctions.append_fields(self._data_array,column_name,values_array,usemask=False,dtypes=dtype)
        self._data_names= self._data_array.dtype.names

        self._data_N_rows=self._data_array.size
        self._data_N_cols=len(self._data_names)


    def _clean_data_nan_inf(self):
        """
        Removes a whole column is all the elements in the are
         - all== inf or
         - all== nan or
         - all== inf or nan

        Removes a row if any of the entry in the row is inf or nan



        """
        #removes a whole column is all the elements in the are
        # all== inf or
        # all== nan or
        # all== inf or nan
        print('| removing nan/inf')
        print("| data initial Rows,Cols=",self._data_N_rows,self._data_N_cols)
        black_names_list=[]
        for ID,name in enumerate(self._data_array.dtype.names):

            if type(self._data_array[name][0])==np.str_ :
                good_colum=False
            else:
                good_colum=~np.all(np.logical_or(np.isnan(self._data_array[name]),np.isinf(self._data_array[name])))


            if good_colum is False:
                black_names_list.append(name)

        print('black list',black_names_list)

        if len(black_names_list)>0:
            self._data_array=recfunctions.drop_fields(self._data_array,black_names_list)

        self._data_N_cols=len(self._data_names)

        #removes a row if any of the entry in the row is inf or nan
        full_col_masked=np.zeros(self._data_array.size,dtype=np.bool)

        for ID,name in enumerate(self._data_array.dtype.names):
            single_col_masked=np.logical_or(np.isnan(self._data_array[name]),np.isinf(self._data_array[name]))
            full_col_masked=np.logical_or(full_col_masked,single_col_masked)

        self._data_array=self._data_array[~full_col_masked]
        self._data_N_rows=self._data_array.size
        self._data_N_cols=len(self._data_names)
        print("| data filtered Rows,Cols=",self._data_N_rows,self._data_N_cols)
        print("")

    def _clean_bad_value(self,bad_value):
        """
        Removes a whole column is all the elements in the are
         - all== bad_value

        Removes a row if any of the entry in the row is == bad_value

        Parameters
        ----------
        bad_value :

        Returns
        -------

        """

        black_names_list=[]

        print('| removing bad value',bad_value)
        print("| data initial Rows,Cols=",self._data_N_rows,self._data_N_cols)

        # removes a whole column is all the elements in the are
        # all== bad_value
        for ID,name in enumerate(self._data_array.dtype.names):


            good_colum=np.all(self._data_array[name]!=bad_value)


            if good_colum is False:
                black_names_list.append(name)

        print('black list',black_names_list)

        if len(black_names_list)>0:
            self._data_array=recfunctions.drop_fields(self._data_array,black_names_list)

        self._data_N_cols=len(self._data_names)

        #removes a row if any of the entry in the row is inf or nan
        full_col_masked=np.zeros(self._data_array.size,dtype=np.bool)
        for ID,name in enumerate(self._data_array.dtype.names):

            #print(row_array,self._data_array.dtype)
            single_col_masked=self._data_array[name]==bad_value
            full_col_masked=np.logical_or(full_col_masked,single_col_masked)



        self._data_array=self._data_array[~full_col_masked]
        self._data_N_rows=self._data_array.size
        self._data_N_cols=len(self._data_names)


    def _check_same_size(self,size1,size2):
            return size1==size2

    def _build_names_list(self,names_list,regex=True,skip=False):
        return  build_names_list(names_list,self._data_names,regex=regex,skip=skip)





    def _check_input_data_type(self,data):
        is_table=False
        is_2dimarray=False
        is_recarray=False

        if len(data.dtype)>1 and data.ndim==1:
            is_table=True
            is_recarray=True

        if data.ndim==2:
            is_table=True
            is_2dimarray=True

        if is_table==False:
            raise RuntimeError("data has to be a 2dim array")

        return is_table,is_2dimarray,is_recarray







    #Constructors
    @classmethod
    def from_fits_file(cls,input_file,id_column_name=None,id_column_num=None,fits_ext=0):
        data=np.array(pf.getdata(input_file,ext=fits_ext))
        return cls(data,id_column_name=id_column_name,id_column_num=id_column_num)

    @classmethod
    def from_ascii_file(cls):
        pass



class DatasetRegression(TableBase):
    def __init__(self):
        super(DatasetRegression,self).__init__()





class DatasetClassification(TableBase):
    def __init__(self):
        super(DatasetClassification,self).__init__()