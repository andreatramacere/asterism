"""
Module: features
==================

Summary
-------
.. autosummary::
   FeaturesSetBase


Module API
-----------
"""
from __future__ import division, absolute_import, print_function
import numpy as np
import copy
from .sampling import  sampling_split
from .tools import build_names_list
from .visualization import plot_feature_histogram,contour_scatter_plot
from  sklearn.cross_validation import StratifiedShuffleSplit,ShuffleSplit

__author__ = 'andrea tramacere'


def write_table(data,fp,target):

    #print (target)
    nrows_tot = data.shape[0]
    for ID in range(nrows_tot):
        #print data[ID].size
        a=''
        for i in range(data[ID].size):
            a+="%s, "%data[ID][i]

        #a+="%s "%data[ID][-1]
        str=a + " %s"%target[ID]
        print(str,file=fp)


def write_weka_file(out_file,features,features_names,class_ids=None,class_labels=None,relation='None',):


    fp=open(out_file,'w')

    str="""
    % 1. Title:
    %
    % 2. Sources:
    """

    str+="@RELATION %s"%(relation)


    print(str,file=fp)

    for col_name in features_names:
            #if 'i1xx' == columns_dtype[col_name]:
            #    datatype='{True,False}'
            #else:
            datatype='NUMERIC'
            str="@ATTRIBUTE "+ col_name +" "+ datatype
            print(str,file=fp)

    #print len(features_names)
    if class_labels is not None:
        data_classes=np.unique(class_labels)
        data_classes_str=''

        for c in data_classes:
            data_classes_str+='%s,'%c

        #if t_set==True:
        str="@ATTRIBUTE class {%s}"%data_classes_str
        print(str,file=fp)
        target=class_labels

    if class_ids is not None:
        datatype='NUMERIC'
        str="@ATTRIBUTE target "+ datatype
        print(str,file=fp)

    str="@DATA"
    print(str,file=fp)
    print('\n',file=fp)


    write_table(features,fp,target)

    fp.close()



class FeaturesSetBase(object):

    def __init__(self,features_array,\
                 features_names=None,\
                 target_array=None,\
                 weight_array=None,\
                 features_original_entry_ID=None):

        self._features_array=features_array

        if features_names is not None:
            self._features_names=features_names
        else:
            self._features_names=['F_%d'%ID for ID in range(features_array.shape[1])]


        if target_array is not None:
            self._target_array=np.copy(target_array)
        else:
            self._target_array=None

        if weight_array is not None:
            self._weight_array=np.copy(weight_array)
        else:
            self._weight_array=None

        self._features_original_entry_ID=features_original_entry_ID

        self.clean_features_nan_inf()
        self._features_N_rows,self._features_N_cols=self._features_array.shape

        print("| features built")
        print("| Rows,Cols",self._features_N_rows,self._features_N_cols)
        #print("| column names",self._features_names)

        #for name in self._features_names:
        #    print("|",name)
        #print("")


    #------------------------------------
    # Properties
    #------------------------------------
    @property
    def features(self):
        return self._features_array


    @property
    def features_names(self):
        return self._features_names

    @features_names.setter
    def features_names(self,names):
        self._check_same_size(len(names),self.features_N_cols)
        self._features_names=names

    @property
    def target(self):
        return self._target_array

    @property
    def target_array(self):
        return self._target_array

    @target_array.setter
    def target_array(self,array):
        self._check_is_1dim(array)
        self._check_same_size(array.size,self.features_N_rows)
        self._target_array=np.copy(array)

    @property
    def weight_array(self):
        return self._weight_array

    @weight_array.setter
    def weight_array(self,array):
        self._check_is_1dim(array)
        self._check_same_size(array.size,self.features_N_rows)
        self._weight_array=np.copy(array)

    @property
    def features_original_entry_ID(self):
        return self._features_original_entry_ID

    @features_original_entry_ID.setter
    def features_original_entry_ID(self,array):
        self._check_is_1dim(array)
        self._check_same_size(array.size,self.features_N_rows)
        self._features_original_entry_ID=np.copy(array)


    @property
    def features_N_rows(self):
        return  self._features_array.shape[0]


    @property
    def features_N_cols(self):
        return  self._features_array.shape[1]



    #------------------------------------
    # constructors
    #------------------------------------
    @classmethod
    def new_from_table(cls,table,\
                                   target_array=None,\
                                   weight_array=None,\
                                   target_col_name=None,\
                                   target_col_num=None,\
                                   original_entry_ID_col_name=None,\
                                   use_col_names_list=[],\
                                   skip_col_names_list=[],\
                                   use_col_num_list=[],\
                                   skip_col_num_list=[],\
                                   regex=True,\
                                   rows_IDs_use_list=[],\
                                   rows_IDs_skip_list=[]):



        print("| building features")
        cols_ID_use_list=range(table._data_N_cols)
        names_list=table._data_names





        #column filtering
        if skip_col_num_list!=[] and skip_col_num_list is not None:
            cols_ID_use_list=[ID for ID in range(table._data_N_cols) if ID not in skip_col_num_list]
            names_list=names_list[cols_ID_use_list]

        if use_col_num_list!=[] and use_col_num_list is not None:
            cols_ID_use_list=use_col_num_list
            names_list=names_list[cols_ID_use_list]



        if use_col_names_list!=[] and use_col_names_list is not None:
            names_list=build_names_list(use_col_names_list,names_list,regex=regex)


        if skip_col_names_list!=[] and skip_col_names_list is not None:
            names_list=build_names_list(skip_col_names_list,names_list,regex=regex,skip=True)

        features_names=names_list
        features_array=np.vstack([table._data_array[item] for item in names_list]).T
        features_original_entry_ID=table._data_array['original_entry_ID']



        if target_col_name is not None:
            if target_col_name in features_names:
                raise RuntimeError("features  can not be used as target")
            else:
                target_array=table._data_array[target_col_name]

        if target_col_num is not None:
            if target_col_num in cols_ID_use_list:
                raise RuntimeError("features  can not be used as target")
            else:
                target_array=table._data_array[:,target_col_num]


        #row filtering
        if rows_IDs_use_list!=[] and rows_IDs_use_list is not None:
            features_array=features_array[rows_IDs_use_list]
            features_original_entry_ID=features_original_entry_ID[rows_IDs_use_list]
            if target_array is not None:
                target_array=target_array[rows_IDs_use_list]
            if weight_array is not None:
                weight_array=weight_array[rows_IDs_use_list]

        if rows_IDs_skip_list!=[] and rows_IDs_skip_list is not None:
            use_list=[ID for ID in range(table._data_N_rows) if ID not in rows_IDs_skip_list]
            features_array=features_array[use_list]
            features_original_entry_ID=features_original_entry_ID[use_list]
            if target_array is not None:
                target_array=target_array[use_list]
            if weight_array is not None:
                weight_array=weight_array[use_list]





        return FeaturesSetBase(features_array,\
                            features_names=features_names,\
                            target_array=target_array,\
                            weight_array=weight_array,\
                            features_original_entry_ID=features_original_entry_ID)




    def new_from_rows(self,selected):
        """
        Creates a new FeaturesSet object from a slice of an existing FeaturesSet object
        Args:
            feature_obj:
            selected: either bool array or IDs array

        Returns:

        """
        new=copy.deepcopy(self)
        new.filter_features_rows(selected)
        return new



    def plot_feature_histogram(self,names,bins=20,normed=False,KDE=False,gridsize=100,bw='scott',ax=None,plot=False,exclude_outliers=None,save_plot=False):
        if type(names)!=list:
            names=[names]
        print(names)
        names_list=build_names_list(names,self.features_names,regex=True)
        for name in names_list:
            plot_feature_histogram(self.get_feature_by_name(name),
                                   name,
                                   self.target,
                                   bins=bins,
                                   normed=normed,
                                   KDE=KDE,
                                   gridsize=gridsize,
                                   bw=bw,
                                   ax=ax,
                                   plot=plot,
                                   exclude_outliers=exclude_outliers,
                                   save_plot=save_plot)


    def plot_contour_scatter_plot(self,name_1,name_2,n_levels=20,ax=None,exclude_outliers=False,show_plot=False,plot_contour=True,plot_scatter=True):

        x=self.get_feature_by_name(name_1)

        y=self.get_feature_by_name(name_2)
        return contour_scatter_plot(x,
                                    y,
                                    self.target,
                                    x_name=name_1,
                                    y_name=name_2,
                                    n_levels=n_levels,
                                    ax=ax,
                                    exclude_outliers=exclude_outliers,
                                    show_plot=show_plot,
                                    plot_contour=plot_contour,
                                    plot_scatter=plot_scatter,
                                    point_size=.5,
                                    color_list=None,
                                    c_map_list=None,
                                    lw=0.5)



    #------------------------------------
    # getters
    #------------------------------------
    def get_feature_by_name(self,feature_name):
        if feature_name not in self._features_names:
            raise RuntimeError('feature name not existing')
        for ID,name in enumerate(self._features_names):
                if name==feature_name:
                    return self._features_array[:,ID]


    #------------------------------------
    # train/test splitting
    #------------------------------------
    def split_train_test(self,train_test_split_ratio,sampling_array=None,stratified=True):

        N_split_train=np.int(self.features_N_rows*train_test_split_ratio)



        if N_split_train==self.features_N_rows or N_split_train==0:
            raise RuntimeError("Splitting ratio produced train set size or test set size ==0")

        else:

            if stratified ==True and sampling_array is None:

                str_shuffle=StratifiedShuffleSplit(self.target, n_iter=1, test_size=1.0-train_test_split_ratio)
                for train_IDs,test_IDs in str_shuffle:
                    pass

            elif stratified==False and sampling_array is None:
                shuffle=ShuffleSplit(self.features_N_rows,1, test_size=1.0-train_test_split_ratio)
                for train_IDs,test_IDs in shuffle:
                    pass



            elif sampling_array is not None:
                train_IDs,ks_p_value=sampling_split(sampling_array,N_split_train)
                test_IDs=~train_IDs



        train_Features=self.new_from_rows(train_IDs)
        test_Features=self.new_from_rows(test_IDs)


        return train_Features,test_Features

    #------------------------------------
    # add/remove
    #------------------------------------
    def add_feature(self,feature,feature_name):
        self._features_array=np.column_stack((self._features_array,feature))
        self._features_names.append(feature_name)
        self._features_N_rows,self._features_N_cols=self._features_array.shape


    def remove_feature(self,removing_feature_names_list,regex=True):

        if type(removing_feature_names_list) != list:
            removing_feature_names_list=[removing_feature_names_list]



        if regex is True:
            names_list=build_names_list(removing_feature_names_list,self._features_names,regex=regex,skip=True)
                #self._build_skip_names_list(removing_feature_names_list,regex=regex)
        else :
            names_list=[name for name in self._features_names if name not in removing_feature_names_list]

        print("| removing features :",names_list)
        print("| features initial Rows,Cols=",self._features_N_rows,self._features_N_cols)
        for feature_name in names_list:
            if feature_name not in self._features_names:
                raise RuntimeError('feature name not existing ')

        remove_ID_list=[]

        for ID,name in enumerate(self._features_names[:]):
            if name not in names_list:
                remove_ID_list.append(ID)
                self._features_names.remove(name)

        if remove_ID_list!=[]:
            self._features_array=np.delete(self._features_array,remove_ID_list,1)


        self._features_N_rows,self._features_N_cols=self._features_array.shape
        print("| features final Rows,Cols=",self.features_N_rows,self.features_N_cols)
        print("")




    #------------------------------------
    # cleaning
    #------------------------------------

    def clean_features_bad_value(self,bad_value):
        print("| features cleaning for bad_value=",bad_value)
        print("| features initial Rows,Cols=",self._features_N_rows,self._features_N_cols)
        #removes a whole column is all the elements in the are
        # all== inf or
        # all== nan or
        # all== inf or nan
        black_list=[]
        use_IDs_list=np.arange(self._features_array.shape[1])
        for ID,name in enumerate(self._features_names):

            bad_column=~np.all(self._features_array[:,ID]==bad_value)

            if bad_column is True:
                use_IDs_list.remove(ID)
                black_list.append(name)

        print("|removed columns",black_list)


        self.select_columns_by_ID(use_IDs_list)


        #removes a row if any of the entry in the row is == bad_value
        masked=np.zeros(self._features_N_rows,dtype=np.bool)
        for ID in range(self._features_array.shape[0]):
            masked[ID]=np.any(self._features_array[ID]==bad_value)
        self.filter_features_rows(~masked)

        print("| features cleaned Rows,Cols=",self.features_N_rows,self.features_N_cols)
        print("")


    def clean_features_nan_inf(self):
        print("| features cleaning for nan/inf")
        print("| features initial Rows,Cols=",self.features_N_rows,self.features_N_cols)
        #removes a whole column is all the elements in the are
        # all== inf or
        # all== nan or
        # all== inf or nan

        use_IDs_list=np.arange(self._features_array.shape[1])
        black_list=[]
        for ID,name in enumerate(self._features_names):

            #we skip strings

            bad_column=~np.all(np.logical_or(np.isnan(self._features_array[:,ID]),np.isinf(self._features_array[:,ID])))

            if bad_column is True:
                use_IDs_list.remove(ID)
                black_list.append(name)

        print("|removed columns",black_list)

        self.select_columns_by_ID(use_IDs_list)


        #removes a row if any of the entry in the row is inf or nan
        masked=np.zeros(self.features_N_rows,dtype=np.bool)
        for ID in range(self._features_array.shape[0]):
            masked[ID]=np.any(np.isinf(self._features_array[ID])) or np.any(np.isnan(self._features_array[ID]))
        #print(self._features_original_entry_ID[masked])
        self.filter_features_rows(~masked)

        print("| features cleaned Rows,Cols=",self.features_N_rows,self.features_N_cols)
        print("")


    def filter_features_rows(self,selected):
        self._features_array=self._features_array[selected]

        if self._features_original_entry_ID is not None:
            self._features_original_entry_ID=self._features_original_entry_ID[selected]

        if self._target_array is not None:
            self._target_array=self._target_array[selected]

        if self._weight_array is not None:
            self._weight_array=self._weight_array[selected]

        self._features_N_rows,self._features_N_cols=self._features_array.shape


    def sort_columns(self,id_array):
        self._features_array=self._features_array[:,id_array]
        self._features_names=self._features_names=[self._features_names[id] for id in id_array]
        self._features_N_rows=self._features_array.shape[0]

    def select_columns_by_ID(self,use_IDs_list):
        print("| select features columns")
        print("| features initial Rows,Cols=",self._features_array.shape)
        if  hasattr(use_IDs_list,'dtype'):
            if use_IDs_list.dtype==np.bool:
                use_IDs_list=np.argwhere(use_IDs_list).flatten().tolist()

        if type(use_IDs_list)!=list:
            use_IDs_list=list(use_IDs_list)

        if len(use_IDs_list) <=len(self._features_names):

            self._features_array=self._features_array[:,use_IDs_list]
            _names=[self._features_names[ID] for ID in  use_IDs_list]
            self._features_names=_names
            self._features_N_cols=len(use_IDs_list)
            self._features_N_rows=self._features_array.shape[0]
        print("| features selected Rows,Cols=",self._features_array.shape)


    def merge(self,feature):

        self._features_array=np.row_stack((self._features_array,feature._features_array))
        self._target_array=np.append(self._target_array,feature._target_array)
        self._features_N_rows=self._features_array.shape[0]
        if self._features_original_entry_ID is not None:
            self._features_original_entry_ID=np.append(self._features_original_entry_ID,feature._features_original_entry_ID)
        if self._weight_array is not None:
            self._weight_array=np.append(self._weight_array,feature._weight_array)

    #------------------------------------
    # check consistency
    #------------------------------------
    def _check_shapes(self):

        if self._features_array.ndim!=2:
            raise RuntimeError("features array has to be a 2dim array")

        if self._features_names is not None:
            if self._features_array.shape[0]!=len(self._features_names):
                raise RuntimeError("features and features names have different size")

        if self._target_array is not None:
            if self._target_array.ndim!=1:
                raise RuntimeError("target array has to 1dim array")
            if self._features_array.shape[0]!=self._target_array.size:
                raise RuntimeError("features and target names have different size")

        if self._weight_array is not None:
            if self._target_array.ndim!=1:
                raise RuntimeError("weight array has to 1dim array")
            if self._features_array.shape[0]!=self._weight_array.size:
                raise RuntimeError("features and weight names have different size")

        if self._features_original_entry_ID is not None:
            if self._features_array.shape[0]!=len(self._features_original_entry_ID):
                raise RuntimeError("features and features_original_entry_ID  have different size")


    def _check_is_2dim(self,array):
        return array.ndim==1

    def _check_is_1dim(self,array):
         return array.ndim==2

    def _check_same_size(self,size1,size2):
         return size1==size2




    #def _build_skip_names_list(self,skip_names_list,regex=True):
    #    return  build_names_list(skip_names_list,self._features_names,regex=regex,skip=True)


    #def _build_use_names_list(self,use_names_list,regex=True):
    #    return  build_names_list(use_names_list,self._features_names,regex=regex,skip=False)
