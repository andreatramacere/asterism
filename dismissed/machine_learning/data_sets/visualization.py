"""
Module: visualization
==================

Summary
-------
.. autosummary::



Module API
-----------
"""

from __future__ import division, absolute_import, print_function
from ...stats.histogram.hisogram_1d import histogram_kde
import  numpy as np
import scipy as sp
from    math import log
from scipy.ndimage import gaussian_filter

try:

    import matplotlib
   # matplotlib.use('Agg')


    import matplotlib.pylab as plt



except:
    print("no pylab found")

__author__ = 'andrea tramacere'



def plot_feature_histogram(feature,feature_name,labels,bins=10,range=None,normed=False,KDE=False,
                           gridsize=1000,
                           pred=None,
                           bw='scott',
                           plot=True,
                           save_plot=False,
                           file_name='feat_histogram',
                           file_type='pdf',
                           path='./',
                           exclude_outliers=None,
                           ax=None):

    print ('feature name',feature_name)
    if pred is None:
        labels=labels
    else:
        labels=pred

    l_labels=np.unique(labels)
    l_labels.sort()
    hist_data=[]
    hist_labels=[]

    for label in l_labels:
        feat=(feature[labels==label]).astype(np.float32)
        if exclude_outliers is not None and exclude_outliers!=False:
            m=feat.mean()
            s=feat.std()
            msk1=feat<m+exclude_outliers*s
            msk2=feat>m-exclude_outliers*s
            feat=feat[msk1*msk2]

        if range is None or range==[] :
            size=feat.size
        else:
            msk1=feat>range[0]
            if range[1] is not None:
                msk2=feat<range[1]
            else:
                msk2=msk1
            feat=feat[msk1*msk2]

            size=feat.size
        hist_data.append( feat )
        hist_labels.append(label)
        #print("label:",label," size:",size)
        #print np.float64(feature[labels==label]).shape

    if plot==True or save_plot==True or ax is not None:
        #import seaborn as sns
        #sns.set_style("white")
        if ax is None:
            fig, ax = plt.subplots()
            ax.set_title(feature_name,fontsize=24)
        if KDE==False:
            ax.hist(hist_data,label=hist_labels,histtype='step',bins=bins,normed=normed,stacked=False,fill=False,lw=2)
        else:
            for ID,label in enumerate(hist_labels):

                if label[0]=='E':
                    color='red'
                    ls='-'
                else:
                    color='blue'
                    ls='-.'
                print("label",label,color,ls)
                pdf,x_bins,x=histogram_kde(hist_data[ID],bins=bins,bw=bw)
                ax.plot(x, pdf,ls=ls,c=color,label=label,lw=1.0)
                #sns.kdeplot(hist_data[ID],label=label,ax=ax,shade=True,gridsize=gridsize,color=color,bw=bw)
        ax.legend(frameon=False,loc=1,fontsize = 'x-small')

    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name+'_'+feature_name,file_type))

    #print ("PLOT",plot)
    if plot==True:
        plt.show()

    if plot==True or save_plot==True:
        plt.close()

    return




def  contour_scatter_plot(x,
                          y,
                          target,
                          labels=None,
                          x_name='x',
                          y_name='y',
                          n_levels=20,
                          ax=None,
                          exclude_outliers=None,
                          show_plot=False,
                          plot_contour=True,
                          plot_scatter=True,
                          point_size=1,
                          color_list=None,
                          c_map_list=None,
                          lw=1):

    x=np.float64(x)
    y=np.float64(y)

    if color_list is None:
        color_list = ['red', 'blue', 'red', 'yellow','orange']
    if c_map_list is None:
        c_map_list = ['Reds', 'Blues', 'Reds', 'Oranges','Reds']



    if exclude_outliers is not None:
        msk=[None,None]
        data=[x,y]
        for ID,d in enumerate(data) :
            m=data[ID].mean()
            s=data[ID].std()
            msk1=data[ID]<m+exclude_outliers*s
            msk2=data[ID]>m-exclude_outliers*s
            msk[ID]=msk1*msk2



        x=data[0][msk[0]*msk[1]]
        y=data[1][msk[0]*msk[1]]
        target=target[msk[0]*msk[1]]

    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig=None



    H_sublcasses=[]
    if labels is None:
        labels=np.unique(target)

    for ID,cl in enumerate(labels):
        x1=x[target==cl]
        y1=y[target==cl]


        H_tot, xedges_tot, yedges_tot = np.histogram2d(x, y, bins=(100,100))
        H_tot.shape, xedges_tot.shape, yedges_tot.shape

        H_s, xedges_s, yedges_s = np.histogram2d(x1, y1, bins=(xedges_tot,yedges_tot))
        H_sublcasses.append(H_s)
        #H_s_plot=gaussian_filter(H_s,smoothing_scale)
        H_s.shape, xedges_s.shape, yedges_s.shape
        extent = [xedges_tot[0], xedges_tot[-1], yedges_s[0], yedges_s[-1]]




        X, Y = np.meshgrid(xedges_s, yedges_s)
        positions = np.vstack([X.ravel(), Y.ravel()])
        values = np.vstack([x1, y1])
        if plot_contour==True:
            kernel = sp.stats.gaussian_kde(values,)
            Z = np.reshape(kernel(positions).T, X.shape)


        if ax is not None:

            if plot_scatter==True:
                ax.scatter(x1,y1,s=point_size  ,color=color_list[ID],label=cl,alpha=0.5,marker='.')
            if plot_contour==True:
                vmin=Z.min()
                vmax=Z.max()*0.95
                levels = np.linspace(Z.min(),Z.max(),n_levels)

                cfset = ax.contour(X,Y,Z ,cmap=c_map_list[ID],levels=levels,linewidths=lw,vmin=vmin,vmax=vmax)
                #



    if ax is not None:

        ax.legend(frameon=False)

        ax.set_xlabel(x_name)

        ax.set_ylabel(y_name)

    if show_plot==True:
        plt.show()

    return x_name,y_name,fig