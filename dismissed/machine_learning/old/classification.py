"""Example Google style docstrings.

This module demonstrates documentation as specified by the `Google Python
Style Guide`_. Docstrings may extend over multiple lines. Sections are created
with a section header and a colon followed by a block of indented text.

Example:
    Examples can be given using either the ``Example`` or ``Examples``
    sections. Sections support any reStructuredText formatting, including
    literal blocks::

        $ python example_google.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""
from __future__ import division, absolute_import #, print_function

__author__ = 'andrea tramacere'

'''
Created on Jan 29, 2015

@author: orion
'''
from  astropy.io import fits as pf
import numpy as np
from numpy.lib.recfunctions import stack_arrays
import argparse
from sklearn.ensemble import RandomForestClassifier,AdaBoostClassifier,GradientBoostingClassifier,ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.neighbors import KNeighborsClassifier,KernelDensity
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline

from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import StratifiedKFold
from sklearn import tree
from sklearn.externals import joblib

#import matplotlib
#from numpy import ndarray
#matplotlib.use('Agg')

try:

    import matplotlib
    matplotlib.use('Agg')


    import matplotlib.pylab as plt
    #from scipy import ndimag
    from matplotlib.patches import Ellipse, Circle

    #from  matplotlib.pyplot.arrow as arrow
    import matplotlib.gridspec as gridspec

    #from scipy import ndimage
    import matplotlib.cm as cm
    #from matplotlib.colors import Normalize

except:
    print "no pylab found"


from scipy.ndimage import gaussian_filter
#from matplotlib.colors import ListedColormap
from math import log
import copy
from sklearn.neighbors.kde import KernelDensity
from sklearn.metrics import roc_curve, auc
#from scipy.constants.constants import alpha
#from wx.lib.agw.persist.persist_handlers import hasSB

def write_table(data,fp,class_col):

    print class_col
    nrows_tot = data.shape[0]
    for ID in xrange(nrows_tot):
        #print data[ID].size
        a=''
        for i in xrange(data[ID].size):
            a+="%s, "%data[ID][i]

        #a+="%s "%data[ID][-1]
        print>>fp, a, class_col[ID]


def write_weka_file(out_file,features,features_names,class_labels,relation='galaxy_shape',):


    fp=open(out_file,'w')

    str="""
    % 1. Title:
    %
    % 2. Sources:
    """

    str+="@RELATION %s"%(relation)


    print>>fp,str

    for col_name in features_names:
            #if 'i1xx' == columns_dtype[col_name]:
            #    datatype='{True,False}'
            #else:
            datatype='NUMERIC'
            print>>fp,"@ATTRIBUTE",col_name , datatype

    #print len(features_names)
    data_classes=np.unique(class_labels)
    data_classes_str=''

    for c in data_classes:
        data_classes_str+='%s,'%c

    #if t_set==True:
    print>>fp,"@ATTRIBUTE", 'Morph', '{%s}'%data_classes_str

    print>>fp,"@DATA"
    print>>fp


    write_table(features,fp,class_labels)

    fp.close()

def do_plot(model,X,Y):

    from sklearn import clone
    cmap = plt.cm.RdYlBu
    plot_colors = "ryb"

    X=X [:,[1,11]]
    clf = clone(model)
    clf = model.fit(X, Y)
    plot_step = 0.02  # fine step width for decision surface contours
    plot_step_coarser = 0.5  # step widths for coarse classifier guesses

    x_min, x_max = X[:, 0   ].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                             np.arange(y_min, y_max, plot_step))
    estimator_alpha = 1.0 / len(model.estimators_)

    for tree in model.estimators_:
                Z = tree.predict(np.c_[xx.ravel(), yy.ravel()])
                Z = Z.reshape(xx.shape)
                cs = plt.contourf(xx, yy, Z, alpha=estimator_alpha, cmap=cmap)

    xx_coarser, yy_coarser = np.meshgrid(np.arange(x_min, x_max, plot_step_coarser),
                                             np.arange(y_min, y_max, plot_step_coarser))
    Z_points_coarser = model.predict(np.c_[xx_coarser.ravel(), yy_coarser.ravel()]).reshape(xx_coarser.shape)
    #cs_points = plt.scatter(xx_coarser, yy_coarser, s=15, c=Z_points_coarser, cmap=cmap, edgecolors="none")

    labels=np.unique(Y)
    for i, c in zip(xrange(2), plot_colors):
            idx = np.where(Y == labels[i])
            plt.scatter(X[idx, 0], X[idx, 1], c=c, label=labels[i],
                        cmap=cmap)


    plt.show()



def do_confusion_matrix(predictions,plot=True,save_plot=False,file_name='conf_matrix',file_type='pdf',path='./'):
    classes=np.unique(predictions['actual'])
    cm = confusion_matrix(predictions['actual'], predictions['pred'],labels=classes)
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print cm
    print cm_normalized
    fs=18-len(classes)
    if fs<9:
        fs=9
    if plot==True or save_plot==True:
        fig, ax = plt.subplots()
        ax.set_title('confusion matrix',fontsize=28)
        ind=np.arange(len(classes))
        #rects1 = ax.bar(ind,features_importance['imp'],width, color='r', yerr=features_importance['imp_err'])
        #rects1 = ax.bar(ind,features_importance['imp'],width, color='r',)
        (j,k)=  cm.shape
        for row in xrange(j):
            for col in range(k):

                plt.text( col+0.1,row+0.5,"%d\n%1.1f%%"%(cm[row,col],cm_normalized[row,col]*100),fontsize=fs)

        plt.pcolor(cm_normalized,cmap=plt.cm.Greens)
        plt.colorbar()
        ax.set_xticks(ind+0.5)
        ax.set_yticks(ind+0.5)
        ax.set_xticklabels( classes ,rotation= 0,fontsize=24)
        ax.set_yticklabels( classes ,rotation= 0,fontsize=24)
        ax.set_ylim([len(classes),0])
        #ax.set_xlim([-1,len(classes)+1])
        ax.set_xlabel('Predicted')
        ax.set_ylabel('Acutal')
        plt.tight_layout()

    if plot==True:
        plt.show()
    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name,file_type))
    print
    print



def do_roc_test(predictions,classes,label, plot=True,save_plot=False,file_name='roc_test',file_type='pdf',path='./'):
    y=predictions['actual']==label
    y=y*1
    prob_col=np.argwhere(classes==label)[0,0]

    fpr, tpr, thresholds = roc_curve(y,predictions['proba'][:,prob_col])
    roc_auc=auc(fpr,tpr)
    if save_plot==True or plot==True:
        fig, ax = plt.subplots()
        ax.set_title('Receiver Operating Characteristic')
        plt.plot(fpr, tpr, 'b',label='AUC = %0.2f'% roc_auc)
        plt.legend(loc='lower right')
        plt.plot([0,1],[0,1],'r--')
        ax.set_xlim([-0.1,1.2])
        ax.set_ylim([-0.1,1.2])
        ax.set_ylabel('True Positive Rate')
        ax.set_xlabel('False Positive Rate')
    if plot==True:
        plt.show()
    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name,file_type))
    return roc_auc,fpr, tpr, thresholds

def eval_feature_entropy(feature,labels,bins=10,range=None):


    n_labels=np.unique(labels)


    hist_feature,bin_edges=np.histogram(feature,bins=bins)

    P_feature=np.zeros((n_labels.size,hist_feature.size))
    H_fetaure=np.zeros((n_labels.size,hist_feature.size))
    H_class=np.zeros(n_labels.size)
    for ID,label in enumerate(n_labels):
        #print np.histogram(feature[labels==label],bins=bin_edges,normed=False)
        P_feature[ID],foo=np.histogram(feature[labels==label],bins=bin_edges,normed=False)
        msk=hist_feature>0.0
        P_feature[ID][msk]=np.float64(P_feature[ID][msk])/hist_feature[msk]
        H_fetaure[ID][msk]=-P_feature[ID][msk]*np.log(P_feature[ID][msk])
        H_fetaure[ID][P_feature[ID]==0.0]=0
        #print hist_tot,histogram_matrix[ID]
        class_p=np.float(labels[labels==label].size)/labels.size
        if class_p>0.0:
            H_class[ID]=-class_p*np.log(class_p)
        else:
            H_class[ID]=0

        H_fetaure[ID]

    H_class=H_class.sum()

    P_feature=P_feature.sum(axis=0)
    H_fetaure=H_fetaure.sum(axis=0)
    H_fetaure=P_feature*H_fetaure
    H_gain=H_class-H_fetaure

    return H_gain,H_fetaure,bin_edges

def do_plot_feature_entropy(feature,feature_name,labels,bins=10,normed=False,range=None,
                            file_name='feat_entropy',
                            plot=True,
                            save_plot=False,
                            path='./',
                            file_type='pdf' ):

    H_norm,H,bin_edges=eval_feature_entropy(feature,labels,bins=bins)
    #print H_norm,H,bin_edges
    x = (bin_edges[1:] + bin_edges[:-1]) / 2.0
    if plot==True or save_plot==True:
        fig, ax = plt.subplots()
        #print x,x.shape
        #print H
        ax.set_title(feature_name,fontsize=24)
        ax.plot(x,H,'-o')

    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name+'_'+feature_name,file_type))

    if plot==True:
        plt.show()
    if plot==True or save_plot==True:
        plt.close()



def plot_feature_histogram(feature,feature_name,labels,bins=10,range=None,normed=False,KDE=False,
                           gridsize=50,
                           pred=None,
                           plot=True,save_plot=False,
                           file_name='feat_histogram',
                           file_type='pdf',
                           path='./',
                           exclude_outliers=None,
                           ax=None):

    print 'feature name',feature_name
    if pred is None:
        labels=labels
    else:
        labels=pred

    l_labels=np.unique(labels)
    hist_data=[]
    hist_labels=[]

    for label in l_labels:
        feat=np.float64(feature[labels==label])
        if exclude_outliers is not None:
            m=feat.mean()
            s=feat.std()
            msk1=feat<m+exclude_outliers*s
            msk2=feat>m-exclude_outliers*s
            feat=feat[msk1*msk2]

        if range is None or range==[] :
            size=feat.size
        else:
            msk1=feat>range[0]
            if range[1] is not None:
                msk2=feat<range[1]
            else:
                msk2=msk1
            feat=feat[msk1*msk2]

            size=feat.size
        hist_data.append( feat )
        hist_labels.append(label+' N=%d'%size)
        print "label:",label," size:",size
        #print np.float64(feature[labels==label]).shape

    if plot==True or save_plot==True or ax is not None:
        import seaborn as sns
        sns.set_style("white")
        if ax is None:
            fig, ax = plt.subplots()
            ax.set_title(feature_name,fontsize=24)
        if KDE==False:
            ax.hist(hist_data,label=hist_labels,histtype='bar',bins=bins,normed=normed,stacked=False,fill=True)
        else:
            for ID,label in enumerate(hist_labels):
                #bandwidth = ((4 * hist_data[ID].std() ** 5) / (3 * len(hist_data[ID]))) ** .2
                #bandwidth = len(hist_data[ID]) ** (-1. / 5)
                if label[ID]=='E':
                    color='red'
                else:
                    color='blue'
                print "label",label,color
                sns.kdeplot(hist_data[ID],label=label,ax=ax,shade=True,gridsize=gridsize,color=color)
        ax.legend()
    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name+'_'+feature_name,file_type))

    if plot==True:
        plt.show()
    if plot==True or save_plot==True:
        plt.close()

    return

class validation_results(object):
    def __init__(self,label,tp,tn,fp,fn,size,scorr_classif=None,wrong_classif=None):

        self.label=label
        self.tp=tp
        self.tn=tn
        self.fp=fp
        self.fn=fn
        self.size=size
        #self.corr_classif=corr_classif
        #self.wrong_classif=wrong_classif



def average_cv_stats(validation_results_obj_list,labels):
    labels=np.unique(labels)


    print "------------------------------------------"
    tot_corr=0
    tot_wrong=0
    size_tot=0
    for label in labels:

        tp_tot=0
        tn_tot=0
        fp_tot=0
        fn_tot=0
        label_size=0


        for result in validation_results_obj_list:


            if result.label==label:

                tp_tot+=float(result.tp)
                tn_tot+=float(result.tn)
                fp_tot+=float(result.fp)
                fn_tot+=float(result.fn)
                #corr_av+=result.corr_classif
                #wrong_av+=result.wrong_classif

                #print "label",result.size,repl
                tot_corr+=result.tp
                tot_wrong+=result.fn

                label_size+=result.size



        size_tot+=label_size

        tp_rate=tp_tot/(tp_tot+fn_tot)
        fp_rate=fp_tot/(fp_tot+tn_tot)
        tn_rate=tn_tot/(fp_tot+tn_tot)
        fn_rate=fn_tot/(tp_tot+fn_tot)
        #tn_av=float(tn_av)/repl
        #fp_av=float(fp_av)/repl
        #fn_av=float(fn_av)/repl
        #corr_av=float(corr_av)/repl
        #wrong_av=flot(wrong_av)/repl

        print "  label:",label,
        print "  size:",label_size
        print "  tp_tot , rate:",tp_tot,tp_rate
        print "  tn_tot , rate:",tn_tot,tn_rate
        print "  fp_tot , rate:",fp_tot,fp_rate
        print "  fn_tot , rate:",fn_tot,fn_rate
        if tp_tot+fp_tot>0:
            print "  precision: ",tp_tot/(tp_tot+fp_tot)


        print "  recall:    ",tp_rate
        print "  correctly classified:",tp_tot , float(tp_tot)/(tp_tot+fn_tot)
        print "  wrongly classified:"  ,fn_tot , float(fn_tot)/(tp_tot+fn_tot)

        print
        print



    print "total size",   size_tot
    print "total correctly classified:",tot_corr,float(tot_corr)/size_tot
    print "tatal wrongly classified:", tot_wrong,float(tot_wrong)/size_tot
    print "------------------------------------------"

    return float(tot_corr)/size_tot,float(tot_wrong)/size_tot




def eval_accuracy(prediction,actual_classes,verbose=False):
    size=len(prediction)
    labels=np.unique(actual_classes)

    corr_classif=0
    wrong_classif=0
    out_list=[]
    print "------------------------------------------"
    for label in labels:



        msk_actual=actual_classes==label
        msk_pred=prediction==label
        msk_tp=msk_actual*msk_pred


        msk_tn=np.logical_not(msk_actual)*np.logical_not(msk_pred)

        tp=msk_tp.sum()
        tn=msk_tn.sum()

        msk_fp=msk_pred*np.logical_not(msk_actual)
        fp= msk_fp.sum()

        msk_fn=np.logical_not(msk_pred)*msk_actual
        fn=msk_fn.sum()

        corr_classif+=tp
        wrong_classif+=fn
        size=msk_actual.sum()

        if tp+fn>0:
            tp_rate=float(tp)/(tp+fn)
            fn_rate=float(fn)/(tp+fn)
        else:
            tp_rate=0
            fn_rate=0
        if fp+tn>0:
            fp_rate=float(fp)/(fp+tn)
            tn_rate=float(tn)/(fp+tn)
        else:
            fp_rate=0
            tn_rate=0


        if verbose==True:
            print "  label:",label
            print "  size:",size
            print "  tp:",tp,' rate',tp_rate
            print "  tn:",tn,' rate',tn_rate
            print "  fp:",fp,' rate',fp_rate
            print "  fn:",fn,' rate',fn_rate

            print "  correctly classified:",tp , float(tp)/(tp+fn)
            print "  wrongly classified:"  ,fn , float(fn)/(tp+fn)

            print " precision",  float(tp)/(tp+fp)

            print " accuracy",  float(tp)/(tp+fn)
            print

        out_list.append(validation_results(label,tp,tn,fp,fn,size))


    print
    print "total correctly classified:",corr_classif , float(corr_classif)/(corr_classif+wrong_classif)
    print "total wrongly classified:", wrong_classif , float(wrong_classif)/(corr_classif+wrong_classif)
    print "------------------------------------------"

    return out_list





def remove_field_name(data, skip_names):
    names = list(data.dtype.names)
    print names
    for del_name in skip_names:
        for name in names[:]:
            if del_name in name:
                print "removing ",name
                names.remove(name)

    data = data[names]
    return data,names

def use_field_name(data, use_names):
    use_list=[]
    names = list(data.dtype.names)
    print names

    for use_name in use_names:
        for name in names:
            if use_name in name:
                print "using ",name
                use_list.append(name)


    for name in names[:]:
        if name not in use_list:
            names.remove(name)
            print "removing ",name

    data = data[names]
    return data,names


def     contour_scatter_plot(x,y,classes,
                             classes_types,
                             H_cmap='grey',
                             x_name='x',
                             y_name='y',
                             n_levels=20,
                             smoothing_scale=3,
                             plot=False,
                             ax=None,
                             exclude_outliers=None,
                             plot_H=True,
                             plot_contour=True,
                             plot_scatter=True,
                             point_size=1,
                             color_list=None,
                             c_map_list=None,
                             lw=1):
    #colors=['b','r','g','y']
    #print x,y
    #colors=cm.rainbow(np.linspace(0,1,len(classes_types)+1))
    x=np.float64(x)
    y=np.float64(y)
    if color_list is None:
        color_list = ['red', 'blue', 'red', 'yellow','orange']
    if c_map_list is None:
        c_map_list = ['Reds', 'Blues', 'Reds', 'Oranges','Reds']



    if exclude_outliers is not None:
        msk=[None,None]
        data=[x,y]
        for ID,d in enumerate(data) :
                m=data[ID].mean()
                s=data[ID].std()
                msk1=data[ID]<m+exclude_outliers*s
                msk2=data[ID]>m-exclude_outliers*s
                msk[ID]=msk1*msk2



        x=data[0][msk[0]*msk[1]]
        y=data[1][msk[0]*msk[1]]
        classes=classes[msk[0]*msk[1]]

    #cmap_contorus = colors.ListedColormap(['blue', 'red','yellow','green','green'])
#     from matplotlib.colors import LinearSegmentedColormap
#
#     vmax = 3.0
#     cmap_contorus = LinearSegmentedColormap.from_list('mycmap', [(0 / vmax, 'blue'),
#                                                         (1 / vmax, 'white'),
#                                                         (3 / vmax, 'red')]
#                                             )
#
#
    if ax is None:
        fig, ax = plt.subplots()
    else:
        fig=None






    if plot==True or ax is not None:
        import seaborn as sns

        #extent = [xedges_tot[0], xedges_tot[-1], yedges_tot[0], yedges_tot[-1]]

        #cmap=plt.get_cmap(cmap_name)
        pass

    H_sublcasses=[]

    for ID,cl in enumerate(classes_types):
        x1=x[classes==cl]
        y1=y[classes==cl]
        print x1.shape
        print y1.shape


        H_tot, xedges_tot, yedges_tot = np.histogram2d(x, y, bins=(100,100))
        H_tot.shape, xedges_tot.shape, yedges_tot.shape

        H_s, xedges_s, yedges_s = np.histogram2d(x1, y1, bins=(xedges_tot,yedges_tot))
        H_sublcasses.append(H_s)
        #H_s_plot=gaussian_filter(H_s,smoothing_scale)
        H_s.shape, xedges_s.shape, yedges_s.shape
        extent = [xedges_tot[0], xedges_tot[-1], yedges_s[0], yedges_s[-1]]




        X, Y = np.meshgrid(xedges_s, yedges_s)
        positions = np.vstack([X.ravel(), Y.ravel()])
        values = np.vstack([x1, y1])
        if plot_contour==True:
            import scipy as sp
            kernel = sp.stats.gaussian_kde(values,)
            Z = np.reshape(kernel(positions).T, X.shape)


        #sns.jointplot(x1,y1, kind="kde")
        #if plot_contour==True:

        #    sns.kdeplot(x1,y1,ax=ax,cmap=c_map_list[ID])
        if plot==True or ax is not None:

            if plot_scatter==True:
                ax.scatter(x1,y1,s=point_size  ,color=color_list[ID],label=cl,alpha=1.0,marker='o')
            if plot_contour==True:
                vmin=Z.min()
                vmax=Z.max()*0.95
                levels = np.linspace(Z.min(),Z.max(),n_levels)
                #c=cmap_contorus(float(ID)/len(classes_types))
                #print c
                cfset = ax.contour(X,Y,Z ,cmap=c_map_list[ID],levels=levels,linewidths=lw,vmin=vmin,vmax=vmax)
                #print H_s_plot
                #cset = ax.contour(X,Y,H_s ,linewidths=1.5,cmap=c_map_list[ID],alpha=1.0,levels=levels,vmin=vmin,vmax=vmax)

                #cset=ax.contour(xedges_tot[:-1],yedges_tot[:-1],H_s_plot.T,linewidths=0.5,color=colors(float(ID)/len(classes_types)))
                #ax.clabel(cset, inline=1, fontsize=10, fmt='%1.0i')
                #print X.shape,H_s_plot.shape


    entr_tot=0
    N_tot=0
    for ID_r,H_tot_r in enumerate(H_tot):
        for ID_c,foo, in enumerate(H_tot_r):
            N=0
            entr=0

            for H_sublcass in H_sublcasses:
                ni=H_sublcass[ID_r,ID_c]
                N+=ni
                N_tot+=ni
                if ni>0:
                    entr+=ni*log(ni)
                    entr_tot+=ni*log(ni)
            if N>0:
                H_tot[ID_r,ID_c]=log(N)-(1./float(N))*entr
            else:
                H_tot[ID_r,ID_c]=0

            #msk1=H_sublcass[0][ID]>0.
            #msk2=H_sublcass[1][ID]>0.
            #msk=msk1+msk2
            #H_tot[ID][msk]=-(H_sublcass[0][ID][msk]-H_sublcass[1][ID][msk])/(H_sublcass[0][ID][msk]+H_sublcass[1][ID][msk])
            #H_tot[ID][np.negative(msk)]=0.0

            #size+=np.fabs(H_sublcass[0][ID][msk]+H_sublcass[1][ID][msk]).sum()

            #power+=(np.fabs(H_tot[ID][msk])*np.fabs(H_sublcass[0][ID][msk]+H_sublcass[1][ID][msk])).sum()

    entr_tot=log(N_tot)-(1./float(N_tot))*entr_tot
    print x_name,y_name,entr_tot

    if plot==True or ax is not None:
        if plot_H==True:
            H_plot=gaussian_filter(H_tot,smoothing_scale)
            p=ax.imshow(H_plot.T, cmap=H_cmap, extent=extent,origin='low',aspect='auto',zorder=-1,vmax=1,vmin=0,alpha=1.0)

            fig.colorbar(p)
        ax.legend(loc=4)
        #ax.legend(loc=4)
        ax.set_xlabel(x_name)

        ax.set_ylabel(y_name)

    if plot==True:
        plt.show()

    return x_name,y_name, entr_tot,fig


def plot_fetures_importance(features_importance,plot=True,save_plot=False,file_name='feat_imp',file_type='pdf',path='./'):
    fig, ax = plt.subplots()
    ax.set_title('features importance',fontsize=24)
    width=0.35
    ind=np.arange(features_importance.size)
    rects1 = ax.bar(ind,features_importance['imp'],width, color='r')
    #rects1 = ax.bar(ind,features_importance['imp'],width, color='r',)

    ax.set_xticks(ind)
    ax.set_xticklabels( features_importance['name'] ,rotation=90,fontsize=5)
    plt.tight_layout()
    if plot==True:
        plt.show()
    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name,file_type))
    print
    print


def plot_feature_correlation_matrix(features_names,features,plot=True,save_plot=False,file_name='feat_corr_matrix',file_type='pdf',path='./'):
    import matplotlib.cm as cm

    c=np.fabs(np.corrcoef(features,rowvar=0))


    if plot==True or save_plot==True:
        fig, ax = plt.subplots()
        ax.set_title('features correlation',fontsize=24)
        ind=np.arange(len(features_names))
        #rects1 = ax.bar(ind,features_importance['imp'],width, color='r', yerr=features_importance['imp_err'])
        #rects1 = ax.bar(ind,features_importance['imp'],width, color='r',)
        (j,k)=  c.shape
        for row in xrange(j):
            for col in range(k):

                plt.text( col,row+0.55,"%+2.2f"%c[row,col],fontsize=5)
        plt.pcolor(c,cmap=cm.jet)
        plt.colorbar()
        ax.set_xticks(ind+0.5)
        ax.set_yticks(ind+0.5)
        ax.set_xticklabels( features_names ,rotation=90,fontsize=12)
        ax.set_yticklabels( features_names ,rotation= 0,fontsize=12)
        ax.set_ylim([len(features_names)+1,-1])
        ax.set_xlim([-1,len(features_names)+1])
        plt.tight_layout()

    if plot==True:
        plt.show()
    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name,file_type))
    print
    print

def eval_features_importance(model,features_names,sort=True,path='./'):
    print model.feature_importances_.size
    features_importance = np.zeros(model.feature_importances_.size,dtype=[('name','S128'),('imp',np.float64),('imp_err',np.float64)])
    important_features_ID=[]
    for ID,feat_imp in enumerate(model.feature_importances_):
        #std = np.std([tree.feature_importances_[ID] for tree in model.estimators_],axis=0)
        #avg=  np.mean([tree.feature_importances_[ID] for tree in model.estimators_],axis=0)
        #   print features_names[ID],feat_imp,avg,std
        avg=feat_imp
        std=0
        features_importance[ID]=(features_names[ID],avg,std )
        important_features_ID.append(ID)
        #print model.feature_importances_[ID],avg,std
        #if feat_imp>np.average(model.feature_importances_):
        #    important_features_ID.append(ID)


    #print 'importance sorted features:'


    if sort==True:
        sorted_idx=np.argsort(features_importance['imp'])
        features_importance.sort(order='imp')
    else:
        sorted_idx=np.argsort(features_importance['imp'])

    #for feature in features_importance:
        #print feature

    return  np.array(features_importance),sorted_idx,important_features_ID


def eval_score(model,features,labels,cv=None):
    if cv is not None:
        scores = cross_val_score(model, features, labels, cv=cv)
        scores_mean=scores.mean()
        scores_std=scores.std()

    else:

        scores=[]
        scores_mean=model.oob_score_
        scores_std=0

    return scores,scores_mean,scores_std

def recursive_features_elimination(features_names,
                                   features,
                                   model,
                                   labels,
                                   percentile_th,
                                   feat_n_min,
                                   path='./',
                                   plot=False,
                                   save_plot=False,
                                   file_type='pdf',
                                   file_name='rec_feat_rem'):

    features_names=features_names[:]
    best_features_set=[f for f in features_names]

    model.fit(features, labels)
    if hasattr(model,'oob_score_'):
            cv=None
    else:
        cv=3
    scores,scores_mean,scores_std = eval_score(model,features,labels,cv=cv)

    best_score=scores_mean
    x_coord=[len( features_names)]

    y=[ scores_mean]
    dy=[scores_std]
    print "-->  score full features", scores_mean,scores_std ,'cv',cv



    features=np.copy(features)
    delta_l=1
    while len(features_names)>feat_n_min and delta_l>0:
        features_importance,sorted_idx,important_features_ID=eval_features_importance(model,features_names,sort=False)
        #print np.percentile(features_importance['imp'],percentile_th)
        th=np.percentile(features_importance['imp'],percentile_th)
        print "th=",th,features_importance['imp'].max()
        remove_list=[]
        for ID,f in enumerate(features_importance):

            if f['imp']<=th:
                print "removing ",features_importance[ID]['name']," importance",features_importance[ID]['imp']
                remove_list.append(ID)


        features=np.delete(features,remove_list,1)



        l1=len(features_names)
        features_names=[v for i, v in enumerate(features_names) if i not in remove_list]
        l2=len(features_names)
        delta_l=l1-l2
        print "delta l",delta_l,l1,l2
        print "FIT"
        model.fit(features, labels)
        scores,scores_mean,scores_std = eval_score(model,features,labels,cv=cv)


        x_coord.append(len( features_names))
        y.append( scores_mean)
        dy.append(scores_std*0.5)
        print "--> score after removing  %f +/- %f"%(scores_mean, scores_std)
        if scores_mean>best_score:
            best_score=scores_mean
            best_features_set=[f for f in features_names]



    if plot==True or save_plot==True:
        fig, ax = plt.subplots()



        title='recursive feature elimination'

        ax.set_title(title,fontsize=24)
        ax.errorbar(x_coord,y,yerr=dy, fmt='o-')
        ax.xaxis.set_tick_params(labelsize=18)
        ax.yaxis.set_tick_params(labelsize=18)
        ax.set_xlabel('features',fontsize=20)
        ax.set_ylabel('score',fontsize=20)


        plt.tight_layout()
    if  plot==True:
        plt.show()
    if  save_plot==True:
         plt.savefig('%s/%s.%s'%(path,file_name,file_type))

    return best_features_set






def show_single_feature(model,train_set,features_names,labels):
    features_names=features_names[:]



    for ID in xrange(len(features_names)):
        train_set=train_set[:,ID]
        print train_set
        model.fit(train_set, labels)
        scores_1 = cross_val_score(model, train_set, labels, cv=10)

        scores_1 = cross_val_score(model, train_set, labels, cv=10)
        scores_1_mean=scores_1.mean()
        scores_1_std=scores_1.std()
        x=[ID]
        y=[ scores_1_mean]
        dy=[scores_1_std]

    plt.errorbar(x,y,yerr=dy, fmt='o')
    plt.show()



def remove_most_corr_features(features,features_names,model,labels,max_corr,stage):
    REMOVED=False
    model.fit(features, labels)
    #print "features names",len(features_names),train_set.shape
    #print "train set shape",train_set.shape
    #print "features_names len",len(features_names)
    features_importance,sorted_idx,important_features_ID=eval_features_importance(model,features_names,sort=False)

    #start from then most important feature
    most_imp_id=0-stage
    sorted_idx= sorted_idx[::most_imp_id]
    for sel_id in sorted_idx[::-1]:
        print "working on feature", features_names[sel_id]

        #eval features corr matrix
        c_matr=np.corrcoef(features,rowvar=0)
        c_arr=np.asarray(c_matr[sel_id])
        #print c_arr, c_arr.shape



        #find the corr terms above max_corr
        cor_ids=np.where(np.fabs(c_arr)>max_corr)[0]

        #remove the diagonal term
        cor_ids_no_diag=cor_ids[cor_ids!=sel_id]

        print "selected ids",cor_ids_no_diag,c_arr[cor_ids_no_diag]
        #print "len c_arr",len(c_arr),features_importance.shape

        if cor_ids_no_diag.size>0 :
            #select less important feature among most correlated
            feature_ids=np.arange(features_importance['imp'].size)[[cor_ids_no_diag]]
            remove_id=np.argmin(features_importance['imp'][cor_ids_no_diag])
            remove_id=feature_ids[remove_id]
            for remove_id in cor_ids_no_diag:
                print "remove id",remove_id,len(features_names)
                #remove the feature from the set
                print "removing ",features_names[remove_id],features_importance['name'][remove_id], 'imp=',features_importance['imp'][remove_id], 'corr=',c_arr[remove_id]

            features_names=[name for ID,name in enumerate(features_names) if ID not in cor_ids_no_diag]
            features=np.delete(features,cor_ids_no_diag,1)

                #rfc.fit(features, labels)
                #scores_1 = cross_val_score(rfc, features, labels, cv=10)
                #scores_1_mean=scores_1.mean()
                #scores_1_std=scores_1.std()

                #print "--> new score", scores_1_mean,scores_1_std

            REMOVED=True
            return REMOVED,features,features_names

    return REMOVED,features,features_names

def remove_correlated_features(model,features,features_names,labels,max_corr,plot=False,save_plot=False,path='./'):




    REMOVED=True
    stage=1
    while REMOVED==True and len(features_names)>2:
        print "stage ",stage
        REMOVED,features,features_names=remove_most_corr_features(features,features_names,model,labels,max_corr,stage)
        stage+=1
        print
        print

    print "correlated featuere selection done"
    model.fit(features, labels)
    print "model score ",eval_score(model,features,labels)



    features_importance,sorted_idx,important_features_ID=eval_features_importance(model,features_names,sort=True)


    plot_fetures_importance(features_importance,plot=plot,save_plot=save_plot,path=path,file_name='feat_imp_post_corr_feat_removal')
    plot_feature_correlation_matrix(features_names,features,plot=plot,save_plot=save_plot,path=path)
    print "features shape",features.shape
    print "features names len",len(features_names)
    return features,features_names


def features_importance_trend(model,features,features_names,labels,n_trees,order_increasing=True,plot=False,save_plot=True,file_type='pdf',file_name='feat_imp_trend',path='./'):
    #rfc = RandomForestClassifier(n_estimators=n_trees)

    features_names=features_names[:]
    ind=np.arange(len(features_names))

    if order_increasing==True:
        sel_id=0
    else:
        sel_id=-1

    scores_1 = cross_val_score(model, features, labels, cv=10)
    scores_1_mean=scores_1.mean()
    scores_1_std=scores_1.std()
    x_coord=[len( features_names)]
    x_lab=['all']
    y=[ scores_1_mean]
    dy=[scores_1_std]
    print "--> score full features", scores_1_mean,scores_1_std
    model.fit(features, labels)
    features_importance,sorted_idx,important_features_ID=eval_features_importance(model,features_names)

    print "removing ",features_importance[sel_id]['name']," importance",features_importance[sel_id]['imp']
    features_names.pop(sorted_idx[sel_id])
    train_set=np.delete(features,sorted_idx[sel_id],1)
    scores_2 = cross_val_score(model, train_set, labels, cv=10)
    scores_2_mean=scores_2.mean()
    scores_2_std=scores_2.std()
    print "--> score after removing %s, %f +/- %f"%(features_importance[sel_id]['name'], scores_2_mean, scores_2_std)

    x_coord.append(len( features_names))
    x_lab.append(features_importance[sel_id]['name'])
    y.append( scores_2_mean)
    dy.append(scores_2_std*0.5)

    while len(features_names)>1:
    #while (scores_2_mean)>=(scores_1_mean):
        socres_1_mean=scores_2_mean
        socres_1_std=scores_2_std
        #rfc = RandomForestClassifier(n_estimators=n_trees)
        model.fit(train_set, labels)
        features_importance,sorted_idx,important_features_ID=eval_features_importance(model,features_names)
        features_names.pop(sorted_idx[sel_id])
        train_set=np.delete(train_set,sorted_idx[sel_id],1)
        print "removing ",features_importance[sel_id]['name']
        scores_2 = cross_val_score(model, train_set, labels, cv=10)
        scores_2_mean=scores_2.mean()
        scores_2_std=scores_2.std()
        #plot_fetures_importance(features_importance)

        x_coord.append(len( features_names))
        x_lab.append(features_importance[sel_id]['name'])
        y.append( scores_2_mean)
        dy.append(scores_2_std*0.5)
        print "--> score after removing %s, %f +/- %f"%(features_importance[sel_id]['name'], scores_2_mean, scores_2_std)

    if plot==True or save_plot==True:
        fig, ax = plt.subplots()


        if order_increasing==True:
            title='remove increasingly important features'
            file_name=file_name+'_incr'
        else:
            title='remove decreasingly important features'
            file_name=file_name+'_decr'

        ax.set_title(title,fontsize=24)
        ax.errorbar(x_coord,y,yerr=dy, fmt='o')
        ax.set_xticks(x_coord)
        ax.set_xticklabels( x_lab ,rotation=90)

        plt.tight_layout()
    if  plot==True:
        plt.show()
    if  save_plot==True:
         plt.savefig('%s/%s.%s'%(path,file_name,file_type))
    #return features_names,train_set

def main (argv=None):

    parser = argparse.ArgumentParser()

    parser.add_argument('sp_infile', type=str, help='fits file with  sp analysis results',default=None )
    parser.add_argument('ell_infile', type=str, help='fits file with ell analysis results',default=None )
    parser.add_argument('-test_set', type=str, help='fits file with ell analysis results',default=None )
    parser.add_argument('-skip_attr', type=str, nargs='+', help='fits file with ell analysis results',default=None )
    parser.add_argument('-ring_infile', type=str, nargs='+', help='fits file with ring analysis results',default=None )

    args = parser.parse_args()

    data_sp=np.array(pf.getdata(args.sp_infile))
    data_ell=np.array(pf.getdata(args.ell_infile))

    if args.test_set is not None:
        test_set=np.array(pf.getdata(args.test_set))
    else:
        test_set=None

    if args.ring_infile is not None:
        ring_data=np.array(pf.getdata(args.ring_infile))
    else:
        ring_data=None

    run(data_sp,data_ell,test_set=test_set,ring_data=ring_data)



def find_best_tree_num(model,features,labels,cv=True,plot=False,save_plot=False,file_name='best_tree_num',file_type='pdf',path='./'):
    if hasattr(model,'n_features'):
        n=model.n_features
    else:
        n=features.shape[1]
    n_min=max(1,n/4)
    n_max=max(1000,n*20)
    n_trees_arr=np.linspace(n_min,n_max,10)
    score_t=np.zeros(n_trees_arr.shape,dtype=np.dtype([('score', np.float64),('score_err',np.float64),(('n_trees', np.int32))]))
    for ID,n_trees in enumerate(n_trees_arr):
        #model = RandomForestClassifier(n_estimators=n_trees,oob_score=True)

        model.n_estimators=np.int(n_trees)
        model.fit(features, labels,)
        if hasattr(model,'oob_score_'):
            cv=False

        if cv==True:
            scores = cross_val_score(model, features, labels, cv=10)

            print
            print("Trees:%d Accuracy: %0.5f (+/- %0.5f)" % (n_trees,scores.mean(), scores.std()))
            #score_t.append(scores.mean())
            score_t[ID]=(scores.mean(),scores.std(),n_trees)
        else:
            print("Trees:%d OOB score: %0.5f " % (n_trees,model.oob_score_))
            score_t[ID]=(model.oob_score_,0,n_trees)

            #results.append( myEvaluationFunc(labels[testcv], [x[1] for x in probas]) )

        #print "Results: " + str( np.array(results).mean() )


    score_t=np.sort(score_t, order=['score', 'n_trees','score_err'])
    best_tree_num=score_t[-1]['n_trees']

    print "best trees number",best_tree_num

    if save_plot==True or plot==True:
        fig, ax = plt.subplots()
        ax.set_title('best  estimators num')
        plt.errorbar(score_t['n_trees'], score_t['score'] ,yerr=score_t['score_err'], fmt='o', label='best tree num = %d'% best_tree_num)
        plt.legend(loc='lower right')
        #plt.legend(loc='lower right')
        #plt.plot([0,1],[0,1],'r--')
        ax.set_xlim([n_trees_arr[0]*0.9,n_trees_arr[-1]*1.1])
        ax.set_ylim([score_t['score'].min()*0.9,score_t['score'].max()*1.1])
        ax.set_ylabel('accuracy')
        ax.set_xlabel('n estimators')
        #ax.set_xscale('log')
    if plot==True:
        plt.show()
    if save_plot==True:
        plt.savefig('%s/%s.%s'%(path,file_name,file_type))


    return best_tree_num

def do_MC(features,labels,n_repl=10):
    score_t=[]
    #rfc = RandomForestClassifier(n_estimators=n_trees)
        #print labels,new_data
    model=''
    for i in xrange(n_repl):


        model.fit(features, labels)
        scores = cross_val_score(model, features, labels, cv=10)
        print
        print(" Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std()*2))
        score_t.append(scores.mean())


            #results.append( myEvaluationFunc(labels[testcv], [x[1] for x in probas]) )

        #print "Results: " + str( np.array(results).mean() )

    plt.hist(score_t,bins=5)
    plt.xlabel('precision',fontsize=20)
    plt.ylabel('N',fontsize=20)
    plt.show()

    return np.array(score_t)

def  do_cross_fold_validation(features,labels,labels_num,original_id,model,n_folds=10,verbose=False):
    cv = StratifiedKFold(labels, n_folds=n_folds,shuffle=True)
    #print cv

    tot_size=0
    for traincv, testcv in cv:
        tot_size+=len(testcv)


    results = []
    ID_CV=0

    prediction_dtype=np.dtype([('image_id',np.int64),('actual', 'S16'),('pred', 'S16'),('error',np.bool),('proba', np.float64, (1,))])

    predictions=np.zeros(tot_size,prediction_dtype)

    for traincv, testcv in cv:
        #pred_class=[]

        if verbose==True:
            print"len(traincv), len(testcv), ID:", len(traincv),len(testcv)
        if ID_CV==0:
            print "FIT"
            pred = model.fit(features[traincv], labels[traincv]).predict_proba(features[testcv])
            labels_num=np.unique(labels[traincv]).size
        else:

            print "FIT"
            pred = model.fit(features[traincv], labels[traincv]).predict_proba(features[testcv])
            #pred = model.fit(features[traincv], labels[traincv])
            #pred_class=model.predict(features[testcv])
            #pred=np.zeros((len(testcv),len(model.classes_)))
            #for ID_1,pc in enumerate(pred_class):
            #    for ID_2,model_c in enumerate(model.classes_):
            #        if pc==model_c:
            #            pred[ID_1][ID_2]=1.0
        for ID,p in enumerate(pred):
            pmax_id=np.argmax(p)
            #pred_class.append(rfc.classes_[pmax_id])
            #print p, pmax_id, rfc.classes_[pmax_id]
            if verbose==True:
                print  "original id",original_id[testcv[ID]],' actual=',labels[testcv][ID], ' predicted=',model.classes_[pmax_id],' proba',p

            predictions['image_id'][ID+ID_CV]=original_id[testcv[ID]]
            predictions['actual'][ID+ID_CV]=labels[testcv][ID]
            predictions['pred'][ID+ID_CV]=model.classes_[pmax_id]
            predictions['error'][ID+ID_CV]=model.classes_[pmax_id]!=labels[testcv][ID]
            #print p
            #print  predictions['proba'][ID+ID_CV].shape, p.shape
            predictions['proba'][ID+ID_CV]=p.max()

        print predictions['pred'][ID_CV:ID_CV+ID+1].size,len(labels[testcv])
        accur_res=eval_accuracy(predictions['pred'][ID_CV:ID_CV+ID+1],labels[testcv],verbose=verbose)
        ID_CV+=ID+1
        results.extend(accur_res)
        #print len(results)
        if verbose==True:
            for p in predictions:
                if p['error']==True:
                    print "Image ID=%d , actual=%s , pred=%s"%(p['image_id'],p['actual'],p['pred'])


    corr_rate,wrong_rate=average_cv_stats(results,labels)


    return predictions,corr_rate,wrong_rate


def do_prediction(model,train_set_features,train_set_labels,test_set_features,test_set_labels,test_set_original_id,labels_num,verbose=False):





        prediction_dtype=np.dtype([('image_id',np.int64),('actual', 'S16'),('pred', 'S16'),('error',np.bool),('proba', np.float64, (labels_num,))])
        predictions=np.zeros(test_set_labels.size,prediction_dtype)
        if hasattr(model, 'predict_proba_ll'):
            print'predict proba'
            pred= model.fit(train_set_features, train_set_labels).predict_proba(test_set_features)
            pred_class=model.predict(test_set_features)
        else:
            #model.fit(train_set_features, train_set_labels)
            print "NOT FITTING"
            pred_class=model.predict(test_set_features)
            pred=np.zeros((len(test_set_labels),len(model.classes_)))
            for ID_1,pc in enumerate(pred_class):
                for ID_2,model_c in enumerate(model.classes_):
                    if pc==model_c:
                        pred[ID_1][ID_2]=1.0


        for ID,p in enumerate(pred):

            pmax_id=np.argmax(p)

            #print p,pred_class[ID]
            #print p, pmax_id, model.classes_[pmax_id]
            #print ID,len(pred),test_set_labels.size
            if (pred_class[ID]==test_set_labels[ID])==True:
                c=''
            else:
                c='+'
            if verbose==True:
                print  "ID= %4d"%test_set_original_id[ID], 'actual ',test_set_labels[ID], 'pred=',pred_class[pmax_id], 'error ',c,' proba',p

            predictions['image_id'][ID]=test_set_original_id[ID]
            predictions['actual'][ID]=test_set_labels[ID]
            predictions['pred'][ID]=pred_class[ID]
            predictions['error'][ID]=pred[ID]!=test_set_labels[ID]
            predictions['proba'][ID]=p


        eval_accuracy(predictions['pred']   ,test_set_labels,verbose=True)
        return predictions


class DataSet(object):

    def __init__(self,name,data_sets_list=[],class_labels_list=[],ID_field_name=None,shuffle=True,classifier='rfc',n_estimators=10):

        self.name=name
        self.class_labels_list=self.check_is_list(class_labels_list)
        self.data_sets_list=self.check_is_list(data_sets_list)
        self.features_names=None
        self.ID_field_name=ID_field_name
        self.originals_ID=None
        self.build(data_sets_list,shuffle=shuffle)
        self.classifier_name=classifier
        self.n_estimators=n_estimators


    def set_classifier(self,classifier_name=None,**kwargs):

        if classifier_name is None:
            classifier_name=self.classifier_name

        if classifier_name=='RFC':
            self.model=RandomForestClassifier(n_estimators=self.n_estimators,oob_score=True,min_samples_split=1)
        elif classifier_name=='AdaBoost':
            self.model=AdaBoostClassifier(n_estimators=self.n_estimators,learning_rate=0.025)
        elif classifier_name=='GradientBoosting':
            self.model=GradientBoostingClassifier(n_estimators=self.n_estimators,warm_start=False,verbose=False,  learning_rate=0.05,subsample=1.0  )
        elif classifier_name=='ExtraTrees':
            self.model=ExtraTreesClassifier(n_estimators=self.n_estimators)
        elif classifier_name=='DecisionTree':
            self.model=DecisionTreeClassifier(random_state=0)
        elif classifier_name=='GaussianNB':
            self.model=GaussianNB()
        elif classifier_name=='SGDC':
            self.model=SGDClassifier(loss="log", penalty="l2")
        elif classifier_name=='LDA':
            self.model=LDA(solver='svd',tol=0.00000000001)
        elif classifier_name=='QDA':
            self.model=QDA()
        elif classifier_name=='KN':
            self.model=KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
               metric_params=None, n_neighbors=50, p=20, weights='uniform')
        elif classifier_name=='KD':
            self.model=KernelDensity()
        elif classifier_name=='LogReg':
            self.model=LogisticRegression(penalty='L2')
        elif classifier_name=='LinearSVC':
            self.model=LinearSVC(loss='l2')
        elif classifier_name=='SVC':
            self.model=SVC(probability=True,kernel='rbf'    ,gamma=2,C=1)

        elif classifier_name=='Neural':
            classifier =LDA()
            rbm = BernoulliRBM(random_state=0, verbose=True,batch_size=4,learning_rate=1E-13,n_components=100000,n_iter=20)
            self.rbm=rbm
            self.model = Pipeline(steps=[('rbm', rbm), ('classifier', classifier)])

        else:
            raise RuntimeError('classifier %s not supported'%classifier_name)

        self.model.fit(self.features,self.class_labels)
        print "Model",self.model


    def check_is_list(self,l):
        if type(l)==list:
            return l
        else:
            raise RuntimeError("set list is not a list ")

    def split_test_set(self,test_fraction=None,image_ids=[]):
        test_set=copy.copy(self)
        test_set.name='test'
        if test_fraction is not None:
            rnd_id=np.arange(self.pop_size)
            np.random.shuffle(rnd_id)
            np.random.shuffle(rnd_id)
            np.random.shuffle(rnd_id)
            split=np.int(self.pop_size*test_fraction)

            test_ids=rnd_id[:split]
            train_ids=rnd_id[split:-1]
        else:
            test_ids=np.where(self.data['image_id']==image_ids)[1]
            train_ids=np.where(self.data['image_id']!=image_ids)[1]


        self.select_features_ids(train_ids)
        test_set.select_features_ids(test_ids)
        return test_set

    def build(self,data_sets_list=None,class_labels_list=None,shuffle=True,features_names=None):
        if data_sets_list is None:
            data_sets_list=self.data_sets_list

        if class_labels_list is None:
            class_labels_list=self.class_labels_list


        self.class_labels=[]

        if np.shape(data_sets_list)==():
            data_sets_list=[data_sets_list]
        if np.shape(class_labels_list)==():
            class_labels_list=[class_labels_list]


        self.data=stack_arrays(self.data_sets_list,asrecarray=True)
        print "--->",type(self.data)
        if features_names is None:
            self.features_names=list(self.data.dtype.names)


        data_list=[]
        for label_set,data_set in zip(class_labels_list,data_sets_list):
            if np.shape(label_set)==():
                label_set=np.asarray([label_set]*data_set.size)
            self.class_labels.extend(label_set)


        self.features= np.asarray(data_list)
        rows=self.data.shape[0]
        cols=len(self.features_names)
        if 'image_id' not in self.features_names:
            cols+=1
            self.data=np.lib.recfunctions.append_fields(self.data,'image_id',np.arange(rows),usemask=False)

            self.features_names.append('image_id')

        self.features=np.zeros((rows,cols),dtype=object)
        for ID,name in enumerate(self.features_names):
            self.features[:,ID]=self.data[name]

        self.features=np.array(self.features,dtype=np.float)

        self.class_labels=np.array(self.class_labels)
        self.class_types=np.unique(self.class_labels)
        self.class_num=self.class_types.size

        (self.pop_size,foo)=self.features.shape

        if self.ID_field_name is None:

            self.originals_ID=np.arange(self.pop_size)
        else:
            self.originals_ID=self.data[self.ID_field_name]

        if shuffle==True:
            self.shuffle_set()




    def  do_feat_std(self):
            print "features Standardization"
            from sklearn import preprocessing

            std_scale = preprocessing.StandardScaler().fit(self.features)
            self.features = std_scale.transform(self.features)

    def  do_pca(self):
            print "features PCA"
            from sklearn.decomposition import PCA

            pca = PCA().fit(self.features)
            self.features = pca.transform(self.features)


            for ID,feat_nane in enumerate(self.features_names):
                self.features_names[ID]='pca_%d'%ID


    def class_mapping(self,class_map_dic):
        for k in class_map_dic.keys():
            msk=self.class_labels==k
            self.class_labels[msk]=class_map_dic[k]


    def get_feature(self,feature_name,label=None,range=[None,None],get_image_id=False):
        if feature_name in  self.data.dtype.names:



            d=self.data[feature_name]
            ids=self.data['image_id']

            labels=self.class_labels


            if label is not None:
                msk=labels==label
                d=d[msk]
                ids=ids[msk]

            if range[0] is not None:
                msk=d>range[0]
                d=d[msk]
                ids=ids[msk]
            if range[1] is not None:
                msk=d<range[1]
                d=d[msk]
                ids=ids[msk]

        else:
            print "no feature found with name",feature_name
            print "features names are",self.data.dtype.names
            d=None
            ids=None

        if get_image_id==True:
            return d,ids
        else:
            return d


    def get_cv_feature(self,feature_name,range=[None,None],get_image_id=False,predicted=None,actual=None,cv_predictions=None):
        if feature_name in  self.data.dtype.names:



            if cv_predictions is None:
                cv_predictions=self.cv_predictions


            if predicted is not None:
                cv_predictions=cv_predictions[cv_predictions['pred']==predicted]
                #print cv_predictions.size
            if actual is not None:
                cv_predictions=cv_predictions[cv_predictions['actual']==actual]
                #print cv_predictions.size

            #select form data all the rows where image_id==image_id in cv_prediction 2d-array
            cv_images_ids=cv_predictions['image_id'].reshape(cv_predictions['image_id'].size,1)
            #print ids
            #print np.where(self.data['image_id']==ids)
            featuare_value_ids=np.where(self.data['image_id']==cv_images_ids)[1]

            featuare_values=np.take(self.data[feature_name],featuare_value_ids)
            image_ids=np.take(self.data['image_id'],featuare_value_ids)
            print range[0],range[1]
            if range[0] is not None:
                msk=featuare_values>range[0]
                featuare_values=featuare_values[msk]
                image_ids=image_ids[msk]
                #print featuare_values
            if range[1] is not None:
                msk=featuare_values<range[1]
                featuare_values=featuare_values[msk]
                image_ids=image_ids[msk]



        else:
            print "no feature found with name",feature_name
            print "features names are",self.data.dtype.names
            featuare_values=None
            image_ids=None

        if get_image_id==True:
            return featuare_values,image_ids
        else:
            return featuare_values

    def shuffle_set(self):
        (size,foo)=self.features.shape
        rnd_id=np.arange(size)
        np.random.shuffle(rnd_id)
        #print "shuffle,",rnd_id.shape,np.unique(rnd_id).shape
        self.select_features_ids(rnd_id)


    def show_features(self,image_id):

        d=self.data[self.data['image_id']==image_id]
        for feat_name in self.data.dtype.names:
            print feat_name,'=',d[feat_name]

    def skip_features(self,del_names):

        #print del_names
        use_list=self.features_names[:]
        for del_name in del_names:
            for name in use_list[:]:
                if del_name in name[:len(del_name)]:
                    #print "removing ",name,len(use_list)
                    use_list.remove(name)

        #print self.features_names
        self.use_features(use_list)

        #self.build( shuffle=False)

    def use_features(self, use_names,whole_name=False):
        print "feture before filtering",len(self.features_names)
        use_list=[]
        for use_name in use_names:
            for ID,name in enumerate(self.features_names):
                if whole_name==False:
                    if use_name in name[:len(use_name)]:
                        use_list.append(ID)
                else:
                    if use_name == name:
                        use_list.append(ID)
                    #print "using ",name,use_list

        use_list=np.unique(use_list)
        use_list.sort()
        use_names=[ self.features_names[i] for i in use_list]

        self.features_names=use_names[:]
        self.features = self.features[:,use_list]
        self.features = self.features[:,use_list]
        print "features left",len(self.features_names),self.features.shape[1]

    def skip_images_id(self,ids):
        print "skip images ids"

        print "size pre-filtering", self.data.shape
        ids=np.array(ids).reshape(len(ids),1)
        data_ids=np.where(self.data['image_id']!=ids)[1]
        self.select_features_ids(data_ids)
        print "size post-filtering", self.data.shape

    def skip_classes(self,skip_classes):
        for sk_c in skip_classes:
            print "skipping", sk_c
            msk_good=self.class_labels!=sk_c
            self.select_features_ids(msk_good)

        self.class_types=np.unique(self.class_labels)
        self.class_num=self.class_types.size

    def filter_nan(self):
        print "filtering nan"
        msk=np.ones(self.pop_size, dtype=np.bool)

        print "size pre-filtering", self.features.shape
        for ID in xrange(self.pop_size):
            #print type( self.features[ID]),np.shape(self.features[ID]),self.features[ID]
            #print self.features[ID][0]

            msk[ID]=True
            for a in self.features[ID]:
                if np.isinf(a)==True:
                    msk[ID]=False

                if np.isnan(a)==True:
                    msk[ID]=False

            #msk[ID]=np.isfinite(a).all()
            #if np.isfinite(self.features[ID]).all()==False:
            #    print ID


        #print self.originals_ID[msk]
        self.select_features_ids(msk)
        print "size post-filtering", self.features.shape
        print


    def select_features_ids(self,ids):
        print "select features ids,",self.originals_ID.shape,np.unique(self.originals_ID).shape
        print "pre selection shape", self.features.shape
        self.features=self.features[ids]
        print self.features.shape
        self.originals_ID=self.originals_ID[ids]
        self.class_labels=self.class_labels[ids]
        self.data=self.data[ids]

        (self.pop_size,foo)=self.features.shape
        print "post selection shape", self.features.shape
        print "select features ids,",self.originals_ID.shape,np.unique(self.originals_ID).shape
        print

    def write_weka_file(self,file_name):
        write_weka_file(file_name, self.features, self.features_names, self.class_labels)




    def plot_feature_correlation_matrix(self,plot=False,save_plot=False,path='./' ):
        plot_feature_correlation_matrix(self.features_names,self.features,plot=plot,save_plot=save_plot,path=path )

    def find_most_corr_features(self,feature_name):
        id=np.argwhere(np.array(self.features_names)==feature_name)[0][0]
        print self.features_names
        print "feature_id",feature_name,id
        c_matr=np.corrcoef(self.features,rowvar=0)
        #print c_matr.shape
        c_arr=c_matr[id]
        print c_arr.shape
        c_arr[id]=0.0
        id_c=np.argmax(np.fabs(c_arr))

        print "most corr feature",self.features_names[id_c],c_arr[id_c]
        for ID,corr in enumerate(c_arr):
            print self.features_names[ID],corr


    def remove_correlated_features(self,n_trees,max_corr,order_increasing=False,plot=False,save_plot=False,path='./' ):
        self.features,self.features_names=remove_correlated_features(self.model,self.features,self.features_names,self.class_labels,max_corr,plot=plot,save_plot=save_plot,path=path)
        print "features shape",self.features.shape
        print "features names len",len(self.features_names)

    def do_cv_confusion_matrix(self,plot=False,save_plot=False,path='./' ):
        do_confusion_matrix(self.cv_predictions,plot=plot,save_plot=save_plot,path=path)

    def do_MC(self,MC_repl,n_trees):
        self.MC_scores= do_MC(self.model,self.features,self.class_labels,n_repl=MC_repl)


    def predict_test_set(self,test_set):
        test_set.use_features(self.features_names)
        test_set.predictions=do_prediction(self.model,self.features,self.class_labels,test_set.features,test_set.class_labels,test_set.originals_ID,self.class_num,verbose=False)


    def find_best_tree_num(self,plot=False,save_plot=False,path='./'):
        self.n_estimators=find_best_tree_num(self.model,self.features, self.class_labels,plot=plot,save_plot=save_plot,path=path)
        self.set_classifier(self.classifier_name)

    def get_cross_validation_score(self,cv=10):
        return cross_val_score(self.model, self.features, self.class_labels, cv=cv)


    def do_cross_fold_validation(self,n_folds=10,verbose=False):
        print "fatures num",self.features.shape
        self.cv_predictions,self.cv_corr_rate,self.cv_wrong_rate=do_cross_fold_validation(self.features,self.class_labels,self.class_num,self.data['image_id'],self.model,n_folds=n_folds,verbose=verbose)
        self.cv_predictions.sort()

    def print_cv_predictions(self):
        for p in self.cv_predictions:
            print p

    def cv_predictions_errors(self,label_class=None):

        if label_class is not None:
            label_class=[label_class]

        for label in self.class_types:
            print "class",label
            msk_class=self.cv_predictions['actual']==label
            msk_err=self.cv_predictions['error']==True
            print self.cv_predictions['image_id'][msk_err*msk_class]
            print




    def recursive_features_elimination(self,
                                       percentile_th=5,
                                       feat_n_min=3,
                                       path='./',
                                       plot=False,
                                       save_plot=False,
                                       file_type='pdf',
                                       file_name='rec_feat_rem'):

        best_feat=recursive_features_elimination(self.features_names,
                                       self.features,
                                       self.model,
                                       self.class_labels,
                                       percentile_th=percentile_th,
                                       feat_n_min=feat_n_min,
                                       path=path,
                                       plot=plot,
                                       save_plot=save_plot,
                                       file_type=file_type,
                                       file_name=file_name)

        file_name='feat_imp_post_rec_rem'
        self.use_features(best_feat,whole_name=True)
        self.model.fit(self.features,self.class_labels)
        self.features_importance,sorted_idx,important_features_ID=eval_features_importance(self.model,self.features_names,sort=True)
        plot_fetures_importance(self.features_importance,plot=plot,save_plot=save_plot,path=path,file_type=file_type,file_name=file_name)
        joblib.dump(self.features_importance,path+'/feat_imp_post_rec_feat_rem.pkl')

    def eval_features_importance(self,sort=True,plot=False,save_plot=False,path='./',file_type='pdf',file_name=None):



        self.features_importance,sorted_idx,important_features_ID=eval_features_importance(self.model,self.features_names,sort=sort,path=path)
        if file_name is None:
            file_name='feat_imp_no_filtered'
        th=self.features_importance['imp'].mean()
        #use_list=[]
        #for i in self.features_importance:
        #    if i['imp']>th:
        #        use_list.append(i['name'])

        #self.use_features(use_list,whole_name=True)
        #self.model.fit(self.features,self.class_labels)
        plot_fetures_importance(self.features_importance,plot=plot,save_plot=save_plot,path=path,file_type=file_type,file_name=file_name)
        joblib.dump(self.features_importance,path+'/feat_imp.pkl')


    def do_features_importance_trend(self,n_trees,order_increasing=True,plot=False,save_plot=False,path='./',file_type='pdf'):
        features_importance_trend(self.model,self.features,self.features_names,self.class_labels,n_trees,order_increasing=order_increasing,plot=plot,save_plot=save_plot,path=path,file_type=file_type)


    def plot_feature_histogram(self,feature_name=None,bins=10,range=None,normed=False,KDE=False,plot=False,save_plot=False,path='./',file_type='pdf',ax=None,exclude_outliers=None):
        if feature_name is not None:
            for name in self.features_names:
                if feature_name in name:
                    data=self.get_feature(name)
                    labels=self.class_labels
                    return plot_feature_histogram(data,name,labels,bins=bins,range=range,normed=normed,KDE=KDE,plot=plot,save_plot=save_plot,path=path,file_type=file_type,ax=ax,exclude_outliers=exclude_outliers)
        else:
            for feature_name in self.features_names:
                data=self.get_feature(feature_name)
                labels=self.class_labels
                return plot_feature_histogram(data,feature_name,labels,bins=bins,range=range,normed=normed,KDE=KDE,plot=plot,save_plot=save_plot,path=path,file_type=file_type,ax=ax,exclude_outliers=exclude_outliers)

    def plot_cv_feature_histogram(self,feature_name=None,bins=10,range=None,normed=False,cv_error=False,cv_predictions=None):
        if cv_predictions is None:
            cv_predictions=self.cv_predictions

        if feature_name is not None:
            for name in self.features_names:
                if feature_name in name:
                    data=self.get_cv_feature(name,cv_predictions=cv_predictions)
                    if cv_error==True:
                        error=cv_predictions['error']
                        labels=np.zeros(error.shape,dtype='S16')
                        labels[error==True]='w'
                        labels[error!=True]='r'
                    else:
                        labels=cv_predictions['pred']

                    plot_feature_histogram(data,name,labels,bins=bins,range=range,normed=normed)
        else:
            for feature_name in self.features_names:
                data=self.get_cv_feature(feature_name,cv_predictions=cv_predictions)
                if cv_error==True:
                    error=cv_predictions['error']
                    labels=np.zeros(error.shape,dtype='S16')
                    labels[error==True]='w'
                    labels[error!=True]='r'
                else:
                    labels=cv_predictions['pred']

                plot_feature_histogram(data,feature_name,labels,bins=bins,range=range,normed=normed)

    def plot_feature_entropy(self,feature_name=None,bins=10,range=None,normed=False,plot=False,save_plot=False,path='./'):
        if feature_name is not None:
            for name in self.features_names:
                if feature_name in name:
                    do_plot_feature_entropy(self.get_feature(name),name,self.class_labels,bins=bins,range=range,normed=normed,plot=plot,save_plot=save_plot,path=path)
        else:
            for feature_name in self.features_names:
                do_plot_feature_entropy(self.get_feature(feature_name),feature_name,self.class_labels,bins=bins,range=range,normed=normed,plot=plot,save_plot=save_plot,path=path)



    def get_feature_cut(self,feature_name,feat_range):
        msk=np.ones(self.get_feature(feature_name).shape,dtype=np.bool)

        if feat_range is not None or feat_range!=[]:
            if feat_range[0] is not None:
                msk1=self.get_feature(feature_name)>feat_range[0]
            else:
                msk1=msk
            if feat_range[1] is not None:
                msk2=self.get_feature(feature_name)<feat_range[1]

            else:
                msk2=msk

            msk=msk1*msk2

        return msk

    def plot_contour_scatter_plot(self,feature_name_x,
                                  feature_name_y,
                                  H_cmap='summer_r',
                                  n_levels=30,
                                  smoothing_scale=1,
                                  plot=False,
                                  labels=None,
                                  ax=None,
                                  exclude_outliers=None,
                                  plot_H=True,
                                  plot_contour=True,
                                  plot_scatter=True,
                                  x_range=[None,None],
                                  y_range=[None,None],
                                  point_size=1,
                                  color_list=None,
                                  c_map_list=None,
                                  lw=1,
                                  classes=None):

        msk=np.ones(self.get_feature(feature_name_x).shape,dtype=np.bool)

        if x_range is not None or x_range!=[]:
            msk1=self.get_feature_cut(feature_name_x,x_range)
            msk=msk*msk1

        if y_range is not None or y_range!=[]:
            msk1=self.get_feature_cut(feature_name_y,y_range)
            msk=msk*msk1


        y=self.get_feature(feature_name_y)[msk]
        x=self.get_feature(feature_name_x)[msk]

        if labels is None:
            labels=self.class_labels[msk]
            class_types=np.unique(labels)

        else:
            labels=labels[msk]
            class_types=np.unique(labels)

        if classes is not None:
            class_types=classes

        return contour_scatter_plot(x,
                                    y,
                                    labels,
                                    class_types,
                                    H_cmap=H_cmap,
                                    x_name=feature_name_x,
                                    y_name=feature_name_y,
                                    n_levels=n_levels,
                                    smoothing_scale=smoothing_scale,
                                    plot=plot,
                                    ax=ax,
                                    exclude_outliers=exclude_outliers,
                                    plot_H=plot_H,
                                    plot_contour=plot_contour,
                                    plot_scatter=plot_scatter,
                                    point_size=point_size,
                                    color_list=color_list,
                                    c_map_list=c_map_list,
                                    lw=lw)

    def decision_tree_pre_filter(self,th=0.75):
        black_list=[]
        for (ID,feature_name) in enumerate(self.features_names):
            print "feature",feature_name
            X=self.get_feature(feature_name)
            clf=tree.DecisionTreeClassifier(criterion="entropy", max_depth=3)

            y=np.array([ 1 if lab== 'S' else 0 for lab in self.class_labels])
            #print y
            X=X.reshape((X.size,1))
            print X.shape
            clf.fit(X,y)

            score = cross_val_score(clf, X, y, cv=10)
            print "score",score.mean()
            if score.mean()<th:
                black_list.append(feature_name)

        self.skip_features(black_list)

    def entroy_pre_filter(self,th=0.75):
        black_list=[]
        for (ID,feature_name) in enumerate(self.features_names):

            H_gain,H,bin_edges=eval_feature_entropy(self.get_feature(feature_name),self.class_labels,bins=100,range=None)

            print "feature",feature_name,'H gain',H_gain


            if H_gain<th:
                black_list.append(feature_name)

        self.skip_features(black_list)

def run(training_set_data_list,training_set_labels_list,test_set_data_list=None,test_set_labels_list=None,
        test_set_split_fraction=None,
        test_set_split_ids=None     ,
        skip_attr=[],
        use_attr=[],
        skip_class=[],
        skip_images_ids=None,
        classifier='RFC',
        skip_nan_entries=True,
        n_estimators=100,
        do_pca=False,
        do_feat_std=False,
        eval_best_tree_num=False,
        eval_feature_importance=False,
        eval_feature_importance_trend=False,
        feature_correlation_matrix=False,
        cross_fold_N=None,
        MC_repl=None,
        max_par_corr=None,
        feature_name=None,
        feature_min=None,
        feature_max=None,
        verbose=False,
        test_th=None,
        entropy_th=None,
        class_mapping_dic=None,
        select_features_ids=None,
        plot=False,
        save_plot=False,
        roc_test_class=None,
        cv_conf_matrix=False,
        path='./'):






    data_sets_list=[]
    training_set=DataSet('training',training_set_data_list,training_set_labels_list,classifier=classifier,n_estimators=n_estimators,)

    if class_mapping_dic is not None:
        training_set.class_mapping(class_mapping_dic)
    print"training_set", training_set.originals_ID.shape
    print"training_set",np.unique(training_set.originals_ID).shape


    data_sets_list.append(training_set)

    if test_set_data_list is not None and test_set_labels_list is not None:
        test_set=DataSet('test',test_set_data_list,test_set_labels_list,classifier=classifier,n_estimators=n_estimators)
        data_sets_list.append(test_set)
    else:
        test_set=None

    if test_set_split_fraction is not None and test_set_data_list is None and test_set_labels_list is None:
        test_set=training_set.split_test_set(test_set_split_fraction)
        data_sets_list.append(test_set)

    elif test_set_split_ids is not None and test_set_data_list is None and test_set_labels_list is None:
        test_set=training_set.split_test_set(image_ids=test_set_split_ids)

    if select_features_ids is not None and select_features_ids!=[]:

        for data_set in data_sets_list:
            if data_set.name=='training':
                data_set.select_features_ids(select_features_ids)

    if skip_images_ids is not None:
         for data_set in data_sets_list:
            if data_set.name=='training':
                print "training set images ids filtering"
                data_set.skip_images_id(skip_images_ids)

    for data_set in data_sets_list:

        if 'success' in data_set.features_names:
            print "filtering success"
            msk_good=data_set.data['success']>0
            data_set.select_features_ids(msk_good)



    if  feature_name is not None and (feature_min is not None or feature_max is not None) :
        if feature_name in data_set.features_names:
            for data_set in data_sets_list:
                if feature_min is not None:
                    msk_good_1=data_set.data[feature_name]>feature_min
                    data_set.select_features_ids(msk_good_1)
                if feature_max is not None:
                    msk_good_2=data_set.data[feature_name]<feature_max
                    data_set.select_features_ids(msk_good_2)


    if skip_class!=[]:
        print "filtering classes",skip_class
        for data_set in data_sets_list:
            data_set.skip_classes(skip_class)


    if use_attr!=[]:
        print "filtering attr, use",use_attr
        for data_set in data_sets_list:
            data_set.use_features(use_attr)

    if skip_attr!=[]:
        print "filtering attr, skip",skip_attr
        for data_set in data_sets_list:
            data_set.skip_features(skip_attr)

    if skip_nan_entries==True:
        for data_set in data_sets_list:
            print "filtering nan"
            data_set.filter_nan()


    for data_set in data_sets_list:
        data_set.filter_nan()

    if test_th is not None:
        black_list=[]
        import itertools as it
        from matplotlib.backends.backend_pdf import PdfPages
        pdf_pages = PdfPages('multi.pdf')

        for i,j in it.combinations(xrange(len(training_set.features_names)),2):
            f_name_1,f_name_2,p,plot= training_set.plot_contour_scatter_plot(training_set.features_names[i],training_set.features_names[j],cmap='bwr',smoothing_scale=2,n_levels=10,plot=True)
            if p< test_th:
                black_list.append(f_name_1)
                black_list.append(f_name_2)
            else:
                pdf_pages.savefig(plot)
        pdf_pages.close()

        training_set.skip_features(black_list)

    if entropy_th is not None:
        training_set.entroy_pre_filter(th=entropy_th)


    for data_set in data_sets_list:
        if data_set.name=='training':
            data_set.set_classifier()

    if eval_feature_importance==True:
        training_set.eval_features_importance(sort=True,plot=plot,save_plot=save_plot,path=path)

        training_set.recursive_features_elimination(percentile_th=10,
                                       feat_n_min=5,
                                       path=path,
                                       plot=plot,
                                       save_plot=save_plot)

        for data_set in data_sets_list:
            if data_set.name=='training':
                data_set.set_classifier()

    if do_feat_std==True:
        training_set.do_feat_std()

    if do_pca==True:
        training_set.do_pca()

        if eval_feature_importance==True:
            training_set.eval_features_importance(sort=True,plot=plot,save_plot=save_plot,path=path,remove=False,file_name='feat_imp_pca')





            for data_set in data_sets_list:
                    if data_set.name=='training':
                        data_set.set_classifier()


    if eval_feature_importance_trend==True:
        training_set.do_features_importance_trend(n_estimators,save_plot=save_plot,path=path)
        training_set.do_features_importance_trend(n_estimators,order_increasing=False,save_plot=save_plot,path=path)


    if feature_correlation_matrix!=False:
        training_set.plot_feature_correlation_matrix()

    if max_par_corr is not None:
        training_set.remove_correlated_features(n_estimators,max_par_corr,plot=plot,save_plot=save_plot,path=path)
        if test_set is not None:
            test_set.use_features(training_set.features_names,whole_name=True)

    if eval_best_tree_num==True:
        training_set.find_best_tree_num(plot=plot,save_plot=save_plot,path=path)


    if MC_repl is not None:
        training_set.do_MC(MC_repl,n_estimators)


    if cross_fold_N is not None:
        training_set.do_cross_fold_validation(n_folds=cross_fold_N,verbose=verbose)

    if cv_conf_matrix==True:
        training_set.do_cv_confusion_matrix(plot=plot,save_plot=save_plot,path=path)

    if roc_test_class is not None:
        training_set.cv_roc_auc,fpr, tpr, thresholds=do_roc_test(training_set.cv_predictions,training_set.model.classes_,label=roc_test_class, plot=plot,save_plot=save_plot,path=path)

    if test_set is not None:
        if feature_correlation_matrix!=False:
            test_set=training_set.split_test_set(test_set_split_fraction)
            test_set.plot_feature_correlation_matrix()


        training_set.predict_test_set(test_set)
        if 1==2:
            pred_original=np.copy(test_set.predictions)




            training_set.set_classifier('RFC')
            print len(training_set.class_labels),training_set.features.shape, test_set.features.shape
            training_set.predict_test_set(test_set)
            pred_2=np.copy(test_set.predictions)
            pred_list=[pred_2]
            for pred in pred_list:

                for p in pred:
                    ID=np.argmax(pred_original['image_id']==p['image_id'])


                    proba_1=pred_original[ID]['proba'].max()
                    proba_2=p['proba'].max()
                    if proba_1>=proba_2:
                        pred_original[ID]['pred']=p['pred']


            eval_accuracy(pred_original['pred']   ,pred_original['actual'],verbose=True)




    return training_set, test_set




def load_data(files_list,label_file=False,label_size=32,label_col_name='class',ext=0,skip_ids=None,max_id=None,msk=None):
        if np.shape(files_list)==():
            files_list=[files_list]

        data_sets_list=[]
        for f in files_list:
            print "loading file",f
            try:
                if label_file==True:
                    print  "label col name",label_col_name
                    data_class=np.array(pf.getdata(f,ext=ext)[label_col_name])
                    data=np.chararray(data_class.shape,label_size)

                    for ID,c in enumerate(data_class):
                        data[ID]=c

                    print data
                else:
                    data=np.array(pf.getdata(f,ext=ext))
            except:
                try:
                    if label_file==True:
                        data=np.genfromtxt(f, dtype=[('image_id',np.int64),('class','S%d'%label_size)])[label_col_name]
                    else:
                        data=np.genfromtxt(f)
                except:
                    raise RuntimeError("data  file %s, is neither txt nor fits, or the column %s is not present in "%(f,label_col_name))

            if skip_ids is not None and msk is None:
                msk=np.ones(data.shape,dtype=np.bool)
                msk[skip_ids]=False
                data=data[msk]
            elif skip_ids is None and msk is not None:
                data=data[msk]

            elif max_id is not None and skip_ids is None and msk is None:
                data=data[:max_id+1]

            data_sets_list.append(data)


        return data_sets_list





if __name__=="__main__":
    main()


