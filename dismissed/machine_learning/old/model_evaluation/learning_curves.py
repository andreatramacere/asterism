"""
"""
from __future__ import division, absolute_import, print_function
import  numpy as np
from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn.learning_curve import learning_curve,validation_curve
from sklearn.metrics import scorer
import  types
import inspect


__author__ = 'andrea tramacere'




def do_learning_curve(model,features_set,sub_sample_sizes_ratios_array,scoring=None,cv_n_folds=3,sampling_array=None):
    """
    Produces a learning curve using a cross-fold validation, and train/test scoring.
    If train_sizes_ratios_array=np.array([0.1,0.5,0.8]) a sub features sets are extracted according
    to sub_sizes_cv=train_sizes_ratios_array*features_set.features_N_rows and cv is performed on each
    sub features_set with sizes sub_sizes_cv
    A train/test score is also extracted for a train/test extracted with train_sizes_ratios_array

    Args:
        model:

        features_set:

        sub_sample_sizes_ratios_array: array-like, shape (n_ticks,), dtype float or int.


            Relative (for float) or absolute (for int) numbers of training examples that will be used to generate the learning curve

        scoring : string, callable or None, optional, default: None
            A string (see model evaluation documentation in skleran) or
            a scorer callable object / function with signature
            ``scorer(estimator, X, y)``.

        cv_n_folds: int, default 3

        sampling_array: array-like, optional, a stratified KFold is performed stratifying on sampling_array

    Returns:
        sub_sizes: array, shape = (n_unique_ticks,), dtype int
        Numbers of training examples that has been used to generate the learning curve.

        Note that the number of ticks might be less than n_ticks because duplicate entries will be removed.

        cv_train_scores: array, shape (n_ticks, n_cv_folds) Scores on training sets

        cv_test_scores: array, shape (n_ticks, n_cv_folds) Scores on training sets

        train_score:

        test_score:

    """
    if type(scoring) == types.FunctionType:
        if len(inspect.getargspec(scoring)[0])!=3:
            raise RuntimeError('scoring function needs a signature: scorer(estimator, X, y)')
        else:
            eval_model_sore=scoring

    elif type(scoring)==types.StringType:
        eval_model_sore=scorer.get_scorer(scoring)

        print("scorer",eval_model_sore)

    if sub_sample_sizes_ratios_array.ndim!=1:
        raise RuntimeError('sub_sample_sizes_ratios_array must be a 1d numpy array')

    size=sub_sample_sizes_ratios_array.size

    test_score=np.zeros(size)
    train_score=np.zeros(size)
    train_sizes=np.zeros(size)

    if sampling_array is not None:
        target_bins=np.digitize(features_set.target,np.linspace(sampling_array.min(),sampling_array.max(),50))
        cv = StratifiedKFold(target_bins, n_folds=cv_n_folds,shuffle=True)
        print("stratified CV")
    else:
        cv=KFold(features_set.features_N_rows, n_folds=cv_n_folds, shuffle=True)

    print('learning curves',cv)
    cv_train_sizes, cv_train_scores, cv_test_scores = learning_curve(model, \
                                                            features_set.features,\
                                                            features_set.target, \
                                                            cv=cv, n_jobs=4,\
                                                            train_sizes=sub_sample_sizes_ratios_array,\
                                                            verbose=True,\
                                                            scoring=scoring)


    #cv_test_sizes=(1./float(cv_n_folds))*train_sizes_array*features_set.features_N_rows


    sub_sample_sizes_ratios_array=np.float32(cv_train_sizes/float(features_set.features_N_rows))
    for ID,train_test_split_ratio in enumerate(sub_sample_sizes_ratios_array):
        #Test size=(1.0-train_ratio)*full_sample_size
        train_features_set,test_features_set=features_set.split_train_test(train_test_split_ratio,sampling_array= sampling_array )



        model.fit(train_features_set.features,train_features_set.target)

        test_score[ID]=eval_model_sore(model,test_features_set.features,test_features_set.target)
        train_score[ID]=eval_model_sore(model,train_features_set.features,train_features_set.target)
        train_sizes[ID]=train_features_set.features_N_rows

        #print("train_test ratio",sub_sample_sizes_ratios_array[ID], 'test err',test_score[ID], 'test size',test_features_set.features_N_rows)



    if scoring=='mean_squared_error':
        cv_train_scores=-1*cv_train_scores
        cv_test_scores=-1*cv_test_scores
        test_score=-1*test_score

    return cv_train_sizes,cv_train_scores,cv_test_scores,train_sizes,train_score,test_score



def plot_learning_curve(cv_train_scores,cv_test_scores,cv_train_sizes,full_size,train_sizes=None,train_score=None,test_score=None,plot=False,scorer_name='score'):
    """
    Plots the learning_curves

    Args:
        cv_train_scores: cross validation train scores

        cv_test_scores:  cross validation test scores

        sub_sizes: array-like, shape (n_ticks,) Numbers of training examples that has been used to generate the learning curve.

        full_size: Num of rows in the full features set, i.e. number of the full featues set training examples

        train_score: optional, the array of train set scores

        test_score: optional, the array of test set scores

        plot: boolean, optional

    Returns:

    """

    import pylab as plt
    import matplotlib.ticker as ticker
    import matplotlib
    matplotlib.rcParams['xtick.labelsize'] = 18
    matplotlib.rcParams['ytick.labelsize'] = 18


    cv_train_scores_mean = np.mean(cv_train_scores, axis=1)
    cv_train_scores_std = np.std(cv_train_scores, axis=1)
    cv_test_scores_mean = np.mean(cv_test_scores, axis=1)
    cv_test_scores_std = np.std(cv_test_scores, axis=1)

    fig, (ax1) = plt.subplots()
    fig.suptitle('learning curve',fontsize=18)
    ax1.grid()


    cv_train_sizes_ratios=np.float32(cv_train_sizes)/full_size
    ax1.fill_between(cv_train_sizes_ratios, cv_train_scores_mean - cv_train_scores_std,
                     cv_train_scores_mean + cv_train_scores_std, alpha=0.1,
                     color="g")
    ax1.plot(cv_train_sizes_ratios, cv_train_scores_mean, 'o-', color="g",
             label=" Training score")

    ax1.fill_between(cv_train_sizes_ratios, cv_test_scores_mean - cv_test_scores_std,
                     cv_test_scores_mean + cv_test_scores_std, alpha=0.1, color="b")
    ax1.plot(cv_train_sizes_ratios, cv_test_scores_mean, 'o-', color="b",
             label=" CV score")

    if train_sizes is not None and test_score is not None:
        train_size_ratios=np.float32(train_sizes)/full_size
        ax1.plot(train_size_ratios, test_score, 'o-', color="orange",
                 label="Test score")

    ax1.legend(loc="best",fontsize=18)
    ax1.set_xlabel('cv train sizes ratios',fontsize=18)
    ax1.set_ylabel('%s'%scorer_name,fontsize=18)
    ymin, ymax = ax1.get_ylim()
    delta=ymax-ymin
    thick_space=int(delta/10)
    if thick_space>0.:
        ax1.yaxis.set_major_locator(ticker.MultipleLocator(thick_space))

    #ax1.set_ylim([-0.1,ymax])
    plt.tight_layout()
    if plot==True:
        plt.show()

    return fig


def do_validation_curve(model,features_set,param_name,param_values_array,scoring,cv_n_folds=3,sampling_array=None,train_test_split_ratio=0.5):
    """

    Parameters
    ----------
    model
    features_set
    param_name
    param_values_array
    scoring
    cv_n_folds
    sampling_array
    train_test_split_ratio

    Returns
    -------

    """
    size=param_values_array.size
    score_test_array=np.zeros(size)


    train_features_set,test_features_set=features_set.split_train_test(train_test_split_ratio,sampling_array= sampling_array )

    if type(scoring) == types.FunctionType:
        if len(inspect.getargspec(scoring)[0])!=3:
            raise RuntimeError('scoring function needs a signature: scorer(estimator, X, y)')
        else:
            eval_model_sore=scoring

    elif type(scoring)==types.StringType:
        eval_model_sore=scorer.get_scorer(scoring)



    if sampling_array is not None:
        target_bins=np.digitize(train_features_set.target,np.linspace(sampling_array.min(),sampling_array.max(),50))
        cv = StratifiedKFold(target_bins, n_folds=cv_n_folds,shuffle=True)
        print("stratified CV")
    else:
        cv=KFold(train_features_set.features_N_rows, n_folds=cv_n_folds, shuffle=True)

    print("|starting validation curve for cv")
    cv_train_scores, cv_test_scores = validation_curve( model,
                                                  train_features_set.features,
                                                  train_features_set.target,
                                                  param_range=param_values_array,
                                                  cv=cv, n_jobs=4,
                                                  param_name=param_name,
                                                  verbose=True,
                                                  scoring=scoring)

    print("|validation curve done")



    print("|starting validation curve for test")
    for ID,param in enumerate(param_values_array):


        setattr(model,param_name,param)

        model.fit(train_features_set.features,train_features_set.target)


        score_test_array[ID]=eval_model_sore(model,test_features_set.features,test_features_set.target)

        print("ID",ID, "par value",param, 'score ',score_test_array[ID])

        #print("train_test ratio",'test err',err_test_array[ID],test_features_set.features_N_rows)
    print("|validation curve done")

    if scoring=='mean_squared_error':
        cv_test_scores=-1*cv_test_scores
        score_test_array=-1*score_test_array

    return cv_train_scores,cv_test_scores,score_test_array,param_values_array


def plot_validation_curve(cv_train_scores,cv_test_scores,score_test_array,param_values_array,par_name,scorer_name):


    cv_train_scores_mean = np.mean(cv_train_scores, axis=1)
    cv_train_scores_std = np.std(cv_train_scores, axis=1)
    cv_test_scores_mean = np.mean(cv_test_scores, axis=1)
    cv_test_scores_std = np.std(cv_test_scores, axis=1)

    import  pylab as plt
    import matplotlib.ticker as ticker
    import matplotlib
    matplotlib.rcParams['xtick.labelsize'] = 18
    matplotlib.rcParams['ytick.labelsize'] = 18
    fig, (ax1) = plt.subplots()
    fig.suptitle('validation curve',fontsize=18)
    ax1.grid()
    ax1.set_xlabel("%s"%par_name,fontsize=18)
    ax1.set_ylabel("%s"%scorer_name,fontsize=18)

    ax1.plot(param_values_array, cv_train_scores_mean,'o-', label=" Training score", color="g")
    ax1.fill_between(param_values_array, cv_train_scores_mean - cv_train_scores_std,
                     cv_train_scores_mean + cv_train_scores_std, alpha=0.2, color="g")

    ax1.plot(param_values_array, cv_test_scores_mean,'o-', label=" CV score",color="b")
    ax1.fill_between(param_values_array, cv_test_scores_mean - cv_test_scores_std,
                     cv_test_scores_mean + cv_test_scores_std, alpha=0.2, color="b")

    ax1.plot(param_values_array, score_test_array, 'o-',label=" Test score",color="orange")

    ymin, ymax = ax1.get_ylim()
    delta=ymax-ymin
    thick_space=int(delta/10)
    if thick_space>0.:
        ax1.yaxis.set_major_locator(ticker.MultipleLocator(thick_space))

    ax1.legend(loc="best",fontsize=18)
    #ax1.set_ylim([-0.1,ymax])
    plt.tight_layout()

    return fig


