"""
Module:
========================



Overview
--------

This modules provides the implementation of the :class:



Classes relations
---------------------------------------
.. figure::
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::




Summary
---------
.. autosummary::

Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object, map, zip)


from asterism.analysis_tasks.morphometry_features.extract_attractors import DoDENCLUEAttractorsTask
from asterism.analysis_tasks.morphometry_features.map_cluster_to_circle import  DoMapClusterToCircle
from asterism.analysis_tasks.morphometry_features.resize_cluster import  DoResizeCluster
from asterism.analysis_tasks.morphometry_features.unsharp_cluster import DoUnsharpCluster

from asterism.pipeline_manager.analysis_processes import AnalysisProcess
from asterism.core.photometry.surface_brightness_profile import ImageBrigthnessEllipiticalRadialProfile
from asterism.pipeline_manager.analysis_products import *
from asterism.plotting.plot_tools import  analysis_plot
import copy

__author__ = 'andrea tramacere'


def test(surf_brigth_prof):
    pass



class MorphometryInputData(object):
    """
    Class to containing products to extract morphometric  features

    """
    def __init__(self,cluster,attractors_cluster_list,core_attractors_cluster,centroid='core',gabor_w1=None,gabor_w2=None):

        __valid_centroid__=['geom','peak','core']


        self.gabor_w1=gabor_w1
        self.gabor_w2=gabor_w2

        if np.shape(centroid)==(2,):
            self.x_c=centroid[0]
            self.y_c=centroid[1]
        elif centroid in __valid_centroid__:

            if centroid=='geom':
                self.x_c=cluster.x_c
                self.y_c=cluster.y_c

            elif centroid=='peak':
                self.x_c=cluster.x_p
                self.y_c=cluster.y_p

            elif centroid=='core':
                if core_attractors_cluster is not None:
                    self.x_c=core_attractors_cluster.x_c
                    self.y_c=core_attractors_cluster.y_c
                else:
                    self.x_c=cluster.x_c
                    self.y_c=cluster.y_c
        else:
            raise  RuntimeError("centroid param",centroid, " not valid either you provide a tuple (x,y) or you provide a valid method %s"%(__valid_centroid__))




        self.cluster=cluster

        self.cluster._add_polar_coords(center=[self.x_c,self.y_c])

        self.core_attractors_cluster=core_attractors_cluster

        self.attractors_cluster_list=attractors_cluster_list

        self.image,self.x_c_image,self.y_c_image=self._get_image(cluster)

        self.surf_brigth_prof=ImageBrigthnessEllipiticalRadialProfile()
        self.surf_brigth_prof.eval(self.image,self.x_c_image,self.y_c_image,a_max=self.cluster.sig_x*3,b_max=self.cluster.sig_y*3,oversampling_ratio=9)

        if attractors_cluster_list is not None:

            self.attr_cartesian,self.attr_polar=self._merge_clusters_geom_repr(attractors_cluster_list)
        else:

            self.attr_cartesian=None
            self.attr_polar=None
            self.attr_w=1

        if self.core_attractors_cluster is not None:

            self.non_core_attractors_cluster_list=[cl  for cl in attractors_cluster_list if cl !=self.core_attractors_cluster ]
            self.non_core_attr_cartesian,self.non_core_attr_polar=self._merge_clusters_geom_repr(self.non_core_attractors_cluster_list)
            self.non_core_attr_w=1
        else:
            self.non_core_attractors_cluster_list=[]
            self.non_core_attr_cartesian=None
            self.non_core_attr_polar=None
            self.non_core_attr_w=None




    def _get_image(self,cluster):
        cluster_image,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=0,border=3)
        x_c_image=self.x_c-off_set_x
        y_c_image=self.y_c-off_set_y
        return  cluster_image.array,x_c_image,y_c_image

    def _merge_clusters_geom_repr(self,cluster_list):
        new_coords_cartesian=None
        new_coords_polar=None

        for (ID,cl) in enumerate(cluster_list):
            if ID==0:
                new_coords_cartesian=copy.deepcopy(cl.cartesian)
                #if cl.polar is None:
                #    cl._add_polar_coords(center=[self.x_c,self.y_c])
                #new_coords_polar=copy.deepcopy(cl.polar)
            else:
                new_coords_cartesian._expand(cl.cartesian.coords)
                #if cl.polar is None:
                #    cl._add_polar_coords(center=[self.x_c,self.y_c])
                #new_coords_polar._expand(cl.polar.coords)

        if new_coords_cartesian is not None:
            new_coords_polar=new_coords_cartesian.to_polar(x_c=self.x_c,y_c=self.y_c)

        return new_coords_cartesian,new_coords_polar


def do_morphometric_input_products_process_func(process,image,image_id=None,cluster=None,no_plot=False,bkg_image=None,image_bkg_value=0,cluster_th_level=0):
    """

    Parameters
    ----------
    process
    image
    image_id
    cluster
    no_plot
    bkg_image
    bkg_lvl

    Returns
    -------

    """



    if cluster is None:
        original_cluster_shape_input_prod=None
        deproj_cluster_shape_input_prod=None
        gabor_cluster_shape_input_prod=None
        unshapr_cluster_input_prod=None
        oc_seg_map=None
        oc_src_stamp=None
    else:


        #Original cluster products
        original_cluster=process.resize_cluster.run(cluster=cluster,image_bkg_value=image_bkg_value,cluster_th_level=cluster_th_level)
        original_cluster._add_polar_coords(center=[cluster.x_c,cluster.y_c])

        if process.do_unsharp_cluster.get_par_value('eval') == True:
            unshapr_cluster=process.do_unsharp_cluster.run(original_cluster=original_cluster,image_bkg_value=image_bkg_value,cluster_th_level=cluster_th_level)
            unshapr_cluster_input_prod=MorphometryInputData(unshapr_cluster,None,None)
        else:
            unshapr_cluster_input_prod=None
            unshapr_cluster=None

        if process.denclue_attractors.get_par_value('eval') == True:
            oc_attr_cl_list,oc_attr_core_cl,oc_lm,oc_core_lm,oc_seg_map,oc_src_stamp=process.denclue_attractors.run(cluster=original_cluster,image_id=image_id,weight_cluster=unshapr_cluster,image=image)
            #oc_attr_cl_list,oc_attr_core_cl,oc_lm,oc_core_lm=process.denclue_attractors.run(cluster=original_cluster,image_id=image_id,weight_cluster=None)
        else:
            oc_attr_cl_list, oc_attr_core_cl, oc_lm, oc_core_lm, oc_seg_map, oc_src_stamp=(None,None,None,None,None,None)

        original_cluster_shape_input_prod=MorphometryInputData(original_cluster,oc_attr_cl_list,oc_attr_core_cl)
        #unshapr_cluster
        #if process.denclue_attractors.get_par_value('eval') == True:
        #    us_attr_cl_list,us_attr_core_cl,us_lm,us_core_lm=process.denclue_attractors.run(cluster=unshapr_cluster,image_id=image_id,weight_cluster=unshapr_cluster)
        #else:
        #    us_attr_cl_list,us_attr_core_cl,us_lm,us_core_lm=(None,None,None,None)

        #unshapr_cluster_shape_input_prod=MorphometryInputData(unshapr_cluster,us_attr_cl_list,us_attr_core_cl)

        #Gabor
        if process.do_gabor_filter.get_par_value('eval') == True:
            gabor_cluster,g_w1,g_w2=process.do_gabor_filter.run(cluster=original_cluster,image_bkg_value=image_bkg_value,cluster_th_level=cluster_th_level)
            if gabor_cluster is not None:
                gabor_cluster._add_polar_coords()
            if process.denclue_attractors.get_par_value('eval') == True:
                gabor_attr_cl_list,gabor_attr_core_cl,gabor_lm,gabor_core_lm,gabor_seg_map,gabor_src_stamp=process.denclue_attractors.run(cluster=gabor_cluster,image_id=image_id)
            else:
                gabor_attr_cl_list,gabor_attr_core_cl,gabor_lm,gabor_core_lm=(None,None,None,None)
            gabor_cluster_shape_input_prod=MorphometryInputData(gabor_cluster,gabor_attr_cl_list,gabor_attr_core_cl,gabor_w1=g_w1,gabor_w2=g_w2)
        else:
            gabor_cluster_shape_input_prod=None

        #Maps cluster to circle
        if process.map_to_circle.get_par_value('eval') == True:
            circular_transf_cluster=process.map_to_circle.run(cluster=original_cluster,image_bkg_value=image_bkg_value,cluster_th_level=cluster_th_level)
            if circular_transf_cluster is not None:
                circular_transf_cluster._add_polar_coords()


            if process.denclue_attractors.get_par_value('eval') == True:
                #ct_attr_cl_list,ct_attr_core_cl,ct_lm,ct_core_lm=process.denclue_attractors.run(cluster=circular_transf_cluster,image_id=image_id)
                ct_attr_cl_list,ct_attr_core_cl,ct_lm,ct_core_lm=(None,None,None,None)
            else:
                ct_attr_cl_list,ct_attr_core_cl,ct_lm,ct_core_lm=(None,None,None,None)
        else:
            circular_transf_cluster=None

        if circular_transf_cluster is not None:
            deproj_cluster_shape_input_prod=MorphometryInputData(circular_transf_cluster,ct_attr_cl_list,ct_attr_core_cl)
        else:
            deproj_cluster_shape_input_prod=None


    seg_map_prod=AnalysisProductFitsImage(oc_seg_map, name='attractors_segmentation_map')
    cls_stamp=AnalysisProductFitsImage(oc_src_stamp, name='cluster_stamp')
    return original_cluster_shape_input_prod,deproj_cluster_shape_input_prod,gabor_cluster_shape_input_prod,unshapr_cluster_input_prod,seg_map_prod,cls_stamp







class DoMorphometricInputProducts(AnalysisProcess):

    def __init__(self,name='do_gal_shape_extraction',func=do_morphometric_input_products_process_func,plot_func=analysis_plot,parser=None,add_plot_task=True):
        super(DoMorphometricInputProducts,self).__init__(name,func,plot_func=plot_func,parser=parser,add_plot_task=add_plot_task)

        self.add_analysis_task(DoDENCLUEAttractorsTask,'denclue_attractors')
        self.add_analysis_task(DoMapClusterToCircle,'map_to_circle')
        self.add_analysis_task(DoResizeCluster,'resize_cluster')
        self.add_analysis_task(DoGaborFilter, 'do_gabor_filter')
        self.add_analysis_task(DoUnsharpCluster, 'do_unsharp_cluster')