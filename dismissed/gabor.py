"""
Module: name
========================



Overview
--------

This modules provides an

The :class:`.class_name` is used to...



Classes relations
---------------------------------------
.. figure::  classes_rel_pkg.sub_pkg.modname.png
   :align:   center


Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram::   classes_pkg.sub_pkg.modname.png




Summary
---------
.. autosummary::
   some func
   some classes


Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object, map, zip)



from asterism.pipeline_manager.analysis_tasks import AnalysisTask
from asterism.core.image_processing.geometry_transformation import RescaleImage
from asterism.core.clustering.connectivity_based.connected_components import  ConnectedComponents
from asterism.core.clustering.cluster_tools import get_cluster_closest_to_position

import  numpy as np

__author__ = 'andrea tramacere'


__author__ = 'andrea tramacere'

from skimage.filters import gabor_kernel

try:
    from skimage.filters import gabor
except:
    from skimage.filters import gabor_filter as gabor

from asterism.core.clustering.connectivity_based.connected_components import  ConnectedComponents

def do_gabor_filter(cluster,image_bkg_value,cluster_th_level,plot=False):
    image,off_set_x,off_set_y,masked_pixels=cluster.get_cluster_Image(bkg=image_bkg_value,border=2)
    sigma_x=cluster.sig_x
    sigma_y=cluster.sig_y

    #filter design
    theta_v=np.linspace(0.,2.0*np.pi,8)
    sigma_scale_v=np.array([0.2,0.5])
    frequency_scale_v=np.array([1.0,2.0])

    filter_size=theta_v.size*sigma_scale_v.size*frequency_scale_v.size


    #normalize image to 1.0
    image._data=image._data/image._data.max()
    (r,c)=image.array.shape
    filtered_c=np.zeros((filter_size,r,c))



    w_t=np.zeros(filter_size )
    gab_w1=np.zeros(filter_size )
    gab_w2=np.zeros(filter_size )

    if plot==True:
        import  pylab as plt
        fig,ax=plt.subplots(8,4)
        ax_i=iter(ax.flatten())

    ID=0


    for theta in theta_v:

        for sigma_scale in sigma_scale_v:
            for frequency_scale in frequency_scale_v:


                f=(1.0/cluster.r_max)*frequency_scale

                filtered_c[ID], filt_imag = gabor(image.array, frequency=f, theta=theta,
                                              sigma_x=sigma_scale*sigma_x, sigma_y=sigma_scale*sigma_x)


                w=np.ones(image.array.shape)
                w[~masked_pixels]=np.fabs(filtered_c[ID][~masked_pixels]-image.array[~masked_pixels] )/np.fabs(image.array[~masked_pixels] )

                gab_w1[ID]=(filtered_c[ID][~masked_pixels]**2).sum()/np.float(cluster.n_points)
                gab_w2[ID]=np.fabs(filtered_c[ID][~masked_pixels]).sum()/np.float(cluster.n_points)

                w_t[ID]=1.0/w.sum()

                if plot==True:
                    ax=ax_i.next()
                    ax.imshow( filtered_c[ID] ,interpolation='nearest')
                    t='%2.2f,%2.2f,%2.2f'%(theta,sigma_scale,f)
                    ax.set_title(t)


                #if ID==0:
                #    i_final=np.corrcoef(filtered_c[ID],image.array)
                #else:
                #    i_final+=np.corrcoef(filtered_c[ID],image.array)
                #feats[k, 0] = filtered.mean()
                #feats[k, 1] = filtered.var()
                ID+=1


    id_s=np.argsort(w_t)[-4:]
    filtered=np.zeros((r,c))

    for id in id_s:
        filtered+=filtered_c[id]*w_t[id]

    #import  pylab as plt
    if plot==True:
        fig,(ax1,ax2)=plt.subplots(1,2)
        ax1.imshow(image.array,interpolation='nearest')
        ax2.imshow(filtered,interpolation='nearest')
        plt.show()

    conn=ConnectedComponents(1)
    conn.run(np.fabs(filtered),0.0,seg_map=~masked_pixels)

    central_cluster=get_cluster_closest_to_position(conn.clusters_list,x_c=image._center_x,y_c=image._center_y)
    g_w_1=[gab_w1.mean(),gab_w1.std(),gab_w1.min(),gab_w1.max()]
    g_w_2=[gab_w2.mean(),gab_w2.std(),gab_w2.min(),gab_w2.max()]

    return central_cluster.SourceCluster_factory(ID=cluster.ID),g_w_1,g_w_2

class DoGaborFilter(AnalysisTask):

    def __init__(self, name='gabor_filter', func=do_gabor_filter, parser=None, process=None):
        super(DoGaborFilter,self).__init__(name=name,func=func,parser=parser,process=process)
        self.parameters_list.add_par('-eval',help=' ',action='store_true')
        self.parameters_list.add_par('-size',  type=np.int ,help='',default=25)
        self.func=func