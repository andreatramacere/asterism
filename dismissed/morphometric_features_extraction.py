"""




Overview
--------

This module provides the implementation of the :class:`.MorphometricFeatureExtractionPipeline` class,
used to create the pipeline for the morphometric features extraction.
The processes in the pipeline are orchestrated by the :func:`.do_morphometric_feature_extraction_pipeline_func`, according to
the algorithm  shown by the following flow chart:


.. digraph::    deblending

        subgraph morphometrt_flow {

        "Image"->"do_src_detection" -> "cluster_list"->"do_morphometric_input_products"->"morph_input_prods"->"do_morphometric_features_extraction";
        "Image" [shape=parallelogram]
        "do_src_detection" [shape=polygon,sides=4]
        "cluster_list" [shape=parallelogram]
        "do_morphometric_input_products" [shape=polygon,sides=4]
        "morph_input_prods"  [shape=parallelogram]
        "do_morphometric_features_extraction" [shape=polygon,sides=4]
        }



Classes and Inheritance Structure
----------------------------------------------
.. inheritance-diagram:: asterism.analysis_pipelines.morphometric_features_extraction.MorphometricFeatureExtractionPipeline




Summary
---------
.. autosummary::
   do_morphometric_feature_extraction_pipeline_func
   MorphometricFeatureExtractionPipeline


Module API
-----------
"""

from __future__ import absolute_import, division, print_function

from builtins import (bytes, str, open, super, range,
                      zip, round, input, int, pow, object, map, zip)

from asterism.pipeline_manager.analysis_pipeline import AnalysisPipeline
from asterism.analysis_processes.source_detection import  DoSrcDetectionProcess
from asterism.analysis_processes.morphometric_features_input_products_euclid import DoMorphometricInputProducts
from asterism.analysis_processes.morphometric_feature_extraction import DoMorphometricFeaturesExtraction
from asterism.plotting.plot_tools import  analysis_plot
from asterism.core.clustering.cluster_tools import  get_cluster_closest_to_position
from asterism.core.image_manager.image import Image

__author__ = 'andrea tramacere'



def do_morphometric_feature_extraction_pipeline_func(pipeline,image,image_id=None,input_seg_map=None,**kwargs):
    """
    Function implementing the Pipeline  algorithm  for the  morphometric features extraction.

        - do_src_detection  :class:`.DoSrcDetectionProcess` , extraction of clusters list from input Image

        - selection of central cluster from clusters list

        - do_morphometric_input_products   :class:`.DoMorphometricInputProducts`, extraction of :class:'asterism.analysis_processes.morphometric_features_input_products.MorphometryInputData'



        - do_morphometric_features_extraction :class:`.DoMorphometricFeaturesExtraction`


    Parameters
    -----------

    pipeline : :class:`.MorphometricFeatureExtractionPipeline`
        instance of the base :class:`.AnalysisPipeline` class

    image : :class:`.Image`
        instance of the class used as input for different processes

    image_id :  int
        id of the image

    **kwargs:

    Returns
    -------

    morphometric_features_analysis_products : :class:`asterism.pipeline_manager.analysis_products.AnalysisProcessProducts`
        class instance storing the analysis products for this pipeline

    """


    #Type checking for image
    if isinstance(image,Image)==True:
        pass
    else:
        raise TypeError('image is not instance of Image class')


    #Detection Process
    analysis_prod_collection=pipeline.do_src_detection.run(image,image_id=image_id,input_seg_map=input_seg_map)

    cluster_list=analysis_prod_collection.get_product_by_name('cluster_list').prods


    if cluster_list==[]:
        #set failed
        cluster_list=[None]

    #bkg_lvl=analysis_prod_collection.get_product_by_name('bkg_threshold').prods
    #x_c=image._center_x
    #y_c=image._center_y

    #The source at the center of the image is used
    #central_cluster=get_cluster_closest_to_position(cluster_list,x_c=x_c,y_c=y_c)

    #Products to be used as input for morphometric features extraction are build

    for ID,cl in enumerate(cluster_list):
        if cl is None:
            cluster_th_level=None
            x_c=None
            y_c=None
            cl_id=-1
        else:
            cluster_th_level=cl.flux.min()
            x_c=cl.x_c
            y_c=cl.y_c
            cl_id=cl.ID

        original_cluster_shape_input_prod,deproj_cluster_shape_input_prod,gabor_prod,unsharp_prod,attr_seg_map_prod,cls_stamp_prod=pipeline.do_morphometric_input_products.run(image,
                                                                                                                      cluster=cl,
                                                                                                                      image_id=image_id,
                                                                                                                      image_bkg_value=0,
                                                                                                                      cluster_th_level=cluster_th_level)

        #Morphometrics features are extracted from the products

        analysis_prod_collection.add_product(cls_stamp_prod)
        analysis_prod_collection.add_product(attr_seg_map_prod)

        morphometric_features_analysis_products_collection=pipeline.do_morphometric_features_extraction.run(image,
                                                                                                 morphometry_input_data_original=original_cluster_shape_input_prod,
                                                                                                 morphometry_input_data_deproj=deproj_cluster_shape_input_prod,
                                                                                                 morphometry_input_data_gabor=gabor_prod,
                                                                                                 morphometry_input_data_unsharp=unsharp_prod,
                                                                                                 image_id=image_id,
                                                                                                 cl_id=cl_id,
                                                                                                 x_c=x_c,
                                                                                                 y_c=y_c)

        morph_feat=morphometric_features_analysis_products_collection.get_product_by_name('morph_features')

        #print(type(morph_feat.array_values),morph_feat.array_values)

        if len(cluster_list)==1 or ID==0:
            out_prod=morph_feat
        else:
            out_prod.append_row(morph_feat.array_values)



    analysis_prod_collection.add_product(out_prod)

    return analysis_prod_collection




class MorphometricFeatureExtractionPipeline(AnalysisPipeline):
    """
    :class:`.AnalysisPipeline` class for the  Morphometric Feature Extraction Pipeline. The following processes are glued
    by the pipeline:

        - do_src_detection                 :class:`.DoSrcDetectionProcess`
        - do_morphometric_input_products   :class:`.DoMorphometricInputProducts`
        - do_morphometric_features_extraction :class:`.DoMorphometricFeaturesExtraction`


    Parameters
    ----------
    name : str
        the name for the Process

    func : :func:
        The function that handles tha pipeline algorithm

    image_id : int
        id of the image

    plot_func : :func:
        plotting function for this process


    parser :

    argv :

    conf_file:


    """
    def __init__(self,name='morph_features_script',func=do_morphometric_feature_extraction_pipeline_func,plot_func=analysis_plot,parser=None,argv=None,conf_file=None):
        super(MorphometricFeatureExtractionPipeline,self).__init__(name,func,plot_func=analysis_plot,parser=parser,argv=argv,conf_file=conf_file)
        self.add_analysis_process(DoSrcDetectionProcess,'do_src_detection' )
        self.add_analysis_process(DoMorphometricInputProducts,'do_morphometric_input_products' )
        self.add_analysis_process(DoMorphometricFeaturesExtraction,'do_morphometric_features_extraction')





