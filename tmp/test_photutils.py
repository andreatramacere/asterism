from astropy.convolution import Gaussian2DKernel
from astropy.stats import gaussian_fwhm_to_sigma
from photutils import detect_sources
from asterism import data_dir
from astropy.io import  fits as pf
from photutils import detect_threshold
image=pf.getdata(data_dir+'/deblending_img1.fits')

sigma = 2.0 * gaussian_fwhm_to_sigma    # FWHM = 2.
kernel = Gaussian2DKernel(sigma, x_size=3, y_size=3)
kernel.normalize()
threshold = detect_threshold(image, snr=3.)
segm = detect_sources(image, threshold, npixels=5, filter_kernel=kernel)