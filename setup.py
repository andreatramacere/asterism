
from __future__ import division, absolute_import, print_function

__author__ = 'andrea tramacere'



#!/usr/bin/env python

from setuptools import setup, find_packages,Extension
from distutils.extension import Extension
from Cython.Distutils import build_ext
import argparse
import sys
import subprocess
import os
import glob
import numpy

argparser = argparse.ArgumentParser(add_help=False)
argparser.add_argument('--minimal', help='minimal installation', action='store_true')
args, unknown = argparser.parse_known_args()


f = open("./requirements.txt",'r')
install_req=f.readlines()
f.close()
print (install_req)

if  args.minimal==True:
    packs=['asterism',
           'asterism.core']

else:
    packs=find_packages()

print ("====>",packs)
for p in packs[::]:
    if 'dismissed' in p:
        packs.remove(p)
print ("====>",packs)

ext_modules=[Extension("asterism.core.cython_tools.denclue",
               ["asterism/core/cython_tools/denclue.pyx",
                "asterism/core/cython_tools/numeric.c"],
                include_dirs = ["asterism/core/cython_tools"],
               libraries=["m"],
               extra_compile_args = ["-ffast-math",'-O3','-m64']),
              ]

#os.environ["CFLAGS"] = numpy.get_include()

entry_points = {
    'console_scripts':[
    'asterism_build_fits_cube = asterism.command_line.asterism_build_fits_cube:main',
    'asterism_detect_srcs = asterism.command_line.asterism_detect_srcs:main',
    'asterism_cluster_submit = asterism.command_line.asterism_cluster_submit:main',
    'asterism_gal_shape_features = asterism.command_line.asterism_gal_shape_features:main',
    'asterism_gal_shape_features_euclid = asterism.command_line.asterism_gal_shape_features_euclid:main'
    ]
}


#scripts_list=glob.glob('./bin/asterism*')
setup(name='asterism',
      version=1.0,
      description='',
      author='Andrea Tramacere',
      author_email='andrea.tramacere@unige.ch',
      url='',
      #scripts=scripts_list,
      packages=packs,
      package_data={'asterism':['data/*']},
      install_requires=install_req,
      cmdclass = {"build_ext": build_ext},
      ext_modules = ext_modules,
      include_dirs=[numpy.get_include()],
      entry_points=entry_points,
)
